/*
# iVertex3dCol.h

- 頂点クラス(カラー付きVertex3d)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef MATERIAL_VERTEX_I_VERTEX_3D_COL_H_
#define MATERIAL_VERTEX_I_VERTEX_3D_COL_H_

#include <functional>
#include "iVertex.h"
#include "../3d/draw3d.h"
#include "../../math/vector2.h"
#include "../../math/vector3.h"
#include "../../base/color.h"

namespace material {
namespace vertex {

class IVertex3dCol : public IVertex
{
public:
    // 編集用頂点の構造体
    //--------------------------------------------------------------------
    struct VERTEX
    {
        Vector3 pos;    // 位置
        Vector3 nor;    // 法線
        Color   col;    // カラー
        Vector2 tex;    // テクスチャ
    };

    // @brief  : すべての頂点の編集
    // @param  : 関数(param:編集する頂点情報,頂点ナンバー)
    //--------------------------------------------------------------------
    virtual void Edit(std::function<void (VERTEX &,const variable::u32 &)>) = 0;

    // @brief  : 頂点の編集
    // @param  : 関数(param:頂点ナンバー return 設定する頂点)
    //--------------------------------------------------------------------
    virtual void EditPos(std::function<Vector3 (const Vector3 &,const variable::u32 &)>) = 0;

    // @brief  : 法線の編集
    // @param  : 関数(param:頂点ナンバー return 設定する色)
    //--------------------------------------------------------------------
    virtual void EditNor(std::function<Vector3 (const Vector3 &,const variable::u32 &)>) = 0;

    // @brief  : カラーの編集
    // @param  : 関数(param:頂点ナンバー return 設定する色)
    //--------------------------------------------------------------------
    virtual void EditCol(std::function<Color (const Color &,const variable::u32 &)>) = 0;

    // @brief  : テクスチャ座標編集
    // @param  : 関数(param:頂点ナンバー return 設定する頂点)
    //--------------------------------------------------------------------
    virtual void EditTex(std::function<Vector2 (const Vector2 &,const variable::u32 &)>) = 0;

    // @brief  : すべての頂点の取り出し
    // @return : 頂点(注：使い終わったらdelete[]すること)
    //--------------------------------------------------------------------
    virtual VERTEX *Export(void) const = 0;

    // @brief  : 頂点の取り出し
    // @return : 頂点(注：使い終わったらdelete[]すること)
    //--------------------------------------------------------------------
    virtual Vector3 *ExportPos(void) const = 0;

    // @brief  : 法線の取り出し
    // @return : 法線(注：使い終わったらdelete[]すること)
    //--------------------------------------------------------------------
    virtual Vector3 *ExportNor(void) const = 0;

    // @brief  : カラーの取り出し
    // @return : カラー(注：使い終わったらdelete[]すること)
    //--------------------------------------------------------------------
    virtual Color *ExportCol(void) const = 0;

    // @brief  : テクスチャ座標の取り出し
    // @return : テクスチャ座標(注：使い終わったらdelete[]すること)
    //--------------------------------------------------------------------
    virtual Vector2 *ExportTex(void) const = 0;

protected:
    // @brief  : コンストラクタ
    // @param  : 頂点数
    //--------------------------------------------------------------------
    IVertex3dCol(const variable::u32 &_count) : IVertex(_count){};

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~IVertex3dCol(){};
};



};  // namespace vertex
};  // namespace material
#endif //  MATERIAL_VERTEX_I_VERTEX_3D_COL_H_
/******************************************************************************
|   End of File
*******************************************************************************/