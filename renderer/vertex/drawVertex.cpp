/*
# draw.h

- 描画クラス(頂点バッファ用)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード -------------------------------------------------------------
#include "draw.h"

namespace material {
namespace vertex {
// @brief  : コンストラクタ
// @param  : 頂点クラス
//         : 描画タイプ
//         : 親クラス
//         : 描画するのか？
//--------------------------------------------------------------------
DrawVertex::DrawVertex(IVertex *_buffer, IVertex::TYPE _type, IDraw *_parent,bool _is_draw) :
    IDraw(_parent,_is_draw),
    m_Vertex(_buffer),
    m_Type(_type)
{
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
DrawVertex::~DrawVertex()
{
    delete m_Vertex;
}

// @brief  : 頂点バッファの取得
//--------------------------------------------------------------------
IVertex *const DrawVertex::GetBuffer(void) const{return m_Vertex;}

// @brief  : 描画
//--------------------------------------------------------------------
void DrawVertex::Draw(void)
{
    m_Vertex->Draw(m_Type);
}



};  // namespace vertex
};  // namespace material
/******************************************************************************
|   End of File
*******************************************************************************/