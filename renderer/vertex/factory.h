/*
# factory.h

- 頂点バッファ生成クラス(抽象)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef MATERIAL_VERTEX_FACTORY_H_
#define MATERIAL_VERTEX_FACTORY_H_

#include "iVertex3dCol.h"

namespace material {
namespace vertex {

class Factory
{
public:
    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    Factory(){};

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~Factory(){};

    // @brief  : カラー付き頂点の作成
    // @param  : 頂点数
    //--------------------------------------------------------------------
    virtual IVertex3dCol *Create(const variable::u32 &_count) const = 0;
};


};  // namespace vertex
};  // namespace material
#endif //  MATERIAL_VERTEX_FACTORY_H_
/******************************************************************************
|   End of File
*******************************************************************************/