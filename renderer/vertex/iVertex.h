/*
# iVertex.h

- 頂点クラス(抽象)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef MATERIAL_VERTEX_I_VERTEX_H_
#define MATERIAL_VERTEX_I_VERTEX_H_

#include "../../variable/bitName.h"

namespace material {
namespace vertex {

class IVertex
{
public:
    // 描画時の設定
    //--------------------------------------------------------------------
    enum TYPE
    {
        TYPE_POINTS = 0,        // 各頂点を単独の点である
        TYPE_LINES,             // 二つの頂点はペアである
        TYPE_LINE_STRIP,        // 線分は順番に連結している
        TYPE_TRIANGLS,          // 頂点はそれぞれ独立した三角形である
        TYPE_TRIANGLE_STRIP,    // 頂点は連続した三角形である

        MAX_TYPE
    };

    // メンバ定数
    //--------------------------------------------------------------------
    const variable::u32 Count;  // 頂点数

    // @brief  : コンストラクタ
    // @param  : 頂点数
    //--------------------------------------------------------------------
    IVertex(const variable::u32 &_count) :Count(_count){};

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~IVertex(){};

    // @brief  : 描画
    // @param  : 描画の種類
    //--------------------------------------------------------------------
    virtual void Draw(const TYPE &) = 0;
};


};  // namespace vertex
};  // namespace material
#endif //  MATERIAL_VERTEX_I_VERTEX_H_
/******************************************************************************
|   End of File
*******************************************************************************/