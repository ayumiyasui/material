/*
# draw.h

- 描画クラス(頂点バッファ用)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef MATERIAL_VERTEX_DRAW_H_
#define MATERIAL_VERTEX_DRAW_H_

#include "../draw.h"
#include "iVertex.h"

namespace material {
namespace vertex {
class DrawVertex : public IDraw
{
public:
    // @brief  : コンストラクタ
    // @param  : 頂点クラス
    //         : 描画タイプ
    //         : 親クラス
    //         : 描画するのか？
    //--------------------------------------------------------------------
    DrawVertex(IVertex *, IVertex::TYPE, IDraw *,bool _is_draw = true);

    // @brief  : 頂点バッファの取得
    //--------------------------------------------------------------------
    IVertex *const GetBuffer(void) const;

private:
    // メンバ変数
    //--------------------------------------------------------------------
    IVertex       *m_Vertex;  // 頂点バッファ
    IVertex::TYPE  m_Type;   // タイプ

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~DrawVertex();

    // @brief  : 描画
    //--------------------------------------------------------------------
    void Draw(void) override;
};


};  // namespace vertex
};  // namespace material
#endif //  MATERIAL_VERTEX_DRAW_H_
/******************************************************************************
|   End of File
*******************************************************************************/