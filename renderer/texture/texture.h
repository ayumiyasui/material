/*
# texture.h

- テクスチャクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __TEXTURE_H__
#define __TEXTURE_H__

#include <string>

#include "../../base/filePass.h"
#include "../../base/tempUseCount.h"
#include "../../base/tempSmartPtr.h"
#include "../../base/node.h"


class ITexture : public TempUseCount<ITexture>, public Node
{
public:
    // プロパティ
    //--------------------------------------------------------------------
    FilePass Name;      // ファイルパス

    // @brief  : レンダリングに設定
    //--------------------------------------------------------------------
    virtual void Set( void ) = 0;

protected:
    // @brief  : コンストラクタ
    // @param  : ファイルパス
    //         : 親
    //--------------------------------------------------------------------
    ITexture( const std::string &, Node * );

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~ITexture();

private:
};

typedef TempSmartPtr<ITexture> Texture;



#endif  // __TEXTURE_H__
/******************************************************************************
|   End of File
*******************************************************************************/