/*
# renderer.cpp

- レンダラークラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード ------------------------------------------------------------
#include "renderer.h"

#include <stdio.h>
#include <stdarg.h>
#include "2d/factory.h"
#include "3d/factory.h"


// @brief  : コンストラクタ
// @param  : 幅
//         : 高さ
//--------------------------------------------------------------------
IRenderer::IRenderer( float _width, float _height, _2d::IFactory *_factory_2d, _3d::IFactory *_factory_3d, effect::Factory *_effect, vertex::Factory *_vertex) :
    m_Width(_width),
    m_Height(_height),
    m_BackColor(0.0f,0.0f,1.0f,1.0f),
    m_Factory2D(_factory_2d),
    m_Factory3D(_factory_3d),
    m_Effect(_effect),
    m_Vertex(_vertex),
    Width(m_Width),
    Height(m_Height),
    BackColor(m_BackColor)
#if _DEBUG
    ,DebugString(str)
    ,DebugIsDraw(is_draw)
#endif
{
#ifdef _DEBUG
    memset(str,'\0',sizeof(str));
    count = 0;
    is_draw = true;
#endif
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
IRenderer::~IRenderer()
{
    delete m_Effect;
    delete m_Vertex;
    delete m_Factory3D;
    delete m_Factory2D;
}


// @brief  : 2Dファクトリーの取得
//--------------------------------------------------------------------
_2d::IFactory *IRenderer::Factory2D( void ) const
{
    return m_Factory2D;
}

// @brief  : 3Dファクトリーの取得
//--------------------------------------------------------------------
_3d::IFactory *IRenderer::Factory3D( void ) const
{
    return m_Factory3D;
}

// @brief  : 頂点ファクトリーの取得
//--------------------------------------------------------------------
vertex::Factory *const IRenderer::Vertex(void) const
{
    return m_Vertex;
}

// @brief  : エフェクトファクトリーの取得
//--------------------------------------------------------------------
effect::Factory *const IRenderer::Effect(void) const
{
    return m_Effect;
}

// @brief  : デバック表示
//--------------------------------------------------------------------
void IRenderer::Print( const char *_str, ... )
{
#ifdef _DEBUG
    // 描画しないなら何もしない
    if( !is_draw ) return;

    // 文字数がオーバーしたらスルー
    if( count >= MAX_STR ) return ;

    // 可変引数から文字列へ
    va_list list;
    va_start( list, _str );
    {
        count += vsprintf_s( &str[count], ( MAX_STR - count ) * sizeof( char ), _str, list );
    }
    va_end( list );
#endif
}

// @brief  : デバック表示の切り替え
//--------------------------------------------------------------------
void IRenderer::SwitchDebugView( void )
{
#ifdef _DEBUG
    is_draw = !is_draw;
#endif
}

#ifdef _DEBUG
// @brief  : デバック文字のリセット
//--------------------------------------------------------------------
void IRenderer::ResetDebugString( void )
{
    memset(str,'\0',sizeof(str));
    count = 0;
}
#endif



/******************************************************************************
|   End of File
*******************************************************************************/