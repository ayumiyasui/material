/*
# draw.cpp

- 描画抽象クラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード ------------------------------------------------------------
#include "draw.h"

#include <assert.h>


#pragma warning(push)   // インクリメントでメンバ変数にthisを渡している
#pragma warning(disable:4355)
// @brief  : コンストラクタ
//--------------------------------------------------------------------
IDraw::IDraw() :
    Node(&TempSingleton<Node>::Instance()),
    m_IsDraw(true),
    IsEnable(m_IsDraw,this)
{
}

// @brief  : コンストラクタ
// @param  : 親クラス
//         : 描画するのか？
//--------------------------------------------------------------------
IDraw::IDraw( IDraw *_parent, bool _is_draw ) :
    Node(_parent),
    m_IsDraw( _is_draw ),
    IsEnable(m_IsDraw,this)
{
    assert(_parent);
    if( IsEnable ) OnEnable();
}
#pragma warning(pop)

// @brief  : デストラクタ
//--------------------------------------------------------------------
IDraw::~IDraw()
{
}


// @brief  : 描画する
//--------------------------------------------------------------------
void IDraw::DrawAll( void )
{
    if(!IsEnable) return;

    Draw();
    ForEach(
        [](Node *_node) {
            assert(dynamic_cast<IDraw *>(_node));
            ((IDraw *)_node)->DrawAll();
        }
    );

    LateDraw();
}

// 内部クラス
//--------------------------------------------------------------------
IDraw::Switch::Switch(bool &_value, IDraw *_master) :
    TempGetSet(_value),
    m_Master(_master)
{
}
IDraw::Switch::~Switch()
{
}

void IDraw::Switch::Set( const bool &_value )
{
    TempGetSet::Set(_value);
    (_value) ? m_Master->OnEnable() : m_Master->OnDisable();
}



/******************************************************************************
|   End of File
*******************************************************************************/