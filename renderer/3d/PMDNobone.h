/*
# PMDNobone.h

- PMDクラス骨なし(3D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __3D_PMD_NOBONE_H__
#define __3D_PMD_NOBONE_H__

#include "draw3d.h"

class Vector3;

NAMESPACE_3D
class PMDNoBone : public IDraw3D
{
public:
    // プロパティ
    //--------------------------------------------------------------------
    const unsigned int MAX_VERTEX;
    const unsigned int MAX_INDEX;

    // @brief  : 頂点 & インデックスの掃き出し
    // @note   : 吐き出された配列データは後でDeleteする必要があります
    //--------------------------------------------------------------------
    virtual Vector3 *CreateVertexAndIndex(void) const = 0;

protected:
    // @brief  : コンストラクタ
    // @param  : 頂点数
    //         : インデックス数
    //         : 親クラス
    //         : 描画するのか？
    //--------------------------------------------------------------------
    PMDNoBone( unsigned int _max_vertex, unsigned int _max_index,
               IDraw *_parent, bool _is_draw ) :
        IDraw3D(_parent,_is_draw),
        MAX_VERTEX(_max_vertex),
        MAX_INDEX(_max_index)
    {
    };

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~PMDNoBone(){};
};



END_NAMESPACE_3D
#endif //  __3D_PMD_NOBONE_H__
/******************************************************************************
|   End of File
*******************************************************************************/