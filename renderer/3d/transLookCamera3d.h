/*
# transLookCamera.h

- 行列変換クラス(3D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __3D_TRANS_LOOKCAMERA_H__
#define __3D_TRANS_LOOKCAMERA_H__

#include "iTransform.h"
#include "../../base/tempGetSet.h"
#include "../../math/Quad.h"

NAMESPACE_3D

    class Camera;

class TransLookCamera : public ITransform
{
private:
    // 内部クラス
    //--------------------------------------------------------------------
    template < typename T > class GetSetMatrix : public TempGetSet<T>
    {
    public:
        GetSetMatrix( T &, TransLookCamera * );
        ~GetSetMatrix();
        GetSetMatrix<T> &operator = ( const T & );

    private:
        TransLookCamera *m_Master;
        void Set( const T & ) override;
    };

public:
    // プロパティ
    //--------------------------------------------------------------------
    TempGetter<Matrix3D>  Matrix;
    GetSetMatrix<Vector3> Position;
    void Cam( Camera * );

protected:
    // @brief  : コンストラクタ
    // @param  : カメラ
    //         : 親クラス
    //         : 描画するのか？
    //--------------------------------------------------------------------
    TransLookCamera( Camera *, IDraw *, bool );

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~TransLookCamera();

    // @brief  : カメラの取得
    //--------------------------------------------------------------------
    Camera *Cam(void) const{ return m_Camera; };

private:
    // メンバ変数
    //--------------------------------------------------------------------
    Matrix3D m_Matrix;
    Camera   *m_Camera;
    Vector3   m_Position;

    // @brief  : 行列の作成
    //--------------------------------------------------------------------
    Matrix3D CreateMatrix(void) const;
};



END_NAMESPACE_3D
#endif //  __3D_TRANS_LOOKCAMERA_H__
/******************************************************************************
|   End of File
*******************************************************************************/