/*
# material.h

- マテリアルクラス(3D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __3D_MATERIAL_H__
#define __3D_MATERIAL_H__

#include "draw3d.h"
#include "../../base/color.h"

NAMESPACE_3D
class Material : public IDraw3D
{
protected:
    // @brief  : コンストラクタ
    // @param  : 親クラス
    //         : 描画するのか？
    //--------------------------------------------------------------------
    Material( IDraw *_parent, bool _is_draw ) : IDraw3D(_parent,_is_draw){};

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~Material(){};
};



class MaterialNormal : public Material
{
public:
    // プロパティ
    //--------------------------------------------------------------------
    TempGetSet<Color> Diffuse;
    TempGetSet<Color> Specular;
    TempGetSet<Color> Ambient;
    TempGetSet<Color> Emissive;

protected:
    // @brief  : コンストラクタ
    // @param  : 親クラス
    //         : 描画するのか？
    //--------------------------------------------------------------------
    MaterialNormal( IDraw *_parent, bool _is_draw ) :
        Material(_parent,_is_draw),
        Diffuse(m_Diffuse),
        Specular(m_Specular),
        Ambient(m_Ambient),
        Emissive(m_Emissive),
        m_Diffuse(1.0f,1.0f,1.0f,1.0f),
        m_Ambient(0.0f,0.0f,0.0f,0.0f),
        m_Specular(0.0f,0.0f,0.0f,0.0f),
        m_Emissive(0.0f,0.0f,0.0f,0.0f)
    {
    };

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~MaterialNormal(){};

private:
    Color m_Diffuse;
    Color m_Specular;
    Color m_Ambient;
    Color m_Emissive;
};


END_NAMESPACE_3D
#endif //  __3D_MATERIAL_H__
/******************************************************************************
|   End of File
*******************************************************************************/