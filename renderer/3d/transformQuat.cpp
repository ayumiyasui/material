/*
# transformQuat.h

- 行列変換クラス(3D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "transform3dQuat.h"


NAMESPACE_3D

#pragma warning(push)   // インクリメントでメンバ変数にthisを渡している
#pragma warning(disable:4355)
// @brief  : コンストラクタ
// @param  : 親クラス
//         : 描画するのか？
//--------------------------------------------------------------------
TransformQuat::TransformQuat( IDraw *_parent, bool _is_draw ) :
    ITransform(_parent,_is_draw),
    m_ParentIsTransform(false),
    m_LocalRotation(Quaternion::IDENTITY),
    IsParent(m_ParentIsTransform),
    Matrix(m_Matrix),
    Position(m_Position,this),
    Rotation(m_LocalRotation,this)
{
}
#pragma warning(pop)

// @brief  : デストラクタ
//--------------------------------------------------------------------
TransformQuat::~TransformQuat()
{
}

// 内部クラス
//--------------------------------------------------------------------
template < typename T >
TransformQuat::GetSetMatrix<T>::GetSetMatrix( T &_value, TransformQuat *_master ) :
    TempGetSet<T>(_value),
    m_Master(_master)
{
}

template < typename T >
TransformQuat::GetSetMatrix<T>::~GetSetMatrix()
{
}

template < typename T >
TransformQuat::GetSetMatrix<T> &TransformQuat::GetSetMatrix<T>::operator = ( const T &_value )
{
    Set(_value);
    return *this;
}

template < typename T >
void TransformQuat::GetSetMatrix<T>::Set( const T &_value )
{
    TempGetSet<T>::Set(_value);
    m_Master->m_Matrix = m_Master->CreateMatrix();
}
template class _3d::TransformQuat::GetSetMatrix<Vector3>;
template class _3d::TransformQuat::GetSetMatrix<Quaternion>;


END_NAMESPACE_3D
/******************************************************************************
|   End of File
*******************************************************************************/