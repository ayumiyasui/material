/*
# camera.h

- カメラクラス(3D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __3D_CAMERA_H__
#define __3D_CAMERA_H__

#include "draw3d.h"
#include "../../base/tempGetSet.h"
#include "../../math/Quad.h"
#include "../../math/vector3.h"
#include "../../math/matrix3d.h"

NAMESPACE_3D
class Camera : public IDraw3D
{
public:
    // プロパティ
    //--------------------------------------------------------------------
    TempGetSet<float>   FovY;
    TempGetSet<float>   Aspect;
    TempGetSet<float>   Near;
    TempGetSet<float>   Far;

    TempGetSet<Vector3> Eye;
    TempGetSet<Vector3> At;
    TempGetSet<Vector3> Up;

    TempGetSet<Quad>    Viewport;

    // @brief  : カメラ行列の作成
    //--------------------------------------------------------------------
    Matrix3D Matrix( void ) const;

protected:
    // @brief  : コンストラクタ
    // @param  : 親クラス
    //         : 描画するのか？
    //--------------------------------------------------------------------
    Camera( IDraw *, bool );

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~Camera();

private:
    // メンバ変数
    //--------------------------------------------------------------------
    float   m_FovY;     // 視野
    float   m_Aspect;   // アスペクト比
    float   m_Near;     // 近いビューのZ値
    float   m_Far;      // 遠いビューのZ値

    Vector3 m_Eye;      // 視点
    Vector3 m_At;       // 注視点
    Vector3 m_Up;       // 上

    Quad    m_Viewport;    // ビュー座標
};



END_NAMESPACE_3D
#endif //  __3D_CAMERA_H__
/******************************************************************************
|   End of File
*******************************************************************************/