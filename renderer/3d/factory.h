/*
# factory.h

- ファクトリクラス(3D)
- 機種・ライブラリ差の吸収のために作成

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __3D_FACTORY_H__
#define __3D_FACTORY_H__

#include <string>
#include "draw3d.h"
#include "../../math/Quad.h"

class IRenderer;

NAMESPACE_3D

    class Camera;
    class DirectionalLight;
    class Transform;
    class TransformQuat;
    class TransLookCamera;
    class XFile;
    class PMDNoBone;
    class Sky;
    class Plate;

class IFactory
{
public:
    // @brief  : カメラクラスの作成
    //--------------------------------------------------------------------
    virtual Camera *CreateCamera( IDraw *, bool ) const = 0;

    // @brief  : ライトクラスの作成
    //--------------------------------------------------------------------
    virtual DirectionalLight *CreateDirLight( IDraw *, bool ) const = 0;

    // @brief  : Transformの作成
    //--------------------------------------------------------------------
    virtual Transform       *CreateTransform( IDraw *, bool ) const = 0;
    virtual TransformQuat   *CreateTransformQuat( IDraw *, bool ) const = 0;
    virtual TransLookCamera *CreateTransformLookCamera( Camera *, IDraw *, bool ) const = 0;

    // @brief  : 板の作成
    // @param  : テクスチャ名
    //         : 大きさ
    //          :親クラス
    //         : 描画するのか？
    //--------------------------------------------------------------------
    virtual Plate *CreatePlate( const std::string &, const Quad &, IDraw *, bool ) = 0;

    // @brief  : 空の作成
    // @param  : テクスチャ名
    //         : 半径
    //         : ブロック数横
    //         : ブロック数縦
    //          :親クラス
    //         : 描画するのか？
    //--------------------------------------------------------------------
    virtual Sky *CreateSky( const std::string &, float, unsigned int, unsigned int, IDraw *, bool ) const = 0;

    // @brief  : XFileの作成
    //--------------------------------------------------------------------
    virtual XFile *CreateXFile( IDraw *, bool, const std::string & ) const = 0;

    // @brief  : PMDの作成(骨なし)
    //--------------------------------------------------------------------
    virtual PMDNoBone *CreatePMDNoBone( const std::string &, IDraw *, bool ) const = 0;

protected:
    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    IFactory(){};

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~IFactory(){};

private:
    // フレンドクラス
    //--------------------------------------------------------------------
    friend IRenderer;
};



END_NAMESPACE_3D
#endif //  __3D_FACTORY_H__
/******************************************************************************
|   End of File
*******************************************************************************/