/*
# plate.h

- 板クラス(3D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __3D_PLATE_H__
#define __3D_PLATE_H__

#include "draw3d.h"
#include "../../base/tempGetSet.h"
#include "../../math/Quad.h"
#include "../texture/texture.h"
#include "../../base/color.h"
#include "material.h"

NAMESPACE_3D
class Plate : public IDraw3D
{
private:
    // 内部クラス
    //--------------------------------------------------------------------
    template < typename T >
    class GetSetVertex : public TempGetSet<T>
    {
    public:
        GetSetVertex( Plate *, T &_value );
        ~GetSetVertex();

        GetSetVertex &operator = ( const T & );
    private:
        Plate *m_Master;
        void Set( const T & ) override;
    };

public:
    // プロパティ
    //--------------------------------------------------------------------
    GetSetVertex<Quad> Size;            // 板の大きさ
    GetSetVertex<Quad> TexUV;           // UV座標
    Texture            Tex;             // テクスチャ
    MaterialNormal *Mat(void) const;    // マテリアル

protected:
    // @brief  : コンストラクタ
    // @param  : マテリアル
    //         : 板サイズ
    //         : 親クラス
    //         : 描画するのか？
    //--------------------------------------------------------------------
    Plate( MaterialNormal *, const Quad &, IDraw *, bool );

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~Plate();

private:
    // メンバ変数
    //--------------------------------------------------------------------
    Quad            m_Size;        // ポリゴンサイズ
    Quad            m_TextureUV;   // テクスチャ座標
    Color           m_Color;       // ポリゴンカラー
    MaterialNormal *m_Material;    // マテリアル

    // @brief  : 頂点の変更
    //--------------------------------------------------------------------
    virtual void SetVertex( void ) = 0;
};



END_NAMESPACE_3D
#endif //  __3D_PLATE_H__
/******************************************************************************
|   End of File
*******************************************************************************/