/*
# camera.cpp

- カメラクラス(3D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード ---------------------------------------------------------------
#include "camera.h"
#include "../../math/math.h"

#include "../../app/win32Macro.h"
#if __APPLE__
#include "../../app/OSXMacro.h"
#endif

NAMESPACE_3D
// @brief  : コンストラクタ
// @param  : 親クラス
//         : 描画するのか？
//--------------------------------------------------------------------
Camera::Camera( IDraw *_parent, bool _is_draw ) :
    IDraw3D(_parent,_is_draw),
    m_FovY(Math::PI/4),
    m_Aspect(1.0f * SCREEN_WIDTH / SCREEN_HEIGHT),
    m_Near(0.01f),
    m_Far(10000.0f),
    m_Eye(0.0f,40.0f,100.0f),
    m_At(0.0f,0.0f,0.0f),
    m_Up(0.0f,1.0f,0.0f),
    m_Viewport(0.0f,SCREEN_WIDTH,0.0f,SCREEN_HEIGHT),
    FovY(m_FovY),
    Aspect(m_Aspect),
    Near(m_Near),
    Far(m_Far),
    Eye(m_Eye),
    At(m_At),
    Up(m_Up),
    Viewport(m_Viewport)
{
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
Camera::~Camera()
{
}

// @brief  : カメラ行列の作成
//--------------------------------------------------------------------
Matrix3D Camera::Matrix( void )const
{
    const Vector3 at  = At;
    const Vector3 eye = Eye;
    const Vector3 up  = Up;

#if 0
    Vector3 forward;
    forward = at- eye;
    forward.Normalize();

    Vector3 side = forward.Cross(up);
    side.Normalize();

    Vector3 uper = side.Cross(forward);
    uper.Normalize();

    return Matrix3D(
     side.x,uper.x,-forward.x,0,
     side.y,uper.y,-forward.y,0,
     side.z,uper.z,-forward.z,0,
     0,0,0,1
    );
#else
    const Vector3 z(Vector3(eye-at).Normalize());
    const Vector3 x(up.Cross(z).Normalize());
    const Vector3 y(z.Cross(x).Normalize());

    return Matrix3D (
        x.x, x.y, x.z, -x.Dot(eye),
        y.x, y.y, y.z, -y.Dot(eye),
        z.x, z.y, z.z, -z.Dot(eye),
        0.0f,0.0f,0.0f, 1.0f
    );
#endif
}



END_NAMESPACE_3D
/******************************************************************************
|   End of File
*******************************************************************************/