/*
# transform.h

- 行列変換クラス(3D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//-- インクルード ---------------------------------------------------------------
#include "transform.h"

#include <typeinfo>

NAMESPACE_3D
#pragma warning(push)   // インクリメントでメンバ変数にthisを渡している
#pragma warning(disable:4355)
// @brief  : コンストラクタ
// @param  : 親クラス
//         : 描画するのか？
//--------------------------------------------------------------------
Transform::Transform( IDraw *_parent, bool _is_draw ) :
    ITransform(_parent,_is_draw),
    m_ParentIsTransform(false),
    m_Position(0.0f,0.0f,0.0f),
    m_LocalRotation(0.0f,0.0f,0.0f),
    IsParent(m_ParentIsTransform),
    Matrix(m_Matrix),
    Position(m_Position,this),
    Rotation(m_LocalRotation,this)
{
}
#pragma warning(pop)

// @brief  : デストラクタ
//--------------------------------------------------------------------
Transform::~Transform()
{
}

// 内部クラス
//--------------------------------------------------------------------
template < typename T >
Transform::GetSetMatrix<T>::GetSetMatrix( T &_value, Transform *_master ) :
    TempGetSet<T>(_value),
    m_Master(_master)
{
}

template < typename T >
Transform::GetSetMatrix<T>::~GetSetMatrix()
{
}

template < typename T >
Transform::GetSetMatrix<T> &Transform::GetSetMatrix<T>::operator = ( const T &_value )
{
    Set(_value);
    return *this;
}

template < typename T >
void Transform::GetSetMatrix<T>::Set( const T &_value )
{
    TempGetSet<T>::Set(_value);
    m_Master->m_Matrix = m_Master->CreateMatrix();
}
template class _3d::Transform::GetSetMatrix<Vector3>;


END_NAMESPACE_3D
/******************************************************************************
|   End of File
*******************************************************************************/