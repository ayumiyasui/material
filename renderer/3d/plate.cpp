/*
# plate.cpp

- 板クラス(3D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "plate.h"


NAMESPACE_3D
#pragma warning(push)   // インクリメントでメンバ変数にthisを渡している
#pragma warning(disable:4355)
// @brief  : コンストラクタ
// @param  : 板サイズ
//         : 親クラス
//         : 描画するのか？
//--------------------------------------------------------------------
Plate::Plate( MaterialNormal *_mat, const Quad &_size, IDraw *_parent, bool _is_draw ) :
    IDraw3D(_parent,_is_draw),
    m_Size(_size),
    m_TextureUV(0.0f,1.0f,0.0f,1.0f),
    m_Color(1.0f,1.0f,1.0f,1.0f),
    Size(this,m_Size),
    TexUV(this,m_TextureUV),
    m_Material(_mat)
{
    this->AddChild(m_Material);
}
#pragma warning(pop)

// @brief  : デストラクタ
//--------------------------------------------------------------------
Plate::~Plate()
{
}

MaterialNormal *Plate::Mat(void) const
{
    return m_Material;
}

// 内部クラス
//--------------------------------------------------------------------
template < typename T >
Plate::GetSetVertex<T>::GetSetVertex( Plate *_master, T &_value ) :
    TempGetSet<T>(_value),
    m_Master(_master)
{
}

template < typename T >
Plate::GetSetVertex<T>::~GetSetVertex()
{
}

template < typename T >
Plate::GetSetVertex<T> &Plate::GetSetVertex<T>::operator = ( const T &_set )
{
    Set(_set);
    return *this;
}

template < typename T >
void Plate::GetSetVertex<T>::Set( const T &_set )
{
    TempGetSet<T>::Set(_set);
    m_Master->SetVertex();
}
template class _3d::Plate::GetSetVertex<Quad>;



END_NAMESPACE_3D
/******************************************************************************
|   End of File
*******************************************************************************/