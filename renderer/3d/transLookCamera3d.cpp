/*
# transLookCamera.cpp

- 行列変換クラス(3D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "transLookCamera3d.h"

#include "camera.h"

NAMESPACE_3D
#pragma warning(push)   // インクリメントでメンバ変数にthisを渡している
#pragma warning(disable:4355)
// @brief  : コンストラクタ
// @param  : カメラ
//         : 親クラス
//         : 描画するのか？
//--------------------------------------------------------------------
TransLookCamera::TransLookCamera( Camera *_cam, IDraw *_parent, bool _is_draw ) :
    ITransform(_parent,_is_draw),
    m_Camera(_cam),
    Matrix(m_Matrix),
    Position(m_Position,this)
{
}
#pragma warning(pop)

// @brief  : デストラクタ
//--------------------------------------------------------------------
TransLookCamera::~TransLookCamera()
{
}

// プロパティ
//--------------------------------------------------------------------
void TransLookCamera::Cam( Camera *_cam )
{
    m_Camera = _cam;
}

// @brief  : 行列の作成
//--------------------------------------------------------------------
Matrix3D TransLookCamera::CreateMatrix(void) const
{
    // 回転行列
    const Matrix3D cam(m_Camera->Matrix());
    Matrix3D rotation_matrix(
        cam._11,cam._21,cam._31,0.0f,
        cam._12,cam._22,cam._32,0.0f,
        cam._13,cam._23,cam._33,0.0f,
           0.0f,   0.0f,   0.0f,1.0f
    );

    // 位置
    const Vector3 position = Position;
    const Matrix3D position_matrix(
        1.0f,0.0f,0.0f,position.x,
        0.0f,1.0f,0.0f,position.y,
        0.0f,0.0f,1.0f,position.z,
        0.0f,0.0f,0.0f,1.0f
    );

    // 合成
    return position_matrix * rotation_matrix;
}

// 内部クラス
//--------------------------------------------------------------------
template < typename T >
TransLookCamera::GetSetMatrix<T>::GetSetMatrix( T &_value, TransLookCamera *_master ) :
    TempGetSet<T>(_value),
    m_Master(_master)
{
}

template < typename T >
TransLookCamera::GetSetMatrix<T>::~GetSetMatrix()
{
}

template < typename T >
TransLookCamera::GetSetMatrix<T> &TransLookCamera::GetSetMatrix<T>::operator = ( const T &_value )
{
    Set(_value);
    return *this;
}

template < typename T >
void TransLookCamera::GetSetMatrix<T>::Set( const T &_value )
{
    TempGetSet<T>::Set(_value);
    m_Master->m_Matrix = m_Master->CreateMatrix();
}

template class _3d::TransLookCamera::GetSetMatrix<Vector3>;

END_NAMESPACE_3D
/******************************************************************************
|   End of File
*******************************************************************************/