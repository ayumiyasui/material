/*
# draw3d.h

- 描画抽象クラス(3D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __DRAW3D_H__
#define __DRAW3D_H__

#include "../draw.h"

// 3D名前空間マクロ
#define NAMESPACE_3D      namespace _3d {
#define END_NAMESPACE_3D  };

NAMESPACE_3D
class IDraw3D : public IDraw
{
protected:
    // @brief  : コンストラクタ
    // @param  : 親クラス
    //         : 描画するのか？
    //--------------------------------------------------------------------
    IDraw3D( IDraw *_parent, bool _is_draw = true ) : IDraw(_parent,_is_draw){};

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~IDraw3D(){};
};



END_NAMESPACE_3D
#endif //  __DRAW3D_H__
/******************************************************************************
|   End of File
*******************************************************************************/