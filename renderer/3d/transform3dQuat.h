/*
# transformQuat.h

- 行列変換クラス(3D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __3D_TRANSFORMQUAT_H__
#define __3D_TRANSFORMQUAT_H__

#include "iTransform.h"
#include "../../base/tempGetter.h"
#include "../../base/tempGetSet.h"
#include "../../math/Quad.h"
#include "../../math/quaternion.h"

NAMESPACE_3D
class TransformQuat : public ITransform
{
private:
    template < typename T > class GetSetMatrix : public TempGetSet<T>
    {
    public:
        GetSetMatrix( T &, TransformQuat * );
        ~GetSetMatrix();
        GetSetMatrix<T> &operator = ( const T & );

    private:
        TransformQuat *m_Master;
        void Set( const T & ) override;
    };

public:
    // プロパティ
    //--------------------------------------------------------------------
    TempGetter<bool>      IsParent;     // 親がTransform
    TempGetter<Matrix3D>  Matrix;
    GetSetMatrix<Vector3> Position;
    GetSetMatrix<Quaternion> Rotation;

protected:
    // @brief  : コンストラクタ
    // @param  : 親クラス
    //         : 描画するのか？
    //--------------------------------------------------------------------
    TransformQuat( IDraw *, bool );

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~TransformQuat();

    // @brief  : 行列の計算
    //--------------------------------------------------------------------
    virtual Matrix3D CreateMatrix(void) const = 0;

private:
    // メンバ変数
    //--------------------------------------------------------------------
    Matrix3D m_Matrix;
    Vector3    m_Position;
    Quaternion m_LocalRotation;
    bool       m_ParentIsTransform;
};



END_NAMESPACE_3D
#endif //  __3D_TRANSFORMQUAT_H__
/******************************************************************************
|   End of File
*******************************************************************************/