/*
# directionalLight.h

- ライトクラス(3D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __3D_DIRECTIONALLIGHT_H__
#define __3D_DIRECTIONALLIGHT_H__

#include "light.h"

NAMESPACE_3D
class DirectionalLight : public Light
{
protected:
    // @brief  : コンストラクタ
    // @param  : 親クラス
    //         : 描画するのか？
    //--------------------------------------------------------------------
    DirectionalLight( IDraw *_parent, bool _is_draw ) : Light(_parent,_is_draw){};

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~DirectionalLight(){};
};



END_NAMESPACE_3D
#endif //  __3D_DIRECTIONALLIGHT_H__
/******************************************************************************
|   End of File
*******************************************************************************/