/*
# iTransform.h

- 行列変換クラス(3D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __3D_I_TRANSFORM_H__
#define __3D_I_TRANSFORM_H__

#include "draw3d.h"
#include "../../base/tempGetSet.h"
#include "../../math/Quad.h"
#include "../../math/vector3.h"
#include "../../math/matrix3d.h"

NAMESPACE_3D

class ITransform : public IDraw3D
{
protected:
    // @brief  : コンストラクタ
    // @param  : 親クラス
    //         : 描画するのか？
    //--------------------------------------------------------------------
    ITransform(IDraw *_parent, bool _is_draw) : IDraw3D(_parent,_is_draw) {};

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~ITransform(){};
};



END_NAMESPACE_3D
#endif //  __3D_I_TRANSFORM_H__
/******************************************************************************
|   End of File
*******************************************************************************/