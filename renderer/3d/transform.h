/*
# transform.h

- 行列変換クラス(3D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __3D_TRANSFORM_H__
#define __3D_TRANSFORM_H__

#include "iTransform.h"
#include "../../base/tempGetSet.h"

NAMESPACE_3D

class Transform : public ITransform
{
private:
    template < typename T > class GetSetMatrix : public TempGetSet<T>
    {
    public:
        GetSetMatrix( T &, Transform * );
        ~GetSetMatrix();
        GetSetMatrix<T> &operator = ( const T & );

    private:
        Transform *m_Master;
        void Set( const T & ) override;
    };

public:
    // プロパティ
    //--------------------------------------------------------------------
    TempGetter<bool>      IsParent;     // 親がTransform
    TempGetter<Matrix3D>  Matrix;
    GetSetMatrix<Vector3> Position;
    GetSetMatrix<Vector3> Rotation;

protected:
    // @brief  : コンストラクタ
    // @param  : 親クラス
    //         : 描画するのか？
    //--------------------------------------------------------------------
    Transform( IDraw *, bool );

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~Transform();

    // @brief  : 計算
    //--------------------------------------------------------------------
    virtual Matrix3D CreateMatrix(void) const = 0;

private:
    // メンバ変数
    //--------------------------------------------------------------------
    Matrix3D m_Matrix;
    Vector3  m_Position;
    Vector3  m_LocalRotation;
    bool     m_ParentIsTransform;
};



END_NAMESPACE_3D
#endif //  __3D_TRANSFORM_H__
/******************************************************************************
|   End of File
*******************************************************************************/