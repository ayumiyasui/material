/*
# XFile.h

- Xファイルクラス(3D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __3D_XFILE_H__
#define __3D_XFILE_H__

#include "draw3d.h"
#include "mesh.h"
#include "material.h"

NAMESPACE_3D
class XFile : public IDraw3D
{
protected:
    // @brief  : コンストラクタ
    // @param  : 親クラス
    //         : 描画するのか？
    //--------------------------------------------------------------------
    XFile( IDraw *_parent, bool _is_draw ) : IDraw3D(_parent,_is_draw){};

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~XFile(){};
};



END_NAMESPACE_3D
#endif //  __3D_XFILE_H__
/******************************************************************************
|   End of File
*******************************************************************************/