/*
# light.h

- ライトクラス(3D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __3D_LIGHT_H__
#define __3D_LIGHT_H__

#include "draw3d.h"
#include "../../base/color.h"
#include "../../math/vector3.h"
#include "../../base/tempGetSet.h"

NAMESPACE_3D
class Light : public IDraw3D
{
private:
    // 内部クラス
    //--------------------------------------------------------------------
    template < typename T > class GetSetLight : public TempGetSet<T>
    {
    public:
        GetSetLight(T &_value, Light *_master) :
            TempGetSet<T>(_value),
            m_Master(_master)
        {
        };
        ~GetSetLight(){};

        GetSetLight<T> &operator = ( const T &_value )
        {
            Set(_value);
            return *this;
        };

    private:
        Light *m_Master;
        void Set( const T &_value ) override
        {
            TempGetSet<T>::Set(_value);
            m_Master->Set();
        };
    };

public:
    // プロパティ
    //--------------------------------------------------------------------
    GetSetLight<Color>   Diffuse;    // 拡散
    GetSetLight<Color>   Specular;   // 反射
    GetSetLight<Color>   Ambient;    // 環境
    GetSetLight<Vector3> Direction;  // 向き

protected:
#pragma warning(push)   // インクリメントでメンバ変数にthisを渡している
#pragma warning(disable:4355)
    // @brief  : コンストラクタ
    // @param  : 親クラス
    //         : 描画するのか？
    //--------------------------------------------------------------------
    Light( IDraw *_parent, bool _is_draw ) :
        IDraw3D(_parent,_is_draw),
        Diffuse(m_Diffuse,this),
        Specular(m_Specular,this),
        Ambient(m_Ambient,this),
        Direction(m_Direction,this)
    {
    };
#pragma warning(pop)

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~Light(){};

private:
    // メンバ変数
    //--------------------------------------------------------------------
    Color   m_Diffuse;      // 拡散光
    Color   m_Specular;     // 反射光
    Color   m_Ambient;      // 環境光
    Vector3 m_Direction;    // 向き

    // @brief  : ライトのセット
    //--------------------------------------------------------------------
    virtual void Set( void ) = 0;
};



END_NAMESPACE_3D
#endif //  __3D_LIGHT_H__
/******************************************************************************
|   End of File
*******************************************************************************/