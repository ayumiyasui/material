/*
# factory3dGL.h

- ファクトリクラス(3D)
- 機種・ライブラリ差の吸収のために作成

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __3D_FACTORY_GL_H__
#define __3D_FACTORY_GL_H__

#include "../3d/factory.h"

class OpenGL;

NAMESPACE_3D
class FactoryGL : public IFactory
{
public:
    // @brief  : カメラクラスの作成
    //--------------------------------------------------------------------
    Camera *CreateCamera( IDraw *, bool ) const override;

    // @brief  : ライトクラスの作成
    //--------------------------------------------------------------------
    DirectionalLight *CreateDirLight( IDraw *, bool ) const override;

    // @brief  : Transformの作成
    //--------------------------------------------------------------------
    Transform *CreateTransform( IDraw *, bool ) const override;
    TransformQuat *CreateTransformQuat( IDraw *, bool ) const override;
    TransLookCamera *CreateTransformLookCamera( Camera *, IDraw *, bool ) const override;

    // @brief  : 板の作成
    // @param  : テクスチャ名
    //         : 大きさ
    //          :親クラス
    //         : 描画するのか？
    //--------------------------------------------------------------------
    Plate *CreatePlate( const std::string &, const Quad &, IDraw *, bool ) override;

    // @brief  : 空の作成
    // @param  : テクスチャ名
    //         : 半径
    //         : ブロック数横
    //         : ブロック数縦
    //          :親クラス
    //         : 描画するのか？
    //--------------------------------------------------------------------
    Sky *CreateSky( const std::string &, float, unsigned int, unsigned int, IDraw *, bool ) const override;

    // @brief  : XFileの作成
    //--------------------------------------------------------------------
    XFile *CreateXFile( IDraw *, bool, const std::string & ) const override;

    // @brief  : PMDの作成(骨なし)
    //--------------------------------------------------------------------
    PMDNoBone *CreatePMDNoBone( const std::string &, IDraw *, bool ) const override;

protected:
    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~FactoryGL();

private:
    // フレンドクラス
    //--------------------------------------------------------------------
    friend OpenGL;

    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    FactoryGL();
};



END_NAMESPACE_3D
#endif //  __3D_FACTORY_GL_H__
/******************************************************************************
|   End of File
*******************************************************************************/