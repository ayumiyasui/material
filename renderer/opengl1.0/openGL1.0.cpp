/*
# openGL1.0.cpp

- レンダラーGLクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード ------------------------------------------------------------
#include "openGL1.0.h"

#include <assert.h>
#include "../draw.h"
#include "textureManagerGL.h"
#include "PMDNoBoneManagerGL.h"
#include "factory2dGL.h"
#include "factory3dGL.h"
#include "effect/factory.h"
#include "vertex/factory.h"
using namespace material;

// @brief  : コンストラクタ
//--------------------------------------------------------------------
OpenGL::OpenGL( float _width, float _height, HWND _window ) :
    IRenderer(_width,_height, new _2d::FactoryGL, new _3d::FactoryGL, new effect::FactoryGL, new vertex::FactoryGL),
    m_Window(_window),
    m_DC(GetDC(_window))
{
    // ピクセルフォーマットを生成
    PIXELFORMATDESCRIPTOR pfd =
    {
        sizeof( PIXELFORMATDESCRIPTOR ), 1, // サイズとバージョン
        PFD_DRAW_TO_WINDOW |                // 描画フラグ
        PFD_SUPPORT_OPENGL |
        PFD_DOUBLEBUFFER,
        PFD_TYPE_RGBA,                      // ピクセル形式
        32,                                 // カラーバッファ
        0, 0, 0, 0, 0, 0,
        0,                                  // アキュムレーションバッファ
        0,
        0,
        0, 0, 0, 0,
        24,                                 // デプスバッファ
        8,                                  // ステンシルバッファ
        0,                                  // フレームバッファ
        PFD_MAIN_PLANE,                     // レイヤタイプ
        0,
        0, 0, 0                          // レイヤ設定
    };

    // ピクセルフォーマットを選択
    // 生成したOpenGLのピクセルフォーマットを
    // Windowsのピクセルフォーマットに合わせる
    const int pixcelFormat = ChoosePixelFormat( m_DC, &pfd );
    assert(pixcelFormat);

    // ピクセルフォーマットを設定
    const BOOL set_pixel_format = SetPixelFormat( m_DC, pixcelFormat, &pfd );
    assert(set_pixel_format);

    // OpenGLレンダリングコンテキストを作成
    m_GLRC = wglCreateContext( m_DC );

    // OpenGLレンダリングをカレントする
    const BOOL current = wglMakeCurrent( m_DC, m_GLRC );
    assert(current);

    // 深度バッファを入れる
    glEnable(GL_DEPTH_TEST);

    // アルファのセット
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

    // カリング
    glEnable(GL_CULL_FACE);
    glFrontFace(GL_CW);

#ifdef _DEBUG
    m_DebugFont = CreateFont(
        18,0,0,0,FW_REGULAR,FALSE,FALSE,FALSE,
        SHIFTJIS_CHARSET,OUT_DEFAULT_PRECIS,
        CLIP_DEFAULT_PRECIS,ANTIALIASED_QUALITY,
        FIXED_PITCH | FF_MODERN,
        "Terminal" );

    SelectObject(m_DC,m_DebugFont);
#endif

    TextureManagerGL::Instance();
    PMDNoBoneManagerGL::Instance();
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
OpenGL::~OpenGL()
{
#ifdef _DEBUG
    DeleteObject(m_DebugFont);
#endif
    // OpenGLレンダリングコンテキストをカレントでなくす
    wglMakeCurrent( NULL, NULL );

    // OpenGLレンダリングコンテキストを削除する
    wglDeleteContext( m_GLRC );

    // デバイスコンテキストを削除する
    ReleaseDC( m_Window, m_DC );
}

// @brief  : 描画
//--------------------------------------------------------------------
void OpenGL::Draw( void )
{
    // バッファをクリアする色を設定
    const Color back_color = BackColor;
    glClearColor( back_color.r, back_color.g, back_color.b, back_color.a );

    // カラーバッファを設定した色でクリア
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT );

    TempNodeSingleton<IDraw>::Instance().DrawAll();

//#ifdef _DEBUG     // 処理としてとても重い
//    if(DebugIsDraw)
//    {
//        glClear( GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT );
//        glOrtho(0, SCREEN_WIDTH, SCREEN_HEIGHT, 0, 0, 1);
//        GLuint  list = glGenLists(MAX_STR);
//        const TCHAR  *str  = DebugString;
//        for(auto i = 0 ; i < MAX_STR ; ++i)
//        {
//            wglUseFontBitmaps(m_DC,str[i],1,list+static_cast<DWORD>(i));
//        }
//
//        glDisable(GL_LIGHTING);
//        glDisable(GL_DEPTH);
//        glRasterPos2i(1,-1);
//        glColor4f(0.0f,0.0f,0.0f,1.0f);
//        for(auto i = 0; i < MAX_STR ; ++i)
//        {
//            glCallList(list+i);
//        }
//        glEnable(GL_LIGHTING);
//        glEnable(GL_DEPTH);
//
//        glDeleteLists(list,MAX_STR);
//    }
//#endif
    // フロント・バックバッファの入れ替え
    SwapBuffers( m_DC );

    // デバックバッファのクリア
#ifdef _DEBUG
    ResetDebugString();
#endif
}



/******************************************************************************
|   End of File
*******************************************************************************/