/*
# transformGL.h

- 変換行列設定クラス(2D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "transform2dGL.h"


NAMESPACE_2D
// @brief  : コンストラクタ
// @param  : 親クラス
//         : 描画するのか？
//--------------------------------------------------------------------
TransformGL::TransformGL( IDraw *_parent, bool _is_draw ) :
    Transform(_parent,_is_draw)
{
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
TransformGL::~TransformGL()
{
}

// @brief  : 描画
//--------------------------------------------------------------------
void TransformGL::Draw( void )
{
    glMatrixMode( GL_MODELVIEW );
    glPushMatrix();

    const float m[] = {
        Matrix->_11, Matrix->_21, 0.0f, Matrix->_31,
        Matrix->_12, Matrix->_22, 0.0f, Matrix->_32,
        0.0f,        0.0f,        1.0f,       0.0f,
        Matrix->_13, Matrix->_23, 0.0f, Matrix->_33
    };
    glMultMatrixf(m);
}

// @brief  : 描画
// @note   : 子が終わった後に動く、後片付け用
//--------------------------------------------------------------------
void TransformGL::LateDraw( void )
{
    glMatrixMode( GL_MODELVIEW );
    glPopMatrix();
}



END_NAMESPACE_2D
/******************************************************************************
|   End of File
*******************************************************************************/