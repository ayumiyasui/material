/*
# skyGL.cpp

- 空クラス(3D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "skyGL.h"

#include <assert.h>
#include "tristMeshGL.h"
#include "../../math/math.h"
#include "../../math/vector3.h"
#include "../../math/quaternion.h"
#include "../../math/matrix3d.h"

NAMESPACE_3D

// @brief  : コンストラクタ
// @param  : テクスチャ
//         : 半径
//         : ブロック数横
//         : ブロック数縦
//          :親クラス
//         : 描画するのか？
//--------------------------------------------------------------------
SkyGL::SkyGL( Texture _tex, float _length, unsigned int _x, unsigned int _y, IDraw *_parent, bool _is_draw ) :
    Sky(_parent,_is_draw)
{
    TriStMeshGL *mesh = new TriStMeshGL(_x,_y,this,true);

    // 頂点設定
    Vector3  pos( 0.0f, _length, 0.0f );
    const Matrix3D mtx_rot_z ( Quaternion(0.0f,0.0f,Math::PI/_x) );
    const Matrix3D mtx_rot_y(Quaternion(-2.0f*Math::PI/_y,0.0f,0.0f));
    TriStMeshGL::VERTEX *vtx = mesh->VertexBuffer();
    unsigned int i = 0;  // 頂点バッファの位置
    for( unsigned int y = 0; y < _y + 1 ; ++y )
    {
        Vector3 v3Y   = pos;
        Vector3 v3Set = v3Y;
        for( unsigned int x = 0 ; x < _x + 1 ; ++x )
        {
            assert(i < mesh->MAX_VERTEX);
            v3Set *= mtx_rot_y;
            vtx[i].pos[0] = v3Set.x;
            vtx[i].pos[1] = v3Set.y;
            vtx[i].pos[2] = v3Set.z;
            const Vector3 v3Nor = v3Set.Normalize();
            vtx[i].nor[0] = -v3Nor.x;
            vtx[i].nor[1] = -v3Nor.y;
            vtx[i].nor[2] = -v3Nor.z;
            vtx[i].tex[0] = v3Nor.x * 0.5f + 0.5f;
            vtx[i].tex[1] = v3Nor.y * 0.5f + 0.5f;

            // 次の頂点へ
            i++;
        }
        pos *= mtx_rot_z;
    }
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
SkyGL::~SkyGL()
{
}



END_NAMESPACE_3D
/******************************************************************************
|   End of File
*******************************************************************************/