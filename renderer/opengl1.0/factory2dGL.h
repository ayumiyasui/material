/*
# factory2dGL.h

- ファクトリクラス(2D)
- 機種・ライブラリ差の吸収のために作成
- 便利な関数ではないので注意

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __2D_FACTORY_GL_H__
#define __2D_FACTORY_GL_H__

#include "../2d/factory.h"

class OpenGL;

NAMESPACE_2D
class FactoryGL : public IFactory
{
public:
    // @brief  : Cameraクラス掃き出し
    //--------------------------------------------------------------------
    Camera *CreateCamera( IDraw *, bool ) const override;

    // @brief  : Transformクラス掃き出し
    //--------------------------------------------------------------------
    Transform* CreateTransform( IDraw *, bool ) const override;

    // @brief  : Spriteクラス掃き出し
    //--------------------------------------------------------------------
    Sprite *CreateSprite(
        const Quad &, const Quad &,
        const std::string &,
        IDraw *, bool ) const override;

private:
    // フレンドクラス
    //--------------------------------------------------------------------
    friend OpenGL;

    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    FactoryGL();

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~FactoryGL();
};



END_NAMESPACE_2D
#endif //  __2D_FACTORY_H__
/******************************************************************************
|   End of File
*******************************************************************************/