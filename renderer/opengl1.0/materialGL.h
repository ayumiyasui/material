/*
# materialGL.h

- マテリアルクラス(3D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __3D_MATERIAL_GL_H__
#define __3D_MATERIAL_GL_H__

#include "opengl1.0.h"
#include "../3d/material.h"

NAMESPACE_3D
class MaterialNormalGL : public MaterialNormal
{
public:
    // @brief  : コンストラクタ
    // @param  : 親クラス
    //         : 描画するのか？
    //--------------------------------------------------------------------
    MaterialNormalGL( IDraw *, bool );

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~MaterialNormalGL();

    // @brief  : 描画
    //--------------------------------------------------------------------
    void Draw(void) override;
};


END_NAMESPACE_3D
#endif //  __3D_MATERIAL_GL_H__
/******************************************************************************
|   End of File
*******************************************************************************/