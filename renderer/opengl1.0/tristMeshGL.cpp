/*
# tristMeshGL.h

- メッシュクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "tristMeshGL.h"

#include <assert.h>


NAMESPACE_3D

// @brief  : コンストラクタ
// @param  : 横ブロック数
//         : 縦ブロック数
//         : 親クラス
//         : 描画するのか？
//--------------------------------------------------------------------
TriStMeshGL::TriStMeshGL( unsigned int _x, unsigned int _y, IDraw *_parent, bool _is_draw ) :
    Mesh(_parent,_is_draw),
    BLOCK_X(_x),
    BLOCK_Y(_y),
    MAX_VERTEX((_x+1)*(_y+1)),
    MAX_INDEX((((_x+1)*2+2)*_y)-2),
    m_VertexBuffer(nullptr),
    m_IndexBuffer(nullptr)
{
    m_VertexBuffer = new VERTEX[MAX_VERTEX];
    m_IndexBuffer  = new GLuint[MAX_INDEX];

    // インデックスを指定
    {
        int upper = 0;
        int lower = BLOCK_X + 1;
        unsigned int i = 0;
        for( unsigned int y = 0; y < BLOCK_Y ; ++y )
        {
            for( unsigned int x = 0; x < BLOCK_X + 1; ++x )
            {
                // 上点
                assert(i < MAX_INDEX);
                m_IndexBuffer[i++] = upper++;

                // 下点
                assert(i < MAX_INDEX);
                m_IndexBuffer[i++] = lower++;
            }

            if( y < BLOCK_Y - 1 )
            {
                upper--;    // ダミー点を打つための調整

                // ダミー点２
                assert(i < MAX_INDEX);
                m_IndexBuffer[i++] = lower;

                // ダミー点１
                assert(i < MAX_INDEX);
                m_IndexBuffer[i++] = upper++;
            }
        }
    }
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
TriStMeshGL::~TriStMeshGL()
{
    if(m_VertexBuffer) delete[] m_VertexBuffer;
    if(m_IndexBuffer)  delete[] m_IndexBuffer;
}

// プロパティ
//--------------------------------------------------------------------
TriStMeshGL::VERTEX *TriStMeshGL::VertexBuffer(void)
{
    return m_VertexBuffer;
}

// @brief  : 描画
//--------------------------------------------------------------------
void TriStMeshGL::Draw( void )
{
    glEnableClientState( GL_VERTEX_ARRAY );
    glEnableClientState( GL_NORMAL_ARRAY );
    glEnableClientState( GL_TEXTURE_COORD_ARRAY );

    static const GLsizei VERTEX_SIZE = sizeof( VERTEX );
    glVertexPointer(  3, GL_FLOAT, VERTEX_SIZE, (const GLvoid *)&m_VertexBuffer[0].pos[0] );
    glNormalPointer(     GL_FLOAT, VERTEX_SIZE, (const GLvoid *)&m_VertexBuffer[0].nor[0] );
    glTexCoordPointer(2, GL_FLOAT, VERTEX_SIZE, (const GLvoid *)&m_VertexBuffer[0].tex[0] );

    glDrawElements( GL_TRIANGLE_STRIP, MAX_INDEX,
                    GL_UNSIGNED_INT  , (const GLvoid *)m_IndexBuffer );

    glDisableClientState( GL_VERTEX_ARRAY );
    glDisableClientState( GL_NORMAL_ARRAY );
    glDisableClientState( GL_TEXTURE_COORD_ARRAY );
}



END_NAMESPACE_3D
/******************************************************************************
|   End of File
*******************************************************************************/