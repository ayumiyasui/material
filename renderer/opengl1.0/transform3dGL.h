/*
# transform3dGL.h

- 行列変換クラス(3D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __3D_TRANSFORM_GL_H__
#define __3D_TRANSFORM_GL_H__

#include "../3d/transform.h"

NAMESPACE_3D

    class FactoryGL;

class TransformGL : public Transform
{
private:
    // フレンドクラス
    //--------------------------------------------------------------------
    friend FactoryGL;

    // @brief  : コンストラクタ
    // @param  : 親クラス
    //         : 描画するのか？
    //--------------------------------------------------------------------
    TransformGL( IDraw *, bool );

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~TransformGL();

    // @brief  : 計算
    //--------------------------------------------------------------------
    Matrix3D CreateMatrix(void) const override;

    // @brief  : 描画
    //--------------------------------------------------------------------
    void Draw( void ) override;

    // @brief  : 描画
    // @note   : 子が終わった後に動く、後片付け用
    //--------------------------------------------------------------------
    void LateDraw( void ) override;
};



END_NAMESPACE_3D
#endif //  __3D_TRANSFORM_GL_H__
/******************************************************************************
|   End of File
*******************************************************************************/