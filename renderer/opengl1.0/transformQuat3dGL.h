/*
# transformQuat.h

- 行列変換クラス(3D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __3D_TRANSFORMQUAT_GL_H__
#define __3D_TRANSFORMQUAT_GL_H__

#include "../3d/transform3dQuat.h"

NAMESPACE_3D

    class FactoryGL;

class TransformQuatGL : public TransformQuat
{
private:
    // フレンドクラス
    //--------------------------------------------------------------------
    friend FactoryGL;

    // @brief  : コンストラクタ
    // @param  : 親クラス
    //         : 描画するのか？
    //--------------------------------------------------------------------
    TransformQuatGL( IDraw *, bool );

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~TransformQuatGL();

    // @brief  : 行列の計算
    //--------------------------------------------------------------------
    Matrix3D CreateMatrix(void) const override;

    // @brief  : 描画
    //--------------------------------------------------------------------
    void Draw( void ) override;

    // @brief  : 描画
    // @note   : 子が終わった後に動く、後片付け用
    //--------------------------------------------------------------------
    void LateDraw( void ) override;
};



END_NAMESPACE_3D
#endif //  __3D_TRANSFORMQUAT_GL_H__
/******************************************************************************
|   End of File
*******************************************************************************/