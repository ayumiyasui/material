/*
# factory2dGL.cpp

- ファクトリクラス(2D)
- 機種・ライブラリ差の吸収のために作成
- 便利な関数ではないので注意

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "factory2dGL.h"

#include "opengl1.0.h"
#include "camera2dGL.h"
#include "transform2dGL.h"
#include "sprite2dGL.h"
#include "textureManagerGL.h"

NAMESPACE_2D
// @brief  : コンストラクタ
//--------------------------------------------------------------------
FactoryGL::FactoryGL()
{
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
FactoryGL::~FactoryGL()
{
}

// @brief  : Cameraクラス掃き出し
//--------------------------------------------------------------------
Camera *FactoryGL::CreateCamera( IDraw *_parent, bool _is_draw ) const
{
    return new CameraGL(_parent,_is_draw);
}

// @brief  : Transformクラス掃き出し
//--------------------------------------------------------------------
Transform* FactoryGL::CreateTransform( IDraw *_parent, bool _is_draw ) const
{
    return new TransformGL(_parent,_is_draw);
}

// @brief  : Spriteクラス掃き出し
//--------------------------------------------------------------------
Sprite *FactoryGL::CreateSprite(
    const Quad &_pol_size, const Quad &_tex_size,
    const std::string &_file_pass,
    IDraw *_parent, bool _is_draw ) const
{
    return new SpriteGL(_pol_size,_tex_size,
        TextureManagerGL::Instance().Load(_file_pass),
        _parent,_is_draw);
}



END_NAMESPACE_2D
/******************************************************************************
|   End of File
*******************************************************************************/