/*
# camera.h

- カメラクラス(3D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "camera3dGL.h"

#include <math.h>
#include "../../math/math.h"
#include "opengl1.0.h"

NAMESPACE_3D
// @brief  : コンストラクタ
// @param  : 親クラス
//         : 描画するのか？
//--------------------------------------------------------------------
CameraGL::CameraGL( IDraw *_parent, bool _is_draw ) :
    Camera(_parent,_is_draw)
{
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
CameraGL::~CameraGL()
{
}

// @brief  : 描画
//--------------------------------------------------------------------
void CameraGL::Draw( void )
{
    // クリア
    glClear( GL_DEPTH_BUFFER_BIT );

    // プロジェクションマトリックスの設定
    glMatrixMode( GL_PROJECTION );
    glPushMatrix();                     // スタック領域に入れる
#if 0
    const GLfloat aspect = Aspect;
    const GLfloat far    = Far;
    const GLfloat near   = Near;
    const GLfloat t = tanf(FovY*0.5f);
    const GLfloat a = near - far;
    const GLfloat m[4*4] =
    {
        t/aspect, 0.0f, 0.0f,           0.0f,
        0.0f,     t,    0.0f,           0.0f,
        0.0f,     0.0f, (far+near)/a,  -1.0f,
        0.0f,     0.0f, (2*far*near)/a, 0.0f
    };
    glLoadMatrixf(m);
#else
    glLoadIdentity();
    gluPerspective(Math::ToDegree(FovY),Aspect,Near,Far);
#endif

    // ワールドマトリックスとビューマトリックス
    glMatrixMode( GL_MODELVIEW );
    glPushMatrix();
    Matrix3D view = Matrix();
    const GLfloat m[4*4] =
    {
        view._11,view._21,view._31,view._41,
        view._12,view._22,view._32,view._42,
        view._13,view._23,view._33,view._43,
        view._14,view._24,view._34,view._44
    };
    glLoadMatrixf(m);

    // ビューポート
    glViewport( (GLint)Viewport->left, (GLint)Viewport->top, (GLint)Viewport->right, (GLint)Viewport->bottom );
}

// @brief  : 描画
// @note   : 子が終わった後に動く、後片付け用
//--------------------------------------------------------------------
void CameraGL::LateDraw( void )
{
    glMatrixMode( GL_MODELVIEW );
    glPopMatrix();
    glMatrixMode( GL_PROJECTION );
    glPopMatrix();
}



END_NAMESPACE_3D
/******************************************************************************
|   End of File
*******************************************************************************/