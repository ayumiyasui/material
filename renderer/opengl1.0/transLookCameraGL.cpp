/*
# transLookCameraGL.cpp

- 行列変換クラス(3D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "transLookCameraGL.h"

NAMESPACE_3D
// @brief  : コンストラクタ
// @param  : カメラ
//         : 親クラス
//         : 描画するのか？
//--------------------------------------------------------------------
TransLookCameraGL::TransLookCameraGL( Camera *_cam, IDraw *_parent, bool _is_draw ) :
    TransLookCamera(_cam,_parent,_is_draw)
{
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
TransLookCameraGL::~TransLookCameraGL()
{
}

// @brief  : 描画
//--------------------------------------------------------------------
void TransLookCameraGL::Draw( void )
{
    glMatrixMode( GL_MODELVIEW );
    glPushMatrix();

    Vector3 pos = Position;
    Position = pos;
    Matrix3D mtx = Matrix;
    const float m[] = {
        mtx._11, mtx._21, mtx._31, mtx._41,
        mtx._12, mtx._22, mtx._32, mtx._42,
        mtx._13, mtx._23, mtx._33, mtx._43,
        mtx._14, mtx._24, mtx._34, mtx._44,
    };
    glMultMatrixf(m);
}

// @brief  : 描画(後始末)
//--------------------------------------------------------------------
void TransLookCameraGL::LateDraw( void )
{
    glMatrixMode( GL_MODELVIEW );
    glPopMatrix();
}



END_NAMESPACE_3D
/******************************************************************************
|   End of File
*******************************************************************************/