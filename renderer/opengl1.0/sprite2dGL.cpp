/*
# sprite2dGL.cpp

- スプライトクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "sprite2dGL.h"

#include "../../app/win32.h"


NAMESPACE_2D
// @brief  : コンストラクタ
// @param  : ポリゴンの大きさ
//         : テクスチャ座標の位置
//         : テクスチャ
//         : 親クラス
//         : 描画するのか？
//--------------------------------------------------------------------
SpriteGL::SpriteGL( const Quad &_pol_size, const Quad &_tex_size,
            const Texture &_tex,
            IDraw *_parent, bool _is_draw ) :
    Sprite(_pol_size,_tex_size,_tex,_parent,_is_draw)
{
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
SpriteGL::~SpriteGL()
{
}

// @brief  : 描画
//--------------------------------------------------------------------
void SpriteGL::Draw( void )
{
    static_cast<Texture>(Tex)->Set();

    glBegin( GL_TRIANGLE_STRIP );
    {
        glColor4f( PolygonColor->r, PolygonColor->g, PolygonColor->b, PolygonColor->a );

        glTexCoord2f( TextureQuad->right, TextureQuad->top );
        glVertex3f( PolygonQuad->right, PolygonQuad->bottom, 0.0f );

        glTexCoord2f( TextureQuad->left, TextureQuad->top );
        glVertex3f( PolygonQuad->left, PolygonQuad->bottom, 0.0f );

        glTexCoord2f( TextureQuad->right, TextureQuad->bottom );
        glVertex3f( PolygonQuad->right, PolygonQuad->top, 0.0f );

        glTexCoord2f( TextureQuad->left, TextureQuad->bottom );
        glVertex3f( PolygonQuad->left, PolygonQuad->top, 0.0f );
    }
    glEnd();
}



END_NAMESPACE_2D
/******************************************************************************
|   End of File
*******************************************************************************/