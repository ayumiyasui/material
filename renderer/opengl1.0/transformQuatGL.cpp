/*
# transformQuatGL.cpp

- 行列変換クラス(3D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "transformQuat3dGL.h"

#include "opengl1.0.h"


NAMESPACE_3D
// @brief  : コンストラクタ
// @param  : 親クラス
//         : 描画するのか？
//--------------------------------------------------------------------
TransformQuatGL::TransformQuatGL( IDraw *_parent, bool _is_draw ) :
    TransformQuat(_parent,_is_draw)
{
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
TransformQuatGL::~TransformQuatGL()
{
}

// @brief  : 行列の計算
//--------------------------------------------------------------------
Matrix3D TransformQuatGL::CreateMatrix(void) const
{
    // 回転行列
    const Quaternion rotation = Rotation;
    const Matrix3D   rotation_matrix(rotation);

    // 位置
    const Vector3 position = Position;
    const Matrix3D position_matrix(
        1.0f,0.0f,0.0f,position.x,
        0.0f,1.0f,0.0f,position.y,
        0.0f,0.0f,1.0f,position.z,
        0.0f,0.0f,0.0f,1.0f
    );

    // 合成
    return position_matrix * rotation_matrix;
}

// @brief  : 描画
//--------------------------------------------------------------------
void TransformQuatGL::Draw( void )
{
    glMatrixMode( GL_MODELVIEW );
    glPushMatrix();

    Matrix3D mtx = Matrix;
    const float m[] = {
        mtx._11, mtx._21, mtx._31, mtx._41,
        mtx._12, mtx._22, mtx._32, mtx._42,
        mtx._13, mtx._23, mtx._33, mtx._43,
        mtx._14, mtx._24, mtx._34, mtx._44,
    };
    glMultMatrixf(m);
}

// @brief  : 描画
// @note   : 子が終わった後に動く、後片付け用
//--------------------------------------------------------------------
void TransformQuatGL::LateDraw( void )
{
    glMatrixMode( GL_MODELVIEW );
    glPopMatrix();
}


END_NAMESPACE_3D
/******************************************************************************
|   End of File
*******************************************************************************/