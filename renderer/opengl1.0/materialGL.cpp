/*
# materialGL.cpp

- マテリアルクラス(3D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "materialGL.h"


NAMESPACE_3D
// @brief  : コンストラクタ
// @param  : 親クラス
//         : 描画するのか？
//--------------------------------------------------------------------
MaterialNormalGL::MaterialNormalGL( IDraw *_parent, bool _is_draw ) :
    MaterialNormal(_parent,_is_draw)
{
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
MaterialNormalGL::~MaterialNormalGL()
{
}

// @brief  : 描画
//--------------------------------------------------------------------
void MaterialNormalGL::Draw(void)
{
    const float diffuse[4] =
    {
        Diffuse->r,
        Diffuse->g,
        Diffuse->b,
        Diffuse->a
    };
    const float specular[4] =
    {
        Specular->r,
        Specular->g,
        Specular->b,
        Specular->a
    };
    const float ambient[4] =
    {
        Ambient->r,
        Ambient->g,
        Ambient->b,
        Ambient->a
    };
    const float emissive[4] =
    {
        Emissive->r,
        Emissive->g,
        Emissive->b,
        Emissive->a
    };

    glMaterialfv(GL_FRONT_AND_BACK,GL_DIFFUSE,diffuse);
    glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,specular);
    glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT,ambient);
    glMaterialfv(GL_FRONT_AND_BACK,GL_EMISSION,emissive);
}



END_NAMESPACE_3D
/******************************************************************************
|   End of File
*******************************************************************************/