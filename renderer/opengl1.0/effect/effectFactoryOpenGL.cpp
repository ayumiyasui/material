/*
# effectFactoryGL.h

- ファクトリクラス(エフェクト)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "factory.h"
#include "alphaBlend.h"
#include "curring.h"

namespace material {
namespace effect {
// @brief  : コンストラクタ
//--------------------------------------------------------------------
FactoryGL::FactoryGL()
{
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
FactoryGL::~FactoryGL()
{
}

// @brief  : アルファブレンドの書き出し
//--------------------------------------------------------------------
AlphaBlend *FactoryGL::CreateAlphaBlend(IDraw *_parent) const
{
    return new AlphaBlendGL(_parent);
}

// @brief  : カリングの書き出し
//--------------------------------------------------------------------
Curring *FactoryGL::CreateCurring(IDraw *_parent) const
{
    return new CurringGL(_parent);
}



}; // namespace effect
}; // namespace material
/******************************************************************************
|   End of File
*******************************************************************************/