/*
# factory.h

- ファクトリクラス(エフェクト)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef MATERIAL_OPENGL_EFFECT_FACTORY_H_
#define MATERIAL_OPENGL_EFFECT_FACTORY_H_

#include "../../effect/factory.h"
#include "../opengl1.0.h"

namespace material {
namespace effect {
class FactoryGL : public Factory
{
public:
    // @brief  : アルファブレンドの書き出し
    //--------------------------------------------------------------------
    AlphaBlend *CreateAlphaBlend(IDraw *_parent) const override;

    // @brief  : カリングの書き出し
    //--------------------------------------------------------------------
    Curring *CreateCurring(IDraw *_parent) const override;

private:
    // フレンドクラス
    //--------------------------------------------------------------------
    friend OpenGL;

    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    FactoryGL();

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~FactoryGL();
};


}; // namespace effect
}; // namespace material
#endif //  MATERIAL_OPENGL_EFFECT_FACTORY_H_
/******************************************************************************
|   End of File
*******************************************************************************/