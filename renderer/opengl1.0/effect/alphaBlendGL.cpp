/*
# alphaBlendOpenGL.cpp

- アルファブレンドクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "alphaBlend.h"

namespace material {
namespace effect {
// 定数
//--------------------------------------------------------------------
const AlphaBlendGL::BLEND AlphaBlendGL::SETTING[MAX_TYPE] =
{
    {GL_FALSE,GL_ONE                , GL_ZERO               }, // アルファブレンドなし
    {GL_TRUE ,GL_SRC_ALPHA          , GL_ONE_MINUS_SRC_ALPHA}, // アルファブレンド
    {GL_TRUE ,GL_SRC_ALPHA          , GL_ONE                }, // 加算
    {GL_TRUE ,GL_ONE                , GL_ZERO               }, // 減算(OpenGL1.1ではできない)
    {GL_TRUE ,GL_ZERO               , GL_SRC_COLOR          }, // 乗算
    {GL_TRUE ,GL_ONE_MINUS_DST_COLOR, GL_ZERO               }, // 反転
};

// グローバル変数
//--------------------------------------------------------------------
std::stack<AlphaBlendGL::BLEND> AlphaBlendGL::g_Stack;    // レンダーステートスタック

// @brief  : コンストラクタ
//--------------------------------------------------------------------
AlphaBlendGL::AlphaBlendGL(IDraw *_parent) :
    AlphaBlend(_parent)
{
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
AlphaBlendGL::~AlphaBlendGL()
{
}

// @brief  : 描画
//--------------------------------------------------------------------
void AlphaBlendGL::Draw(void)
{
    auto set = SETTING[Get()];
    SetBlend(set);
    g_Stack.push(set);
}

// @brief  : 描画
//--------------------------------------------------------------------
void AlphaBlendGL::LateDraw(void)
{
    SetBlend((g_Stack.empty()) ? g_Stack.top() : SETTING[TYPE_ALPHA]);
    g_Stack.pop();
}


// @brief  : アルファブレンドの取得
//--------------------------------------------------------------------
AlphaBlendGL::BLEND AlphaBlendGL::GetBlend(void)
{
    BLEND re = {0};
    //re.Enable = glIsEnabled(GL_BLEND);
    //glBlendFunc
    return re;
}

// @brief  : アルファブレンドのセット
// @param  : アルファブレンド設定
//--------------------------------------------------------------------
void AlphaBlendGL::SetBlend(const AlphaBlendGL::BLEND &_set)
{
    if(_set.Enable)
    {
        glEnable(GL_BLEND);
        glBlendFunc(_set.Src,_set.Dest);
    }
    else{glDisable(GL_BLEND);}
}



}; //namespace effect
}; //namespace material
/******************************************************************************
|   End of File
*******************************************************************************/