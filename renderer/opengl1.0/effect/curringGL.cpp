/*
# curringGL.cpp

- カリングクラス(OpenGL)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード -------------------------------------------------------------
#include "curring.h"

namespace material {
namespace effect {
// @brief  : コンストラクタ
//--------------------------------------------------------------------
CurringGL::CurringGL(IDraw *_parent) :
    Curring(_parent)
{
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
CurringGL::~CurringGL()
{
}

// @brief  : アルファブレンド変更
// @param  : 種類
//--------------------------------------------------------------------
void CurringGL::Set(TYPE _type)
{
    const GLenum setting[MAX_TYPE] =
    {
        GL_NONE,
        GL_CW,
        GL_CCW,
    };
    m_Setting = setting[_type];
}

// @brief  : 描画
//--------------------------------------------------------------------
void CurringGL::Draw(void)
{
    if(m_Setting == GL_NONE)
    {
        glDisable(GL_CULL_FACE);
        return;
    }

    glEnable(GL_CULL_FACE);
    glFrontFace(m_Setting);
}

// @brief  : 描画
//--------------------------------------------------------------------
void CurringGL::LateDraw(void)
{
    glEnable(GL_CULL_FACE);
    glFrontFace(GL_CW);
}


}; //namespace effect
}; //namespace material
/******************************************************************************
|   End of File
*******************************************************************************/