/*
# alphaBlend.h

- アルファブレンドクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef MATERIAL_OPENGL_EFFECT_ALPHABLEND_H_
#define MATERIAL_OPENGL_EFFECT_ALPHABLEND_H_

#include <stack>
#include "../../effect/alphaBlend.h"
#include "../opengl1.0.h"

namespace material {
namespace effect {
class FactoryGL;

class AlphaBlendGL : public AlphaBlend
{
public:
    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    AlphaBlendGL(IDraw *);

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~AlphaBlendGL();

private:
    // 構造体
    //--------------------------------------------------------------------
    struct BLEND
    {
        GLboolean Enable;
        GLenum    Src;
        GLenum    Dest;
    };

    // 定数
    //--------------------------------------------------------------------
    static const BLEND SETTING[MAX_TYPE];

    // グローバル変数
    //--------------------------------------------------------------------
    static std::stack<BLEND> g_Stack;    // レンダーステートスタック

    // @brief  : 描画
    //--------------------------------------------------------------------
    void Draw(void) override;

    // @brief  : 描画
    //--------------------------------------------------------------------
    void LateDraw(void) override;

    // @brief  : アルファブレンドの取得
    //--------------------------------------------------------------------
    static BLEND GetBlend(void);

    // @brief  : アルファブレンドのセット
    // @param  : アルファブレンド設定
    //--------------------------------------------------------------------
    static void SetBlend(const BLEND &);
};


}; //namespace effect
}; //namespace material
#endif //  MATERIAL_OPENGL_EFFECT_ALPHABLEND_H_
/******************************************************************************
|   End of File
*******************************************************************************/