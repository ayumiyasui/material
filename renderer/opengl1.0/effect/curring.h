/*
# curring.h

- カリングクラス(OpenGL)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef MATERIAL_RENDERER_EFFECT_CURRING_GL_H_
#define MATERIAL_RENDERER_EFFECT_CURRING_GL_H_

#include <stack>
#include "../opengl1.0.h"
#include "../../effect/curring.h"

namespace material {
namespace effect {
class CurringGL : public Curring
{
public:
    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    CurringGL(IDraw *);

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~CurringGL();

    // @brief  : アルファブレンド変更
    // @param  : 種類
    //--------------------------------------------------------------------
    void Set(TYPE _type);

private:
    // メンバ変数
    //--------------------------------------------------------------------
    GLenum m_Setting;  // カリング設定

    // @brief  : 描画
    //--------------------------------------------------------------------
    void Draw(void) override;

    // @brief  : 描画
    //--------------------------------------------------------------------
    void LateDraw(void) override;
};


}; //namespace effect
}; //namespace material
#endif //  MATERIAL_RENDERER_EFFECT_CURRING_GL_H_
/******************************************************************************
|   End of File
*******************************************************************************/