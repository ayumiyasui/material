/*
# directionalLight.h

- ライトクラス(3D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "directionalLightGL.h"

#include <assert.h>
#include "opengl1.0.h"


NAMESPACE_3D

// 定数
//--------------------------------------------------------------------
const GLenum DirectionalLightGL::GL_LIGHT_DEF[8] =
{
    GL_LIGHT0,
    GL_LIGHT1,
    GL_LIGHT2,
    GL_LIGHT3,
    GL_LIGHT4,
    GL_LIGHT5,
    GL_LIGHT6,
    GL_LIGHT7
};

// グローバル変数
//--------------------------------------------------------------------
bool DirectionalLightGL::g_UseLightIndex[8] = { false };

// @brief  : コンストラクタ
// @param  : 親クラス
//         : 描画するのか？
//--------------------------------------------------------------------
DirectionalLightGL::DirectionalLightGL( IDraw *_parent, bool _is_draw ) :
    DirectionalLight(_parent,_is_draw)
{
    for( auto i = 0 ; i < 8 ; ++i )
    {
        if(!g_UseLightIndex[i])
        {
            m_Index = GL_LIGHT_DEF[i];
            Set();
            (_is_draw) ? OnEnable() : OnDisable();
            return;
        }
    }

    assert(false);
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
DirectionalLightGL::~DirectionalLightGL()
{
    
}

// @brief  : ライトのセット
//--------------------------------------------------------------------
void DirectionalLightGL::Set( void )
{
    // ライトの色
    const Color col_dif = Diffuse;
    const float diffuse[4] = 
    {
        col_dif.r,
        col_dif.g,
        col_dif.b,
        col_dif.a
    };

    const Color col_spe = Specular;
    const float specular[4] = 
    {
        col_spe.r,
        col_spe.g,
        col_spe.b,
        col_spe.a
    };

    const Color col_amb = Ambient;
    const float ambient[4] = 
    {
        col_amb.r,
        col_amb.g,
        col_amb.b,
        col_amb.a
    };

    // ライトの位置をセット
    glLightfv( m_Index, GL_DIFFUSE,  diffuse );
    glLightfv( m_Index, GL_SPECULAR, specular );
    glLightfv( m_Index, GL_AMBIENT,  ambient );
}

// @brief  : 描画が有効のときの挙動
//--------------------------------------------------------------------
void DirectionalLightGL::OnEnable( void )
{
    glEnable(m_Index);
}

// @brief  : 描画が無効のときの挙動
//--------------------------------------------------------------------
void DirectionalLightGL::OnDisable( void )
{
    glDisable(m_Index);
}

// @brief  : 描画
//--------------------------------------------------------------------
void DirectionalLightGL::Draw( void )
{
    // ライトの位置
    const Vector3 pos_v = Direction;
    const float pos[4] =
    {
        -pos_v.x,
        -pos_v.y,
        -pos_v.z,
        0.0f
    };

    // ライトの位置をセット
    glLightfv( m_Index, GL_POSITION, pos );
}



END_NAMESPACE_3D
/******************************************************************************
|   End of File
*******************************************************************************/