/*
# camera2dGL.cpp

- カメラクラス(2D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "camera2dGL.h"

#include "opengl1.0.h"


NAMESPACE_2D
// @brief  : コンストラクタ
// @param  : 親クラス
//         : 描画するのか？
//--------------------------------------------------------------------
CameraGL::CameraGL( IDraw *_parent, bool _is_draw ) :
    Camera(_parent,_is_draw)
{
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
CameraGL::~CameraGL()
{
}

// @brief  : 描画
//--------------------------------------------------------------------
void CameraGL::Draw( void )
{
    // ライティングの無効
    glDisable( GL_LIGHTING );

    // 深度バッファの無効
    glDisable( GL_DEPTH_TEST );

    // クリア
    glClear( GL_DEPTH_BUFFER_BIT );

    // プロジェクションマトリックスの設定
    glMatrixMode( GL_PROJECTION );
    glPushMatrix();                     // スタック領域に入れる
    glLoadIdentity();                   // 単位行列の設定
    glOrtho( Size->left, Size->right, Size->bottom, Size->top, Near, Far );    // 正射影行列

    // ワールドマトリックスとビューマトリックス
    glMatrixMode( GL_MODELVIEW );
    glLoadIdentity();

    // ビューポート
    glViewport( (GLint)Viewport->left, (GLint)Viewport->top, (GLint)Viewport->right, (GLint)Viewport->bottom );
}

// @brief  : 描画
// @note   : 子が終わった後に動く、後片付け用
//--------------------------------------------------------------------
void CameraGL::LateDraw( void )
{
    glMatrixMode( GL_MODELVIEW );
    glPopMatrix();
    glMatrixMode( GL_PROJECTION );
    glPopMatrix();
    glEnable( GL_DEPTH_TEST );
    glEnable( GL_LIGHTING );
}



END_NAMESPACE_2D
/******************************************************************************
|   End of File
*******************************************************************************/