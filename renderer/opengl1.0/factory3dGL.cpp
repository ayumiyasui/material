/*
# factory3dGL.h

- ファクトリクラス(3D)
- 機種・ライブラリ差の吸収のために作成

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "factory3dGL.h"

#include "textureManagerGL.h"
#include "camera3dGL.h"
#include "directionalLightGL.h"
#include "transform3dGL.h"
#include "transformQuat3dGL.h"
#include "transLookCameraGL.h"

#include "plateGL.h"
#include "PMDNoBoneManagerGL.h"
#include "skyGL.h"


NAMESPACE_3D
// @brief  : コンストラクタ
//--------------------------------------------------------------------
FactoryGL::FactoryGL()
{
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
FactoryGL::~FactoryGL()
{
}

// @brief  : カメラクラスの作成
//--------------------------------------------------------------------
Camera *FactoryGL::CreateCamera( IDraw *_parent, bool _is_draw ) const
{
    return new CameraGL(_parent,_is_draw);
}

// @brief  : ライトクラスの作成
//--------------------------------------------------------------------
DirectionalLight *FactoryGL::CreateDirLight( IDraw *_parent, bool _is_draw ) const
{
    return new DirectionalLightGL(_parent,_is_draw);
}

// @brief  : Transformの作成
//--------------------------------------------------------------------
Transform *FactoryGL::CreateTransform( IDraw *_parent, bool _is_draw ) const
{
    return new TransformGL(_parent,_is_draw);
}
TransformQuat *FactoryGL::CreateTransformQuat( IDraw *_parent, bool _is_draw ) const
{
    return new TransformQuatGL(_parent,_is_draw);
}
TransLookCamera *FactoryGL::CreateTransformLookCamera( Camera *_cam, IDraw *_parent, bool _is_draw ) const
{
    return new TransLookCameraGL(_cam,_parent,_is_draw);
}

// @brief  : 板の作成
// @param  : テクスチャ名
//         : 大きさ
//          :親クラス
//         : 描画するのか？
//--------------------------------------------------------------------
Plate *FactoryGL::CreatePlate( const std::string &_file_name, const Quad &_size, IDraw *_parent, bool _is_draw )
{
    PlateGL *create = new PlateGL(_size,_parent,_is_draw);
    create->Tex = TextureManagerGL::Instance().Load(_file_name);
    return create;
}

// @brief  : 空の作成
// @param  : テクスチャ名
//         : 半径
//         : ブロック数横
//         : ブロック数縦
//          :親クラス
//         : 描画するのか？
//--------------------------------------------------------------------
Sky *FactoryGL::CreateSky( const std::string &_file_name,
    float _length, unsigned int _x, unsigned int _y,
    IDraw *_parent, bool _is_draw ) const
{
    return new SkyGL(TextureManagerGL::Instance().Load(_file_name),_length,_x,_y,_parent,_is_draw);
}

// @brief  : XFileの作成
//--------------------------------------------------------------------
XFile *FactoryGL::CreateXFile( IDraw *_parent, bool _is_draw, const std::string &_file_name ) const
{
    return nullptr;
}

// @brief  : PMDの作成(骨なし)
//--------------------------------------------------------------------
PMDNoBone *FactoryGL::CreatePMDNoBone( const std::string &_file_name, IDraw *_parent, bool _is_draw ) const
{
    return PMDNoBoneManagerGL::Instance().Load(_parent,_is_draw,_file_name);
}



END_NAMESPACE_3D
/******************************************************************************
|   End of File
*******************************************************************************/