/*
 # openGL.mm
 
 - レンダラーGLクラス
 - 参考： http://d.hatena.ne.jp/white_wheels/20110820/p5
 
 @author : Ayumi Yasui
 *-----------------------------------------------------------------------------*/

//--- インクルード -------------------------------------------------------------
#include "opengl1.0.h"

#include "../../app/app.h"
#include "../draw.h"
#include "textureManagerGL.h"
#include "PMDNoBoneManagerGL.h"
#include "factory2dGL.h"
#include "factory3dGL.h"
#include "effect/factory.h"
#include "vertex/factory.h"

//--- クラス宣言 ---------------------------------------------------------------


// @brief  : コンストラクタ
// @param  : スクリーンサイズ
//--------------------------------------------------------------------
OpenGL::OpenGL( float _width, float _height ) :
    IRenderer(_width,_height, new _2d::FactoryGL, new _3d::FactoryGL, new effect::FactoryGL, new vertex::FactoryGL)
{
    NSRect frame_Rectangle = NSMakeRect(0, 0, _width, _height);
    
    // OpenGLViewの作成
    m_OpenGLView = [[MyOpenGLView alloc]initWithFrame:frame_Rectangle];
    
    // OpenGLViewの設定
    AppDelegate *app_delegate = App::Instance().Delegate();
    [app_delegate.Window setContentView:m_OpenGLView];
    [app_delegate.Window makeFirstResponder:m_OpenGLView];

    // 深度バッファを入れる
    glEnable(GL_DEPTH_TEST);
    
    // アルファのセット
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
    
    // カリング
    glEnable(GL_CULL_FACE);
    glFrontFace(GL_CW);
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
OpenGL::~OpenGL()
{
}

// @brief  : 描画
//--------------------------------------------------------------------
void OpenGL::Draw( void )
{
    // コンテキストの切り替え
    [[m_OpenGLView openGLContext] makeCurrentContext ];
    
    // バッファのクリア
    const Color back_color = BackColor;
    glClearColor( back_color.r, back_color.g, back_color.b, back_color.a );
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT );

    // 描画
    TempNodeSingleton<IDraw>::Instance().DrawAll();
    glFinish();
    
    //バッファ入れ替え
    [[m_OpenGLView openGLContext] flushBuffer];

#ifdef _DEBUG
    ResetDebugString();
#endif
}


//--- クラス宣言 ---------------------------------------------------------------
@implementation MyOpenGLView

- (id)initWithFrame:(NSRect)frame_Rectangle
{
    // ピクセルフォーマットの作成
    NSOpenGLPixelFormatAttribute attr[]= {
        NSOpenGLPFADoubleBuffer,
        NSOpenGLPFAColorSize,   32,
        NSOpenGLPFAAlphaSize,   8,
        NSOpenGLPFADepthSize,   24,
        NSOpenGLPFAStencilSize, 8,
        0
    };
    NSOpenGLPixelFormat *pixelFormat = [[NSOpenGLPixelFormat alloc] initWithAttributes:attr];
    
    // ピクセルフォーマットの設定
    self = [super initWithFrame:frame_Rectangle pixelFormat:pixelFormat];
    
    // コンテキストを有効にする
    auto glContext = [self openGLContext];
    [glContext makeCurrentContext];     
    
    return self;
}

@end



/******************************************************************************
 |   End of File
 *******************************************************************************/