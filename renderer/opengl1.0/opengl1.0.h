/*
# openGL1.0.h

- レンダラーGLクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __OPENGL_H__
#define __OPENGL_H__

#if _WIN32
#include "../../app/win32.h"
#include <gl/GL.h>
#include <gl/GLU.h>
#include "../../app/win32Macro.h"
#pragma comment ( lib, "opengl32.lib" )
#pragma comment ( lib, "glu32.lib" )
#endif

#ifdef __APPLE__
#include "../../app/OSXMacro.h"
#include "../../app/app.h"
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#ifdef   __OBJC__
@interface MyOpenGLView : NSOpenGLView
{
}

- (id)initWithFrame:(NSRect)frame_rect;
@end

#else

class MyOpenGLView;

#endif // __OBJC__

#endif

#include "../renderer.h"
#include "../../base/tempSingleton.h"
#include "../../app/app.h"



class OpenGL : public IRenderer, public TempSingleton<OpenGL>
{
public:
    // @brief  : 描画
    //--------------------------------------------------------------------
    void Draw( void ) override;

    // @brief  : スクリーンショット
    // @param  : スクリーンショット名
    //--------------------------------------------------------------------
    void ScreenShort( const std::string & ) const override{};

private:
    // フレンドクラス
    //--------------------------------------------------------------------
    friend TempSingleton<OpenGL>;

    // メンバ変数
    //--------------------------------------------------------------------
#if _WIN32
    HWND  m_Window; // ウィンドウハンドル
    HDC   m_DC;     // デバイスコンテキスト
    HGLRC m_GLRC;   // レンダリングコンテキスト
#ifdef _DEBUG
    HFONT m_DebugFont;  // デバックフォント
#endif // _DEBUG
#endif // _WIN32
#ifdef     __APPLE__
    MyOpenGLView *m_OpenGLView;
#endif // __APPLE__

#if _WIN32
    // @brief  : コンストラクタ
    // @param  : ウィンドウハンドル
    //--------------------------------------------------------------------
    OpenGL( float _width = SCREEN_WIDTH, float _height = SCREEN_HEIGHT, HWND _window = App::Instance().Win32->GetHWnd() );
#endif

#if __APPLE__
    // @brief  : コンストラクタ
    // @param  : スクリーンサイズ
    //--------------------------------------------------------------------
    OpenGL( float _width = SCREEN_WIDTH, float _height = SCREEN_HEIGHT );
#endif
    
    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~OpenGL();
};



#endif // __OPENGL_H__
/******************************************************************************
|   End of File
*******************************************************************************/