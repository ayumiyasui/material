/*
# PMDNoBoneManagerGL.cpp

- PMDクラス骨なし(3D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#define _CRT_SECURE_NO_WARNINGS
#include "PMDNoBoneManagerGL.h"

#include <stdio.h>
#include "../../base/node.h"
#include "../../base/tempUseCount.h"
#include "../../base/tempSmartPtr.h"
#include "../../base/color.h"
#include "../../math/vector3.h"
#include "../3d/mesh.h"
#include "../3d/material.h"
#include "../3d/PMDNobone.h"
#include "../resourceLoader.h"
#include "opengl1.0.h"
#include "textureManagerGL.h"

//--- データクラス -------------------------------------------------------------
class PMDDataGL : public TempUseCount<PMDDataGL>, public Node
{
public:
    struct MATERIAL
    {
        float diffuse[4];
        float specular[4];
        float ambient[4];
        Texture      tex;
        unsigned int start_index;
        unsigned int max_index;
    };
    struct VERTEX
    {
        float pos[3];
        float nor[3];
        float tex[2];
    };
    VERTEX         *m_VertexBuffer;
    unsigned int    m_MaxVertex;
    unsigned short *m_IndexBuffer;
    unsigned int    m_MaxIndex;
    MATERIAL       *m_Material;
    unsigned int    m_MaxMaterial;
    FilePass        m_FilePass;

    PMDDataGL( const std::string &_file_name ) :
      Node(&PMDNoBoneManagerGL::Instance()),
      m_FilePass(_file_name),
      m_VertexBuffer(nullptr),
      m_IndexBuffer(nullptr),
      m_Material(nullptr)
    {
        if(_file_name.empty()) return;

        PMDLoader::DATA *data = PMDLoader::Load(_file_name);
        if(!data) return;

        LoadMesh(data);
        LoadMaterial(data);
        delete[] data->vertex;
        delete[] data->face_vert_index;
        delete[] data->bone;
        delete[] data->material;
        delete data;
    };
    ~PMDDataGL()
    {
        if(m_Material)     delete[] m_Material;
        if(m_VertexBuffer) delete[] m_VertexBuffer;
        if(m_IndexBuffer)  delete[] m_IndexBuffer;
    };

private:
    void LoadMesh(const PMDLoader::DATA *_data)
    {
        // 頂点バッファ
        m_VertexBuffer = new VERTEX[_data->vert_count.u];
        m_MaxVertex    = _data->vert_count.u;
        for( unsigned int i = 0; i < m_MaxVertex ; ++i)
        {
            m_VertexBuffer[i].pos[0] = _data->vertex[i].pos[0].f;
            m_VertexBuffer[i].pos[1] = _data->vertex[i].pos[1].f;
            m_VertexBuffer[i].pos[2] = _data->vertex[i].pos[2].f;
            m_VertexBuffer[i].nor[0] = _data->vertex[i].normal_vec[0].f;
            m_VertexBuffer[i].nor[1] = _data->vertex[i].normal_vec[1].f;
            m_VertexBuffer[i].nor[2] = _data->vertex[i].normal_vec[2].f;
            m_VertexBuffer[i].tex[0] = _data->vertex[i].uv[0].f;
            m_VertexBuffer[i].tex[1] = 1.0f-_data->vertex[i].uv[1].f;
        }

        // インデックスバッファ
        m_IndexBuffer = new unsigned short[_data->face_vert_count.u];
        m_MaxIndex    = _data->face_vert_count.u;
        for( unsigned int i = 0; i < m_MaxIndex ; ++i)
        {
            m_IndexBuffer[i] = _data->face_vert_index[i].u;
        }
    };
    void LoadMaterial(const PMDLoader::DATA *_data)
    {
        //マテリアル情報
        m_Material = new MATERIAL[_data->material_count.u];
        m_MaxMaterial = _data->material_count.u;
        unsigned int start_index = 0;
        for( unsigned int i = 0 ; i < m_MaxMaterial ; ++i )
        {
            // インデックス頂点設定
            m_Material[i].start_index = start_index;
            m_Material[i].max_index   = _data->material[i].face_vert_count.u;
            start_index              += _data->material[i].face_vert_count.u;

            // マテリアル設定
            m_Material[i].diffuse[0]  = _data->material[i].diffuse_color[0].f;
            m_Material[i].diffuse[1]  = _data->material[i].diffuse_color[1].f;
            m_Material[i].diffuse[2]  = _data->material[i].diffuse_color[2].f;
            m_Material[i].diffuse[3]  = _data->material[i].alpha.f;
            m_Material[i].specular[0] = _data->material[i].specular_color[0].f;
            m_Material[i].specular[1] = _data->material[i].specular_color[1].f;
            m_Material[i].specular[2] = _data->material[i].specular_color[2].f;
            m_Material[i].specular[3] = _data->material[i].specularity.f;
            m_Material[i].ambient[0]  = _data->material[i].mirror_color[0].f;
            m_Material[i].ambient[1]  = _data->material[i].mirror_color[1].f;
            m_Material[i].ambient[2]  = _data->material[i].mirror_color[2].f;
            m_Material[i].ambient[3]  = 1.0f;

            // テクスチャ設定
            const std::string TEXTURE_PASS("data/TEXTURE/");
            std::string name = TEXTURE_PASS + _data->material[i].texture_file_name;
            m_Material[i].tex  = TextureManagerGL::Instance().Load(name.c_str());
        }
    };
};

//--- マテリアルクラス ----------------------------------------------------------
class PMDMaterialGL : public _3d::Material
{
public:
    PMDMaterialGL( PMDDataGL::MATERIAL *_mat, TempSmartPtr<PMDDataGL> _data,
        IDraw *_parent,bool _is_draw) :
        Material(_parent,_is_draw),
        m_Material(_mat),
        m_Data(_data)
    {
    };
    ~PMDMaterialGL(){};

private:
    PMDDataGL::MATERIAL    *m_Material;
    TempSmartPtr<PMDDataGL> m_Data;

    void Draw( void ) override
    {
        // テクスチャ設定
        m_Material->tex->Set();

        // マテリアル設定
        glMaterialfv(GL_FRONT_AND_BACK,GL_DIFFUSE,m_Material->diffuse);
        glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,m_Material->specular);
        glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT,m_Material->ambient);

        // 描画
        glDrawElements( GL_TRIANGLES ,     m_Material->max_index,
                        GL_UNSIGNED_SHORT , (const GLvoid *)&m_Data->m_IndexBuffer[m_Material->start_index] );

    };
    void LateDraw(void) override
    {
        static const float DIFFUSE[4] = {1.0f,1.0f,1.0f,1.0f};
        static const float OTHER[4]   = {0.0f,0.0f,0.0f,0.0f};
        glMaterialfv(GL_FRONT_AND_BACK,GL_DIFFUSE,DIFFUSE);
        glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,OTHER);
        glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT,OTHER);
    };
};

//--- メッシュクラス ------------------------------------------------------------
class PMDMeshGL : public _3d::Mesh
{
public:
    PMDMeshGL(TempSmartPtr<PMDDataGL> _data,
        IDraw *_parent, bool _is_draw) :
        Mesh(_parent,_is_draw),
        m_Data(_data)
    {
    };
    ~PMDMeshGL(){};

private:
    TempSmartPtr<PMDDataGL> m_Data;

    void Draw(void) override
    {
        // カリング
        glEnable(GL_CULL_FACE);
        glFrontFace(GL_CCW);

        glEnableClientState( GL_VERTEX_ARRAY );
        glEnableClientState( GL_NORMAL_ARRAY );
        glEnableClientState( GL_TEXTURE_COORD_ARRAY );

        static const GLsizei VERTEX_SIZE = sizeof( PMDDataGL::VERTEX );
        glVertexPointer(  3, GL_FLOAT, VERTEX_SIZE, (const GLvoid *)&m_Data->m_VertexBuffer[0].pos[0] );
        glNormalPointer(     GL_FLOAT, VERTEX_SIZE, (const GLvoid *)&m_Data->m_VertexBuffer[0].nor[0] );
        glTexCoordPointer(2, GL_FLOAT, VERTEX_SIZE, (const GLvoid *)&m_Data->m_VertexBuffer[0].tex[0] );
    };
    void LateDraw(void) override
    {
        glDisableClientState( GL_VERTEX_ARRAY );
        glDisableClientState( GL_NORMAL_ARRAY );
        glDisableClientState( GL_TEXTURE_COORD_ARRAY );

        // カリング
        glEnable(GL_CULL_FACE);
        glFrontFace(GL_CW);
    };
};


//--- モデルクラス --------------------------------------------------------------
class PMDNoBoneGL : public _3d::PMDNoBone
{
public:
    PMDNoBoneGL(PMDDataGL *_data,IDraw *_parent,bool _is_draw) :
        PMDNoBone(_data->m_MaxVertex,_data->m_MaxIndex,_parent,_is_draw),
        m_Data(_data)
    {
        m_Data->Retain();
    };

        // @brief  : 頂点 & インデックスの掃き出し
    // @note   : 吐き出された配列データは後でDeleteする必要があります
    //--------------------------------------------------------------------
    Vector3 *CreateVertexAndIndex(void) const override
    {
        Vector3 *re = new Vector3[MAX_INDEX];

        const PMDDataGL::VERTEX *vtx   = m_Data->m_VertexBuffer;
        const unsigned short    *index = m_Data->m_IndexBuffer;
        for( unsigned int i = 0 ; i < MAX_INDEX ; ++i)
        {
            re[i].x = vtx[index[i]].pos[0];
            re[i].y = vtx[index[i]].pos[1];
            re[i].z = vtx[index[i]].pos[2];
        }

        return re;
    };
private:
    PMDDataGL *m_Data;
    ~PMDNoBoneGL()
    {
        m_Data->Release();
    };
};

// @brief  : コンストラクタ
//--------------------------------------------------------------------
PMDNoBoneManagerGL::PMDNoBoneManagerGL()
{
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
PMDNoBoneManagerGL::~PMDNoBoneManagerGL()
{
}

// @brief  : PMDファイルの取り出し
// @param  : 親クラス
//         : 描画するのか？
//         : ファイル名
//--------------------------------------------------------------------
_3d::PMDNoBone *PMDNoBoneManagerGL::Load( IDraw *_parent, bool _is_draw, const std::string &_file_name )
{
    assert(!_file_name.empty());

    // ファイルの検索
    PMDDataGL *data = nullptr;
    Node *i = Begin();
    while(i)
    {
        data = dynamic_cast<PMDDataGL *>(i);
        std::string name = data->m_FilePass;
        if( name.compare(_file_name) == 0 ) break;
        i = i->Next();
        data = nullptr;
    }

    // ファイルの作成
    if(!data)
    {
        data = new PMDDataGL(_file_name);
    }

    // モデルの作成
    PMDNoBoneGL *re = new PMDNoBoneGL(data,_parent,_is_draw);
    PMDMeshGL *mesh = new PMDMeshGL(data,re,true);
    for(unsigned i = 0 ; i < data->m_MaxMaterial ; ++i)
    {
        new PMDMaterialGL(&data->m_Material[i],data,mesh,true);
    }

    // 作成
    return re;
}


/******************************************************************************
|   End of File
*******************************************************************************/