/*
# transformGL.h

- 変換行列設定クラス(2D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __2D_TRANSFORM_GL_H__
#define __2D_TRANSFORM_GL_H__

#include "opengl1.0.h"
#include "../2d/transfrom.h"

NAMESPACE_2D

class FactoryGL;

class TransformGL : public Transform
{
private:
    // フレンドクラス
    //--------------------------------------------------------------------
    friend FactoryGL;

    // @brief  : コンストラクタ
    // @param  : 親クラス
    //         : 描画するのか？
    //--------------------------------------------------------------------
    TransformGL( IDraw *, bool );

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~TransformGL();

    // @brief  : 描画
    //--------------------------------------------------------------------
    void Draw( void ) override;

    // @brief  : 描画
    // @note   : 子が終わった後に動く、後片付け用
    //--------------------------------------------------------------------
    void LateDraw( void ) override;
};



END_NAMESPACE_2D
#endif //  __2D_TRANSFORM_GL_H__
/******************************************************************************
|   End of File
*******************************************************************************/