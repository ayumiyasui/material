/*
# transLookCameraGL.h

- 行列変換クラス(3D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __3D_TRANS_LOOKCAMERA_GL_H__
#define __3D_TRANS_LOOKCAMERA_GL_H__

#include "opengl1.0.h"
#include "../3d/transLookCamera3d.h"

NAMESPACE_3D

    class FactoryGL;

class TransLookCameraGL : public TransLookCamera
{
private:
    // フレンドクラス
    //--------------------------------------------------------------------
    friend FactoryGL;

    // @brief  : コンストラクタ
    // @param  : カメラ
    //         : 親クラス
    //         : 描画するのか？
    //--------------------------------------------------------------------
    TransLookCameraGL( Camera *, IDraw *, bool );

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~TransLookCameraGL();

    // @brief  : 描画
    //--------------------------------------------------------------------
    void Draw( void ) override;

    // @brief  : 描画(後始末)
    //--------------------------------------------------------------------
    void LateDraw( void ) override;
};



END_NAMESPACE_3D
#endif //  __3D_TRANS_LOOKCAMERA_GL_H__
/******************************************************************************
|   End of File
*******************************************************************************/