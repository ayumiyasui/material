/*
# vertexFactoryGL.cpp

- 頂点バッファ生成クラス(OpenGL)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "factory.h"
#include "vertex3dCol.h"

namespace material {
namespace vertex {

// @brief  : コンストラクタ
//--------------------------------------------------------------------
FactoryGL::FactoryGL()
{
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
FactoryGL::~FactoryGL()
{
}

// @brief  : カラー付き頂点の作成
// @param  : 頂点数
//--------------------------------------------------------------------
vertex::IVertex3dCol *FactoryGL::Create(const variable::u32 &_count) const
{
    return new Vertex3dColGL(_count);
}


};  // namespace vertex
};  // namespace material
/******************************************************************************
|   End of File
*******************************************************************************/