/*
# factory.h

- 頂点バッファ生成クラス(OpenGL)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef MATERIAL_VERTEX_FACTORY_GL_H_
#define MATERIAL_VERTEX_FACTORY_GL_H_

#include "../../vertex/factory.h"

namespace material {
namespace vertex {

class FactoryGL : public Factory
{
public:
    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    FactoryGL();

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~FactoryGL();

    // @brief  : カラー付き頂点の作成
    // @param  : 頂点数
    //--------------------------------------------------------------------
    vertex::IVertex3dCol *Create(const variable::u32 &_count) const override;
};


};  // namespace vertex
};  // namespace material
#endif //  MATERIAL_VERTEX_FACTORY_GL_H_
/******************************************************************************
|   End of File
*******************************************************************************/