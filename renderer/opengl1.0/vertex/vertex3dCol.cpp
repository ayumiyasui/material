/*
# vertex3dCol.cpp

- 頂点クラス(カラー付きVertex3d : OpenGL)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード -------------------------------------------------------------
#include "vertex3dCol.h"

namespace material {
namespace vertex {

// @brief  : コンストラクタ
// @param  : 頂点数
//--------------------------------------------------------------------
Vertex3dColGL::Vertex3dColGL(const variable::u32 & _count) :
    IVertex3dCol(_count),
    m_Buffer(new VERTEX_OPENGL[_count])
{
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
Vertex3dColGL::~Vertex3dColGL()
{
    delete[] m_Buffer;
}

// @brief  : すべての頂点の編集
// @param  : 関数(param:編集する頂点情報,頂点ナンバー)
//--------------------------------------------------------------------
void Vertex3dColGL::Edit(std::function<void (VERTEX &,const variable::u32 &)> _func)
{
    for(variable::u32 i = 0 ; i < Count ; ++i)
    {
        VERTEX vertex =
        {
            Vector3(m_Buffer[i].pos[0],m_Buffer[i].pos[1],m_Buffer[i].pos[2]),
            Vector3(m_Buffer[i].nor[0],m_Buffer[i].nor[1],m_Buffer[i].nor[2]),
            Color(m_Buffer[i].col[0],m_Buffer[i].col[1],m_Buffer[i].col[2],m_Buffer[i].col[3]),
            Vector2(m_Buffer[i].tex[0],m_Buffer[i].tex[1])
        };
        _func(vertex,i);
        m_Buffer[i].pos[0] = vertex.pos.x;
        m_Buffer[i].pos[1] = vertex.pos.y;
        m_Buffer[i].pos[2] = vertex.pos.z;
        m_Buffer[i].nor[0] = vertex.nor.x;
        m_Buffer[i].nor[1] = vertex.nor.y;
        m_Buffer[i].nor[2] = vertex.nor.z;
        m_Buffer[i].col[0] = vertex.col.r;
        m_Buffer[i].col[1] = vertex.col.g;
        m_Buffer[i].col[2] = vertex.col.b;
        m_Buffer[i].col[3] = vertex.col.a;
        m_Buffer[i].tex[0] = vertex.tex.x;
        m_Buffer[i].tex[1] = vertex.tex.y;
    }
}

// @brief  : 頂点の編集
// @param  : 関数(param:頂点ナンバー return 設定する頂点)
//--------------------------------------------------------------------
void Vertex3dColGL::EditPos(std::function<Vector3 (const Vector3 &,const variable::u32 &)> _func)
{
    for(variable::u32 i = 0 ; i < Count ; ++i)
    {
        Vector3 vec(m_Buffer[i].pos[0],m_Buffer[i].pos[1],m_Buffer[i].pos[2]);
        auto vertex = _func(vec,i);
        m_Buffer[i].pos[0] = vertex.x;
        m_Buffer[i].pos[1] = vertex.y;
        m_Buffer[i].pos[2] = vertex.z;
    }
}

// @brief  : 法線の編集
// @param  : 関数(param:頂点ナンバー return 設定する色)
//--------------------------------------------------------------------
void Vertex3dColGL::EditNor(std::function<Vector3 (const Vector3 &,const variable::u32 &)> _func)
{
    for(variable::u32 i = 0 ; i < Count ; ++i)
    {
        Vector3 vec(m_Buffer[i].nor[0],m_Buffer[i].nor[1],m_Buffer[i].nor[2]);
        auto vertex = _func(vec,i);
        m_Buffer[i].nor[0] = vertex.x;
        m_Buffer[i].nor[1] = vertex.y;
        m_Buffer[i].nor[2] = vertex.z;
    }
}

// @brief  : カラーの編集
// @param  : 関数(param:頂点ナンバー return 設定する色)
//--------------------------------------------------------------------
void Vertex3dColGL::EditCol(std::function<Color (const Color &,const variable::u32 &)> _func)
{
    for(variable::u32 i = 0 ; i < Count ; ++i)
    {
        Color col(m_Buffer[i].col[0],m_Buffer[i].col[1],m_Buffer[i].col[2],m_Buffer[i].col[3]);
        auto vertex = _func(col,i);
        m_Buffer[i].col[0] = vertex.r;
        m_Buffer[i].col[1] = vertex.g;
        m_Buffer[i].col[2] = vertex.b;
        m_Buffer[i].col[3] = vertex.a;
    }
}

// @brief  : テクスチャ座標編集
// @param  : 関数(param:頂点ナンバー return 設定する頂点)
//--------------------------------------------------------------------
void Vertex3dColGL::EditTex(std::function<Vector2 (const Vector2 &,const variable::u32 &)> _func)
{
    for(variable::u32 i = 0 ; i < Count ; ++i)
    {
        Vector2 vec(m_Buffer[i].tex[0],m_Buffer[i].tex[1]);
        auto vertex = _func(vec,i);
        m_Buffer[i].tex[0] = vertex.x;
        m_Buffer[i].tex[1] = vertex.y;
    }
}

// @brief  : すべての頂点の取り出し
// @return : 頂点(注：使い終わったらdelete[]すること)
//--------------------------------------------------------------------
vertex::IVertex3dCol::VERTEX *Vertex3dColGL::Export(void) const
{
    auto *re = new VERTEX[Count];
    for(variable::u32 i = 0 ; i < Count ; ++i)
    {
        re[i].pos.x = m_Buffer[i].pos[0];
        re[i].pos.y = m_Buffer[i].pos[1];
        re[i].pos.z = m_Buffer[i].pos[2];
        re[i].nor.x = m_Buffer[i].nor[0];
        re[i].nor.y = m_Buffer[i].nor[1];
        re[i].nor.z = m_Buffer[i].nor[2];
        re[i].col.r = m_Buffer[i].col[0];
        re[i].col.g = m_Buffer[i].col[1];
        re[i].col.b = m_Buffer[i].col[2];
        re[i].col.a = m_Buffer[i].col[3];
        re[i].tex.x = m_Buffer[i].tex[0];
        re[i].tex.y = m_Buffer[i].tex[1];
    }
    return re;
}

// @brief  : 頂点の取り出し
// @return : 頂点(注：使い終わったらdelete[]すること)
//--------------------------------------------------------------------
Vector3 *Vertex3dColGL::ExportPos(void) const
{
    auto *re = new Vector3[Count];
    for(variable::u32 i = 0 ; i < Count ; ++i)
    {
        re[i].x = m_Buffer[i].pos[0];
        re[i].y = m_Buffer[i].pos[1];
        re[i].z = m_Buffer[i].pos[2];
    }
    return re;
}

// @brief  : 法線の取り出し
// @return : 法線(注：使い終わったらdelete[]すること)
//--------------------------------------------------------------------
Vector3 *Vertex3dColGL::ExportNor(void) const
{
    auto *re = new Vector3[Count];
    for(variable::u32 i = 0 ; i < Count ; ++i)
    {
        re[i].x = m_Buffer[i].nor[0];
        re[i].y = m_Buffer[i].nor[1];
        re[i].z = m_Buffer[i].nor[2];
    }
    return re;
}

// @brief  : カラーの取り出し
// @return : カラー(注：使い終わったらdelete[]すること)
//--------------------------------------------------------------------
Color *Vertex3dColGL::ExportCol(void) const
{
    auto *re = new Color[Count];
    for(variable::u32 i = 0 ; i < Count ; ++i)
    {
        re[i].r = m_Buffer[i].col[0];
        re[i].g = m_Buffer[i].col[1];
        re[i].b = m_Buffer[i].col[2];
        re[i].a = m_Buffer[i].col[3];
    }
    return re;
}

// @brief  : テクスチャ座標の取り出し
// @return : テクスチャ座標(注：使い終わったらdelete[]すること)
//--------------------------------------------------------------------
Vector2 *Vertex3dColGL::ExportTex(void) const
{
    auto *re = new Vector2[Count];
    for(variable::u32 i = 0 ; i < Count ; ++i)
    {
        re[i].x = m_Buffer[i].tex[0];
        re[i].y = m_Buffer[i].tex[1];
    }
    return re;
}

// @brief  : バッファの取得
//--------------------------------------------------------------------
Vertex3dColGL::VERTEX_OPENGL *Vertex3dColGL::GetBuffer(void) const {return m_Buffer;}

// @brief  : 描画
// @param  : 描画の種類
//--------------------------------------------------------------------
void Vertex3dColGL::Draw(const TYPE &_type)
{
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    {
        const auto vertex_size = sizeof(VERTEX_OPENGL);
        glVertexPointer(  3, GL_FLOAT, vertex_size, (const GLvoid *)&m_Buffer[0].pos[0] );
        glNormalPointer(     GL_FLOAT, vertex_size, (const GLvoid *)&m_Buffer[0].nor[0] );
        glColorPointer(   4, GL_FLOAT, vertex_size, (const GLvoid *)&m_Buffer[0].col[0] );
        glTexCoordPointer(2, GL_FLOAT, vertex_size, (const GLvoid *)&m_Buffer[0].tex[0] );

        const GLenum mode[MAX_TYPE] =
        {
            GL_POINTS,
            GL_LINES,
            GL_LINE_STRIP,
            GL_TRIANGLES,
            GL_TRIANGLE_STRIP
        };
        glDrawArrays(mode[_type],0,Count);
    }
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glDisableClientState(GL_COLOR_ARRAY);
    glDisableClientState(GL_NORMAL_ARRAY);
    glDisableClientState(GL_VERTEX_ARRAY);
}


};  // namespace vertex
};  // namespace material
/******************************************************************************
|   End of File
*******************************************************************************/