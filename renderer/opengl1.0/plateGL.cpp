/*
# plate.h

- 板クラス(3D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "plateGL.h"

#include "materialGL.h"


NAMESPACE_3D
// @brief  : コンストラクタ
// @param  : 板サイズ
//         : 親クラス
//         : 描画するのか？
//--------------------------------------------------------------------
PlateGL::PlateGL( const Quad &_size, IDraw *_parent, bool _is_draw ) :
    Plate(new MaterialNormalGL(&TempNodeSingleton<IDraw>::Instance(),true),_size,_parent,_is_draw)
{
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
PlateGL::~PlateGL()
{
}

// @brief  : 頂点の変更
//--------------------------------------------------------------------
void PlateGL::SetVertex( void )
{
}

// @brief  : 描画
//--------------------------------------------------------------------
void PlateGL::LateDraw( void )
{
    static_cast<Texture>(Tex)->Set();
    const Quad  texUV = TexUV;
    const Quad  size  = Size;

    glBegin( GL_TRIANGLE_STRIP );
    {
        glNormal3f(0.0f,0.0f,1.0f);
        glTexCoord2f( texUV.left, texUV.bottom );
        glVertex3f( size.left, size.top, 0.0f );

        glNormal3f(0.0f,0.0f,1.0f);
        glTexCoord2f( texUV.left, texUV.top );
        glVertex3f( size.left, size.bottom, 0.0f );

        glNormal3f(0.0f,0.0f,1.0f);
        glTexCoord2f( texUV.right, texUV.bottom );
        glVertex3f( size.right, size.top, 0.0f );

        glNormal3f(0.0f,0.0f,1.0f);
        glTexCoord2f( texUV.right, texUV.top );
        glVertex3f( size.right, size.bottom, 0.0f );
    }
    glEnd();
}



END_NAMESPACE_3D
/******************************************************************************
|   End of File
*******************************************************************************/