/*
# tristMeshGL.h

- メッシュクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __TRISTMESH_GL_H__
#define __TRISTMESH_GL_H__

#include "opengl1.0.h"
#include "../3d/mesh.h"
#include "../../base/tempGetter.h"

NAMESPACE_3D

class TriStMeshGL : public Mesh
{
public:
    // 構造体
    //--------------------------------------------------------------------
    struct VERTEX
    {
        GLfloat pos[3];
        GLfloat nor[3];
        GLfloat tex[2];
    };

    // プロパティ
    //--------------------------------------------------------------------
    const unsigned int   BLOCK_X;       // 横ブロック数
    const unsigned int   BLOCK_Y;       // 縦ブロック数
    GLuint               MAX_VERTEX;    // 最大頂点数
    GLuint               MAX_INDEX;     // 最大インデックス数
    VERTEX *VertexBuffer(void);

    // @brief  : コンストラクタ
    // @param  : 横ブロック数
    //         : 縦ブロック数
    //         : 親クラス
    //         : 描画するのか？
    //--------------------------------------------------------------------
    TriStMeshGL( unsigned int, unsigned int, IDraw *, bool );

private:
    // メンバ変数
    //--------------------------------------------------------------------
    VERTEX  *m_VertexBuffer;    // 頂点バッファ
    GLuint *m_IndexBuffer;      // インデックスバッファ

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~TriStMeshGL();

    // @brief  : 描画
    //--------------------------------------------------------------------
    void Draw( void ) override;
};



END_NAMESPACE_3D
#endif //  __3D_PMD_NOBONE_H__
/******************************************************************************
|   End of File
*******************************************************************************/