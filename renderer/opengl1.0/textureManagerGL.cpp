/*
# textureManagerGL.h

- テクスチャマネージャクラス(OpenGL)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード ------------------------------------------------------------
#define _CRT_SECURE_NO_WARNINGS
#include "textureManagerGL.h"

#include "opengl1.0.h"

#pragma comment ( lib, "winmm.lib" )
#pragma comment ( lib, "opengl32.lib" )

// Nullテクスチャクラス
//--------------------------------------------------------------------
class NullTexture : public ITexture
{
public:
    NullTexture(Node *_parent) :
        ITexture(std::string(),_parent)
    {
    };

    void Set(void) override
    {
        glDisable( GL_TEXTURE_2D );
    };

private:
    ~NullTexture(){};
};


// TGAクラス
//--------------------------------------------------------------------
class TGA : public ITexture
{
public:
    TGA( const std::string &_name, Node *_parent ) :
        ITexture(_name,_parent)
    {
        if(_name.empty()) return;
        
        // ファイルのオープン
        FILE *file = fopen( _name.c_str(),"rb");
        if( file == NULL )
        {
            return;  // 失敗
        }

        // ヘッダ部分の読み込み
        GLubyte header[18];
        fread( header, sizeof(GLubyte), 18, file );

        // IDフィールドの長さ
        GLuint idFieldLength = header[0];

        // カラーマップタイプの有無
        GLuint colormapType  = header[1];

        // 画像タイプ
        GLuint type = header[2];

        // カラーマップ原点
        GLuint colormapOrigin = header[3] + header[4] * 256;

        // カラーマップの長さ
        GLuint colormapLength = header[5] + header[6] * 256;

        // カラーマップの深度
        GLuint colormapDepth  = header[7];

        // 画像の原点
        GLuint originX = header[8]  + header[9]  * 256;
        GLuint originY = header[10] + header[11] * 256;

        // 画像サイズを取得
        m_nWidth  = header[13] * 256 + header[12];
        m_nHeight = header[15] * 256 + header[14];

        // 画像ピクセルサイズ
        GLuint bpp = header[16];

        // 画像デスクリプタ
        GLuint descriptor = header[17];

        // 1ピクセルあたりのバイト数
        GLuint pxSize = bpp/8;

        // データサイズ
        GLuint dataSize = m_nWidth * m_nHeight * pxSize;

        // 画像データを格納する領域確保
        GLubyte *image = new GLubyte[dataSize];

        // 画像データの格納
        fread( image, sizeof(GLubyte) , dataSize , file );

        // BGA(A)をRGB(A)にコンバート
        for( int i = 0 ; i < (int)dataSize ; i += pxSize )
        {
            GLubyte temp = image[i+0];
            image[i+0]   = image[i+2];
            image[i+2]   = temp;
        }

        // ファイルを閉じる
        fclose( file );

        // テクスチャの名前を取得
        glGenTextures( 1, &m_nIdx );

        // 操作対象のテクスチャを設定する
        glBindTexture( GL_TEXTURE_2D, m_nIdx );

        // 画像データの形式を設定する
        // カラーマップタイプにより、変化する
        GLuint format;  // フォーマット
        switch(bpp)
        {
            case 24:
            {
                format = GL_RGB;
                glPixelStorei( GL_UNPACK_ALIGNMENT, 4 );
                break;
            }

            case 32:
            {
                format = GL_RGBA;
                glPixelStorei( GL_UNPACK_ALIGNMENT, 1 );
                break;
            }

            default:    // 失敗
                break;
        }

        // テクスチャパラメータの設定
        // テクスチャの繰り返し設定
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );

        // テクスチャの拡大・縮小の処理の設定
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );

        // 画像データをテクスチャとして登録する
        glTexImage2D( GL_TEXTURE_2D, 0, format, m_nWidth, m_nHeight, 0, format, GL_UNSIGNED_BYTE, image );

        // 操作対象のテクスチャをなしにする
        glBindTexture( GL_TEXTURE_2D, 0 );

        // メモリの開放
        delete[] image;
    };

    // @brief  : レンダリングに設定
    //--------------------------------------------------------------------
    void Set( void ) override
    {
        if(!Name)
        {
            glDisable( GL_TEXTURE_2D );
        }
        glEnable( GL_TEXTURE_2D );
        glBindTexture( GL_TEXTURE_2D, m_nIdx );
    };

protected:
    GLuint   m_nIdx;        // テクスチャ名(ID)
    GLuint   m_nWidth;      // 幅
    GLuint   m_nHeight;     // 高さ

    ~TGA()
    {
        glDeleteTextures( 1 , &m_nIdx );
    }
};

// @brief  : コンストラクタ
//--------------------------------------------------------------------
TextureManagerGL::TextureManagerGL()
{
    new NullTexture(this);
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
TextureManagerGL::~TextureManagerGL()
{
}

// @brief  : ファイルの読み込み
// @param  : ファイル名
//--------------------------------------------------------------------
Texture TextureManagerGL::Load( const std::string &_name )
{
    if(_name.empty()) return TempSmartPtr<ITexture>((ITexture *)Begin());
    // 拡張子の判断
    {
        const std::string            name_copy(_name);
        const std::string::size_type ex = name_copy.find_last_not_of(".");
    }
    return LoadTGA(_name);
}

// @brief  : tgaファイルの読み込み
// @param  : ファイル名
//--------------------------------------------------------------------
Texture TextureManagerGL::LoadTGA( const std::string &_name )
{
    assert(dynamic_cast<ITexture *>(Begin()));
    auto data = TempSmartPtr<ITexture>((ITexture *)Begin());

    if(_name.empty()) return data;

    // ファイルの検索
    bool is_hit = false;
    ForEach(
        [&](Node *_node) {
            assert(dynamic_cast<ITexture *>(_node));

            ITexture *tex = dynamic_cast<ITexture *>(_node);
            std::string name = tex->Name;
            if( name.compare(_name) == 0 )
            {
                is_hit = true;
                data   = TempSmartPtr<ITexture>(tex);
            }
        }
    );
    if(is_hit) return data;

    // 作成
    return TempSmartPtr<ITexture>(new TGA(_name,this));
}


/******************************************************************************
|   End of File
*******************************************************************************/