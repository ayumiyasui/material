/*
# PMDNoBoneManagerGL.h

- PMDクラス骨なし(3D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __3D_PMD_NOBONE_MANAGER_GL_H__
#define __3D_PMD_NOBONE_MANAGER_GL_H__

#include <string>
#include "../../base/tempNodeSingleton.h"
#include "../3d/PMDNobone.h"


class PMDNoBoneManagerGL : public TempNodeSingleton<PMDNoBoneManagerGL>
{
public:
    // @brief  : PMDファイルの取り出し
    // @param  : 親クラス
    //         : 描画するのか？
    //         : ファイル名
    //--------------------------------------------------------------------
    _3d::PMDNoBone *Load( IDraw *, bool, const std::string & );

private:
    // フレンドクラス
    //--------------------------------------------------------------------
    friend TempNodeSingleton<PMDNoBoneManagerGL>;

    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    PMDNoBoneManagerGL();

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~PMDNoBoneManagerGL();
};



#endif //  __3D_PMD_NOBONE_MANAGER_GL_H__
/******************************************************************************
|   End of File
*******************************************************************************/