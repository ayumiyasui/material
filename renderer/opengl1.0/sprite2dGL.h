/*
# sprite2dGL.h

- スプライトクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __2D_SPRITE_GL_H__
#define __2D_SPRITE_GL_H__

#include "OpenGL1.0.h"
#include "../2d/sprite2d.h"

NAMESPACE_2D
    class FactoryGL;
class SpriteGL : public Sprite
{
private:
    // フレンドクラス
    //--------------------------------------------------------------------
    friend FactoryGL;

    // @brief  : コンストラクタ
    // @param  : ポリゴンの大きさ
    //         : テクスチャ座標の位置
    //         : テクスチャ
    //         : 親クラス
    //         : 描画するのか？
    //--------------------------------------------------------------------
    SpriteGL( const Quad &, const Quad &,
               const Texture &,
               IDraw *, bool );

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~SpriteGL();

    // @brief  : 描画
    //--------------------------------------------------------------------
    void Draw( void ) override;
};



END_NAMESPACE_2D
#endif //  __2D_SPRITE_GL_H__
/******************************************************************************
|   End of File
*******************************************************************************/