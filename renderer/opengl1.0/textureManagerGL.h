/*
# textureManagerGL.h

- テクスチャマネージャクラス(OpenGL)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __TEXTUREMANAGERGL_H__
#define __TEXTUREMANAGERGL_H__

#include <string>
#include "../texture/texture.h"
#include "../../base/tempNodeSingleton.h"

class TextureManagerGL : public TempNodeSingleton<TextureManagerGL>
{
public:
    // @brief  : ファイルの読み込み
    // @param  : ファイル名
    //--------------------------------------------------------------------
    Texture Load( const std::string & );

private:
    // フレンドクラス
    //--------------------------------------------------------------------
    friend TempNodeSingleton<TextureManagerGL>;

    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    TextureManagerGL();

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~TextureManagerGL();

    // @brief  : tgaファイルの読み込み
    // @param  : ファイル名
    //--------------------------------------------------------------------
    Texture LoadTGA( const std::string & );

};



#endif  // __TEXTUREMANAGERGL_H__
/******************************************************************************
|   End of File
*******************************************************************************/