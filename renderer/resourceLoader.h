/*
# resourceLoader.h

- リソースローダー

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef MATERIAL_RESOURCE_LOADER_H__
#define MATERIAL_RESOURCE_LOADER_H__

#include <string>
#include "../variable/bitName.h"

class PMDLoader
{
public:
    // 構造体
    //--------------------------------------------------------------------
#pragma pack(push,1)
    struct DATA
    {
        struct t_header // ヘッダ
        {
            char  magic[3];                     // "Pmd"
            material::variable::uni32_f version;  // バージョン
            char  model_name[20];               // モデル名
            char  comment[256];                 // コメント
        };
        struct t_vertex // 頂点データ
        {
            material::variable::uni32_f pos[3];          // 座標(x,y,z)
            material::variable::uni32_f normal_vec[3];   // 法線ベクトル(x,y,z)
            material::variable::uni32_f uv[2];           // UV座標(x,y)
            material::variable::uni16_s bone_num[2];     // 影響を受けるボーン番号
            material::variable::u8      bone_weight;        // ウェイト( 0~100 )　この値がborn_num[0]に対するウェイト。[1]は( 100 - born_weight )となる。
            material::variable::u8      edge_flag;          // 通常0 , エッジ無効なら1   現状あまり気にしなくてもよい
        };
        struct t_material   // マテリアル
        {
            material::variable::uni32_f diffuse_color[3];  // 減衰(デフューズ)
            material::variable::uni32_f alpha;             // 不透明度
            material::variable::uni32_f specularity;       // スペキュラ
            material::variable::uni32_f specular_color[3]; // 光沢(スペキュラI
            material::variable::uni32_f mirror_color[3];   // 環境色(アンビエント)
            material::variable::u8      toon_index;        // トゥーン用：現状気にしない
            material::variable::u8      edge_flag;         // エッジ:現状気にしない
            material::variable::uni32_u face_vert_count;   // 面頂点数  , 面数でなく、この材質で使う面頂点リストのデータ数です。
            char texture_file_name[20]; // テクスチャファイル名またはスフィアファイル名
        };
        struct t_bone   // ボーン
        {
            char bone_name[20];                             // ボーン名
            material::variable::uni16_s parent_bone_index;    // 親ボーンインデックス(無しの場合は0xFFFFとなる)
            material::variable::uni16_s tail_pos_bone_index;  // 末端位置ボーンのインデックス(末端の場合は0)
            material::variable::u8      bone_type;            // ボーンタイプ
            material::variable::uni16_s ik_parent_bone_index; // IKボーンインデックス(影響IKボーン、無い場合0)
            material::variable::uni32_f bone_head_pos[3];     // ボーンの位置
        };
        enum BONE_TYPE  // ボーンタイプ
        {
            ROTATE = 0,         // 回転
            ROTATE_AND_MOVE,    // 回転と移動
            IK_DESTINATION,     // IK
            UNKNOWN,            // 不明
            UNDER_IK,           // IK影響下
            UNDER_ROTATE,       // 回転影響下
            IK_TARGET,          // IK接続先
            NO_DISP,            // 非表示
            TWIST,              // ねじり
            FOLLOW_ROTATE       // 回転運動
        };
        

        t_header                     header;          // ヘッダ
        material::variable::uni32_u  vert_count;      // 頂点数
        t_vertex                    *vertex;          // 頂点データ
        material::variable::uni32_u  face_vert_count; // 面の数
        material::variable::uni16_u *face_vert_index; // 面頂点リスト
        material::variable::uni32_u  material_count;  // 材質数
        t_material                  *material;        // 材質データ(70Bytes)
        material::variable::uni16_u  bone_count;      // ボーン数
        t_bone                      *bone;            // ボーンデータ(39Bytes)
    };
    //アライメント制御終わり！
#pragma pack(pop)

    // @brief  : PMDファイルの取り出し
    //         : ファイル名
    //--------------------------------------------------------------------
    static DATA *Load( const std::string & );

private:
    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    PMDLoader();

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~PMDLoader();
};

#endif //MATERIAL_RESOURCE_LOADER_H__
/******************************************************************************
|   End of File
*******************************************************************************/