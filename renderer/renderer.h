/*
# renderer.h

- レンダラー抽象クラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __RENDERER_H__
#define __RENDERER_H__

#include <string>
#include "../base/notCopy.h"
#include "../base/tempGetter.h"
#include "../base/tempGetSet.h"
#include "../base/color.h"
#include "texture/texture.h"
#include "2d/factory.h"
#include "3d/factory.h"
#include "effect/factory.h"
#include "vertex/factory.h"

using namespace material;
class IRenderer : public NotCopy
{
public:
    // プロパティ
    //--------------------------------------------------------------------
    TempGetter<float> Width;
    TempGetter<float> Height;
    TempGetSet<Color> BackColor;

    // @brief  : 描画
    //--------------------------------------------------------------------
    virtual void Draw( void ) = 0;

    // @brief  : スクリーンショット
    // @param  : スクリーンショット名
    //--------------------------------------------------------------------
    virtual void ScreenShort( const std::string & ) const = 0;

    // @brief  : 2Dファクトリーの取得
    //--------------------------------------------------------------------
    _2d::IFactory *Factory2D( void ) const;

    // @brief  : 3Dファクトリーの取得
    //--------------------------------------------------------------------
    _3d::IFactory *Factory3D( void ) const;

    // @brief  : 頂点ファクトリーの取得
    //--------------------------------------------------------------------
    vertex::Factory *const Vertex(void) const;

    // @brief  : エフェクトファクトリーの取得
    //--------------------------------------------------------------------
    effect::Factory *const Effect(void) const;

    // @brief  : デバック表示
    // @param  : 書式付き文字列
    //--------------------------------------------------------------------
    void Print(const char *str, ...);

    // @brief  : デバック表示の切り替え
    //--------------------------------------------------------------------
    void SwitchDebugView(void);

protected:
#if _DEBUG
    // 定数
    //--------------------------------------------------------------------
    static const unsigned int MAX_STR = 1024;

    // プロパティ
    //--------------------------------------------------------------------
    TempGetter<char [MAX_STR]> DebugString;
    TempGetter<bool>    DebugIsDraw;

    // @brief  : デバック文字のリセット
    //--------------------------------------------------------------------
    void ResetDebugString( void );
#endif

    // @brief  : コンストラクタ
    // @param  : ウィンドウ幅
    //         : ウィンドウ高さ
    //         : 2Dファクトリ
    //         : 3Dファクトリ
    //         : エフェクトファクトリー
    //         : 頂点ファクトリー
    //--------------------------------------------------------------------
    IRenderer(float _width, float _height,
        _2d::IFactory *,
        _3d::IFactory *,
        effect::Factory *,
        vertex::Factory *);

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~IRenderer();

private:
    // メンバ変数
    //--------------------------------------------------------------------
    float            m_Width;
    float            m_Height;
    Color            m_BackColor;
    _2d::IFactory   *m_Factory2D;
    _3d::IFactory   *m_Factory3D;
    effect::Factory *m_Effect;  // エフェクトファクトリー
    vertex::Factory *m_Vertex;  // 頂点ファクトリー

#ifdef _DEBUG
    char         str[MAX_STR];   // 表示する文字数
    unsigned int count;          // 入力した文字数
    bool         is_draw;        // 描画するのか
#endif
};



#endif // __RENDERER_H__
/******************************************************************************
|   End of File
*******************************************************************************/