/*
# resourceLoader.cpp

- リソースローダー

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#define _CRT_SECURE_NO_WARNINGS
#include "resourceLoader.h"
#include <stdio.h>

// @brief  : コンストラクタ
//--------------------------------------------------------------------
PMDLoader::PMDLoader()
{
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
PMDLoader::~PMDLoader()
{
}

// @brief  : PMDファイルの取り出し
//         : ファイル名
//--------------------------------------------------------------------
PMDLoader::DATA *PMDLoader::Load( const std::string &_file_name )
{
    FILE *fp = fopen(_file_name.c_str(), "rb");
    
    DATA *data = new DATA;

    //ヘッダー読み込み
    fread(&data->header, sizeof(data->header), 1, fp);

    //頂点数読み込み
    fread( &data->vert_count , sizeof(data->vert_count) , 1 , fp );

    //頂点データ読み込み
    data->vertex = new DATA::t_vertex[data->vert_count.u];
    fread( data->vertex , sizeof(data->vertex[0]), data->vert_count.u , fp );

    //面頂点リスト読み込み
    fread( &data->face_vert_count , sizeof(data->face_vert_count), 1, fp );

    //面頂点リストデータ読み込み
    data->face_vert_index = new material::variable::uni16_u[data->face_vert_count.u];
    fread( data->face_vert_index , sizeof(data->face_vert_index[0]) , data->face_vert_count.u , fp );

    //材質数読み込み
    fread( &data->material_count , sizeof(data->material_count), 1, fp );

    //材質リスト読み込み
    data->material = new DATA::t_material[data->material_count.u];
    fread( data->material , sizeof(data->material[0]), data->material_count.u , fp );

    //ボーン数読み込み
    fread( &data->bone_count , sizeof(data->bone_count), 1, fp );

    //ボーンリスト読み込み
    data->bone = new DATA::t_bone[data->bone_count.u];
    fread( data->bone , sizeof(data->bone[0]), data->bone_count.u , fp );
    
    fclose( fp );

    return data;
}

/******************************************************************************
|   End of File
*******************************************************************************/