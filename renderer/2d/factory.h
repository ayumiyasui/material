/*
# factory.h

- ファクトリクラス(2D)
- 機種・ライブラリ差の吸収のために作成

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __2D_FACTORY_H__
#define __2D_FACTORY_H__


#include "camera.h"
#include "transfrom.h"
#include "sprite2d.h"

class IRenderer;

NAMESPACE_2D
class IFactory
{
public:
    // @brief  : Cameraクラス掃き出し
    //--------------------------------------------------------------------
    virtual Camera *CreateCamera( IDraw *, bool ) const = 0;

    // @brief  : Transformクラス掃き出し
    //--------------------------------------------------------------------
    virtual Transform* CreateTransform( IDraw *, bool ) const = 0;

    // @brief  : Spriteクラス掃き出し
    //--------------------------------------------------------------------
    virtual Sprite *CreateSprite(
        const Quad &, const Quad &,
        const std::string &,
        IDraw *, bool ) const = 0;

protected:
    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    IFactory(){};

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~IFactory(){};

private:
    // フレンドクラス
    //--------------------------------------------------------------------
    friend IRenderer;

};



END_NAMESPACE_2D
#endif //  __2D_FACTORY_H__
/******************************************************************************
|   End of File
*******************************************************************************/