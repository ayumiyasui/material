/*
# transform.h

- 変換行列設定クラス(2D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __2D_TRANSFORM_H__
#define __2D_TRANSFORM_H__

#include "draw2d.h"
#include "../../base/tempGetSet.h"
#include "../../base/tempGetter.h"
#include "../../math/vector2.h"
#include "../../math/matrix2d.h"

NAMESPACE_2D
class Transform : public IDraw2D
{
private:
    template < typename T > class GetSetMatrix : public TempGetSet<T>
    {
    public:
        GetSetMatrix( T &, Transform * );
        ~GetSetMatrix();
        GetSetMatrix<T> &operator = ( const T & );

    private:
        Transform *m_Master;
        void Set( const T & ) override;
    };

public:
    // プロパティ
    //--------------------------------------------------------------------
    TempGetter<Matrix2D>  Matrix;
    GetSetMatrix<Vector2> Position;
    GetSetMatrix<float>   Rotation;
    GetSetMatrix<Vector2> Scale;

    // @brief  : 子の追加
    //--------------------------------------------------------------------
    void AddChild( IDraw * );
    void AddChild( Transform * );


protected:
    // @brief  : コンストラクタ
    // @param  : 親クラス
    //         : 描画するのか？
    //--------------------------------------------------------------------
    Transform( IDraw *, bool _is_draw = true );

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~Transform();

private:
    // メンバ変数
    //--------------------------------------------------------------------
    bool     m_ParentIsTransform;   // 親がトランスフォームクラス
    Matrix2D m_WorldMatrix;         // ワールド行列
    Vector2  m_Position;       // 位置
    float    m_LocalRotation;       // 回転
    Vector2  m_LocalScale;          // 大きさ
};



END_NAMESPACE_2D
#endif //  __2D_TRANSFORM_H__
/******************************************************************************
|   End of File
*******************************************************************************/