/*
# sprite.cpp

- スプライトクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード -------------------------------------------------------------
#include "sprite2d.h"

NAMESPACE_2D

// @brief  : コンストラクタ
// @param  : ポリゴンの大きさ
//         : テクスチャ座標の位置
//         : テクスチャ
//         : 親クラス
//         : 描画するのか？
//--------------------------------------------------------------------
Sprite::Sprite( const Quad &_polygon_Quad, const Quad &_texture_Quad,
        const Texture &_texture,
        IDraw *_parent, bool _is_draw ) :
    IDraw2D(_parent,_is_draw),
    m_Texture(_texture),
    m_PolygonQuad(_polygon_Quad),
    m_PolygonColor(1.0f,1.0f,1.0f,1.0f),
    m_TextureQuad(_texture_Quad),
    PolygonQuad(m_PolygonQuad),
    TextureQuad(m_TextureQuad),
    Tex(m_Texture),
    PolygonColor(m_PolygonColor)
{
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
Sprite::~Sprite()
{
}

// @brief  : ポリゴンの大きさをセット
// @param  : ポリゴンの大きさ
//--------------------------------------------------------------------
void Sprite::SetPolygonQuad( const Quad &_size )
{
    m_PolygonQuad = _size;
}

// @brief  : テクスチャの大きさをセット
// @param  : テクスチャ
//--------------------------------------------------------------------
void Sprite::SetTextureQuad( const Quad &_size )
{
    m_TextureQuad = _size;
}



END_NAMESPACE_2D
/******************************************************************************
|   End of File
*******************************************************************************/