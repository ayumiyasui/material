/*
# sprite.h

- スプライトクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __2D_SPRITE_H__
#define __2D_SPRITE_H__

#include "draw2d.h"
#include "../texture/texture.h"
#include "../../math/Quad.h"
#include "../../base/color.h"
#include "../../base/tempGetter.h"
#include "../../base/tempGetSet.h"


NAMESPACE_2D
class Sprite : public IDraw2D
{
public:
    // プロパティ
    //--------------------------------------------------------------------
    TempGetter<Quad>    PolygonQuad;
    TempGetter<Quad>    TextureQuad;
    TempGetSet<Texture> Tex;
    TempGetSet<Color>   PolygonColor;

    // @brief  : ポリゴンの大きさをセット
    // @param  : ポリゴンの大きさ
    //--------------------------------------------------------------------
    virtual void SetPolygonQuad( const Quad & );

    // @brief  : テクスチャの大きさをセット
    // @param  : テクスチャ
    //--------------------------------------------------------------------
    virtual void SetTextureQuad( const Quad & );

protected:
    // @brief  : コンストラクタ
    // @param  : ポリゴンの大きさ
    //         : テクスチャ座標の位置
    //         : テクスチャ
    //         : 親クラス
    //         : 描画するのか？
    //--------------------------------------------------------------------
    Sprite( const Quad &, const Quad &,
            const Texture &,
            IDraw *, bool _is_draw = true );

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~Sprite();

private:
    // メンバ変数
    //--------------------------------------------------------------------
    Quad    m_PolygonQuad;
    Quad    m_TextureQuad;
    Texture m_Texture;
    Color   m_PolygonColor;
};



END_NAMESPACE_2D
#endif //  __SPRITE_H__
/******************************************************************************
|   End of File
*******************************************************************************/