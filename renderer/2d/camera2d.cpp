/*
# camera.cpp

- カメラクラス(2D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "camera.h"

#include "../../app/app.h"

NAMESPACE_2D
// @brief  : コンストラクタ
// @param  : 親クラス
//         : 描画するのか？
//--------------------------------------------------------------------
Camera::Camera( IDraw *_parent, bool _is_draw ) :
    IDraw2D(_parent,_is_draw),
    m_Quad(0,App::Instance().Renderer()->Width,0,App::Instance().Renderer()->Height),
    m_Near(0.0f),
    m_Far(1.0f),
    m_Viewport(0,App::Instance().Renderer()->Width,0,App::Instance().Renderer()->Height),
    Size(m_Quad),
    Near(m_Near),
    Far(m_Far),
    Viewport(m_Viewport)
{
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
Camera::~Camera()
{
}



END_NAMESPACE_2D
/******************************************************************************
|   End of File
*******************************************************************************/