/*
# draw2d.h

- 描画抽象クラス(2D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __DRAW2D_H__
#define __DRAW2D_H__

#include "../draw.h"

// 2D名前空間マクロ
#define NAMESPACE_2D      namespace _2d {
#define END_NAMESPACE_2D  };

NAMESPACE_2D
class IDraw2D : public IDraw
{
protected:
    // @brief  : コンストラクタ
    // @param  : 親クラス
    //         : 描画するのか？
    //--------------------------------------------------------------------
    IDraw2D( IDraw *, bool _is_draw = true );

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~IDraw2D();
};



END_NAMESPACE_2D
#endif //  __DRAW2D_H__
/******************************************************************************
|   End of File
*******************************************************************************/