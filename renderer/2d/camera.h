/*
# camera.h

- カメラクラス(2D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __2D_CAMERA_H__
#define __2D_CAMERA_H__

#include "draw2d.h"
#include "../../math/Quad.h"
#include "../../base/tempGetSet.h"

NAMESPACE_2D
class Camera : public IDraw2D
{
public:
    // プロパティ
    //--------------------------------------------------------------------
    TempGetSet<Quad>  Size;     // カメラサイズ
    TempGetSet<float> Near;     // 視点から最も近い位置
    TempGetSet<float> Far;      // 視点から最も遠い位置
    TempGetSet<Quad>  Viewport; // ビューポート

protected:
    // @brief  : コンストラクタ
    // @param  : 親クラス
    //         : 描画するのか？
    //--------------------------------------------------------------------
    Camera( IDraw *, bool _is_draw = true );

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~Camera();

private:
    // メンバ変数
    //--------------------------------------------------------------------
    Quad  m_Quad;       // カメラの大きさ
    float m_Near;       // 視点から最も近い位置
    float m_Far;        // 視点から最も遠い位置
    Quad  m_Viewport;   // ビューポート
};



END_NAMESPACE_2D
#endif //  __2D_CAMERA_H__
/******************************************************************************
|   End of File
*******************************************************************************/