/*
# transform.cpp

- 変換行列設定クラス(2D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード -------------------------------------------------------------
#include "transfrom.h"
#include "../../math/vector2.h"
#include <math.h>
#include <typeinfo>


NAMESPACE_2D
#pragma warning(push)   // インクリメントでメンバ変数にthisを渡している
#pragma warning(disable:4355)
// @brief  : コンストラクタ
// @param  : 親クラス
//         : 描画するのか？
//--------------------------------------------------------------------
Transform::Transform( IDraw * _parent, bool _is_draw ) :
    IDraw2D(_parent,_is_draw),
    m_ParentIsTransform(false),
    m_Position(0.0f,0.0f),
    m_LocalRotation(0.0f),
    m_LocalScale(1.0f,1.0f),
    Matrix(m_WorldMatrix),
    Position(m_Position,this),
    Rotation(m_LocalRotation,this),
    Scale(m_LocalScale,this)
{
    Vector2 pos = Position;
    Position = pos;
}
#pragma warning(pop)

// @brief  : デストラクタ
//--------------------------------------------------------------------
Transform::~Transform()
{
}

// @brief  : 子の追加
//--------------------------------------------------------------------
void Transform::AddChild( IDraw *_add )
{
    if(typeid(*_add) == typeid(Transform))
    {
        dynamic_cast<Transform *>(_add)->m_ParentIsTransform = false;
    }
    Node::AddChild(_add);
}
void Transform::AddChild( Transform *_add )
{
    _add->m_ParentIsTransform = true;
    Node::AddChild(_add);
}

// 内部クラス
//--------------------------------------------------------------------
template < typename T >
Transform::GetSetMatrix<T>::GetSetMatrix( T &_value, Transform *_master ) :
    m_Master(_master),
    TempGetSet<T>(_value)
{
}
template < typename T >
Transform::GetSetMatrix<T>::~GetSetMatrix()
{
}
template < typename T >
Transform::GetSetMatrix<T> &Transform::GetSetMatrix<T>::operator = (const T &_set)
{
    Set(_set);
    return *this;
}

template < typename T >
void Transform::GetSetMatrix<T>::Set( const T &_value )
{
    TempGetSet<T>::Set(_value);

    // スケール行列
    const Vector2 scale = m_Master->Scale;
    const Matrix2D scale_matrix(
        scale.x, 0.0f,    0.0f,
        0.0f,    scale.y, 0.0f,
        0.0f,    0.0f,    1.0f
    );

    // 回転行列
    const float rotation = m_Master->Rotation;
    const float sin_      = sinf(rotation);
    const float cos_      = cosf(rotation);
    const Matrix2D rotation_matrix(
        cos_,-sin_,0.0f,
        sin_, cos_,0.0f,
        0.0f, 0.0f,1.0f
    );

    // 位置
    const Vector2 position = m_Master->Position;
    const Matrix2D position_matrix(
        1.0f,0.0f,position.x,
        0.0f,1.0f,position.y,
        0.0f,0.0f,1.0f
    );

    // 合成
    m_Master->m_WorldMatrix =
        position_matrix * scale_matrix * rotation_matrix;
    //if(m_Master->m_ParentIsTransform)
    //{
    //    m_Master->m_WorldMatrix
    //        = dynamic_cast<Transform *>(m_Master->Parent())->m_WorldMatrix
    //        * m_Master->m_WorldMatrix;
    //}
}
template class _2d::Transform::GetSetMatrix<float>;
template class _2d::Transform::GetSetMatrix<Vector2>;



END_NAMESPACE_2D
/******************************************************************************
|   End of File
*******************************************************************************/