/*
# factory.h

- ファクトリクラス(エフェクト)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef MATERIAL_RENDERER_EFFECT_FACTORY_H_
#define MATERIAL_RENDERER_EFFECT_FACTORY_H_

#include "alphaBlend.h"
#include "curring.h"

namespace material {
namespace effect {
class Factory
{
public:
    // @brief  : アルファブレンドの書き出し
    //--------------------------------------------------------------------
    virtual AlphaBlend *CreateAlphaBlend(IDraw *_parent) const = 0;

    // @brief  : カリングの書き出し
    //--------------------------------------------------------------------
    virtual Curring *CreateCurring(IDraw *_parent) const = 0;

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~Factory(){};

protected:
    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    Factory(){};
};


}; // namespace effect
}; // namespace material
#endif //  MATERIAL_RENDERER_EFFECT_FACTORY_H_
/******************************************************************************
|   End of File
*******************************************************************************/