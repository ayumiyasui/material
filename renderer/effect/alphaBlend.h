/*
# alphaBlend.h

- アルファブレンドクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef MATERIAL_RENDERER_EFFECT_ALPHABLEND_H_
#define MATERIAL_RENDERER_EFFECT_ALPHABLEND_H_

#include "../draw.h"

namespace material {
namespace effect {
class AlphaBlend : public IDraw
{
public:
    // アルファブレンドの種類
    //--------------------------------------------------------------------
    enum TYPE
    {
        TYPE_NONE   = 0, // アルファブレンドなし
        TYPE_ALPHA,      // アルファブレンド
        TYPE_ADD,        // 加算
        TYPE_DIFFERENCE, // 減算
        TYPE_MULTPLY,    // 乗算
        TYPE_REVERSAL,   // 反転

        MAX_TYPE
    };

    // @brief  : アルファブレンド変更
    // @param  : 種類
    // @return : 前の設定
    //--------------------------------------------------------------------
    TYPE Set(TYPE _type){auto old = m_Type; m_Type = _type; return old;};

    // @brief  : アルファブレンドの取得
    // @return : 現在のアルファブレンド
    //--------------------------------------------------------------------
    TYPE Get(void){return m_Type;};

protected:
    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    AlphaBlend(IDraw *_parent) : IDraw(_parent,true){};

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~AlphaBlend(){};

private:
    // メンバ変数
    //--------------------------------------------------------------------
    TYPE m_Type;    // 設定したアルファブレンド
};


}; //namespace effect
}; //namespace material
#endif //  MATERIAL_RENDERER_EFFECT_ALPHABLEND_H_
/******************************************************************************
|   End of File
*******************************************************************************/