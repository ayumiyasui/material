/*
# curring.h

- カリングクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef MATERIAL_RENDERER_EFFECT_CURRING_H_
#define MATERIAL_RENDERER_EFFECT_CURRING_H_

#include "../draw.h"

namespace material {
namespace effect {
class Curring : public IDraw
{
public:
    // カリングの種類
    //--------------------------------------------------------------------
    enum TYPE
    {
        TYPE_NONE   = 0, // カリングしない
        TYPE_FACE,       // 表面の表示
        TYPE_BACK,       // 裏面の表示

        MAX_TYPE
    };

    // @brief  : アルファブレンド変更
    // @param  : 種類
    //--------------------------------------------------------------------
    virtual void Set(TYPE _type) = 0;

protected:
    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    Curring(IDraw *_parent) : IDraw(_parent,true){};

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~Curring(){};
};


}; //namespace effect
}; //namespace material
#endif //  MATERIAL_RENDERER_EFFECT_CURRING_H_
/******************************************************************************
|   End of File
*******************************************************************************/