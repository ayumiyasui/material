/*
# sprite2dDx9.h

- スプライトクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __2D_SPRITE_Dx9_H__
#define __2D_SPRITE_Dx9_H__

#include "directx9.h"
#include "../2d/sprite2d.h"

NAMESPACE_2D

    class FactoryDx9;

class SpriteDx9 : public Sprite
{
public:
    // @brief  : ポリゴンの大きさをセット
    // @param  : ポリゴンの大きさ
    //--------------------------------------------------------------------
    void SetPolygonQuad( const Quad &_size ) override;

    // @brief  : テクスチャの大きさをセット
    // @param  : テクスチャ
    //--------------------------------------------------------------------
    void SetTextureQuad( const Quad &_size ) override;

private:
    // フレンドクラス
    //--------------------------------------------------------------------
    friend FactoryDx9;

    // 構造体
    //--------------------------------------------------------------------
    struct VERTEX   // 頂点バッファ情報
    {
        D3DXVECTOR3 pos;
        D3DCOLOR    col;
        D3DXVECTOR2 tex;
    };

    // 定数
    //--------------------------------------------------------------------
    static const DWORD VERTEX_FVF;          // 頂点フォーマット

    // メンバ変数
    //--------------------------------------------------------------------
    LPDIRECT3DVERTEXBUFFER9 m_VertexBuffer; // 頂点バッファ

    // @brief  : コンストラクタ
    // @param  : ポリゴンの大きさ
    //         : テクスチャ座標の位置
    //         : テクスチャ
    //         : 親クラス
    //         : 描画するのか？
    //--------------------------------------------------------------------
    SpriteDx9( const Quad &, const Quad &,
               const Texture &,
               IDraw *, bool );

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~SpriteDx9();

    // @brief  : 描画
    //--------------------------------------------------------------------
    void Draw( void ) override;
};



END_NAMESPACE_2D
#endif //  __2D_SPRITE_Dx9_H__
/******************************************************************************
|   End of File
*******************************************************************************/