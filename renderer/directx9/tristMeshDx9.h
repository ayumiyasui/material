/*
# tristMeshDx9.h

- メッシュクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __TRISTMESH_Dx9_H__
#define __TRISTMESH_Dx9_H__

#include "../3d/mesh.h"
#include "directx9.h"
#include "../../base/tempGetter.h"

NAMESPACE_3D

class TriStMeshDx9 : public Mesh
{
public:
    // 構造体
    //--------------------------------------------------------------------
    struct VERTEX
    {
        D3DXVECTOR3 pos;    // 頂点座標
        D3DXVECTOR3 nor;    // 法線
        D3DXVECTOR2 tex;    // テクスチャ座標
    };

    // プロパティ
    //--------------------------------------------------------------------
    const unsigned       BLOCK_X;       // 横ブロック数
    const unsigned       BLOCK_Y;       // 縦ブロック数
    UINT                 MAX_VERTEX;    // 最大頂点数
    UINT                 MAX_INDEX;     // 最大インデックス数
    LPDIRECT3DVERTEXBUFFER9 VertexBuffer(void);

    // @brief  : コンストラクタ
    // @param  : 横ブロック数
    //         : 縦ブロック数
    //         : 親クラス
    //         : 描画するのか？
    //--------------------------------------------------------------------
    TriStMeshDx9( unsigned, unsigned, IDraw *, bool );

private:
    // 定数
    //--------------------------------------------------------------------
    static const DWORD FVF; // 頂点フォーマット

    // メンバ変数
    //--------------------------------------------------------------------
    LPDIRECT3DVERTEXBUFFER9 m_VertexBuffer; // 頂点バッファ
    LPDIRECT3DINDEXBUFFER9  m_IndexBuffer;  // インデックスバッファ

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~TriStMeshDx9();

    // @brief  : 描画
    //--------------------------------------------------------------------
    void Draw( void ) override;
};



END_NAMESPACE_3D
#endif //  __3D_PMD_NOBONE_H__
/******************************************************************************
|   End of File
*******************************************************************************/