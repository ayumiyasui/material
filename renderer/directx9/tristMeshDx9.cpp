/*
# tristMeshDx9.cpp

- メッシュクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "tristMeshDx9.h"

#include <assert.h>


NAMESPACE_3D
// 定数
//--------------------------------------------------------------------
const DWORD TriStMeshDx9::FVF = ( D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1 );

// プロパティ
//--------------------------------------------------------------------
LPDIRECT3DVERTEXBUFFER9 TriStMeshDx9::VertexBuffer(void)
{
    return m_VertexBuffer;
}

// @brief  : コンストラクタ
// @param  : 横ブロック数
//         : 縦ブロック数
//         : 親クラス
//         : 描画するのか？
//--------------------------------------------------------------------
TriStMeshDx9::TriStMeshDx9( unsigned _x, unsigned _y,
    IDraw *_parent, bool _is_draw ) :
    Mesh(_parent,_is_draw),
    BLOCK_X(_x),
    BLOCK_Y(_y),
    MAX_VERTEX((_x+1)*(_y+1)),
    MAX_INDEX((((_x+1)*2+2)*_y)-2),
    m_VertexBuffer(nullptr),
    m_IndexBuffer(nullptr)
{
    LPDIRECT3DDEVICE9 device = DirectX9::Instance().Device;
    HRESULT hr;

    // 頂点設定
    hr = device->CreateVertexBuffer(
        sizeof(VERTEX) * MAX_VERTEX,    // バッファサイズ
        D3DUSAGE_WRITEONLY,             // 頂点バッファの使用方法
        FVF,                            // 使用する頂点フォーマット
        D3DPOOL_MANAGED,                // バッファを保持するメモリクエストを指定
        &m_VertexBuffer,                // 頂点のバッファの先頭アドレス
        NULL );

    // インデックスを指定
    {
        // 生成(確保)
        hr = device->CreateIndexBuffer(
            sizeof( WORD ) * MAX_INDEX,     // バッファサイズ
            D3DUSAGE_WRITEONLY,             // 頂点バッファの使用方法
            D3DFMT_INDEX16,                 // 使用する頂点フォーマット
            D3DPOOL_MANAGED,                // バッファを保持するメモリクエストを指定
            &m_IndexBuffer,                 // 頂点インデックスのバッファの先頭アドレス
            NULL );

        unsigned upper = 0;
        unsigned lower = BLOCK_X + 1;
        unsigned i = 0;
        WORD *index;
        hr = m_IndexBuffer->Lock( 0, 0, ( void ** )&index, D3DLOCK_NOSYSLOCK );
        for( unsigned y = 0; y < BLOCK_Y ; ++y )
        {
            for( unsigned x = 0; x < BLOCK_X + 1; ++x )
            {
                // 下点
                assert(i < MAX_INDEX);
                index[i++] = lower++;

                // 上点
                assert(i < MAX_INDEX);
                index[i++] = upper++;
            }

            if( y < BLOCK_Y - 1 )
            {
                upper--;    // ダミー点を打つための調整

                // ダミー点１
                assert(i < MAX_INDEX);
                index[i++] = upper++;

                // ダミー点２
                assert(i < MAX_INDEX);
                index[i++] = lower;
            }
        }
        hr = m_IndexBuffer->Unlock(); // インデックスバッファの固定を解除
    }
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
TriStMeshDx9::~TriStMeshDx9()
{
    if(m_VertexBuffer) m_VertexBuffer->Release();
    if(m_IndexBuffer)  m_IndexBuffer->Release();
}

// @brief  : 描画
//--------------------------------------------------------------------
void TriStMeshDx9::Draw( void )
{
    LPDIRECT3DDEVICE9 device = DirectX9::Instance().Device;

    HRESULT hr;

    hr = device->SetStreamSource(0,m_VertexBuffer,0,sizeof(VERTEX));
    hr = device->SetFVF(FVF);
    hr = device->SetIndices(m_IndexBuffer);
    hr = device->DrawIndexedPrimitive(
        D3DPT_TRIANGLESTRIP,
        0,
        0,
        MAX_VERTEX,
        0,
        MAX_INDEX - 2 );
}



END_NAMESPACE_3D
/******************************************************************************
|   End of File
*******************************************************************************/