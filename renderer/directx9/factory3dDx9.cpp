/*
# factory.h

- ファクトリクラス(3D)
- 機種・ライブラリ差の吸収のために作成

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "factory3dDx9.h"

#include "camera3dDx9.h"
#include "transform3dDx9.h"
#include "transformQuat3dDx9.h"
#include "transLookCameraDx9.h"
#include "directionalLightDx9.h"
#include "plateDx9.h"
#include "skyDx9.h"
#include "textureManagerDx9.h"
#include "XFileManagerDx9.h"
#include "PMDNoBoneManagerDx9.h"


NAMESPACE_3D
// @brief  : コンストラクタ
//--------------------------------------------------------------------
FactoryDx9::FactoryDx9()
{
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
FactoryDx9::~FactoryDx9()
{
}

// @brief  : カメラクラスの作成
//--------------------------------------------------------------------
Camera *FactoryDx9::CreateCamera( IDraw *_parent, bool _is_draw ) const
{
    return new CameraDx9(_parent,_is_draw);
}

// @brief  : ライトクラスの作成
//--------------------------------------------------------------------
DirectionalLight *FactoryDx9::CreateDirLight( IDraw *_parent, bool _is_draw ) const
{
    return new DirectionalLightDx9(_parent,_is_draw);
}

// @brief  : Transformの作成
//--------------------------------------------------------------------
Transform *FactoryDx9::CreateTransform( IDraw *_parent, bool _is_draw ) const
{
    return new TransformDx9(_parent,_is_draw);
}
TransformQuat *FactoryDx9::CreateTransformQuat( IDraw *_parent, bool _is_draw ) const
{
    return new TransformQuatDx9(_parent,_is_draw);
}
TransLookCamera *FactoryDx9::CreateTransformLookCamera( Camera *_cam, IDraw *_parent, bool _is_draw ) const
{
    return new TransLookCameraDx9(_cam,_parent,_is_draw);
}

// @brief  : 板の作成
// @param  : テクスチャ名
//         : 大きさ
//          :親クラス
//         : 描画するのか？
//--------------------------------------------------------------------
Plate *FactoryDx9::CreatePlate( const std::string &_file_name, const Quad &_size, IDraw *_parent, bool _is_draw )
{
    Plate *create = new PlateDx9(_size,_parent,_is_draw);
    create->Tex   = TextureManagerDx9::Instance().Load(_file_name);
    return create;
}

// @brief  : 空の作成
// @param  : テクスチャ名
//         : 半径
//         : ブロック数横
//         : ブロック数縦
//          :親クラス
//         : 描画するのか？
//--------------------------------------------------------------------
Sky *FactoryDx9::CreateSky( const std::string &_file_name,
    float _length, unsigned _x, unsigned _y,
    IDraw *_parent, bool _is_draw ) const
{
    return new SkyDx9(TextureManagerDx9::Instance().Load(_file_name),_length,_x,_y,_parent,_is_draw);
}

// @brief  : XFileの作成
//--------------------------------------------------------------------
XFile *FactoryDx9::CreateXFile( IDraw *_parent, bool _is_draw, const std::string &_file_name ) const
{
    return XFileManagerDx9::Instance().Load(_parent,_is_draw,_file_name);
}

// @brief  : PMDの作成(骨なし)
//--------------------------------------------------------------------
PMDNoBone *FactoryDx9::CreatePMDNoBone( const std::string &_file_name, IDraw *_parent, bool _is_draw ) const
{
    return PMDNoBoneManagerDx9::Instance().Load(_parent,_is_draw,_file_name);
};



END_NAMESPACE_3D
/******************************************************************************
|   End of File
*******************************************************************************/