/*
# textureManagerDx9.h

- テクスチャマネージャクラス(DirectX9)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __TEXTUREMANAGERDX9_H__
#define __TEXTUREMANAGERDX9_H__

#include "../../base/tempNodeSingleton.h"

class DirectX9;

class TextureManagerDx9 :public TempNodeSingleton<TextureManagerDx9>
{
public:
    // @brief  : ファイルの読み込み
    // @param  : ファイル名
    //--------------------------------------------------------------------
    Texture Load( const std::string & );

private:
    // フレンドクラス
    //--------------------------------------------------------------------
    friend TempNodeSingleton<TextureManagerDx9>;

    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    TextureManagerDx9();

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~TextureManagerDx9();
};



#endif  // __TEXTUREMANAGERDX9_H__
/******************************************************************************
|   End of File
*******************************************************************************/