/*
# directionalLightDx9.h

- ライトクラス(3D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __3D_DIRECTIONALLIGHT_Dx9_H__
#define __3D_DIRECTIONALLIGHT_Dx9_H__

#include "../3d/directionalLight.h"

NAMESPACE_3D

    class FactoryDx9;

class DirectionalLightDx9 : public DirectionalLight
{
private:
    // グローバル変数
    //--------------------------------------------------------------------
    static bool g_UseLightIndex[8];       // ライトの数

    // メンバ変数
    //--------------------------------------------------------------------
    unsigned m_Index;  // ライトのインデックス

    // フレンドクラス
    //--------------------------------------------------------------------
    friend FactoryDx9;

    // @brief  : コンストラクタ
    // @param  : 親クラス
    //         : 描画するのか？
    //--------------------------------------------------------------------
    DirectionalLightDx9( IDraw *, bool );

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~DirectionalLightDx9();

    // @brief  : 描画が有効のときの挙動
    //--------------------------------------------------------------------
    void OnEnable( void ) override;

    // @brief  : 描画が無効のときの挙動
    //--------------------------------------------------------------------
    void OnDisable( void ) override;

    // @brief  : ライトのセット
    //--------------------------------------------------------------------
    void Set( void ) override;
};



END_NAMESPACE_3D
#endif //  __3D_DIRECTIONALLIGHT_Dx9_H__
/******************************************************************************
|   End of File
*******************************************************************************/