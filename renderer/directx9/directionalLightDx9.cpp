/*
# directionalLightDx9.cpp

- ライトクラス(3D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "directionalLightDx9.h"

#include <assert.h>
#include "directx9.h"


NAMESPACE_3D

// グローバル変数
//--------------------------------------------------------------------
bool DirectionalLightDx9::g_UseLightIndex[8] = {false};

// @brief  : コンストラクタ
// @param  : 親クラス
//         : 描画するのか？
//--------------------------------------------------------------------
DirectionalLightDx9::DirectionalLightDx9( IDraw *_parent, bool _is_draw ) :
    DirectionalLight(_parent,_is_draw)
{
    for( unsigned i = 0 ; i < 8 ; ++i )
    {
        if(!g_UseLightIndex[i])
        {
            g_UseLightIndex[i] = true;
            m_Index = i;
            Set();
            (_is_draw) ? OnEnable() : OnDisable();
            return;
        }
    }

    assert(false);
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
DirectionalLightDx9::~DirectionalLightDx9()
{
    g_UseLightIndex[m_Index] = false;
}

// @brief  : 描画が有効のときの挙動
//--------------------------------------------------------------------
void DirectionalLightDx9::OnEnable( void )
{
    LPDIRECT3DDEVICE9 device = DirectX9::Instance().Device;
    device->SetRenderState(D3DRS_LIGHTING,TRUE);
    device->SetRenderState(D3DRS_AMBIENT,TRUE);
    device->LightEnable(m_Index,TRUE);
}

// @brief  : 描画が無効のときの挙動
//--------------------------------------------------------------------
void DirectionalLightDx9::OnDisable( void )
{
    LPDIRECT3DDEVICE9 device = DirectX9::Instance().Device;
    device->LightEnable(m_Index,FALSE);
}

// @brief  : ライトのセット
//--------------------------------------------------------------------
void DirectionalLightDx9::Set( void )
{
    LPDIRECT3DDEVICE9 device = DirectX9::Instance().Device;
    const D3DLIGHT9 light =
    {
        D3DLIGHT_DIRECTIONAL,
        D3DXCOLOR(Diffuse->r,Diffuse->g,Diffuse->b,Diffuse->a),
        D3DXCOLOR(Specular->r,Specular->g,Specular->b,Specular->a),
        D3DXCOLOR(Ambient->r,Ambient->g,Ambient->b,Ambient->a),
        D3DXVECTOR3(0.0f,0.0f,0.0),
        D3DXVECTOR3(Direction->x,Direction->y,-Direction->z),
        0.0f,
        0.0f,
        0.0f,
        0.0f,
        0.0f,
        0.0f,
        0.0f
    };
    device->SetLight(m_Index,&light);
}


END_NAMESPACE_3D
/******************************************************************************
|   End of File
*******************************************************************************/