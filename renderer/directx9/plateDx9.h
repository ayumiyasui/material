/*
# plateDx9.h

- 板クラス(3D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __3D_PLATE_Dx9_H__
#define __3D_PLATE_Dx9_H__

#include "directx9.h"
#include "../3d/plate.h"

NAMESPACE_3D

    class FactoryDx9;

class PlateDx9 : public Plate
{
private:
    // フレンドクラス
    //--------------------------------------------------------------------
    friend FactoryDx9;

    // メンバ変数
    //--------------------------------------------------------------------
    LPDIRECT3DVERTEXBUFFER9 m_VertexBuffer;     // 頂点バッファ

    // @brief  : コンストラクタ
    // @param  : 板サイズ
    //         : 親クラス
    //         : 描画するのか？
    //--------------------------------------------------------------------
    PlateDx9( const Quad &, IDraw *, bool );

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~PlateDx9();

    // @brief  : 頂点の変更
    //--------------------------------------------------------------------
    void SetVertex( void ) override;

    // @brief  : 描画
    //--------------------------------------------------------------------
    void LateDraw( void ) override;
};



END_NAMESPACE_3D
#endif //  __3D_PLATE_Dx9_H__
/******************************************************************************
|   End of File
*******************************************************************************/