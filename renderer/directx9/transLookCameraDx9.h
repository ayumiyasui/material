/*
# transLookCamera.h

- 行列変換クラス(3D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __3D_TRANS_LOOKCAMERA_Dx9_H__
#define __3D_TRANS_LOOKCAMERA_Dx9_H__

#include "directx9.h"
#include "../3d/transLookCamera3d.h"

NAMESPACE_3D

    class FactoryDx9;

class TransLookCameraDx9 : public TransLookCamera
{
private:
    // フレンドクラス
    //--------------------------------------------------------------------
    friend FactoryDx9;

    // メンバ変数
    //--------------------------------------------------------------------
    D3DXMATRIX m_Matrix;

    // @brief  : コンストラクタ
    // @param  : カメラ
    //         : 親クラス
    //         : 描画するのか？
    //--------------------------------------------------------------------
    TransLookCameraDx9( Camera *, IDraw *, bool );

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~TransLookCameraDx9();

    // @brief  : 行列の作成
    //--------------------------------------------------------------------
    void CreateMatrix(void);

    // @brief  : 描画
    //--------------------------------------------------------------------
    void Draw(void) override;

    // @brief  : 描画
    //--------------------------------------------------------------------
    void LateDraw(void) override;
};



END_NAMESPACE_3D
#endif //  __3D_TRANS_LOOKCAMERA_Dx9_H__
/******************************************************************************
|   End of File
*******************************************************************************/