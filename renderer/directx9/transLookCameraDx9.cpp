/*
# transLookCamera.cpp

- 行列変換クラス(3D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "transLookCameraDx9.h"

#include "../3d/camera.h"

NAMESPACE_3D
// @brief  : コンストラクタ
// @param  : カメラ
//         : 親クラス
//         : 描画するのか？
//--------------------------------------------------------------------
TransLookCameraDx9::TransLookCameraDx9( Camera *_cam, IDraw *_parent, bool _is_draw ) :
    TransLookCamera(_cam,_parent,_is_draw)
{
    D3DXMatrixIdentity(&m_Matrix);
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
TransLookCameraDx9::~TransLookCameraDx9()
{
}

// @brief  : 描画
//--------------------------------------------------------------------
void TransLookCameraDx9::Draw(void)
{
    // デバイスの取得
    auto &directx = DirectX9::Instance();
    LPDIRECT3DDEVICE9 device = directx.Device;

    // 行列の初期化
    Position = Position;
    CreateMatrix();

    // 行列の設定
    const D3DXMATRIX &parent = directx.Matrix();
    D3DXMATRIX mtx;
    D3DXMatrixMultiply(&mtx,&m_Matrix,&parent);
    device->SetTransform(D3DTS_WORLD,&m_Matrix);
    directx.PushMatirxStack(m_Matrix);
}

// @brief  : 描画
//--------------------------------------------------------------------
void TransLookCameraDx9::LateDraw(void)
{
    DirectX9 &directx = DirectX9::Instance();
    LPDIRECT3DDEVICE9 device = directx.Device;
    const D3DXMATRIX &mtx = directx.Matrix();
    directx.PopMatirxStack();
    device->SetTransform(D3DTS_WORLD,&mtx);
}

// @brief  : 行列の作成
//--------------------------------------------------------------------
void TransLookCameraDx9::CreateMatrix(void)
{
    const D3DXVECTOR3 pos = Vector3(Position);
    const Camera *cam = Cam();

    D3DXMATRIX mtx_rot;
    {
        D3DXVECTOR3 eye = -Vector3(cam->Eye);
        D3DXVECTOR3 at  = -Vector3(cam->At);
        D3DXVECTOR3 up  = -Vector3(cam->Up);

        D3DXMatrixLookAtLH(&mtx_rot,&eye,&at,&up);
        D3DXMatrixInverse(&mtx_rot,NULL,&mtx_rot);
        mtx_rot._14 = mtx_rot._24 = mtx_rot._34 =
            mtx_rot._41 = mtx_rot._42 = mtx_rot._43 = 0.0f;
        mtx_rot._44 = 1.0f;
    }

    D3DXMATRIX mtx_pos;
    D3DXMatrixTranslation(&mtx_pos,pos.x,pos.y,pos.z);
    D3DXMatrixMultiply(&m_Matrix,&mtx_rot,&mtx_pos);
}


END_NAMESPACE_3D
/******************************************************************************
|   End of File
*******************************************************************************/