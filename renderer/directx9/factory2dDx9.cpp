/*
# factory2dDx9.h

- ファクトリクラス(2D)
- 機種・ライブラリ差の吸収のために作成
- 便利な関数ではないので注意

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "factory2dDx9.h"

#include "directx9.h"
#include "camera2dDx9.h"
#include "transform2dDx9.h"
#include "sprite2dDx9.h"
#include "textureManagerDx9.h"

NAMESPACE_2D
// @brief  : コンストラクタ
//--------------------------------------------------------------------
FactoryDx9::FactoryDx9()
{
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
FactoryDx9::~FactoryDx9()
{
}

// @brief  : Cameraクラス掃き出し
//--------------------------------------------------------------------
Camera *FactoryDx9::CreateCamera( IDraw *_parent, bool _is_draw ) const
{
    return new CameraDx9(_parent,_is_draw);
}

// @brief  : Transformクラス掃き出し
//--------------------------------------------------------------------
Transform *FactoryDx9::CreateTransform( IDraw *_parent, bool _is_draw ) const
{
    return new TransformDx9(_parent,_is_draw);
}

// @brief  : Spriteクラス掃き出し
//--------------------------------------------------------------------
Sprite *FactoryDx9::CreateSprite(
    const Quad &_pol_size, const Quad &_tex_size,
    const std::string &_file_pass,
    IDraw *_parent, bool _is_draw ) const
{
    return new SpriteDx9(_pol_size,_tex_size,
        TextureManagerDx9::Instance().Load(_file_pass),
        _parent,_is_draw);
}



END_NAMESPACE_2D
/******************************************************************************
|   End of File
*******************************************************************************/