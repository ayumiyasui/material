/*
# materialDx9.h

- マテリアルクラス(3D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __3D_MATERIAL_Dx9_H__
#define __3D_MATERIAL_Dx9_H__

#include "directx9.h"
#include "../3d/material.h"

NAMESPACE_3D
class MaterialNormalDx9 : public MaterialNormal
{
public:
    // @brief  : コンストラクタ
    // @param  : 親クラス
    //         : 描画するのか？
    //--------------------------------------------------------------------
    MaterialNormalDx9( IDraw *, bool );

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~MaterialNormalDx9();

    // @brief  : 描画
    //--------------------------------------------------------------------
    void Draw(void) override;
};


END_NAMESPACE_3D
#endif //  __3D_MATERIAL_Dx9_H__
/******************************************************************************
|   End of File
*******************************************************************************/