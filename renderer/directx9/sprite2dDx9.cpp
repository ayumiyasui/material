/*
# sprite2dDx9.h

- スプライトクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "sprite2dDx9.h"

#include "directx9.h"

NAMESPACE_2D
// 定数
//--------------------------------------------------------------------
const DWORD SpriteDx9::VERTEX_FVF = ( D3DFVF_XYZ | D3DFVF_DIFFUSE | D3DFVF_TEX1 );

// @brief  : コンストラクタ
// @param  : ポリゴンの大きさ
//         : テクスチャ座標の位置
//         : テクスチャ
//         : 親クラス
//         : 描画するのか？
//--------------------------------------------------------------------
SpriteDx9::SpriteDx9( const Quad &_pol_size, const Quad &_tex_size,
            const Texture &_tex,
            IDraw *_parent, bool _is_draw ) :
    Sprite(_pol_size,_tex_size,_tex,_parent,_is_draw)
{
    HRESULT hr;
    LPDIRECT3DDEVICE9 device = DirectX9::Instance().Device;

    hr = device->CreateVertexBuffer(
        sizeof(VERTEX) * 4,
        D3DUSAGE_WRITEONLY, // 使用方法
        VERTEX_FVF,         // フォーマット
        D3DPOOL_MANAGED,    // バッファを保持するメモリクラスを指定
        &m_VertexBuffer,    // 頂点バッファのポインタ
        NULL
    );

    VERTEX *vtx;
    m_VertexBuffer->Lock(0,0,(void**)&vtx,0);
    for( auto i = 0 ; i < 4 ; ++i )
    {
        vtx[i].pos.z = 0.0f;
    }
    m_VertexBuffer->Unlock();

    SetPolygonQuad(_pol_size);
    SetTextureQuad(_tex_size);
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
SpriteDx9::~SpriteDx9()
{
    m_VertexBuffer->Release();
}

// @brief  : ポリゴンの大きさをセット
// @param  : ポリゴンの大きさ
//--------------------------------------------------------------------
void SpriteDx9::SetPolygonQuad( const Quad &_size )
{
    Sprite::SetPolygonQuad(_size);

    VERTEX *vtx;
    m_VertexBuffer->Lock(0,0,(void**)&vtx,0);

    vtx[0].pos.x = _size.left;
    vtx[0].pos.y = _size.top;
    vtx[1].pos.x = _size.right;
    vtx[1].pos.y = _size.top;
    vtx[2].pos.x = _size.left;
    vtx[2].pos.y = _size.bottom;
    vtx[3].pos.x = _size.right;
    vtx[3].pos.y = _size.bottom;

    m_VertexBuffer->Unlock();
}

// @brief  : テクスチャの大きさをセット
// @param  : テクスチャ
//--------------------------------------------------------------------
void SpriteDx9::SetTextureQuad( const Quad &_size )
{
    Sprite::SetTextureQuad(_size);

    VERTEX *vtx;
    m_VertexBuffer->Lock(0,0,(void**)&vtx,0);

    vtx[0].tex.x = _size.left;
    vtx[0].tex.y = _size.top;
    vtx[1].tex.x = _size.right;
    vtx[1].tex.y = _size.top;
    vtx[2].tex.x = _size.left;
    vtx[2].tex.y = _size.bottom;
    vtx[3].tex.x = _size.right;
    vtx[3].tex.y = _size.bottom;

    m_VertexBuffer->Unlock();
}

// @brief  : 描画
//--------------------------------------------------------------------
void SpriteDx9::Draw( void )
{
    HRESULT hr;
    LPDIRECT3DDEVICE9 device = DirectX9::Instance().Device;

    auto col = static_cast<D3DXCOLOR>(static_cast<Color>(PolygonColor));
    VERTEX *vtx;
    m_VertexBuffer->Lock(0,0,(void**)&vtx,0);
    for( auto i = 0 ; i < 4 ; ++i )
    {
        vtx[i].col = col;
    }
    m_VertexBuffer->Unlock();

    ((Texture)Tex)->Set();

    hr = device->SetStreamSource(0,m_VertexBuffer,0,sizeof(VERTEX));
    hr = device->SetFVF(VERTEX_FVF);
    hr = device->DrawPrimitive(D3DPT_TRIANGLESTRIP,0,2);
}

END_NAMESPACE_2D
/******************************************************************************
|   End of File
*******************************************************************************/