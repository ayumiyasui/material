/*
# directx9.h

- DirectX9レンダラークラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#ifdef _WIN32
#pragma once
#ifndef __DIRECTX9_H__
#define __DIRECTX9_H__

//--- インクルード ------------------------------------------------------------
#include "../../app/win32.h"
#define _USE_MATH_DEFINES           // 円周率を使えるようにする
#define D3D_DEBUG_INFO             // デバックでIUnkownの中身を見る

#include <stack>
#include <d3dx9.h>                  // DirectXライブラリver9.x
#include <DxErr.h>                  // DirectXエラー

#include "../renderer.h"
#include "../../base/tempSingleton.h"
#include "../../base/tempGetter.h"
#include "../../app/app.h"

//--- ライブラリのリンク ------------------------------------------------------
#pragma comment ( lib,"d3d9.lib"  ) // DirectX 9.0
#if defined(DEBUG) || defined(_DEBUG)
#pragma comment ( lib,"d3dx9d.lib" ) // DirectX 9.x
#else
#pragma comment ( lib,"d3dx9.lib" ) // DirectX 9.x
#endif
#pragma comment ( lib,"dxguid.lib") // DirectXコンポーメント使用に必要
#pragma comment( lib, "dxerr.lib" ) // DirectXエラー

//--- マクロ定義 ---------------------------------------------------------------
#include "../../app/win32Macro.h"


class DirectX9 : public IRenderer, public TempSingleton<DirectX9>
{
public:
    // プロパティ
    //--------------------------------------------------------------------
    TempGetter<LPDIRECT3DDEVICE9> Device;   // デバイス

    // @brief  : 描画
    //--------------------------------------------------------------------
    void Draw( void ) override;

    // @brief  : スクリーンショット
    // @param  : スクリーンショット名
    //--------------------------------------------------------------------
    void ScreenShort( const std::string & ) const override;

    // @brief  : 行列スタック操作
    //--------------------------------------------------------------------
    void PushMatirxStack(const D3DXMATRIX &_mtx){m_Matrix.push(_mtx);};
    void PopMatirxStack(void){m_Matrix.pop();};
    const D3DXMATRIX &Matrix(void) const{return m_Matrix.top();};

private:
    // フレンドクラス
    //--------------------------------------------------------------------
    friend TempSingleton<DirectX9>;

    // メンバ変数
    //--------------------------------------------------------------------
    LPDIRECT3D9       direct3d;         // DirectX9
    LPDIRECT3DDEVICE9 direct3d_device;  // DirectX9デバイス
    HWND              window;           // ウィンドウ
    std::stack<D3DXMATRIX> m_Matrix;    // 行列スタック

#ifdef _DEBUG
    // メンバ変数
    //--------------------------------------------------------------------
    LPD3DXFONT m_DebugFont;
#endif

    // @brief  : コンストラクタ
    // @param  : 幅
    //         : 高さ
    //         : フルスクリーンにするのか
    //         : ウィンドウクラス
    //--------------------------------------------------------------------
    DirectX9( float _width = SCREEN_WIDTH, float _height = SCREEN_HEIGHT, bool = CONFIG_IS_FULL, HWND _window = App::Instance().Win32->GetHWnd() );

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~DirectX9();

    // @brief  : Direct3Dオブジェクトの生成
    // @return : 成功(turue),失敗(false)
    //--------------------------------------------------------------------
    bool CreateDirect3D( void );

    // @brief  : ディスプレイ設定の作成 - ウィンドウ
    // @return : ディスプレイ設定
    //--------------------------------------------------------------------
    D3DPRESENT_PARAMETERS Window( void );

    // @brief  : ディスプレイ設定の作成 - フルスクリーン
    // @return : ディスプレイ設定
    //--------------------------------------------------------------------
    D3DPRESENT_PARAMETERS FullScreen( void );

    // @brief  : ディスプレイ設定の取得
    // @return : ディスプレイ情報
    //--------------------------------------------------------------------
    D3DDISPLAYMODE Display( void );

    // @brief  : 描画,頂点計算をGPUで行う
    // @param  : ウィンドウハンドル
    //         : ディスプレイ設定
    // @return : 対応している(true),していない(false)
    //--------------------------------------------------------------------
    bool DeviceHALHardware( HWND, D3DPRESENT_PARAMETERS & );

    // @brief  : 描画をGPU,頂点計算をCPUで行う
    // @param  : ウィンドウハンドル
    //         : ディスプレイ設定
    // @return : 対応している(true),していない(false)
    //--------------------------------------------------------------------
    bool DeviceHALSoftware( HWND, D3DPRESENT_PARAMETERS & );

    // @brief  : 描画,頂点計算をCPUで行う
    // @param  : ウィンドウハンドル
    //         : ディスプレイ設定
    // @return : 対応している(true),していない(false)
    //--------------------------------------------------------------------
    bool DeviceREFSoftware( HWND, D3DPRESENT_PARAMETERS & );
};



#endif //  __DIRECTX9_H__
#endif // _WIN32
/******************************************************************************
|   End of File
*******************************************************************************/