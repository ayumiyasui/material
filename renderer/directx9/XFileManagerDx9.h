/*
# XFileManager.h

- Xファイルマネージャ

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __XFILEMANAGER_Dx9_H__
#define __XFILEMANAGER_Dx9_H__

#include <string>
#include "../3d/XFile.h"
#include "../../base/tempNodeSingleton.h"

class XFileManagerDx9 : public TempNodeSingleton<XFileManagerDx9>
{
public:
    // @brief  : 読み込み
    // @param  : 親
    //         : 描画するのか
    //         : ファイル名
    //--------------------------------------------------------------------
    _3d::XFile *Load( IDraw *, bool,const std::string & );

protected:
    // フレンドクラス
    //--------------------------------------------------------------------
    friend TempNodeSingleton<XFileManagerDx9>;

    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    XFileManagerDx9();

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~XFileManagerDx9();
};



#endif  // __XFILEMANAGER_Dx9_H__
/******************************************************************************
|   End of File
*******************************************************************************/