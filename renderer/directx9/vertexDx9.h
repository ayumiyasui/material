/*
# vertexDx9.h

- 頂点クラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __VERTEX_DX9_H__
#define __VERTEX_DX9_H__

#include "directx9.h"

namespace Vertex3D {
// 定数
//--------------------------------------------------------------------
static const DWORD FVF = ( D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1 );

// 構造体
//--------------------------------------------------------------------
struct VERTEX
{
    D3DXVECTOR3 pos;    // 頂点座標
    D3DXVECTOR3 nor;    // 法線
    D3DXVECTOR2 tex;    // テクスチャ座標
};

};



#endif //  __3D_PMD_NOBONE_H__
/******************************************************************************
|   End of File
*******************************************************************************/