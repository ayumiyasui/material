/*
# textureManagerDx9.cpp

- テクスチャマネージャクラス(DirectX9)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード ------------------------------------------------------------
#define _CRT_SECURE_NO_WARNINGS // strcpy警告回避
#include "directx9.h"
#include "textureManagerDx9.h"
#include <assert.h>



//--- マクロ定義 --------------------------------------------------------------
#define DEVICE DirectX9::Instance().Device  // LPDIRECT3DDEVICE9

// Dx9テクスチャクラス
//--------------------------------------------------------------------
class DxTexture9 : public ITexture
{
public:
    DxTexture9( const std::string &_name, Node *_parent ) :
        ITexture(_name,_parent),
        m_Data(nullptr)
    {
        if(_name.empty()) return;
        HRESULT hr = D3DXCreateTextureFromFile( DEVICE, _name.c_str(), &m_Data );
        if(FAILED(hr)) m_Data = nullptr;
    };

    ~DxTexture9()
    {
        if(m_Data) m_Data->Release();
    };

    void Set( void ) override
    {
        LPDIRECT3DDEVICE9 device = DEVICE;
        if(m_Data) device->SetTexture(0,m_Data);
        if(!m_Data) device->SetTexture(0,nullptr);
    };

private:
    LPDIRECT3DTEXTURE9  m_Data;
};

// @brief  : コンストラクタ
//--------------------------------------------------------------------
TextureManagerDx9::TextureManagerDx9()
{
    DxTexture9 *null = new DxTexture9(std::string(),this);
    null->Retain();
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
TextureManagerDx9::~TextureManagerDx9()
{
}

// @brief  : ファイルの読み込み
// @param  : ファイル名
//--------------------------------------------------------------------
Texture TextureManagerDx9::Load( const std::string &_name )
{
    if(_name.empty()) return TempSmartPtr<ITexture>(dynamic_cast<ITexture *>(Begin()));

    // ファイルの検索
    Node *i = Begin();
    while(i)
    {
        ITexture *tex = (ITexture *)i;
        std::string name = tex->Name;
        if( name.compare(_name) == 0 ) return TempSmartPtr<ITexture>(tex);
        i = i->Next();
    }

    // 作成
    return TempSmartPtr<ITexture>(new DxTexture9(_name,this));
}



/******************************************************************************
|   End of File
*******************************************************************************/