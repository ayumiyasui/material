/*
# transformQuat3dDx9.h

- 行列変換クラス(3D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __3D_TRANSFORMQUAT_Dx9_H__
#define __3D_TRANSFORMQUAT_Dx9_H__

#include "../3d/transform3dQuat.h"

NAMESPACE_3D

    class FactoryDx9;

class TransformQuatDx9 : public TransformQuat
{
private:
    // フレンドクラス
    //--------------------------------------------------------------------
    friend FactoryDx9;

    // @brief  : コンストラクタ
    // @param  : 親クラス
    //         : 描画するのか？
    //--------------------------------------------------------------------
    TransformQuatDx9( IDraw *, bool );

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~TransformQuatDx9();

    // @brief  : 行列の計算
    //--------------------------------------------------------------------
    Matrix3D CreateMatrix(void) const override;

    // @brief  : 描画
    //--------------------------------------------------------------------
    void Draw( void ) override;

    // @brief  : 描画
    //--------------------------------------------------------------------
    void LateDraw( void ) override;
};



END_NAMESPACE_3D
#endif //  __3D_TRANSFORMQUAT_Dx9_H__
/******************************************************************************
|   End of File
*******************************************************************************/