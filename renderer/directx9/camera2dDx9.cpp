/*
# camera2dDx9.cpp

- カメラクラス(2D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "camera2dDx9.h"

#include "directx9.h"

NAMESPACE_2D
// @brief  : コンストラクタ
// @param  : 親クラス
//         : 描画するのか？
//--------------------------------------------------------------------
CameraDx9::CameraDx9( IDraw *_parent, bool _is_draw ) :
    Camera(_parent,_is_draw)
{
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
CameraDx9::~CameraDx9()
{
}

// @brief  : 描画
//--------------------------------------------------------------------
void CameraDx9::Draw( void )
{
    LPDIRECT3DDEVICE9 device = DirectX9::Instance().Device;
    HRESULT hr;

    hr = device->Clear( 0, NULL, ( D3DCLEAR_ZBUFFER ),
        D3DCOLOR_RGBA( 128, 128, 128, 255 ),
        1.0f,
        0 );

    hr = device->SetRenderState(D3DRS_LIGHTING,FALSE);
    hr = device->SetRenderState(D3DRS_ZENABLE,FALSE);
    hr = device->SetRenderState(D3DRS_ZWRITEENABLE,FALSE);

    D3DXMATRIX projection;
    D3DXMatrixOrthoOffCenterLH( &projection,
        Size->left, Size->right,
        Size->bottom , Size->top,
        Near, Far );
    hr = device->SetTransform( D3DTS_PROJECTION, &projection );

    D3DXMATRIX view;
    D3DXMatrixIdentity(&view);
    hr = device->SetTransform( D3DTS_VIEW, &view );

    const D3DVIEWPORT9 viewport = {
        static_cast<DWORD>(Viewport->left),  static_cast<DWORD>(Viewport->top),
        static_cast<DWORD>(Viewport->right), static_cast<DWORD>(Viewport->bottom),
        0.0f,1.0f
    };

    hr = device->SetViewport(&viewport);
}

// @brief  : 描画
// @note   : 子が終わった後に動く、後片付け用
//--------------------------------------------------------------------
void CameraDx9::LateDraw( void )
{
    LPDIRECT3DDEVICE9 device = DirectX9::Instance().Device;
    HRESULT hr;

    hr = device->SetRenderState(D3DRS_LIGHTING,TRUE);
    hr = device->SetRenderState(D3DRS_ZENABLE,TRUE);
    hr = device->SetRenderState(D3DRS_ZWRITEENABLE,TRUE);

}



END_NAMESPACE_2D
/******************************************************************************
|   End of File
*******************************************************************************/