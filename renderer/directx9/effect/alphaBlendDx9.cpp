/*
# alphaBlendDx9.cpp

- アルファブレンドクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード -------------------------------------------------------------
#include "alphaBlend.h"
#include <assert.h>

namespace material {
namespace effect {
// 定数
//--------------------------------------------------------------------
const AlphaBlendDx9::RENDER_STATE AlphaBlendDx9::SETTING[MAX_TYPE] =
{
    {FALSE, D3DBLENDOP_ADD     , D3DBLEND_SRCALPHA , D3DBLEND_INVSRCALPHA },  // TYPE_NONE
    {TRUE , D3DBLENDOP_ADD     , D3DBLEND_SRCALPHA , D3DBLEND_INVSRCALPHA },  // TYPE_ALPHA
    {TRUE , D3DBLENDOP_ADD     , D3DBLEND_SRCALPHA , D3DBLEND_ONE         },  // TYPE_ADD
    {TRUE , D3DBLENDOP_SUBTRACT, D3DBLEND_SRCALPHA , D3DBLEND_ONE         },  // TYPE_DIFFERENCE
    {TRUE , D3DBLENDOP_ADD     , D3DBLEND_DESTCOLOR, D3DBLEND_ZERO        },  // TYPE_MULTPLY
};

// グローバル変数
//--------------------------------------------------------------------
std::stack<AlphaBlendDx9::RENDER_STATE> AlphaBlendDx9::g_Stack;    // レンダーステートスタック

// @brief  : コンストラクタ
//--------------------------------------------------------------------
AlphaBlendDx9::AlphaBlendDx9(IDraw *_parent) :
    AlphaBlend(_parent)
{
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
AlphaBlendDx9::~AlphaBlendDx9()
{
}

// @brief  : 描画
//--------------------------------------------------------------------
void AlphaBlendDx9::Draw(void)
{
    // スタックにプッシュ
    g_Stack.push(GetRenderState());

    // 設定
    SetRenderState(SETTING[Get()]);
}

// @brief  : 描画
//--------------------------------------------------------------------
void AlphaBlendDx9::LateDraw(void)
{
    // 元に戻す
    SetRenderState(g_Stack.top());

    // スタックからポップ
    g_Stack.pop();
}

// @brief  : デバイスからレンダーステートを取得
//--------------------------------------------------------------------
AlphaBlendDx9::RENDER_STATE AlphaBlendDx9::GetRenderState(void)
{
    RENDER_STATE re;
    DWORD get_data = 0;

    // デバイスの取得
    LPDIRECT3DDEVICE9 device = DirectX9::Instance().Device;
    HRESULT hr;

    // AlphaBlendEnable
    hr = device->GetRenderState(D3DRS_ALPHABLENDENABLE,&get_data);
    assert(SUCCEEDED(hr));
    re.AlphaBlendEnable = (BOOL)get_data;

    if(re.AlphaBlendEnable)
    {
        // BlendOP
        hr = device->GetRenderState(D3DRS_BLENDOP         ,&get_data);
        assert(SUCCEEDED(hr));
        re.BlendOP = (D3DBLENDOP)get_data;

        // SrcBlend
        hr = device->GetRenderState(D3DRS_SRCBLEND        ,&get_data);
        assert(SUCCEEDED(hr));
        re.SrcBlend = (D3DBLEND)get_data;

        // DestBlend
        hr = device->GetRenderState(D3DRS_DESTBLEND       ,&get_data);
        assert(SUCCEEDED(hr));
        re.DestBlend = (D3DBLEND)get_data;
    }

    return re;
}

// @brief  : デバイスからレンダーステートを保存
//--------------------------------------------------------------------
void AlphaBlendDx9::SetRenderState(const RENDER_STATE &_set)
{
    LPDIRECT3DDEVICE9 device = DirectX9::Instance().Device;

    HRESULT hr;
    hr = device->SetRenderState(D3DRS_ALPHABLENDENABLE,_set.AlphaBlendEnable);
    assert(SUCCEEDED(hr));

    if(_set.AlphaBlendEnable)
    {
        hr = device->SetRenderState(D3DRS_BLENDOP         ,_set.BlendOP         );
        assert(SUCCEEDED(hr));
        hr = device->SetRenderState(D3DRS_SRCBLEND        ,_set.SrcBlend        );
        assert(SUCCEEDED(hr));
        hr = device->SetRenderState(D3DRS_DESTBLEND       ,_set.DestBlend       );
        assert(SUCCEEDED(hr));
    }
}



}; //namespace effect
}; //namespace material
/******************************************************************************
|   End of File
*******************************************************************************/