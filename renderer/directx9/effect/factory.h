/*
# factory.h

- ファクトリクラス(エフェクト)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef MATERIAL_RENDERERDX9_EFFECT_FACTORY_H_
#define MATERIAL_RENDERERDX9_EFFECT_FACTORY_H_

#include "../../effect/factory.h"
#include "../directx9.h"

namespace material {
namespace effect {
class FactoryDx9 : public Factory
{
public:
    // @brief  : アルファブレンドの書き出し
    //--------------------------------------------------------------------
    AlphaBlend *CreateAlphaBlend(IDraw *_parent) const override;

    // @brief  : カリングの書き出し
    //--------------------------------------------------------------------
    Curring *CreateCurring(IDraw *_parent) const override;

private:
    // フレンドクラス
    //--------------------------------------------------------------------
    friend DirectX9;

    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    FactoryDx9();

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~FactoryDx9();

};


}; // namespace effect
}; // namespace material
#endif //  MATERIAL_RENDERERDX9_EFFECT_FACTORY_H_
/******************************************************************************
|   End of File
*******************************************************************************/