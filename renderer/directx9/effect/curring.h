/*
# curring.h

- カリングクラス(DirectX9)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef MATERIAL_RENDERER_EFFECT_CURRING_DX9_H_
#define MATERIAL_RENDERER_EFFECT_CURRING_DX9_H_

#include <stack>
#include "../directx9.h"
#include "../../effect/curring.h"

namespace material {
namespace effect {
class CurringDx9 : public Curring
{
public:
    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    CurringDx9(IDraw *);

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~CurringDx9();

    // @brief  : アルファブレンド変更
    // @param  : 種類
    //--------------------------------------------------------------------
    void Set(TYPE) override;

private:
    // メンバ変数
    //--------------------------------------------------------------------
    D3DCULL m_Setting;  // カリング設定

    // @brief  : 描画
    //--------------------------------------------------------------------
    void Draw(void) override;

    // @brief  : 描画
    //--------------------------------------------------------------------
    void LateDraw(void) override;
};


}; //namespace effect
}; //namespace material
#endif //  MATERIAL_RENDERER_EFFECT_CURRING_DX9_H_
/******************************************************************************
|   End of File
*******************************************************************************/