/*
# alphaBlend.h

- アルファブレンドクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef MATERIAL_RENDERERDX9_EFFECT_ALPHABLEND_H_
#define MATERIAL_RENDERERDX9_EFFECT_ALPHABLEND_H_

#include <stack>
#include "../../effect/alphaBlend.h"
#include "../directx9.h"

namespace material {
namespace effect {
class FactoryDx9;

class AlphaBlendDx9 : public AlphaBlend
{
private:
    // フレンドクラス
    //--------------------------------------------------------------------
    friend FactoryDx9;

    // 構造体
    //--------------------------------------------------------------------
    struct RENDER_STATE
    {
        BOOL       AlphaBlendEnable;
        D3DBLENDOP BlendOP;
        D3DBLEND   SrcBlend;
        D3DBLEND   DestBlend;
    };

    // 定数
    //--------------------------------------------------------------------
    static const RENDER_STATE SETTING[MAX_TYPE];

    // グローバル変数
    //--------------------------------------------------------------------
    static std::stack<RENDER_STATE> g_Stack;    // レンダーステートスタック

    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    AlphaBlendDx9(IDraw *);

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~AlphaBlendDx9();

    // @brief  : 描画
    //--------------------------------------------------------------------
    void Draw(void) override;

    // @brief  : 描画
    //--------------------------------------------------------------------
    void LateDraw(void) override;

    // @brief  : デバイスからレンダーステートを取得
    //--------------------------------------------------------------------
    static RENDER_STATE GetRenderState(void);

    // @brief  : デバイスからレンダーステートを保存
    //--------------------------------------------------------------------
    static void SetRenderState(const AlphaBlendDx9::RENDER_STATE &);
};


}; //namespace effect
}; //namespace material
#endif //  MATERIAL_RENDERERDX9_EFFECT_ALPHABLEND_H_
/******************************************************************************
|   End of File
*******************************************************************************/