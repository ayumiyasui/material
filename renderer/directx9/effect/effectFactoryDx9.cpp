/*
# effectFactoryDx9.cpp

- ファクトリクラス(エフェクト)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード -------------------------------------------------------------
#include "factory.h"
#include "alphaBlend.h"
#include "curring.h"

namespace material {
namespace effect {
// @brief  : コンストラクタ
//--------------------------------------------------------------------
FactoryDx9::FactoryDx9()
{
}


// @brief  : デストラクタ
//--------------------------------------------------------------------
FactoryDx9::~FactoryDx9()
{
}

// @brief  : アルファブレンドの書き出し
//--------------------------------------------------------------------
AlphaBlend *FactoryDx9::CreateAlphaBlend(IDraw *_parent) const
{
    return new AlphaBlendDx9(_parent);
}

// @brief  : カリングの書き出し
//--------------------------------------------------------------------
Curring *FactoryDx9::CreateCurring(IDraw *_parent) const
{
    return new CurringDx9(_parent);
}


}; // namespace effect
}; // namespace material
/******************************************************************************
|   End of File
*******************************************************************************/