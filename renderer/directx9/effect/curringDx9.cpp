/*
# curringDx9.cpp

- カリングクラス(DirectX)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード -------------------------------------------------------------
#include "curring.h"
#include <assert.h>

namespace material {
namespace effect {
// @brief  : コンストラクタ
//--------------------------------------------------------------------
CurringDx9::CurringDx9(IDraw *_parent) :
    Curring(_parent)
{
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
CurringDx9::~CurringDx9()
{
}

// MAX_TYPE@brief  : アルファブレンド変更
// @param  : 種類
//--------------------------------------------------------------------
void CurringDx9::Set(TYPE _type)
{
    const D3DCULL setting[MAX_TYPE] =
    {
        D3DCULL_NONE,
        D3DCULL_CCW,
        D3DCULL_CW
    };
    m_Setting = setting[_type];
}

// @brief  : 描画
//--------------------------------------------------------------------
void CurringDx9::Draw(void)
{
    DirectX9          &directx = DirectX9::Instance();
    LPDIRECT3DDEVICE9  device   = directx.Device;
    HRESULT hr = device->SetRenderState(D3DRS_CULLMODE,m_Setting);
    assert(SUCCEEDED(hr));
}

// @brief  : 描画
//--------------------------------------------------------------------
void CurringDx9::LateDraw(void)
{
    DirectX9          &directx = DirectX9::Instance();
    LPDIRECT3DDEVICE9  device   = directx.Device;
    HRESULT hr = device->SetRenderState(D3DRS_CULLMODE,D3DCULL_CCW);
    assert(SUCCEEDED(hr));
}



}; //namespace effect
}; //namespace material
/******************************************************************************
|   End of File
*******************************************************************************/