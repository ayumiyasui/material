/*
# materialDx9.cpp

- マテリアルクラス(3D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "materialDx9.h"


NAMESPACE_3D
// @brief  : コンストラクタ
// @param  : 親クラス
//         : 描画するのか？
//--------------------------------------------------------------------
MaterialNormalDx9::MaterialNormalDx9( IDraw *_parent, bool _is_draw ) :
    MaterialNormal(_parent,_is_draw)
{
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
MaterialNormalDx9::~MaterialNormalDx9()
{
}

// @brief  : 描画
//--------------------------------------------------------------------
void MaterialNormalDx9::Draw(void)
{
    LPDIRECT3DDEVICE9 device = DirectX9::Instance().Device;
    HRESULT hr;

    const D3DMATERIAL9 mat =
    {
        D3DXCOLOR(Color(Diffuse)),
        D3DXCOLOR(Color(Ambient)),
        D3DXCOLOR(Color(Specular)),
        D3DXCOLOR(Color(Emissive)),
        0.0f
    };

    hr = device->SetMaterial(&mat);
}



END_NAMESPACE_3D
/******************************************************************************
|   End of File
*******************************************************************************/