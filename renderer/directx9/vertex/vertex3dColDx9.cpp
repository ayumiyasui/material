/*
# vertex3dCol.h

- 頂点クラス(カラー付きVertex3d)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "vertex3dCol.h"

namespace material {
namespace vertex {

// 定数
//--------------------------------------------------------------------
const DWORD Vertex3dColDx9::FVF = ( D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_DIFFUSE | D3DFVF_TEX1 ); // 頂点フォーマット

// @brief  : コンストラクタ
// @param  : 頂点数
//--------------------------------------------------------------------
Vertex3dColDx9::Vertex3dColDx9(const variable::u32 &_count) :
    IVertex3dCol(_count),
    m_Buffer(nullptr)
{
    LPDIRECT3DDEVICE9 device = DirectX9::Instance().Device;
    HRESULT hr = device->CreateVertexBuffer (
        sizeof(VERTEX_DX) * _count,
        D3DUSAGE_WRITEONLY,
        FVF,
        D3DPOOL_MANAGED,
        &m_Buffer,
        NULL
    );
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
Vertex3dColDx9::~Vertex3dColDx9()
{
    if(m_Buffer) m_Buffer->Release();
}

// @brief  : 描画
// @param  : 描画の種類
//--------------------------------------------------------------------
void Vertex3dColDx9::Draw(const TYPE &_type)
{
    const D3DPRIMITIVETYPE type[MAX_TYPE] =
    {
        D3DPT_POINTLIST,
        D3DPT_LINELIST,
        D3DPT_LINESTRIP,
        D3DPT_TRIANGLELIST,
        D3DPT_TRIANGLESTRIP
    };
    const variable::u32 count = Count;
    const UINT primitive_count[MAX_TYPE] =
    {
        count,
        count/2,
        count-1,
        count/3,
        count-2
    };

    HRESULT hr;
    LPDIRECT3DDEVICE9 device = DirectX9::Instance().Device;

    hr = device->SetStreamSource(0,m_Buffer,0,sizeof(VERTEX_DX));
    hr = device->SetFVF(FVF);
    hr = device->DrawPrimitive(type[_type],0,primitive_count[_type]);
}

// @brief  : すべての頂点の編集
// @param  : 関数(param:編集する頂点情報,頂点ナンバー)
//--------------------------------------------------------------------
void Vertex3dColDx9::Edit(std::function<void (VERTEX &,const variable::u32 &)> _func)
{
    VERTEX_DX *vtx;
    HRESULT hr = m_Buffer->Lock(0,0,(void **)&vtx,0);

    for(UINT i = 0;i < Count; ++i)
    {
        VERTEX vertex;
        _func(vertex,i);
        vtx[i].pos = vertex.pos;
        vtx[i].nor = vertex.nor;
        vtx[i].col = D3DXCOLOR(vertex.col);
        vtx[i].tex = vertex.tex;
    }

    hr = m_Buffer->Unlock();
}

// @brief  : 頂点の編集
// @param  : 関数(param:頂点ナンバー return 設定する頂点)
//--------------------------------------------------------------------
void Vertex3dColDx9::EditPos(std::function<Vector3 (const Vector3 &,const variable::u32 &)> _func)
{
    VERTEX_DX *vtx;
    HRESULT hr = m_Buffer->Lock(0,0,(void **)&vtx,0);

    for(UINT i = 0;i < Count; ++i)
    {
        Vector3 vec(vtx[i].pos.x,vtx[i].pos.y,-vtx[i].pos.z);
        vtx[i].pos = _func(vec,i);
    }

    hr = m_Buffer->Unlock();
}

// @brief  : 法線の編集
// @param  : 関数(param:頂点ナンバー return 設定する色)
//--------------------------------------------------------------------
void Vertex3dColDx9::EditNor(std::function<Vector3 (const Vector3 &,const variable::u32 &)> _func)
{
    VERTEX_DX *vtx;
    HRESULT hr = m_Buffer->Lock(0,0,(void **)&vtx,0);

    for(UINT i = 0;i < Count; ++i)
    {
        Vector3 vec(vtx[i].nor.x,vtx[i].nor.y,-vtx[i].nor.z);
        vtx[i].nor = _func(vec,i);
    }

    hr = m_Buffer->Unlock();
}

// @brief  : カラーの編集
// @param  : 関数(param:頂点ナンバー return 設定する色)
//--------------------------------------------------------------------
void Vertex3dColDx9::EditCol(std::function<Color (const Color &,const variable::u32 &)> _func)
{
    VERTEX_DX *vtx;
    HRESULT hr = m_Buffer->Lock(0,0,(void **)&vtx,0);

    for(UINT i = 0;i < Count; ++i)
    {
        D3DXCOLOR d3dxcol = vtx[i].col;
        Color col(d3dxcol.r,d3dxcol.g,d3dxcol.b,d3dxcol.a);
        vtx[i].col = D3DXCOLOR(_func(col,i));
    }

    hr = m_Buffer->Unlock();
}

// @brief  : テクスチャ座標編集
// @param  : 関数(param:頂点ナンバー return 設定する頂点)
//--------------------------------------------------------------------
void Vertex3dColDx9::EditTex(std::function<Vector2 (const Vector2 &,const variable::u32 &)> _func)
{
    VERTEX_DX *vtx;
    HRESULT hr = m_Buffer->Lock(0,0,(void **)&vtx,0);

    for(UINT i = 0;i < Count; ++i)
    {
        Vector3 vec(vtx[i].tex.x,vtx[i].tex.y);
        vtx[i].tex = _func(vec,i);
    }

    hr = m_Buffer->Unlock();
}

// @brief  : すべての頂点の取り出し
// @return : 頂点(注：使い終わったらdelete[]すること)
//--------------------------------------------------------------------
vertex::IVertex3dCol::VERTEX *Vertex3dColDx9::Export(void) const
{
    auto vertex = new VERTEX[Count];

    VERTEX_DX *vtx;
    HRESULT hr = m_Buffer->Lock(0,0,(void **)&vtx,D3DLOCK_READONLY);

    for(UINT i=0; i < Count ; ++i)
    {
        vertex[i].pos.x =  vtx[i].pos.x;
        vertex[i].pos.y =  vtx[i].pos.y;
        vertex[i].pos.z = -vtx[i].pos.z;

        vertex[i].nor.x =  vtx[i].nor.x;
        vertex[i].nor.y =  vtx[i].nor.y;
        vertex[i].nor.z = -vtx[i].nor.z;

        D3DXCOLOR col = vtx[i].col;
        vertex[i].col.r =  col.r;
        vertex[i].col.g =  col.g;
        vertex[i].col.b =  col.b;
        vertex[i].col.a =  col.a;

        vertex[i].tex.x =  vtx[i].tex.x;
        vertex[i].tex.y =  vtx[i].tex.y;
    }

    hr = m_Buffer->Unlock();

    return vertex;
}

// @brief  : 頂点の取り出し
// @return : 頂点(注：使い終わったらdelete[]すること)
//--------------------------------------------------------------------
Vector3 *Vertex3dColDx9::ExportPos(void) const
{
    auto vertex = new Vector3[Count];

    VERTEX_DX *vtx;
    HRESULT hr = m_Buffer->Lock(0,0,(void **)&vtx,D3DLOCK_READONLY);

    for(UINT i=0; i < Count ; ++i)
    {
        vertex[i].x =  vtx[i].pos.x;
        vertex[i].y =  vtx[i].pos.y;
        vertex[i].z = -vtx[i].pos.z;
    }

    hr = m_Buffer->Unlock();

    return vertex;
}

// @brief  : 法線の取り出し
// @return : 法線(注：使い終わったらdelete[]すること)
//--------------------------------------------------------------------
Vector3 *Vertex3dColDx9::ExportNor(void) const
{
    auto vertex = new Vector3[Count];

    VERTEX_DX *vtx;
    HRESULT hr = m_Buffer->Lock(0,0,(void **)&vtx,D3DLOCK_READONLY);

    for(UINT i=0; i < Count ; ++i)
    {
        vertex[i].x =  vtx[i].nor.x;
        vertex[i].y =  vtx[i].nor.y;
        vertex[i].z = -vtx[i].nor.z;
    }

    hr = m_Buffer->Unlock();

    return vertex;
}

// @brief  : カラーの取り出し
// @return : カラー(注：使い終わったらdelete[]すること)
//--------------------------------------------------------------------
Color *Vertex3dColDx9::ExportCol(void) const
{
    auto vertex = new Color[Count];

    VERTEX_DX *vtx;
    HRESULT hr = m_Buffer->Lock(0,0,(void **)&vtx,D3DLOCK_READONLY);

    for(UINT i=0; i < Count ; ++i)
    {
        D3DXCOLOR col(vtx[i].col);
        vertex[i].r = col.r;
        vertex[i].g = col.g;
        vertex[i].b = col.b;
        vertex[i].a = col.a;
    }

    hr = m_Buffer->Unlock();

    return vertex;
}

// @brief  : テクスチャ座標の取り出し
// @return : テクスチャ座標(注：使い終わったらdelete[]すること)
//--------------------------------------------------------------------
Vector2 *Vertex3dColDx9::ExportTex(void) const
{
    auto vertex = new Vector2[Count];

    VERTEX_DX *vtx;
    HRESULT hr = m_Buffer->Lock(0,0,(void **)&vtx,D3DLOCK_READONLY);

    for(UINT i=0; i < Count ; ++i)
    {
        vertex[i].x = vtx[i].tex.x;
        vertex[i].y = vtx[i].tex.y;
    }

    hr = m_Buffer->Unlock();

    return vertex;
}

// @brief  : バッファの取得
//--------------------------------------------------------------------
LPDIRECT3DVERTEXBUFFER9 Vertex3dColDx9::GetBuffer(void) const{return m_Buffer;}


};  // namespace vertex
};  // namespace material
/******************************************************************************
|   End of File
*******************************************************************************/