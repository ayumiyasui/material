/*
# vertexFactoryDx9.cpp

- 頂点バッファ生成クラス(DirectX9)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード -------------------------------------------------------------
#include "factory.h"
#include "vertex3dCol.h"

namespace material {
namespace vertex {

// @brief  : コンストラクタ
//--------------------------------------------------------------------
FactoryDx9::FactoryDx9()
{
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
FactoryDx9::~FactoryDx9()
{
}

// @brief  : カラー付き頂点の作成
// @param  : 頂点数
//--------------------------------------------------------------------
vertex::IVertex3dCol *FactoryDx9::Create(const variable::u32 &_count) const
{
    return new Vertex3dColDx9(_count);
}


};  // namespace vertex
};  // namespace material
/******************************************************************************
|   End of File
*******************************************************************************/