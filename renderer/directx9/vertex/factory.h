/*
# factory.h

- 頂点バッファ生成クラス(DirectX9)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef MATERIAL_VERTEX_FACTORY_DX9_H_
#define MATERIAL_VERTEX_FACTORY_DX9_H_

#include "../directx9.h"
#include "../../vertex/factory.h"

namespace material {
namespace vertex {

class FactoryDx9 : public vertex::Factory
{
public:
    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    FactoryDx9();

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~FactoryDx9();

    // @brief  : カラー付き頂点の作成
    // @param  : 頂点数
    //--------------------------------------------------------------------
    IVertex3dCol *Create(const variable::u32 &) const override;
};


};  // namespace vertex
};  // namespace material
#endif //  MATERIAL_VERTEX_FACTORY_DX9_H_
/******************************************************************************
|   End of File
*******************************************************************************/