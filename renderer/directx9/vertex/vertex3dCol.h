/*
# vertex3dColDx9.h

- 頂点クラス(カラー付きVertex3d)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef MATERIAL_DIRECTX9_VERTEX_3D_COL_H_
#define MATERIAL_DIRECTX9_VERTEX_3D_COL_H_

#include "../directx9.h"
#include "../../vertex/iVertex3dCol.h"

namespace material {
namespace vertex {

class Vertex3dColDx9 : public IVertex3dCol
{
public:
    // 定数
    //--------------------------------------------------------------------
    static const DWORD FVF; // 頂点フォーマット

    // DirectX頂点の構造体
    //--------------------------------------------------------------------
    struct VERTEX_DX
    {
        D3DXVECTOR3 pos;    // 位置
        D3DXVECTOR3 nor;    // 法線
        D3DCOLOR col;    // カラー
        D3DXVECTOR2 tex;    // テクスチャ
    };

    // @brief  : コンストラクタ
    // @param  : 頂点数
    //--------------------------------------------------------------------
    Vertex3dColDx9(const variable::u32 &);

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~Vertex3dColDx9();

    // @brief  : 描画
    // @param  : 描画の種類
    //--------------------------------------------------------------------
    void Draw(const TYPE &) override;

    // @brief  : すべての頂点の編集
    // @param  : 関数(param:編集する頂点情報,頂点ナンバー)
    //--------------------------------------------------------------------
    void Edit(std::function<void (VERTEX &,const variable::u32 &)>) override;

    // @brief  : 頂点の編集
    // @param  : 関数(param:頂点ナンバー return 設定する頂点)
    //--------------------------------------------------------------------
    void EditPos(std::function<Vector3 (const Vector3 &,const variable::u32 &)>) override;

    // @brief  : 法線の編集
    // @param  : 関数(param:頂点ナンバー return 設定する色)
    //--------------------------------------------------------------------
    void EditNor(std::function<Vector3 (const Vector3 &,const variable::u32 &)>) override;

    // @brief  : カラーの編集
    // @param  : 関数(param:頂点ナンバー return 設定する色)
    //--------------------------------------------------------------------
    void EditCol(std::function<Color (const Color &,const variable::u32 &)>) override;

    // @brief  : テクスチャ座標編集
    // @param  : 関数(param:頂点ナンバー return 設定する頂点)
    //--------------------------------------------------------------------
    void EditTex(std::function<Vector2 (const Vector2 &,const variable::u32 &)>) override;

    // @brief  : すべての頂点の取り出し
    // @return : 頂点(注：使い終わったらdelete[]すること)
    //--------------------------------------------------------------------
    VERTEX *Export(void) const override;

    // @brief  : 頂点の取り出し
    // @return : 頂点(注：使い終わったらdelete[]すること)
    //--------------------------------------------------------------------
    Vector3 *ExportPos(void) const override;

    // @brief  : 法線の取り出し
    // @return : 法線(注：使い終わったらdelete[]すること)
    //--------------------------------------------------------------------
    Vector3 *ExportNor(void) const override;

    // @brief  : カラーの取り出し
    // @return : カラー(注：使い終わったらdelete[]すること)
    //--------------------------------------------------------------------
    Color *ExportCol(void) const override;

    // @brief  : テクスチャ座標の取り出し
    // @return : テクスチャ座標(注：使い終わったらdelete[]すること)
    //--------------------------------------------------------------------
    Vector2 *ExportTex(void) const override;

    // @brief  : バッファの取得
    //--------------------------------------------------------------------
    LPDIRECT3DVERTEXBUFFER9 GetBuffer(void) const;

private:
    // メンバ変数
    //--------------------------------------------------------------------
    LPDIRECT3DVERTEXBUFFER9 m_Buffer;   // 頂点バッファ
};



};  // namespace vertex
};  // namespace material
#endif //  MATERIAL_DIRECTX9_VERTEX_3D_COL_H_
/******************************************************************************
|   End of File
*******************************************************************************/