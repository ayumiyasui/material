/*
# factory3dDx9.cpp

- ファクトリクラス(3D)
- 機種・ライブラリ差の吸収のために作成

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "camera3dDx9.h"

#include "directx9.h"

NAMESPACE_3D
// @brief  : コンストラクタ
//--------------------------------------------------------------------
CameraDx9::CameraDx9( IDraw *_parent, bool _is_draw ) :
    Camera(_parent,_is_draw)
{
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
CameraDx9::~CameraDx9()
{
}

// @brief  : 描画
//--------------------------------------------------------------------
void CameraDx9::Draw( void )
{
    HRESULT hr;
    LPDIRECT3DDEVICE9 device = DirectX9::Instance().Device;

    hr = device->Clear( 0, NULL, ( D3DCLEAR_ZBUFFER ),
        D3DCOLOR_RGBA( 128, 128, 128, 255 ),
        1.0f,
        0 );

    // プロジェクション行列の作成
    {
        D3DXMATRIX projection;
        D3DXMatrixPerspectiveFovLH(&projection,FovY,Aspect,Near,Far);
        hr = device->SetTransform(D3DTS_PROJECTION,&projection);
    }

    // ビュー行列の作成
    {
        D3DXMATRIX view;
        D3DXVECTOR3 eye = Vector3(Eye);
        D3DXVECTOR3 at  = Vector3(At);
        D3DXVECTOR3 up  = Vector3(Up);
        D3DXMatrixLookAtLH(&view,&eye,&at,&up);
        hr = device->SetTransform(D3DTS_VIEW,&view);
    }

    // ビューポート行列の作成
    {
        const Quad rect = Viewport;
        const D3DVIEWPORT9 viewport =
        {
            static_cast<DWORD>(rect.left),
            static_cast<DWORD>(rect.top),
            static_cast<DWORD>(rect.right  - rect.left),
            static_cast<DWORD>(rect.bottom - rect.top),
            0.0f,
            1.0f
        };
        hr = device->SetViewport(&viewport);
    }
}



END_NAMESPACE_3D
/******************************************************************************
|   End of File
*******************************************************************************/