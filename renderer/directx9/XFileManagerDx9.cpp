/*
# XFileManager.h

- Xファイルマネージャ

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "XFileManagerDx9.h"

#include <assert.h>
#include "directx9.h"
#include "textureManagerDx9.h"
#include "../../base/filePass.h"
#include "../../base/tempUseCount.h"

//--- データクラス -------------------------------------------------------------
class XFileData : public TempUseCount<XFileData>, public Node
{
public:
    struct MATERIAL
    {
        D3DMATERIAL9 mat;
        Texture      tex;
    };
    LPD3DXMESH  m_Mesh;
    MATERIAL   *m_Material;
    DWORD       m_MaxMaterial;
    FilePass    m_FilePass;

    XFileData( const std::string &_file_name ) :
      Node(&XFileManagerDx9::Instance()),
      m_FilePass(_file_name)
    {
        if(_file_name.empty()) return;

        HRESULT hr;

        // ファイル読み込み
        LPD3DXBUFFER lpD3DBuff;
        hr = D3DXLoadMeshFromX( _file_name.c_str(), // ファイルパス
            D3DXMESH_SYSTEMMEM,
            DirectX9::Instance().Device,    // デバイス
            NULL,
            &lpD3DBuff,                 // マテリアルバッファ
            NULL,
            &m_MaxMaterial,             // マテリアル数
            &m_Mesh );                  // メッシュ
        assert( SUCCEEDED(hr));

        // マテリアルの数だけ領域を確保する
        m_Material = new MATERIAL[m_MaxMaterial];

        // マテリアルバッファから、マテリアル情報のポインタを取得
        D3DXMATERIAL *pMatBuff = ( D3DXMATERIAL * )lpD3DBuff->GetBufferPointer();

        // マテリアルの数だけループ
        for( unsigned i = 0 ; i < m_MaxMaterial ; ++i )
        {
            // マテリアルをコピー
            m_Material[i].mat = pMatBuff[i].MatD3D;

            // テクスチャファイルの読み込み
            m_Material[i].tex = TextureManagerDx9::Instance().Load(pMatBuff[i].pTextureFilename);
        }

        // マテリアルバッファの解放
        if( lpD3DBuff )
        {
            lpD3DBuff->Release();
            lpD3DBuff = NULL;
        }
    };

    ~XFileData()
    {
        if(m_MaxMaterial >  0) delete[] m_Material;
        if(m_MaxMaterial == 0) delete m_Material;
        m_Mesh->Release();
    }
};

//--- マテリアルクラス ----------------------------------------------------------
class XFileMaterial : public _3d::Material
{
public:
    XFileMaterial( XFileData::MATERIAL *_mat, TempSmartPtr<XFileData> _data,
        IDraw *_parent,bool _is_draw) :
        Material(_parent,_is_draw),
        m_Material(_mat),
        m_Data(_data)
    {
    };

    ~XFileMaterial(){};

private:
    XFileData::MATERIAL     *m_Material;
    TempSmartPtr<XFileData> m_Data;

    void Draw( void ) override
    {
        HRESULT hr;
        LPDIRECT3DDEVICE9 device = DirectX9::Instance().Device;

        hr = device->SetMaterial(&m_Material->mat);
        m_Material->tex->Set();
    }
};

//--- メッシュクラス ------------------------------------------------------------
class XFileMesh : public _3d::Mesh
{
public:
    XFileMesh(unsigned _mesh, TempSmartPtr<XFileData> _data,
        IDraw *_parent, bool _is_draw) :
        Mesh(_parent,_is_draw),
        m_Mesh(_mesh),
        m_Data(_data)
    {
    };

    ~XFileMesh(){};

private:
    unsigned                m_Mesh;
    TempSmartPtr<XFileData> m_Data;

    void Draw(void) override
    {
        HRESULT hr = m_Data->m_Mesh->DrawSubset(m_Mesh);
    };
};

//--- 描画クラス ---------------------------------------------------------------
class XFileDraw : public _3d::XFile
{
public:
    XFileDraw(IDraw *_parent,bool _is_draw) :
      XFile(_parent,_is_draw)
    {
    };

    ~XFileDraw(){};
};

// @brief  : コンストラクタ
//--------------------------------------------------------------------
XFileManagerDx9::XFileManagerDx9()
{
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
XFileManagerDx9::~XFileManagerDx9()
{
}

// @brief  : 読み込み
//--------------------------------------------------------------------
_3d::XFile *XFileManagerDx9::Load( IDraw *_parent, bool _is_draw, const std::string &_file_name )
{
    assert(!_file_name.empty());

    // ファイルの検索
    XFileData *data = nullptr;
    Node *i = Begin();
    while(i)
    {
        data = dynamic_cast<XFileData *>(i);
        std::string name = data->m_FilePass;
        if( name.compare(_file_name) == 0 ) break;
        i = i->Next();
        data = nullptr;
    }

    // ファイルの作成
    if(!data)
    {
        data = new XFileData(_file_name);
    }

    // Xファイルクラスの作成
    XFileDraw *re = new XFileDraw(_parent,_is_draw);
    for(unsigned i = 0 ; i < data->m_MaxMaterial ; ++i)
    {
        new XFileMaterial(&data->m_Material[i],data,re,true);
        new XFileMesh(i,data,re,true);
    }

    // 作成
    return re;
}



/******************************************************************************
|   End of File
*******************************************************************************/