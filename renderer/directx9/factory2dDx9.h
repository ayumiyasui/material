/*
# factory2dDx9.h

- ファクトリクラス(2D)
- 機種・ライブラリ差の吸収のために作成
- 便利な関数ではないので注意

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __2D_FACTORY_Dx9_H__
#define __2D_FACTORY_Dx9_H__

#include <string>
#include "../2d/factory.h"

class DirectX9;

NAMESPACE_2D
class FactoryDx9 : public IFactory
{
public:
    // @brief  : Cameraクラス掃き出し
    //--------------------------------------------------------------------
    Camera *CreateCamera( IDraw *, bool ) const override;

    // @brief  : Transformクラス掃き出し
    // @return : new Transform
    //--------------------------------------------------------------------
    Transform* CreateTransform( IDraw *, bool ) const override;

    // @brief  : Spriteクラス掃き出し
    //--------------------------------------------------------------------
    Sprite *CreateSprite(
        const Quad &, const Quad &,
        const std::string &,
        IDraw *, bool ) const override;

protected:
    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~FactoryDx9();

private:
    // フレンドクラス
    //--------------------------------------------------------------------
    friend DirectX9;

    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    FactoryDx9();
};



END_NAMESPACE_2D
#endif //  __2D_FACTORY_Dx9_H__
/******************************************************************************
|   End of File
*******************************************************************************/