/*
# plateDx9.cpp

- 板クラス(3D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "plateDx9.h"

#include "materialDx9.h"
#include "vertexDx9.h"

NAMESPACE_3D
// @brief  : コンストラクタ
// @param  : 板サイズ
//         : 親クラス
//         : 描画するのか？
//--------------------------------------------------------------------
PlateDx9::PlateDx9( const Quad &_size, IDraw *_parent, bool _is_draw ) :
    Plate(new MaterialNormalDx9(&TempNodeSingleton<IDraw>::Instance(),true),_size,_parent,_is_draw)
{
    LPDIRECT3DDEVICE9 device = DirectX9::Instance().Device;
    HRESULT hr;

    // 頂点バッファの生成
    hr = device->CreateVertexBuffer(
        sizeof(Vertex3D::VERTEX) * 4, // バッファサイズ
        D3DUSAGE_WRITEONLY,           // 頂点バッファの使用方法
        Vertex3D::FVF,                // 使用する頂点フォーマット
        D3DPOOL_MANAGED,              // バッファを保持するメモリクエストを指定
        &m_VertexBuffer,              // 頂点のバッファの先頭アドレス
        NULL );

    // 頂点の設定
    const Quad size = Size;
    const Quad tex  = TexUV;
    Vertex3D::VERTEX *vtx;

    hr = m_VertexBuffer->Lock(0,0,(void**)&vtx,0);

    vtx[0].pos.x = size.left;
    vtx[0].pos.y = size.top;
    vtx[0].pos.z = 0.0f;
    vtx[0].nor   = D3DXVECTOR3(0.0f,0.0f,1.0f);
    vtx[0].tex.x = tex.left;
    vtx[0].tex.y = tex.top;

    vtx[1].pos.x = size.right;
    vtx[1].pos.y = size.top;
    vtx[1].pos.z = 0.0f;
    vtx[1].nor   = D3DXVECTOR3(0.0f,0.0f,1.0f);
    vtx[1].tex.x = tex.right;
    vtx[1].tex.y = tex.top;

    vtx[2].pos.x = size.left;
    vtx[2].pos.y = size.bottom;
    vtx[2].pos.z = 0.0f;
    vtx[2].nor   = D3DXVECTOR3(0.0f,0.0f,1.0f);
    vtx[2].tex.x = tex.left;
    vtx[2].tex.y = tex.bottom;

    vtx[3].pos.x = size.right;
    vtx[3].pos.y = size.bottom;
    vtx[3].pos.z = 0.0f;
    vtx[3].nor   = D3DXVECTOR3(0.0f,0.0f,1.0f);
    vtx[3].tex.x = tex.right;
    vtx[3].tex.y = tex.bottom;

    hr = m_VertexBuffer->Unlock();
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
PlateDx9::~PlateDx9()
{
    if(m_VertexBuffer) m_VertexBuffer->Release();
}

// @brief  : 頂点の変更
//--------------------------------------------------------------------
void PlateDx9::SetVertex( void )
{
    HRESULT hr;
    const Quad size = Size;
    const Quad tex  = TexUV;
    Vertex3D::VERTEX *vtx;

    hr = m_VertexBuffer->Lock(0,0,(void**)&vtx,0);

    vtx[0].pos.x = size.left;
    vtx[0].pos.y = size.top;
    vtx[0].tex.x = tex.left;
    vtx[0].tex.y = tex.top;

    vtx[1].pos.x = size.right;
    vtx[1].pos.y = size.top;
    vtx[1].tex.x = tex.right;
    vtx[1].tex.y = tex.top;

    vtx[2].pos.x = size.left;
    vtx[2].pos.y = size.bottom;
    vtx[2].tex.x = tex.left;
    vtx[2].tex.y = tex.bottom;

    vtx[3].pos.x = size.right;
    vtx[3].pos.y = size.bottom;
    vtx[3].tex.x = tex.right;
    vtx[3].tex.y = tex.bottom;

    hr = m_VertexBuffer->Unlock();
}

// @brief  : 描画
//--------------------------------------------------------------------
void PlateDx9::LateDraw( void )
{
    HRESULT hr;
    LPDIRECT3DDEVICE9 device = DirectX9::Instance().Device;

    const D3DCOLORVALUE none = {0.0f,0.0f,0.0f,0.0f};

    ((Texture)Tex)->Set();

    hr = device->SetStreamSource(0,m_VertexBuffer,0,sizeof(Vertex3D::VERTEX));
    hr = device->SetFVF(Vertex3D::FVF);
    hr = device->DrawPrimitive(D3DPT_TRIANGLESTRIP,0,2);
}


END_NAMESPACE_3D
/******************************************************************************
|   End of File
*******************************************************************************/