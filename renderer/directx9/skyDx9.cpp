/*
# skyDx9.h

- 空クラス(3D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "skyDx9.h"

#include "tristMeshDx9.h"


NAMESPACE_3D
// @brief  : コンストラクタ
// @param  : テクスチャ
//         : 半径
//         : ブロック数横
//         : ブロック数縦
//          :親クラス
//         : 描画するのか？
//--------------------------------------------------------------------
SkyDx9::SkyDx9( Texture _texture,
    float _length, unsigned _x, unsigned _y,
    IDraw *_parent, bool _is_draw ) :
    Sky(_parent,_is_draw),
    m_Texture(_texture)
{
    TriStMeshDx9 *mesh = new TriStMeshDx9(_x,_y,this,true);
    LPDIRECT3DVERTEXBUFFER9 vertex_buffer = mesh->VertexBuffer();
    HRESULT hr;

    // 頂点
    D3DXVECTOR3 v3Pos  = D3DXVECTOR3( 0.0f, _length, 0.0f );
    D3DXMATRIX mtxRotZ;
    D3DXMatrixRotationZ( &mtxRotZ, D3DX_PI / _x );
    D3DXMATRIX mtxRotY;
    D3DXMatrixRotationY( &mtxRotY, -2.0f * D3DX_PI / _x );

    // 頂点設定
    TriStMeshDx9::VERTEX *vtx;
    hr = vertex_buffer->Lock(0,0,(void**)&vtx,0);
    unsigned i = 0;  // 頂点バッファの位置
    for( unsigned y = 0; y < _y + 1 ; ++y )
    {
        D3DXVECTOR3 v3Y = v3Pos;
        D3DXVECTOR3 v3Set = v3Y;
        for( unsigned x = 0 ; x < _x + 1 ; ++x )
        {
            D3DXVec3TransformCoord( &v3Set, &v3Set, &mtxRotY );
            vtx[i].pos = v3Set;
            D3DXVECTOR3 v3Nor;
            D3DXVec3Normalize( &v3Nor, &v3Set );
            vtx[i].nor = v3Nor;
            vtx[i].tex.x = v3Nor.x * 0.5f + 0.5f;;
            vtx[i].tex.y = v3Nor.y * 0.5f + 0.5f;;

            // 次の頂点へ
            i++;
        }
        D3DXVec3TransformCoord( &v3Pos, &v3Y, &mtxRotZ );
    }
    hr = vertex_buffer->Unlock();
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
SkyDx9::~SkyDx9()
{
}

// @brief  : 描画
//--------------------------------------------------------------------
void SkyDx9::Draw( void )
{
    LPDIRECT3DDEVICE9 device = DirectX9::Instance().Device;
    HRESULT hr;

    // マテリアルのセット
    const D3DMATERIAL9 mat =
    {
        static_cast<D3DXCOLOR>(static_cast<Color>(Diffuse)),
        D3DXCOLOR(0.0f,0.0f,0.0f,0.0f),
        D3DXCOLOR(0.0f,0.0f,0.0f,0.0f),
        D3DXCOLOR(0.0f,0.0f,0.0f,0.0f),
        0.0f
    };
    device->SetMaterial(&mat);

    // テクスチャの設定
    m_Texture->Set();

    // 深度バッファテストを切る
    hr = device->SetRenderState( D3DRS_ZWRITEENABLE, FALSE );
}

// @brief  : 描画
//--------------------------------------------------------------------
void SkyDx9::LateDraw( void )
{
    LPDIRECT3DDEVICE9 device = DirectX9::Instance().Device;
    HRESULT hr;

    // 深度バッファテストを入れる
    hr = device->SetRenderState( D3DRS_ZWRITEENABLE, TRUE );
}



END_NAMESPACE_3D
/******************************************************************************
|   End of File
*******************************************************************************/