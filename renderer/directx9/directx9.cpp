/*
# directx9.cpp

- DirectX9レンダラークラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード ------------------------------------------------------------
#include "directx9.h"

#include <string>
#include "../draw.h"
#include "textureManagerDx9.h"
#include "factory2dDx9.h"
#include "factory3dDx9.h"
#include "effect/factory.h"
#include "vertex/factory.h"
#include "XFileManagerDx9.h"
#include "PMDNoBoneManagerDx9.h"


// @brief  : コンストラクタ
// @param  : 幅
//         : 高さ
//         : フルスクリーンにするのか
//         : ウィンドウクラス
//--------------------------------------------------------------------
DirectX9::DirectX9( float _width, float _height, bool _is_full, HWND _window ) :
    IRenderer(_width,_height,new _2d::FactoryDx9, new _3d::FactoryDx9, new effect::FactoryDx9, new vertex::FactoryDx9()),
    Device(direct3d_device),
    window(_window)
{
    // Direct3Dオブジェクトの生成
    if( !CreateDirect3D() ) return ;

    // クライアント領域作成
    D3DPRESENT_PARAMETERS d3dpp = ( _is_full ) ? FullScreen() : Window();

    // 描画,頂点計算をGPUで行う
    if( !DeviceHALSoftware( _window, d3dpp ) )
    {
        // 描画をGPU,頂点計算をCPUで行う
        if( !DeviceHALSoftware( _window, d3dpp ) )
        {
            // 描画,頂点計算をCPUで行う
            if( !DeviceREFSoftware( _window, d3dpp ) )
            {
                // Graphic 9がありません
                MessageBox( _window, "このPCはDirectX 9に対応していません", "エラー", MB_OK );
                return ;
            }
        }
    }

    // レンダーステートの設定
    direct3d_device->SetRenderState(D3DRS_CULLMODE,         D3DCULL_CCW         );
    direct3d_device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE                );
    direct3d_device->SetRenderState(D3DRS_BLENDOP,          D3DBLENDOP_ADD      );
    direct3d_device->SetRenderState(D3DRS_SRCBLEND,         D3DBLEND_SRCALPHA   );
    direct3d_device->SetRenderState(D3DRS_DESTBLEND,        D3DBLEND_INVSRCALPHA);

    // サンプラーステートの設定
    direct3d_device->SetSamplerState( 0, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP );
    direct3d_device->SetSamplerState( 0, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP );
    direct3d_device->SetSamplerState( 0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR );
    direct3d_device->SetSamplerState( 0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR );

    // テクスチャステートメントの設定
    direct3d_device->SetTextureStageState( 0, D3DTSS_ALPHAOP, D3DTOP_MODULATE );
    direct3d_device->SetTextureStageState( 0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE );
    direct3d_device->SetTextureStageState( 0, D3DTSS_ALPHAARG2, D3DTA_CURRENT );

    // Node解放順序の制御
    TextureManagerDx9::Instance();
    XFileManagerDx9::Instance();
    PMDNoBoneManagerDx9::Instance();

    // 行列スタックの初期化
    {
        D3DXMATRIX mtx;
        D3DXMatrixIdentity(&mtx);
        m_Matrix.push(mtx);
    }

#if _DEBUG
    // デバック表示の準備
    const HRESULT hr =
        D3DXCreateFont(
            direct3d_device, 18, 0, 0, 0, FALSE,
            SHIFTJIS_CHARSET, OUT_DEFAULT_PRECIS,
            DEFAULT_QUALITY, DEFAULT_PITCH,
            "Terminal", &m_DebugFont );

    _ASSERT( SUCCEEDED( hr ) );
#endif
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
DirectX9::~DirectX9()
{
#if _DEBUG
    m_DebugFont->Release();
#endif
    if( direct3d_device )  direct3d_device->Release();
    if( direct3d ) direct3d->Release();
}

// @brief  : 描画
//--------------------------------------------------------------------
void DirectX9::Draw( void )
{
    HRESULT hr;

    // 画面のクリア
    const D3DXCOLOR back_color = Color(BackColor);
    hr = direct3d_device->Clear( 0, NULL, ( D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER ), back_color, 1.0f, 0 );

    // 描画開始
    hr = direct3d_device->BeginScene();
    if( FAILED( hr ) )
    {
        return ;
    }

    // 描画
    TempNodeSingleton<IDraw>::Instance().DrawAll();
#ifdef _DEBUG
    if(DebugIsDraw)
    {
        RECT rect = { 0, 0, (LONG)Width, (LONG)Height };

        // テキストの描画
        HRESULT hr = m_DebugFont->
            DrawText( NULL, DebugString, -1, &rect, DT_LEFT,
                      D3DCOLOR_RGBA( 0, 0, 0, 255 ) );

        // テキストのリセット
        ResetDebugString();
    }
#endif

    // 描画終了
    hr = direct3d_device->EndScene();

    // バッファ入れ替え
    hr = direct3d_device->Present( NULL, NULL, NULL, NULL );
}

// @brief  : スクリーンショット
// @param  : スクリーンショット名
//--------------------------------------------------------------------
void DirectX9::ScreenShort( const std::string &_str ) const
{
    // バックバッファを取得
    LPDIRECT3DSURFACE9 pBackBuff;
    direct3d_device->GetRenderTarget( 0, &pBackBuff );

    // スクショ出力
    D3DXSaveSurfaceToFile( _str.c_str(), D3DXIFF_BMP, pBackBuff, NULL, NULL );

    // 取得したものを解放
    pBackBuff->Release();
}

// @brief  : Direct3Dオブジェクトの生成
// @return : 成功(turue),失敗(false)
//--------------------------------------------------------------------
bool DirectX9::CreateDirect3D( void )
{
    direct3d = Direct3DCreate9( D3D_SDK_VERSION );
    if( direct3d == NULL )
    {
        // Graphic 9がありません
        const std::string not_directx_9( "DirectX 9.0がありません\nDirectX 9.0を入手してください" );
        MessageBox( NULL, not_directx_9.c_str(), "エラー", MB_OK );
        return false;
    }

    return true;
}


// @brief  : ディスプレイ設定の作成 - ウィンドウ
// @return : ディスプレイ設定
//--------------------------------------------------------------------
D3DPRESENT_PARAMETERS DirectX9::Window( void )
{
    // ウィンドウスタイルの変更
    SetWindowLong(window,GWL_STYLE,WS_OVERLAPPEDWINDOW | WS_VISIBLE);

    D3DPRESENT_PARAMETERS d3dpp;
    ZeroMemory( &d3dpp, sizeof( d3dpp ) );            // まっさらにする

    // 現在のディスプレイモードを取得
    const D3DDISPLAYMODE d3ddm = Display();

    // ウィンドウの大きさ
    d3dpp.BackBufferWidth =  (UINT)Width;        // ゲーム画面の幅
    d3dpp.BackBufferHeight = (UINT)Height;       // ゲーム画面の高さ

    // バックバッファのフォーマット
    d3dpp.BackBufferFormat = d3ddm.Format;   // カラーモード
    d3dpp.BackBufferCount = 1;               // バックバッファの数

                                             // バッファの入れ替え
    d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;

    // ウィンドウモード
    d3dpp.Windowed = TRUE;

    // デプスバッファステンシルバッファで作成
    d3dpp.EnableAutoDepthStencil = TRUE;
    d3dpp.AutoDepthStencilFormat = D3DFMT_D24S8;

    // ディスプレイアダプタによるリフレッシュレート
    d3dpp.FullScreen_RefreshRateInHz = 0;
    d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_ONE;    //プログラムのタイミングで

    return d3dpp;
}


// @brief  : ディスプレイ設定の作成 - フルスクリーン
// @return : ディスプレイ設定
//--------------------------------------------------------------------
D3DPRESENT_PARAMETERS DirectX9::FullScreen( void )
{
    // ウィンドウスタイルの変更
    SetWindowLong(window,GWL_STYLE, WS_POPUP | WS_VISIBLE);
    SetMenu(window,NULL);

    D3DPRESENT_PARAMETERS d3dpp;
    ZeroMemory( &d3dpp, sizeof( d3dpp ) );            // まっさらにする

                                                      //現在のディスプレイモードを取得
    const D3DDISPLAYMODE d3ddm = Display();

    // カラーモードのセット
    d3dpp.BackBufferFormat = d3ddm.Format;

    // バックバッファの数は1
    d3dpp.BackBufferCount = 1;

    d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;

    // ウィンドウモードにしない
    d3dpp.Windowed = FALSE;

    // デプスバッファステンシルバッファで作成
    d3dpp.EnableAutoDepthStencil = TRUE;

    // 16bitのテクスチャバッファを使用
    d3dpp.AutoDepthStencilFormat = D3DFMT_D24S8;

    // PCのタイミングで
    d3dpp.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;
    d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_DEFAULT;

    // バッファの幅
    d3dpp.BackBufferWidth = (UINT)Width;

    // バッファの高さ
    d3dpp.BackBufferHeight = (UINT)Height;

    return d3dpp;
}


// @brief  : ディスプレイ設定の取得
// @return : ディスプレイ情報
//--------------------------------------------------------------------
D3DDISPLAYMODE DirectX9::Display( void )
{
    D3DDISPLAYMODE d3ddm;
    const HRESULT hr = direct3d->GetAdapterDisplayMode( D3DADAPTER_DEFAULT, &d3ddm );
    if( FAILED( hr ) )
    {
        // ディスプレイ情報がないヨ！
        MessageBox( NULL, "ディスプレイがありません", "エラー", MB_OK );
    }
    return d3ddm;
}

// @brief  : 描画,頂点計算をGPUで行う
// @param  : ウィンドウハンドル
//         : ディスプレイ設定
// @return : 対応している(true),していない(false)
//--------------------------------------------------------------------
bool DirectX9::DeviceHALHardware( HWND _window, D3DPRESENT_PARAMETERS &_d3dpp )
{
    const HRESULT hr = direct3d->
        CreateDevice(
            D3DADAPTER_DEFAULT,                     // アダプター
            D3DDEVTYPE_HAL,                         // 描画
            _window,                                // ウィンドウのハンドル
            D3DCREATE_HARDWARE_VERTEXPROCESSING,    // 頂点の計算
            &_d3dpp,                                // ディスプレイ情報
            &direct3d_device
            );
    return SUCCEEDED( hr );
}

// @brief  : 描画をGPU,頂点計算をCPUで行う
// @param  : ウィンドウハンドル
//         : ディスプレイ設定
// @return : 対応している(true),していない(false)
//--------------------------------------------------------------------
bool DirectX9::DeviceHALSoftware( HWND _window, D3DPRESENT_PARAMETERS &_d3dpp )
{
    const HRESULT hr = direct3d->
        CreateDevice(
            D3DADAPTER_DEFAULT,                     // アダプター
            D3DDEVTYPE_HAL,                         // 描画
            _window,                                // ウィンドウのハンドル
            D3DCREATE_SOFTWARE_VERTEXPROCESSING,    // 頂点の計算
            &_d3dpp,                                // ディスプレイ情報
            &direct3d_device
            );
    return SUCCEEDED( hr );
}

// @brief  : 描画,頂点計算をCPUで行う
// @param  : ウィンドウハンドル
//         : ディスプレイ設定
// @return : 対応している(true),していない(false)
//--------------------------------------------------------------------
bool DirectX9::DeviceREFSoftware( HWND _window, D3DPRESENT_PARAMETERS &_d3dpp )
{
    const HRESULT hr = direct3d->
        CreateDevice(
            D3DADAPTER_DEFAULT,                     // アダプター
            D3DDEVTYPE_REF,                         // 描画
            _window,                                // ウィンドウのハンドル
            D3DCREATE_SOFTWARE_VERTEXPROCESSING,    // 頂点の計算
            &_d3dpp,                                // ディスプレイ情報
            &direct3d_device
            );
    return SUCCEEDED( hr );
}



/******************************************************************************
|   End of File
*******************************************************************************/