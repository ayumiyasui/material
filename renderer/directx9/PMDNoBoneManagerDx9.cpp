/*
# PMDNoBoneManagerDx9.cpp

- PMDクラス骨なし(3D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#define _CRT_SECURE_NO_WARNINGS
#include "PMDNoBoneManagerDx9.h"

#include <stdio.h>
#include "../../base/node.h"
#include "../../base/tempUseCount.h"
#include "../../base/tempSmartPtr.h"
#include "../../math/vector3.h"
#include "../3d/mesh.h"
#include "../3d/material.h"
#include "../3d/PMDNobone.h"
#include "../resourceLoader.h"
#include "directx9.h"
#include "vertexDx9.h"
#include "textureManagerDx9.h"

//--- データクラス -------------------------------------------------------------
class PMDDataDx9 : public TempUseCount<PMDDataDx9>, public Node
{
public:
    struct MATERIAL
    {
        D3DMATERIAL9 mat;
        Texture      tex;
        UINT         start_index;
        UINT         max_index;
    };
    LPDIRECT3DVERTEXBUFFER9  m_VertexBuffer;
    UINT                     m_MaxVertex;
    LPDIRECT3DINDEXBUFFER9   m_IndexBuffer;
    UINT                     m_MaxIndex;
    MATERIAL                *m_Material;
    DWORD                    m_MaxMaterial;
    FilePass                 m_FilePass;

    PMDDataDx9( const std::string &_file_name ) :
      Node(&PMDNoBoneManagerDx9::Instance()),
      m_FilePass(_file_name)
    {
        if(_file_name.empty()) return;

        PMDLoader::DATA *data = PMDLoader::Load(_file_name);
        if(!data) return;

        LoadMesh(data);
        LoadMaterial(data);
        delete[] data->vertex;
        delete[] data->face_vert_index;
        delete[] data->bone;
        delete[] data->material;
        delete data;
    };
    ~PMDDataDx9()
    {
        if(m_Material) delete[] m_Material;
        if(m_IndexBuffer)  m_IndexBuffer->Release();
        if(m_VertexBuffer) m_VertexBuffer->Release();
    }

private:
    void LoadMesh(const PMDLoader::DATA *_data)
    {
        LPDIRECT3DDEVICE9 device = DirectX9::Instance().Device;
        HRESULT hr;

        // 頂点バッファの作成
        m_MaxVertex = _data->vert_count.u;
        hr = device->CreateVertexBuffer(
            sizeof(Vertex3D::VERTEX) * _data->vert_count.u,
            D3DUSAGE_WRITEONLY,                // 頂点バッファの使用方法
            Vertex3D::FVF,                     // 使用する頂点フォーマット
            D3DPOOL_MANAGED,                   // バッファを保持するメモリクエストを指定
            &m_VertexBuffer,                   // 頂点のバッファの先頭アドレス
            NULL );

        Vertex3D::VERTEX *vtx;
        hr = m_VertexBuffer->Lock(0,0,(void**)&vtx,0);
        for( unsigned int i = 0; i < _data->vert_count.u ; i++)
        {
            vtx[i].pos.x =   _data->vertex[i].pos[0].f;
            vtx[i].pos.y =   _data->vertex[i].pos[1].f;
            vtx[i].pos.z =  -_data->vertex[i].pos[2].f;
            vtx[i].nor.x =  _data->vertex[i].normal_vec[0].f;
            vtx[i].nor.y =  _data->vertex[i].normal_vec[1].f;
            vtx[i].nor.z = -_data->vertex[i].normal_vec[2].f;
            vtx[i].tex.x =  _data->vertex[i].uv[0].f;
            vtx[i].tex.y =  _data->vertex[i].uv[1].f;
        }
        hr = m_VertexBuffer->Unlock();

        // インデックスバッファの作成
        m_MaxIndex = _data->face_vert_count.u;
        hr = device->CreateIndexBuffer(
            sizeof( WORD ) * m_MaxIndex,    // バッファサイズ
            D3DUSAGE_WRITEONLY,             // 頂点バッファの使用方法
            D3DFMT_INDEX16,                 // 使用する頂点フォーマット
            D3DPOOL_MANAGED,                // バッファを保持するメモリクエストを指定
            &m_IndexBuffer,                 // 頂点インデックスのバッファの先頭アドレス
            NULL );

        WORD *idx;
        hr = m_IndexBuffer->Lock(0,0,(void**)&idx,0);
        for( unsigned int i=0; i < _data->face_vert_count.u ; i++)
        {
            idx[i] = _data->face_vert_index[i].u;
        }
        hr = m_IndexBuffer->Unlock();
    }
    void LoadMaterial(const PMDLoader::DATA *_data)
    {
        //マテリアル情報
        m_Material = new MATERIAL[_data->material_count.u];
        m_MaxMaterial = _data->material_count.u;
        DWORD start_index = 0;
        for( DWORD i = 0 ; i < m_MaxMaterial ; ++i )
        {
            // インデックス頂点設定
            m_Material[i].start_index = start_index;
            m_Material[i].max_index   = _data->material[i].face_vert_count.u;
            start_index              += _data->material[i].face_vert_count.u;

            // マテリアル設定
            m_Material[i].mat.Diffuse.r  = _data->material[i].diffuse_color[0].f;
            m_Material[i].mat.Diffuse.g  = _data->material[i].diffuse_color[1].f;
            m_Material[i].mat.Diffuse.b  = _data->material[i].diffuse_color[2].f;
            m_Material[i].mat.Diffuse.a  = _data->material[i].alpha.f;
            m_Material[i].mat.Specular.r = _data->material[i].specular_color[0].f;
            m_Material[i].mat.Specular.g = _data->material[i].specular_color[1].f;
            m_Material[i].mat.Specular.b = _data->material[i].specular_color[2].f;
            m_Material[i].mat.Specular.a = 1.0f;
            m_Material[i].mat.Ambient.r  = _data->material[i].mirror_color[0].f;
            m_Material[i].mat.Ambient.g  = _data->material[i].mirror_color[1].f;
            m_Material[i].mat.Ambient.b  = _data->material[i].mirror_color[2].f;
            m_Material[i].mat.Ambient.a  = 1.0f;
            m_Material[i].mat.Emissive   = D3DXCOLOR(0.0f,0.0f,0.0f,0.0f);
            m_Material[i].mat.Power      = _data->material[i].specularity.f;

            // テクスチャ設定
            if(_data->material[i].texture_file_name)
            {
                const std::string TEXTURE_PASS("data/TEXTURE/");
                const std::string name = TEXTURE_PASS + _data->material[i].texture_file_name;
                m_Material[i].tex  = TextureManagerDx9::Instance().Load(name);
            }
            else
            {
                m_Material[i].tex  = TextureManagerDx9::Instance().Load(std::string());
            }
        }
    };
};

//--- マテリアルクラス ----------------------------------------------------------
class PMDMaterialDx9 : public _3d::Material
{
public:
    PMDMaterialDx9( PMDDataDx9::MATERIAL *_mat, TempSmartPtr<PMDDataDx9> _data,
        IDraw *_parent,bool _is_draw) :
        Material(_parent,_is_draw),
        m_Material(_mat),
        m_Data(_data),
        m_MaxPrimitive(_mat->max_index/3)
    {
    };
    ~PMDMaterialDx9(){};

private:
    PMDDataDx9::MATERIAL    *m_Material;
    UINT                  m_MaxPrimitive;
    TempSmartPtr<PMDDataDx9> m_Data;

    void Draw( void ) override
    {
        HRESULT hr;
        LPDIRECT3DDEVICE9 device = DirectX9::Instance().Device;

        hr = device->SetMaterial(&m_Material->mat);
        m_Material->tex->Set();
        hr = device->DrawIndexedPrimitive(
            D3DPT_TRIANGLELIST,
            0,
            0,
            m_Data->m_MaxVertex,
            m_Material->start_index,
            m_MaxPrimitive);
    }
};

//--- メッシュクラス ------------------------------------------------------------
class PMDMeshDx9 : public _3d::Mesh
{
public:
    PMDMeshDx9(TempSmartPtr<PMDDataDx9> _data,
        IDraw *_parent, bool _is_draw) :
        Mesh(_parent,_is_draw),
        m_Data(_data)
    {
    };

    ~PMDMeshDx9(){};

private:
    TempSmartPtr<PMDDataDx9> m_Data;

    void Draw(void) override
    {
        LPDIRECT3DDEVICE9 device;
        m_Data->m_VertexBuffer->GetDevice(&device);
        HRESULT hr;

        hr = device->SetFVF(Vertex3D::FVF);
        hr = device->SetStreamSource(0,m_Data->m_VertexBuffer,0,sizeof(Vertex3D::VERTEX));
        hr = device->SetIndices(m_Data->m_IndexBuffer);
        hr = device->SetRenderState( D3DRS_CULLMODE, D3DCULL_CW );
    };

    void LateDraw(void) override
    {
        LPDIRECT3DDEVICE9 device;
        m_Data->m_VertexBuffer->GetDevice(&device);
        HRESULT hr;

        hr = device->SetRenderState( D3DRS_CULLMODE, D3DCULL_CCW );
    }
};


//--- モデルクラス --------------------------------------------------------------
class PMDNoBoneDx9 : public _3d::PMDNoBone
{
public:
    PMDNoBoneDx9( PMDDataDx9 *_data,
                  IDraw *_parent, bool _is_draw) :
        PMDNoBone(_data->m_MaxVertex,_data->m_MaxIndex,_parent,_is_draw),
        m_Data(_data)
    {
        m_Data->Retain();
    };

    // @brief  : 頂点 & インデックスの掃き出し
    // @note   : 吐き出された配列データは後でDeleteする必要があります
    //--------------------------------------------------------------------
    Vector3 *CreateVertexAndIndex(void) const override
    {
        Vector3 *re = new Vector3[MAX_INDEX];

        WORD *index;
        m_Data->m_IndexBuffer->Lock(0,0,(void **)&index,D3DLOCK_READONLY);
        Vertex3D::VERTEX *vtx;
        m_Data->m_VertexBuffer->Lock(0,0,(void **)&vtx,D3DLOCK_READONLY);

        for( unsigned i = 0 ; i < MAX_INDEX ; ++i)
        {
            re[i].x = vtx[index[i]].pos.x;
            re[i].y = vtx[index[i]].pos.y;
            re[i].z = vtx[index[i]].pos.z;
        }

        m_Data->m_VertexBuffer->Unlock();
        m_Data->m_IndexBuffer->Unlock();
        return re;
    };

private:
    PMDDataDx9 *m_Data;
    ~PMDNoBoneDx9()
    {
        m_Data->Release();
    };
};

// @brief  : コンストラクタ
//--------------------------------------------------------------------
PMDNoBoneManagerDx9::PMDNoBoneManagerDx9()
{
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
PMDNoBoneManagerDx9::~PMDNoBoneManagerDx9()
{
}

// @brief  : PMDファイルの取り出し
// @param  : 親クラス
//         : 描画するのか？
//         : ファイル名
//--------------------------------------------------------------------
_3d::PMDNoBone *PMDNoBoneManagerDx9::Load( IDraw *_parent, bool _is_draw, const std::string &_file_name )
{
    assert(!_file_name.empty());

    // ファイルの検索
    PMDDataDx9 *data = nullptr;
    Node *i = Begin();
    while(i)
    {
        data = dynamic_cast<PMDDataDx9 *>(i);
        std::string name = data->m_FilePass;
        if( name.compare(_file_name) == 0 ) break;
        i = i->Next();
        data = nullptr;
    }

    // ファイルの作成
    if(!data)
    {
        data = new PMDDataDx9(_file_name);
    }

    // モデルの作成
    PMDNoBoneDx9 *re = new PMDNoBoneDx9(data,_parent,_is_draw);
    PMDMeshDx9 *mesh = new PMDMeshDx9(data,re,true);
    for(unsigned i = 0 ; i < data->m_MaxMaterial ; ++i)
    {
        new PMDMaterialDx9(&data->m_Material[i],data,mesh,true);
    }

    // 作成
    return re;
}


/******************************************************************************
|   End of File
*******************************************************************************/