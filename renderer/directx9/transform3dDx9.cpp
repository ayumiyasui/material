/*
# transform3dDx9.h

- 行列変換クラス(3D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include <d3dx9math.h>
#include "transform3dDx9.h"

#include "directx9.h"


NAMESPACE_3D
// @brief  : コンストラクタ
// @param  : 親クラス
//         : 描画するのか？
//--------------------------------------------------------------------
TransformDx9::TransformDx9( IDraw *_parent, bool _is_draw ) :
    Transform(_parent,_is_draw)
{
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
TransformDx9::~TransformDx9()
{
}

// @brief  : 計算
//--------------------------------------------------------------------
Matrix3D TransformDx9::CreateMatrix(void) const
{
    D3DXMATRIX mtx;
    {
        const D3DXVECTOR3 pos = Vector3(Position);
        const D3DXVECTOR3 rot = Vector3(Rotation);

        D3DXMatrixRotationYawPitchRoll(&mtx,rot.y,rot.x,rot.z);

        D3DXMATRIX mtx_pos;
        D3DXMatrixTranslation( &mtx_pos, pos.x, pos.y, pos.z);
        D3DXMatrixMultiply(&mtx,&mtx,&mtx_pos);
    }
    return Matrix3D(mtx);
}

// @brief  : 描画
//--------------------------------------------------------------------
void TransformDx9::Draw( void )
{
    // デバイスの取得
    DirectX9 &directx = DirectX9::Instance();
    LPDIRECT3DDEVICE9 device = directx.Device;

    // 行列の初期化
    D3DXMATRIX mtx = Matrix3D(Matrix);
    const D3DXMATRIX parent = DirectX9::Instance().Matrix();
    D3DXMatrixMultiply(&mtx,&mtx,&parent);

    // 行列の設定
    device->SetTransform(D3DTS_WORLD,&mtx);
    directx.PushMatirxStack(mtx);
}

// @brief  : 描画
//--------------------------------------------------------------------
void TransformDx9::LateDraw( void )
{
    DirectX9 &directx = DirectX9::Instance();

    LPDIRECT3DDEVICE9 device = directx.Device;

    directx.PopMatirxStack();
    const D3DXMATRIX &mtx = directx.Matrix();
    device->SetTransform(D3DTS_WORLD,&mtx);
}

END_NAMESPACE_3D
/******************************************************************************
|   End of File
*******************************************************************************/