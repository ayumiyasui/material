/*
# transform.h

- 変換行列設定クラス(2D),DirectX9

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __2D_TRANSFORMDx9_H__
#define __2D_TRANSFORMDx9_H__

#include "../2d/transfrom.h"

NAMESPACE_2D

class FactoryDx9;

class TransformDx9 : public Transform
{
private:
    // フレンドクラス
    //--------------------------------------------------------------------
    friend FactoryDx9;

    // @brief  : コンストラクタ
    // @param  : 親クラス
    //         : 描画するのか？
    //--------------------------------------------------------------------
    TransformDx9( IDraw *, bool _is_draw = true );

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~TransformDx9();

    // @brief  : 描画
    //--------------------------------------------------------------------
    void Draw( void ) override;

    // @brief  : 描画
    //--------------------------------------------------------------------
    void LateDraw( void ) override;
};



END_NAMESPACE_2D
#endif //  __2D_TRANSFORMDx9_H__
/******************************************************************************
|   End of File
*******************************************************************************/