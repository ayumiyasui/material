/*
# PMDNoBoneManagerDx9.h

- PMDクラス骨なし(3D)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __3D_PMD_NOBONE_MANAGER_Dx9_H__
#define __3D_PMD_NOBONE_MANAGER_Dx9_H__

#include <string>
#include "../../base/tempNodeSingleton.h"
#include "../3d/PMDNobone.h"


class PMDNoBoneManagerDx9 : public TempNodeSingleton<PMDNoBoneManagerDx9>
{
public:
    // @brief  : PMDファイルの取り出し
    // @param  : 親クラス
    //         : 描画するのか？
    //         : ファイル名
    //--------------------------------------------------------------------
    _3d::PMDNoBone *Load( IDraw *, bool, const std::string & );

private:
    // フレンドクラス
    //--------------------------------------------------------------------
    friend TempNodeSingleton<PMDNoBoneManagerDx9>;

    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    PMDNoBoneManagerDx9();

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~PMDNoBoneManagerDx9();
};



#endif //  __3D_PMD_NOBONE_MANAGER_Dx9_H__
/******************************************************************************
|   End of File
*******************************************************************************/