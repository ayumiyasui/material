/*
# transform.cpp

- 変換行列設定クラス(2D),DirectX9

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include <d3dx9math.h>
#include "../../math/matrix2d.h"
#include "transform2dDx9.h"

#include "directx9.h"

NAMESPACE_2D
// @brief  : コンストラクタ
// @param  : 親クラス
//         : 描画するのか？
//--------------------------------------------------------------------
TransformDx9::TransformDx9( IDraw *_parent, bool _is_draw ) :
    Transform(_parent,_is_draw)
{
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
TransformDx9::~TransformDx9()
{
}

// @brief  : 描画
//--------------------------------------------------------------------
void TransformDx9::Draw( void )
{
    // デバイスの取得
    DirectX9 &directx = DirectX9::Instance();
    LPDIRECT3DDEVICE9 device = directx.Device;

    // 行列の初期化
    D3DXMATRIX mtx = *Matrix;
    const D3DXMATRIX &parent = directx.Matrix();
    D3DXMatrixMultiply(&mtx,&mtx,&parent);

    // 行列の設定
    device->SetTransform(D3DTS_WORLD,&mtx);
    directx.PushMatirxStack(mtx);
}

// @brief  : 描画
//--------------------------------------------------------------------
void TransformDx9::LateDraw( void )
{
    DirectX9 &directx = DirectX9::Instance();

    LPDIRECT3DDEVICE9 device = directx.Device;

    directx.PopMatirxStack();
    const D3DXMATRIX &mtx = directx.Matrix();
    device->SetTransform(D3DTS_WORLD,&mtx);
}


END_NAMESPACE_2D
/******************************************************************************
|   End of File
*******************************************************************************/