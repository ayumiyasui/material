/*
# inputMouse.h

- DirectInputクラス(マウス)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef _INPUT_MOUSE_H_
#define _INPUT_MOUSE_H_

#include "input.h"
#include "../base/tempNodeSingleton.h"

/******************************************************************************
|   マウスクラス
*-----------------------------------------------------------------------------*/
class Mouse : public IInputDevice, public TempNodeSingleton<Mouse>
{
public:
    // 列挙体
    enum MOUSE_BUTTON
    {
        MOUSE_LEFT = 0,
        MOUSE_RIGHT,
        MOUSE_CENTER,
        MOUSE_4,
        MOUSE_5,
        MOUSE_6,
        MOUSE_7,
        MOUSE_8,

        MOUSE_KEY_MAX
    };

    // @brief  : プレスの取得
    //--------------------------------------------------------------------
    bool GetPress( BYTE ) const override;

    // @brief  : トリガーの取得
    //--------------------------------------------------------------------
    bool GetTrigger( BYTE ) const override;

    // @brief  : リリースの取得
    //--------------------------------------------------------------------
    bool GetRelease( BYTE ) const override;

    // @brief  : リピートの取得
    //--------------------------------------------------------------------
    bool GetRepeat( BYTE ) const override;

    // @brief  : 相対座標の取得関数
    //--------------------------------------------------------------------
    const POINT &GetLocal( void ) const;

    // @brief  : 絶対座標の取得関数
    //--------------------------------------------------------------------
    const POINT &GetWorld( void ) const;

    // @brief  : マウスホイール取得関数
    //--------------------------------------------------------------------
    LONG GetWheel( void ) const;

private:
    // フレンドクラス
    //--------------------------------------------------------------------
    friend TempNodeSingleton<Mouse>;

    // メンバ変数
    //--------------------------------------------------------------------
    LPDIRECTINPUTDEVICE8    m_pMouse;   // マウス
    HWND                    m_hWnd;     // ウィンドウハンドル
    KEY                     m_Key[8];   // ボタン
    POINT                   m_Local;    // 相対座標
    POINT                   m_World;    // 絶対座標
    LONG                    m_Wheel;    // マウスホイールの相対値

    // @brief  : コンストラクタ
    // @param  : ウィンドウハンドル
    //         : 入力ポインタ
    //--------------------------------------------------------------------
    Mouse( HWND           _window = App::Instance().Win32->GetHWnd(),
           LPDIRECTINPUT8 _input  = Input::Instance().Get() );

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~Mouse();

    // @brief  : 更新
    //--------------------------------------------------------------------
    void Update( void ) override;
};


#endif // _INPUT_MOUSE_H_
/******************************************************************************
|   End of File
*******************************************************************************/