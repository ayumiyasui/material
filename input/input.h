/*
# input.h

- DirectInputクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#if _WIN32
#ifndef __DIRECTINPUT_H__
#define __DIRECTINPUT_H__

//--- インクルード ------------------------------------------------------------
#include "../app/win32.h"
#define  DIRECTINPUT_VERSION    (0x0800)
#include <dInput.h>
#include "../base/tempNodeSingleton.h"
#include "../base/object.h"
#include "../base/tempGetSet.h"
#include "../app/app.h"

#pragma comment (lib,"dInput8.lib") // Direct Input ver8.
#pragma comment ( lib,"dxguid.lib") // DirectXコンポーメント使用に必要

class Input : public TempNodeSingleton<Input>
{
public:
    // プロパティ
    //--------------------------------------------------------------------
    TempGetSet<unsigned> StartRepeatFrame;      // 何フレーム後にリピートか
    TempGetSet<unsigned> RepeatInterbal;        // 何フレーム毎にOnになるか

    // @brief  : DirectInput本体
    //--------------------------------------------------------------------
    LPDIRECTINPUT8 Get(void) const{ return m_pInput; };

    // @brief  : すべての更新
    //--------------------------------------------------------------------
    void UpdateAll( void );

private:
    // フレンドクラス
    //--------------------------------------------------------------------
    friend TempNodeSingleton<Input>;

    // メンバ変数
    //--------------------------------------------------------------------
    LPDIRECTINPUT8 m_pInput;          // Input本体
    unsigned       m_StartRepeatFrame;
    unsigned       m_RepeatInterbal;

    // @brief  : コンストラクタ
    // @param  : インスタンスハンドル
    //         : ウィンドウハンドル
    //--------------------------------------------------------------------
    Input( HINSTANCE hInstance = App::Instance().Win32->GetHInstance(), HWND hWnd = App::Instance().Win32->GetHWnd() );

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~Input();
};


class IInputDevice : public Node
{
public:
    // @brief  : プレスの取得
    //--------------------------------------------------------------------
    virtual bool GetPress( BYTE ) const = 0;

    // @brief  : トリガーの取得
    //--------------------------------------------------------------------
    virtual bool GetTrigger( BYTE ) const = 0;

    // @brief  : リリースの取得
    //--------------------------------------------------------------------
    virtual bool GetRelease( BYTE ) const = 0;

    // @brief  : リピートの取得
    //--------------------------------------------------------------------
    virtual bool GetRepeat( BYTE ) const = 0;

protected:
    // 構造体
    //--------------------------------------------------------------------
    struct KEY
    {
        BYTE     press;
        BYTE     trig;
        BYTE     release;
        BYTE     repeat;
        unsigned nCntRepeat;
    };

    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    IInputDevice();

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~IInputDevice();

private:
    // フレンドクラス
    //--------------------------------------------------------------------
    friend Input;

    // @brief  : 更新
    //--------------------------------------------------------------------
    virtual void Update( void ) = 0;
};

#endif // __DIIRECTINPUT_H__
#endif // _WIN32
/******************************************************************************
|   End of File
*******************************************************************************/