/*
 # inputMac.mm
 
 - Mac OSXにおけるinputクラス
 
 @author : Ayumi Yasui
 *-----------------------------------------------------------------------------*/
#ifdef __APPLE__

//--- インクルード ------------------------------------------------------------
#include "inputMac.h"
#include "../app/app.h"
#include <iostream>

// キーボード&マウス受付クラス
//-----------------------------------------------------------------------------
@implementation InputKeyMouse

// @brief  : 初期化
//--------------------------------------------------------------------
- (id)init
{
    for(int i=0 ; i<256 ; ++i)
    {
        m_KeyPress[i]  = false;
    }
    
    return self;
}

// プロパティもどき
//-----------------------------------------------------------------------------
- (bool) KeyPress   : (unsigned short)_key_code {return m_KeyPress[_key_code];}


// @brief  : ファーストレスポンダになれるのか
// @return : なれる(YES)
//--------------------------------------------------------------------
- (BOOL) acceptFirstResponder
{
    return YES;
}

// @brief  : ファーストレスポンダになるときに呼ばれる
// @return : なれる(YES)
//--------------------------------------------------------------------
- (BOOL) becomeFirstResponder
{
    return YES;
}

// @brief  : キーボードが押された時
// @param  : 押されたキーの情報
//--------------------------------------------------------------------
- (void) keyDown : (NSEvent *)_event
{
    // キーコードの取得
    unsigned short key_code = [_event keyCode];
    
    // escキーでアプリ終了
    if(key_code == kVK_Escape) [NSApp terminate:self]; // アプリケーションの終了
    
    m_KeyPress[key_code] = true;
}

// @brief  : キーボードが離れた時
// @param  : 離れたキーの情報
//--------------------------------------------------------------------
- (void) keyUp : (NSEvent *)_event
{
    // キーコードの取得
    unsigned short key_code = [_event keyCode];
    
    m_KeyPress[key_code] = false;
}

@end


// 入力クラス
//-----------------------------------------------------------------------------
// @brief  : コンストラクタ
//--------------------------------------------------------------------
InputMac::InputMac() :
    m_StartRepeatFrame(60),
    m_RepeatInterbal(30)
{
    m_KeyMouse = [[InputKeyMouse alloc] init];
    
    // AppDelegateの取得
    AppDelegate *app_delegate = App::Instance().Delegate();
    
    // レスポンダに登録
    [app_delegate.Window makeFirstResponder : m_KeyMouse];

    // キーボードのリピート初期化
    for(int i=0 ; i < 256 ; ++i)
    {
        m_KeyPress[i]  = false;
        m_KeyTrigger[i] = false;
        m_KeyRelease[i] = false;
        m_KeyRepeat[i] = false;
        m_KeyRepeatTime[i] = 0;
    }
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
InputMac::~InputMac()
{
}

// @brief  : 更新
//--------------------------------------------------------------------
void InputMac::Update(void)
{
    NSEvent *event = [NSApp
                      nextEventMatchingMask:(NSKeyUpMask|NSKeyDownMask) // どのイベントを取るか
                      untilDate: [NSDate date]             // 待機時間
                      inMode: NSEventTrackingRunLoopMode   // ループモード
                      dequeue:YES];                        // 前のデータを削除して良いか
    [NSApp sendEvent:event];
    

    // キーボード状態の編集
    for(int i=0 ; i<256 ; ++i)
    {
        bool press = [m_KeyMouse KeyPress:i];
        
        // キーボードのトリガー
        m_KeyTrigger[i] = (m_KeyPress[i] != press) && press;
        
        // キーボードのリリース
        m_KeyRelease[i] = (m_KeyPress[i] != press) && &m_KeyPress[i];
        
        // キーボードのリピート
        if(++m_KeyRepeatTime[i] >= m_StartRepeatFrame)
        {
            m_KeyRepeat[i] = press;
            m_KeyRepeatTime[i] = m_StartRepeatFrame - m_RepeatInterbal;
        }
        else
        {
            m_KeyRepeat[i] = m_KeyTrigger[i];
            if(!press) m_KeyRepeatTime[i] = 0;
        }
        
        // キーボードのプレス
        m_KeyPress[i] = press;
    }
}


// @brief  : キーボードのプレス状態の取得
//--------------------------------------------------------------------
bool InputMac::GetKeyboardPress(unsigned short _key_code)
{
    return m_KeyPress[_key_code];
}
    
// @brief  : キーボードのトリガー状態の取得
//--------------------------------------------------------------------
bool InputMac::GetKeyboardTrigger(unsigned short _key_code)
{
    return m_KeyTrigger[_key_code];
}

// @brief  : キーボードのリリース状態の取得
//--------------------------------------------------------------------
bool InputMac::GetKeyboardRelease(unsigned short _key_code)
{
    return m_KeyRelease[_key_code];
}

// @brief  : キーボードのリピート状態の取得
//--------------------------------------------------------------------
bool InputMac::GetKeyboardRepeat(unsigned short _key_code)
{
    return m_KeyRepeat[_key_code];
}

#endif // __APPLE__
/******************************************************************************
 |   End of File
 *******************************************************************************/