/*
# inputMouse.cpp

- DirectInputクラス(マウス)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード ------------------------------------------------------------
#include "inputMouse.h"

// @brief  : コンストラクタ
// @param  : ウィンドウハンドル
//         : 入力ポインタ
//--------------------------------------------------------------------
Mouse::Mouse( HWND hWnd, LPDIRECTINPUT8 lpDInput ) :
    m_pMouse(NULL),
    m_hWnd(hWnd)
{
    HRESULT hr;

    // マウスの作成
    hr = lpDInput->CreateDevice( GUID_SysMouse, &m_pMouse, NULL );
    _ASSERT(SUCCEEDED(hr));
    if( FAILED(hr) ) delete this;

    // データフォーマットの設定
    hr = m_pMouse->SetDataFormat(&c_dfDIMouse2);
    _ASSERT(SUCCEEDED(hr));
    if( FAILED(hr) ) delete this;

    // コーオペレイティブレベル指定
    hr = m_pMouse->SetCooperativeLevel(hWnd,(DISCL_FOREGROUND | DISCL_NONEXCLUSIVE));
    _ASSERT(SUCCEEDED(hr));
    if( FAILED(hr) ) delete this;

    // 軸モード
    const DIPROPDWORD diprop =
    {
        {
            sizeof(diprop),
            sizeof(diprop.diph),
            0,
            DIPH_DEVICE
        },
        DIPROPAXISMODE_REL  // 相対値モード
    };
    hr = m_pMouse->SetProperty(DIPROP_AXISMODE, &diprop.diph);
    _ASSERT(SUCCEEDED(hr));
    if( FAILED(hr) ) delete this;

    m_pMouse->Acquire();
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
Mouse::~Mouse()
{
    if(m_pMouse)
    {
        m_pMouse->Unacquire();
        m_pMouse->Release();
    }
}

// @brief  : 更新
//--------------------------------------------------------------------
void Mouse::Update( void )
{
    // 絶対座標の取得
    GetCursorPos(&m_World);
    ScreenToClient(m_hWnd,&m_World);

    // マウスからデータを取得
    DIMOUSESTATE2   dims;
    if(SUCCEEDED(m_pMouse->GetDeviceState(sizeof(DIMOUSESTATE2), &dims)))
    {
        // マウスの座標の相対値
        m_Local.x = dims.lX;
        m_Local.y = dims.lY;
        m_Wheel   = dims.lZ;

        // キー状態を判別
        for(int nKey = 0; nKey < MOUSE_KEY_MAX; nKey++)
        {
            // トリガー
            m_Key[nKey].trig = ~m_Key[nKey].press & dims.rgbButtons[nKey];

            // リリース
            m_Key[nKey].release = m_Key[nKey].press & ~dims.rgbButtons[nKey];

            // リピート
            auto nMaxStartRepeat = Input::Instance().StartRepeatFrame;
            auto nRepeatInterbal = Input::Instance().RepeatInterbal;
            ( dims.rgbButtons[nKey] == 0x80 ) ? m_Key[nKey].nCntRepeat+= 1 : m_Key[nKey].nCntRepeat = 0;
            if( m_Key[nKey].nCntRepeat >= nMaxStartRepeat )
            {
                m_Key[nKey].repeat = dims.rgbButtons[nKey];
                m_Key[nKey].nCntRepeat = nMaxStartRepeat - nRepeatInterbal;
            }
            else
            {
                m_Key[nKey].repeat = m_Key[nKey].trig;
            }

            // プレス
            m_Key[nKey].press = dims.rgbButtons[nKey];

        }

        return ;
    }
    else
    {
        m_pMouse->Acquire();
    }
}

// @brief  : プレスの取得
//--------------------------------------------------------------------
bool Mouse::GetPress( BYTE nKey ) const
{
    _ASSERT(nKey < MOUSE_KEY_MAX);
    return (m_Key[nKey].press & 0x80) ? true : false;
}

// @brief  : トリガーの取得
//--------------------------------------------------------------------
bool Mouse::GetTrigger( BYTE nKey ) const
{
    _ASSERT(nKey < MOUSE_KEY_MAX);
    return (m_Key[nKey].trig & 0x80) ? true : false;
}

// @brief  : リリースの取得
//--------------------------------------------------------------------
bool Mouse::GetRelease( BYTE nKey ) const
{
    _ASSERT(nKey < MOUSE_KEY_MAX);
    return (m_Key[nKey].release & 0x80) ? true : false;
}

// @brief  : リピートの取得
//--------------------------------------------------------------------
bool Mouse::GetRepeat( BYTE nKey ) const
{
    _ASSERT(nKey < MOUSE_KEY_MAX);
    return (m_Key[nKey].repeat & 0x80) ? true : false;
}

// @brief  : 相対座標の取得関数
//--------------------------------------------------------------------
const POINT &Mouse::GetLocal( void ) const
{
    return m_Local;
}

// @brief  : 絶対座標の取得関数
//--------------------------------------------------------------------
const POINT &Mouse::GetWorld( void ) const
{
    return m_World;
}

// @brief  : 相対座標X取得関数
//--------------------------------------------------------------------
LONG Mouse::GetWheel( void ) const
{
    return m_Wheel;
}


/******************************************************************************
|   End of File
*******************************************************************************/