/*
# inputKeyboard.h

- DirectInputクラス(キー入力)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#if _WIN32
#pragma once
#ifndef _INPUT_KEYBOARD_H_
#define _INPUT_KEYBOARD_H_

#include "input.h"
#include "../base/tempNodeSingleton.h"

class Keyboard : public IInputDevice, public TempNodeSingleton<Keyboard>
{
public:
    // @brief  : プレスの取得
    //--------------------------------------------------------------------
    bool GetPress( BYTE ) const override;

    // @brief  : トリガーの取得
    //--------------------------------------------------------------------
    bool GetTrigger( BYTE ) const override;

    // @brief  : リリースの取得
    //--------------------------------------------------------------------
    bool GetRelease( BYTE ) const override;

    // @brief  : リピートの取得
    //--------------------------------------------------------------------
    bool GetRepeat( BYTE ) const override;

private:
    // フレンドクラス
    //--------------------------------------------------------------------
    friend TempNodeSingleton<Keyboard>;

    // メンバ変数
    //--------------------------------------------------------------------
    LPDIRECTINPUTDEVICE8 m_pKeyboard;         // キーボード
    KEY                  m_Key[256];          // キーボードの状態

    // @brief  : コンストラクタ
    // @param  : ウィンドウハンドル
    //         : Input本体
    //--------------------------------------------------------------------
    Keyboard( HWND _window = App::Instance().Win32->GetHWnd(),
              LPDIRECTINPUT8 _input = Input::Instance().Get() );

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~Keyboard();

    // @brief  : 更新
    //--------------------------------------------------------------------
    void Update( void ) override;
};


#endif // _INPUT_KEYBOARD_H_
#endif // _WIN32
/******************************************************************************
|   End of File
*******************************************************************************/