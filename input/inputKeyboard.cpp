/*
# inputKeyboard.cpp

- DirectInputクラス(キー入力)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード ------------------------------------------------------------
#include "inputKeyboard.h"

// @brief  : コンストラクタ
// @param  : Input本体
//         : ウィンドウハンドル
//--------------------------------------------------------------------
Keyboard::Keyboard( HWND hWnd, LPDIRECTINPUT8 lpDirectInput ) :
    m_pKeyboard(NULL)
{
    HRESULT hr;

    // ポインタを生成
    hr = lpDirectInput->CreateDevice( GUID_SysKeyboard, &m_pKeyboard, NULL );
    _ASSERT(SUCCEEDED(hr));
    if( FAILED(hr) ) delete this;

    // フォーマットを設定
    hr = m_pKeyboard->SetDataFormat( &c_dfDIKeyboard );
    _ASSERT(SUCCEEDED(hr));
    if( FAILED(hr) ) delete this;

    // 協調モードを設定
    hr = m_pKeyboard->SetCooperativeLevel( hWnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE );
    _ASSERT(SUCCEEDED(hr));
    if( FAILED(hr) ) delete this;

    // アクセス権を取得
    hr = m_pKeyboard->Acquire();
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
Keyboard::~Keyboard()
{
    if(m_pKeyboard)
    {
        m_pKeyboard->Unacquire();
        m_pKeyboard->Release();
    }
}

// @brief  : 更新
//--------------------------------------------------------------------
void Keyboard::Update( void )
{
    // 現在のキーボードの状態を取得
    BYTE aKeyState[256];
    if( SUCCEEDED( m_pKeyboard->GetDeviceState( sizeof( aKeyState ), &aKeyState[0] ) ) )
    {
        // 各キーごとに処理
        int loopCnt = 256 + 1;
        KEY *pKey = &m_Key[0];
        BYTE *pNow = &aKeyState[0];
        while( --loopCnt )
        {
            // トリガー
            pKey->trig = ( pKey->press ^ *pNow ) & *pNow;

            // リリース
            pKey->release =( pKey->press ^ *pNow ) & pKey->press;

            // リピート
            auto nMaxStartRepeat = Input::Instance().StartRepeatFrame;
            auto nRepeatInterbal = Input::Instance().RepeatInterbal;
            ( *pNow == 0x80 ) ? pKey->nCntRepeat+= 1 : pKey->nCntRepeat = 0;
            if( pKey->nCntRepeat >= nMaxStartRepeat )
            {
                pKey->repeat = *pNow;
                pKey->nCntRepeat = nMaxStartRepeat - nRepeatInterbal;
            }
            else
            {
                pKey->repeat = pKey->trig;
            }

            // プレス
            pKey->press = *pNow;

            // 次のキーへ
            pNow += 1;
            pKey += 1;
        }
    }
    else    //取得できないなら
    {
        m_pKeyboard->Acquire();    //アクセス権ください
    }
}

// @brief  : プレスの取得
//--------------------------------------------------------------------
bool Keyboard::GetPress( BYTE nKey ) const
{
    return (m_Key[nKey].press & 0x80) ? true : false;
}

// @brief  : トリガーの取得
//--------------------------------------------------------------------
bool Keyboard::GetTrigger( BYTE nKey ) const
{
    return (m_Key[nKey].trig & 0x80) ? true : false ;
}

// @brief  : リリースの取得
//--------------------------------------------------------------------
bool Keyboard::GetRelease( BYTE nKey ) const
{
    return (m_Key[nKey].release & 0x80) ? true : false ;
}

// @brief  : リピートの取得
//--------------------------------------------------------------------
bool Keyboard::GetRepeat( BYTE nKey ) const
{
    return (m_Key[nKey].repeat & 0x80) ? true : false ;
}


/******************************************************************************
|   End of File
*******************************************************************************/