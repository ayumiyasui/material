/*
# input.cpp

- DirectInputクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード ------------------------------------------------------------
#include "input.h"

// @brief  : コンストラクタ
// @param  : インスタンスハンドル
//         : ウィンドウハンドル
//--------------------------------------------------------------------
Input::Input( HINSTANCE hInstance, HWND hWnd ) :
    TempNodeSingleton(&TempSingleton<Node>::Instance()),
    m_StartRepeatFrame(60),
    m_RepeatInterbal(30),
    StartRepeatFrame(m_StartRepeatFrame),
    RepeatInterbal(m_RepeatInterbal)
{
    HRESULT hr;

    // LPDIRECTINPUT8作成
    hr = DirectInput8Create(
        hInstance,              // インスタンスハンドル
        DIRECTINPUT_VERSION,    // バージョン情報
        IID_IDirectInput8,      // 
        (LPVOID *)&m_pInput,    // アドレスを渡す変数
        NULL                    //
    );
    _ASSERT(SUCCEEDED(hr));
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
Input::~Input()
{
    m_pInput->Release();
}

// @brief  : すべての更新
//--------------------------------------------------------------------
void Input::UpdateAll( void )
{
    auto i = Begin();
    while(i)
    {
        dynamic_cast<IInputDevice*>(i)->Update();
        i = i->Next();
    }
}



/******************************************************************************
|   入力デバイスクラス(抽象)
*-----------------------------------------------------------------------------*/
// @brief  : コンストラクタ
//--------------------------------------------------------------------
IInputDevice::IInputDevice() :
    Node(&Input::Instance())
{
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
IInputDevice::~IInputDevice()
{
}



/******************************************************************************
|   End of File
*******************************************************************************/