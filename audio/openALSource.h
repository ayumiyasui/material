/*
# openAL.h

- 音響系クラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef MATERIAL_AUDIO_OPENAL_SOURCE_H_
#define MATERIAL_AUDIO_OPENAL_SOURCE_H_

#include "iAudio.h"
#include "openAL.h"

namespace material {
namespace resource {
    class Wave;
};

namespace openal {

class Source : public IAudio::ISource
{
public:
    // @brief  : コンストラクタ
    // @param  : ファイルパス
    //         : ラベル
    //------------------------------------------------------------------------
    Source(const std::string &, const std::string &);

    // @brief  : デストラクタ
    //------------------------------------------------------------------------
    ~Source();

    // @brief  : 再生
    //------------------------------------------------------------------------
    void Play(void) override;

    // @brief  : 停止
    //------------------------------------------------------------------------
    void Stop(void) override;

    // @brief  : ラベルの取得
    // @param  : ラベル
    //------------------------------------------------------------------------
    const std::string &GetLabel(void) const;

    // @brief  : alSourcei代行
    // @param  : ALenum
    //------------------------------------------------------------------------
    void Sourcei(ALenum, ALint);   // alSourcei

private:
    // メンバ変数
    //------------------------------------------------------------------------
    std::string     m_Label;    // ラベル
    ALuint          m_Buffer;   // バッファ
    ALuint          m_Source;   // ソース
    resource::Wave *m_Wave;     // Waveデータ
};


};  // namespace openal
};  // namespace material
#endif //  MATERIAL_AUDIO_OPENAL_SOURCE_H_
/******************************************************************************
|   End of File
*******************************************************************************/