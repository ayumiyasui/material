/*
# openALSource.h

- 音響系クラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード -------------------------------------------------------------
#include "openALSource.h"
#include "resource/wave.h"

namespace material {
namespace openal {

// @brief  : コンストラクタ
// @param  : ファイルパス
//         : ラベル
//------------------------------------------------------------------------
Source::Source(const std::string &_file_pass, const std::string &_label) :
    m_Label(_file_pass+_label)
{
    // データの取得
    m_Wave = new resource::Wave(_file_pass);
    const ALenum  channel = m_Wave->ChannelNum;
    const ALsizei bit     = m_Wave->BitParSample;
    const ALsizei size    = m_Wave->ChunkSize;
    const ALsizei freq    = m_Wave->SamplingRate;
    const ALvoid *data    = m_Wave->Buffer();

    // バッファの作成
    alGenBuffers(1,&m_Buffer);
    if(channel == 1 && bit == 8)
    {
        alBufferData(m_Buffer,AL_FORMAT_MONO8,data,size,freq);
    }
    else if(channel == 1 && bit == 16)
    {
        alBufferData(m_Buffer,AL_FORMAT_MONO16,data,size,freq);
    }
    else if(channel == 2 && bit == 8)
    {
        alBufferData(m_Buffer,AL_FORMAT_STEREO8,data,size,freq);
    }
    else if(channel == 2 && bit == 16)
    {
        alBufferData(m_Buffer,AL_FORMAT_STEREO16,data,size,freq);
    }

    // ソースの作成
    alGenSources(1,&m_Source);
    Sourcei(AL_BUFFER,m_Buffer);
}

// @brief  : デストラクタ
//------------------------------------------------------------------------
Source::~Source()
{
    alDeleteSources(1, &m_Source);
    alDeleteBuffers(1, &m_Buffer);
    delete m_Wave;
}

// @brief  : 再生
//------------------------------------------------------------------------
void Source::Play(void)
{
    alSourcePlay(m_Source);
}

// @brief  : 停止
//------------------------------------------------------------------------
void Source::Stop(void)
{
    alSourceStop(m_Source);
}

// @brief  : ラベルの取得
// @param  : ラベル
//------------------------------------------------------------------------
const std::string &Source::GetLabel(void) const
{
    return m_Label;
}

// @brief  : alSourcei代行
// @param  : ALenum
//------------------------------------------------------------------------
void Source::Sourcei(ALenum _enum,ALint _val)
{
    alSourcei(m_Source,_enum,_val);
}



};  // namespace openal
};  // namespace material
/******************************************************************************
|   End of File
*******************************************************************************/