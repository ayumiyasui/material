/*
# xAudio.cpp

- 音響系クラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#if defined(WIN32) || defined(WIN64)
//--- インクルード --------------------------------------------------------------
#include "xAudio.h"
#include <assert.h>
#include <algorithm>

namespace material {
namespace xaudio {
// @brief  : コンストラクタ
//--------------------------------------------------------------------
Audio::Audio()
{
    // COMライブラリの初期化
    {
        const HRESULT hr = CoInitializeEx( NULL, COINIT_MULTITHREADED );
        assert(SUCCEEDED(hr));
    }

    // XAudio2オブジェクトの作成
    {
#ifdef _DEBUG
        const HRESULT hr = XAudio2Create(&m_XAudio2,XAUDIO2_DEBUG_ENGINE);
#else
        const HRESULT hr = XAudio2Create(&m_XAudio2);
#endif
        assert(SUCCEEDED(hr));
    }

    // マスターボイスの生成
    {
        const HRESULT hr = m_XAudio2->CreateMasteringVoice( &m_MasteringVoice );
        assert(SUCCEEDED(hr));
    }
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
Audio::~Audio()
{
    // リストから破棄
    std::for_each(m_List.begin(),m_List.end(),
        [](Source *_source) {
            delete _source;
    });
    m_List.clear();

    // マスターボイスの破棄
    if( m_MasteringVoice ) m_MasteringVoice->DestroyVoice();

    // XAudio2オブジェクトの開放
    if( m_XAudio2 ) m_XAudio2->Release();

    // COMライブラリ終了
    CoUninitialize();
}

// @brief  : BGMの読み込み
// @param  : ファイル名
//         : ラベル
//--------------------------------------------------------------------
IAudio::ISource *Audio::LoadBGM( const std::string &_file_name, const std::string &_label )
{
    // 名前検索
    if(m_List.empty())
    {
        auto name = _file_name + _label;
        auto find_data = std::find_if(m_List.begin(),m_List.end(),
            [&](Source *_source) {
                return std::strcmp(_source->GetLabel().c_str(),name.c_str()) == 0;
        });

        if(find_data != m_List.end()) return *find_data;
    }

    // 作成
    auto new_source = new Source(_file_name,_label,true,this);
    m_List.push_back(new_source);
    return new_source;
}

// @brief  : SEの読み込み
// @param  : ファイル名
//         : ラベル
//--------------------------------------------------------------------
IAudio::ISource *Audio::LoadSE( const std::string & _file_name, const std::string &_label )
{
    // 名前検索
    if(m_List.empty())
    {
        auto name = _file_name + _label;
        auto find_data = std::find_if(m_List.begin(),m_List.end(),
            [&](Source *_source) {
                return std::strcmp(_source->GetLabel().c_str(),name.c_str()) == 0;
        });

        if(find_data != m_List.end()) return *find_data;
    }

    // 作成
    auto new_source = new Source(_file_name,_label,false,this);
    m_List.push_back(new_source);
    return new_source;
}



};  // namespace xaudio
};  // namespace material
#endif // WIN32,WIN64
/******************************************************************************
|   End of File
*******************************************************************************/