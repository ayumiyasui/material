/*
# openAL.h

- 音響系クラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef MATERIAL_AUDIO_OPENAL_H_
#define MATERIAL_AUDIO_OPENAL_H_

#include "iAudio.h"
#include <list>

// Windows
#ifdef WIN32

#include <al.h>
#include <alc.h>
#pragma comment (lib,"OpenAL32.lib")

#elif WIN64
#include <al.h>
#include <alc.h>
#pragma comment (lib,"OpenAL64.lib")

#elif __APPLE__
#include <OpenAL/OpenAL.h>
#endif

namespace material {
namespace openal {
class Source;

class Audio : public IAudio
{
public:
    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    Audio();

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~Audio();

    // @brief  : BGMの読み込み
    // @param  : ファイル名
    //         : ラベル
    //--------------------------------------------------------------------
    ISource *LoadBGM(const std::string &, const std::string &) override;

    // @brief  : SEの読み込み
    // @param  : ファイル名
    //         : ラベル
    //--------------------------------------------------------------------
    ISource *LoadSE(const std::string &, const std::string &) override;

private:
    // メンバ変数
    //--------------------------------------------------------------------
    ALCdevice           *m_Device;  // デバイス
    ALCcontext          *m_Context; // OpenALコンテキスト
    std::list<Source *>  m_List;    // ソースリスト
};


};  // namespace openal
};  // namespace material
#endif //  MATERIAL_AUDIO_OPENAL_H_
/******************************************************************************
|   End of File
*******************************************************************************/