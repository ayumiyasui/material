/*
# xAudio.h

- 音響系クラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#if defined(WIN32) || defined(WIN64)
#pragma once
#ifndef MATERIAL_AUDIO_XAUDIO_H_
#define MATERIAL_AUDIO_XAUDIO_H_

#ifndef STRICT 
#define STRICT                  // 厳密な型チェック
#endif
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN     // 必要最低限のヘッダのみインクルード
#endif

#include <list>
#include <string>
#include <Windows.h>
#include <XAudio2.h>
#include "iAudio.h"

namespace material {
namespace xaudio {

class Audio : public IAudio
{
public:
    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    Audio();

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~Audio();

    // @brief  : BGMの読み込み
    // @param  : ファイル名
    //         : ラベル
    //--------------------------------------------------------------------
    ISource *LoadBGM(const std::string &,const std::string &) override;

    // @brief  : SEの読み込み
    // @param  : ファイル名
    //         : ラベル
    //--------------------------------------------------------------------
    ISource *LoadSE(const std::string &,const std::string &) override;

private:
    // 内部クラス
    //--------------------------------------------------------------------
    class Source : public ISource
    {
    public:
        Source(const std::string &, const std::string &, bool, Audio *const);
        ~Source();

        void Play(void) override;
        void Stop(void) override;

        const std::string &GetLabel(void) const;

    private:
        std::string m_Label;              // 名前
        Audio *m_Audio;                   // オーディオ
        IXAudio2SourceVoice *m_Voice;     // ボイスデータ
        XAUDIO2_BUFFER       m_Buffer;    // バッファ
        HRESULT CheckChunk(HANDLE,DWORD,DWORD *,DWORD *);
        HRESULT ReadChunkData(HANDLE,void *,DWORD,DWORD);
    };

    // メンバ変数
    //--------------------------------------------------------------------
    IXAudio2               *m_XAudio2;
    IXAudio2MasteringVoice *m_MasteringVoice;
    std::list<Source *>     m_List;
};


};  // namespace xaudio
};  // namespace material
#endif //  MATERIAL_AUDIO_XAUDIO_H_
#endif // WIN32,WIN64
/******************************************************************************
|   End of File
*******************************************************************************/