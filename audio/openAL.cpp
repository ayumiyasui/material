/*
# openAL.cpp

- 音響系クラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード -------------------------------------------------------------
#include "openAL.h"
#include <assert.h>
#include <algorithm>
#include "openALSource.h"

namespace material {
namespace openal {

// @brief  : コンストラクタ
//--------------------------------------------------------------------
Audio::Audio()
{
    // デバイスへ接続
    m_Device = alcOpenDevice(NULL);
    assert(m_Device);

    // コンテキストの生成
    m_Context = alcCreateContext(m_Device,NULL);
    assert(m_Context);

    // コンテキストの選択
    auto current = alcMakeContextCurrent(m_Context);
    assert(current==ALC_TRUE);
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
Audio::~Audio()
{
    // ソースの破棄
    std::for_each(m_List.begin(),m_List.end(),
        [](Source *_source) {
            delete _source;
    });
    m_List.clear();

    // コンテキストの無効化
    alcMakeContextCurrent(NULL);

    // コンテキストの破棄
    alcDestroyContext(m_Context);

    // デバイスから切断
    alcCloseDevice(m_Device);
}

// @brief  : BGMの読み込み
// @param  : ファイル名
//         : ラベル
//--------------------------------------------------------------------
IAudio::ISource *Audio::LoadBGM(const std::string &_file_pass, const std::string &_label)
{
    // 名前検索
    if(m_List.empty())
    {
        auto name = _file_pass + _label;
        auto find_data = std::find_if(m_List.begin(),m_List.end(),
            [&](Source *_source) {
                return std::strcmp(_source->GetLabel().c_str(),name.c_str()) == 0;
        });
        if(find_data != m_List.end()) return *find_data;
    }


    auto new_source = new Source(_file_pass,_label);
    new_source->Sourcei(AL_LOOPING,AL_TRUE);
    m_List.push_back(new_source);
    return new_source;
}

// @brief  : SEの読み込み
// @param  : ファイル名
//         : ラベル
//--------------------------------------------------------------------
IAudio::ISource *Audio::LoadSE(const std::string &_file_pass, const std::string &_label)
{
    // 名前検索
    if(m_List.empty())
    {
        auto name = _file_pass + _label;
        auto find_data = std::find_if(m_List.begin(),m_List.end(),
            [&](Source *_source) {
                return std::strcmp(_source->GetLabel().c_str(),name.c_str()) == 0;
        });
        if(find_data != m_List.end()) return *find_data;
    }


    auto new_source = new Source(_file_pass,_label);
    m_List.push_back(new_source);
    return new_source;
}



};  // namespace openal
};  // namespace material
/******************************************************************************
|   End of File
*******************************************************************************/