/*
# xAudio.cpp

- 音響系クラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#if defined(WIN32) || defined(WIN64)

//--- インクルード --------------------------------------------------------------
#include "xAudio.h"
#include <assert.h>

namespace material {
namespace xaudio {

// @brief  : コンストラクタ
// @param  : ファイルパス
//         : ラベル
//         : ループするのか
//         : 管理クラス
//--------------------------------------------------------------------
Audio::Source::Source(const std::string &_file_pass, const std::string &_label, bool _is_loop, Audio *const _audio ) :
    m_Label(_file_pass+_label),
    m_Audio(_audio),
    m_Voice(nullptr)
{
    m_Buffer.pAudioData = nullptr;

    // 初期化
    HANDLE hFile;
    DWORD dwChunkSize = 0;
    DWORD dwChunkPosition = 0;
    DWORD dwFiletype;
    WAVEFORMATEXTENSIBLE wfx;
    XAUDIO2_BUFFER *pBuffer = &m_Buffer;

    // バッファのクリア
    memset( &wfx, 0, sizeof( WAVEFORMATEXTENSIBLE ) );
    memset( pBuffer, 0, sizeof( XAUDIO2_BUFFER ) );

    // サウンドデータファイルの生成
    hFile = CreateFile( _file_pass.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL );
    if( hFile == INVALID_HANDLE_VALUE )
    {
        auto hr = HRESULT_FROM_WIN32( GetLastError() );
        return ;
    }

    // ファイルポインタを先頭に移動
    if( SetFilePointer( hFile, 0, NULL, FILE_BEGIN ) == INVALID_SET_FILE_POINTER )
    {
        auto hr = HRESULT_FROM_WIN32( GetLastError() );
        return ;
    }

    // WAVEファイルのチェック
    HRESULT hr;
    hr = CheckChunk( hFile, 'FFIR', &dwChunkSize, &dwChunkPosition );
    if( FAILED( hr ) )
    {
        return ;
    }

    hr = ReadChunkData( hFile, &dwFiletype, sizeof( DWORD ), dwChunkPosition );
    if( FAILED( hr ) )
    {
        return ;
    }
    if( dwFiletype != 'EVAW' )
    {
        return ;
    }

    // フォーマットチェック
    hr = CheckChunk( hFile, ' tmf', &dwChunkSize, &dwChunkPosition );
    if( FAILED( hr ) )
    {
        return ;
    }
    hr = ReadChunkData( hFile, &wfx, dwChunkSize, dwChunkPosition );
    if( FAILED( hr ) )
    {
        return ;
    }

    // オーディオデータ読み込み
    hr = CheckChunk( hFile, 'atad', (DWORD *)&m_Buffer.AudioBytes, &dwChunkPosition );
    if( FAILED( hr ) )
    {
        return ;
    }
    m_Buffer.pAudioData = (BYTE *)malloc( m_Buffer.AudioBytes );
    hr = ReadChunkData( hFile, (VOID *)m_Buffer.pAudioData, m_Buffer.AudioBytes, dwChunkPosition );
    if( FAILED( hr ) )
    {
        return ;
    }

    // ソースボイスの生成
    hr = m_Audio->m_XAudio2->CreateSourceVoice( &m_Voice, &( wfx.Format ) );
    if( FAILED( hr ) )
    {
        return ;
    }

    // バッファの値設定
    m_Buffer.Flags = XAUDIO2_END_OF_STREAM;
    if( _is_loop == FALSE )
    {
        m_Buffer.LoopCount = 0;
        m_Voice->ExitLoop();
    }
    else
    {
        m_Buffer.LoopCount = XAUDIO2_LOOP_INFINITE;
    }
    m_Buffer.pContext = NULL;
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
Audio::Source::~Source()
{
    // 一時停止
    Stop();

    // m_Voiceの破棄
    if( m_Voice )
    {
        m_Voice->FlushSourceBuffers();
        m_Voice->DestroyVoice();
    }

    // バッファの破棄
    if( m_Buffer.pAudioData )
    {
        free( (void *)m_Buffer.pAudioData );
    }
}

// @brief  : 再生
//--------------------------------------------------------------------
void Audio::Source::Play( void )
{
    if( !m_Voice ) return ;

    // 再生していたら停止
    Stop();

    // オーディオバッファの登録
    m_Voice->SubmitSourceBuffer( &m_Buffer );

    // 再生
    m_Voice->Start(0);
}

// @brief  : 停止
//--------------------------------------------------------------------
void Audio::Source::Stop( void )
{
    if( !m_Voice ) return ;

    // ステータスの取得
    XAUDIO2_VOICE_STATE xa2state;
    m_Voice->GetState( &xa2state );

    // 再生していないなら何もしない
    if( xa2state.BuffersQueued == 0 ) return ;

    // 停止
    m_Voice->Stop( 0 );

    // オーディオバッファの削除
    m_Voice->FlushSourceBuffers();
}

// @brief  : ラベルの取得
// @return : ラベル
//--------------------------------------------------------------------
const std::string &Audio::Source::GetLabel(void) const{return m_Label;}

// @brief  : チャンクのチェック
//--------------------------------------------------------------------
HRESULT Audio::Source::CheckChunk(HANDLE hFile, DWORD format, DWORD *pChunkSize, DWORD *pChunkDataPosition)
{
    HRESULT hr = S_OK;
    DWORD dwRead;
    DWORD dwChunkType;
    DWORD dwChunkDataSize;
    DWORD dwRIFFDataSize = 0;
    DWORD dwFileType;
    DWORD dwBytesRead = 0;
    DWORD dwOffset = 0;

    if( SetFilePointer( hFile, 0, NULL, FILE_BEGIN ) == INVALID_SET_FILE_POINTER )
    {// ファイルポインタを先頭に移動
        hr = HRESULT_FROM_WIN32( GetLastError() );
        return( hr );
    }

    while( hr == S_OK )
    {
        if( ReadFile( hFile, &dwChunkType, sizeof( DWORD ), &dwRead, NULL ) == 0 )
        {// チャンクの読み込み
            hr = HRESULT_FROM_WIN32( GetLastError() );
        }

        if( ReadFile( hFile, &dwChunkDataSize, sizeof( DWORD ), &dwRead, NULL ) == 0 )
        {// チャンクデータの読み込み
            hr = HRESULT_FROM_WIN32( GetLastError() );
        }

        switch( dwChunkType )
        {
        case 'FFIR':
        dwRIFFDataSize = dwChunkDataSize;
        dwChunkDataSize = 4;
        if( ReadFile( hFile, &dwFileType, sizeof( DWORD ), &dwRead, NULL ) == 0 )
        {// ファイルタイプの読み込み
            hr = HRESULT_FROM_WIN32( GetLastError() );
        }
        break;

        default:
        if( SetFilePointer( hFile, dwChunkDataSize, NULL, FILE_CURRENT ) == INVALID_SET_FILE_POINTER )
        {// ファイルポインタをチャンクデータ分移動
            hr = HRESULT_FROM_WIN32( GetLastError() );
            return( hr );
        }
        }

        dwOffset += sizeof( DWORD ) * 2;
        if( dwChunkType == format )
        {
            *pChunkSize = dwChunkDataSize;
            *pChunkDataPosition = dwOffset;

            return S_OK;
        }

        dwOffset += dwChunkDataSize;
        if( dwBytesRead >= dwRIFFDataSize )
        {
            return S_FALSE;
        }
    }

    return S_OK;
}

// @brief  : チャンクチェック(読み込み)
//--------------------------------------------------------------------
HRESULT Audio::Source::ReadChunkData( HANDLE hFile, void *pBuffer, DWORD dwBuffersize, DWORD dwBufferoffset )
{
    DWORD dwRead;
    HRESULT hr;

    if( SetFilePointer( hFile, dwBufferoffset, NULL, FILE_BEGIN ) == INVALID_SET_FILE_POINTER )
    {// ファイルポインタを指定位置まで移動
        hr = HRESULT_FROM_WIN32( GetLastError() );
        return( hr );
    }

    if( ReadFile( hFile, pBuffer, dwBuffersize, &dwRead, NULL ) == 0 )
    {// データの読み込み
        hr = HRESULT_FROM_WIN32( GetLastError() );
        return( hr );
    }

    return S_OK;
}



};  // namespace xaudio
};  // namespace material
#endif // WIN32,WIN64
/******************************************************************************
|   End of File
*******************************************************************************/