/*
# AudioNone.h

- 音響系クラス(何もしない)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef MATERIAL_AUDIO_NONE_H_
#define MATERIAL_AUDIO_NONE_H_


#include "iAudio.h"

namespace material {
class AudioNone : public IAudio
{
public:
    // 内部クラス
    //--------------------------------------------------------------------
    class Source : public IAudio::ISource
    {
    public:
        Source(){};
        ~Source(){};
        void Play(void){};
        void Stop(void){};
    };

    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    AudioNone(){m_Source = new Source;};

    // @brief  : BGMの読み込み
    // @param  : ファイル名
    //         : ラベル
    //--------------------------------------------------------------------
    ISource *LoadBGM(const std::string &, const std::string &)
    {
        return m_Source;
    };

    // @brief  : SEの読み込み
    // @param  : ファイル名
    //         : ラベル
    //--------------------------------------------------------------------
    ISource *LoadSE(const std::string &, const std::string &)
    {
        return m_Source;
    };

private:
    // メンバ変数
    //--------------------------------------------------------------------
    Source *m_Source;   // 何もしないソース

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~AudioNone(){delete m_Source;};
};

};  // namespace material
#endif //  MATERIAL_I_AUDIO_H_
/******************************************************************************
|   End of File
*******************************************************************************/