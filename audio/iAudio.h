/*
# iAudio.h

- 音響系クラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef MATERIAL_I_AUDIO_H_
#define MATERIAL_I_AUDIO_H_

#include <string>

namespace material {
class IAudio
{
public:
    // 内部クラス
    //--------------------------------------------------------------------
    class ISource
    {
    public:
        virtual void Play(void) = 0;
        virtual void Stop(void) = 0;
    protected:
        ISource(){};
        virtual ~ISource(){};
    };

    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    IAudio(){};

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~IAudio(){};

    // @brief  : BGMの読み込み
    // @param  : ファイル名
    //         : ラベル
    //--------------------------------------------------------------------
    virtual ISource *LoadBGM(const std::string &, const std::string &_label = "\0") = 0;

    // @brief  : SEの読み込み
    // @param  : ファイル名
    //         : ラベル
    //--------------------------------------------------------------------
    virtual ISource *LoadSE(const std::string &, const std::string &_label = "\0") = 0;
};

};  // namespace material
#endif //  MATERIAL_I_AUDIO_H_
/******************************************************************************
|   End of File
*******************************************************************************/