/*
# wave.h

- WAVEファイルクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef MATERIAL_AUDIO_RESOURCE_WAVE_H_
#define MATERIAL_AUDIO_RESOURCE_WAVE_H_

#include <string>
#include "../../variable/bitName.h"
#include "../../base/tempGetter.h"

namespace material {
namespace resource {

class Wave
{
public:
    // データフォーマット
    //------------------------------------------------------------------------
    enum FORMAT
    {
        MONARUAL_8,
        MONARUAL_16,
        STEREO_8,
        STEREO_16,
    };

    // @brief  : コンストラクタ
    // @param  : ファイルパス
    //         : ラベル
    //------------------------------------------------------------------------
    Wave(const std::string &);

    // @brief  : デストラクタ
    //------------------------------------------------------------------------
    ~Wave();

    // プロパティ
    //------------------------------------------------------------------------
    TempGetter<variable::u16> ChannelNum;
    TempGetter<variable::u32> SamplingRate;
    TempGetter<variable::u16> BitParSample;
    TempGetter<variable::u32> ChunkSize;
    TempGetter<variable::u32> DataSize;
    int *                     Buffer(void) const;

private:
#pragma pack(push,1)
    // 8ビットステレオデータ
    //------------------------------------------------------------------------
    struct SAMPLE_STEREO_8
    {
        variable::s8 l;
        variable::s8 r;
    };

    // 16ビットステレオデータ
    //------------------------------------------------------------------------
    struct SAMPLE_STEREO_16
    {
        variable::s16 l;
        variable::s16 r;
    };
#pragma pack(pop)
    // データ構造体
    //------------------------------------------------------------------------
    struct DATA
    {
        union
        {
            int              *buffer;      // バッファポインタ
            variable::u8     *monarual8;   // 8ビットモノラル
            variable::s16    *monarual16;  // 16ビットモノラル
            SAMPLE_STEREO_8  *stereo8;     // 8ビットステレオ
            SAMPLE_STEREO_16 *stereo16;    // 16ビットステレオ
        };
    };

    // メンバ変数
    //------------------------------------------------------------------------
    variable::u16 m_ChannelNum;     // チャンネル数
    variable::u32 m_SamplingRate;   // サンプルレート
    variable::u16 m_BitParSample;   // 量子化ビット数
    variable::u32 m_ChunkSize;      // 波形データサイズ
    variable::u32 m_DataSize;       // モノラルならサンプル数,ステレオなら1サンプルずつの組の数
    DATA          m_Data;           // データ
};


};  // namespace resource
};  // namespace material
#endif //  MATERIAL_AUDIO_RESOURCE_WAVE_H_
/******************************************************************************
|   End of File
*******************************************************************************/