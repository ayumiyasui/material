/*
# wave.cpp

- WAVEファイルクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#define _CRT_SECURE_NO_WARNINGS
#include "wave.h"
#include <iostream>
#include <assert.h>

namespace material {
namespace resource {

// @brief  : コンストラクタ
// @param  : ファイルパス
//         : ラベル
//------------------------------------------------------------------------
Wave::Wave(const std::string &_file_pass) :
    ChannelNum(m_ChannelNum),
    SamplingRate(m_SamplingRate),
    BitParSample(m_BitParSample),
    ChunkSize(m_ChunkSize),
    DataSize(m_DataSize)
{
    auto file = std::fopen(_file_pass.c_str(),"rb");
    assert(file);

    // フォーマットチャンクチェック
    char header[20];
    std::fread(header,sizeof(char),20,file);
    assert(std::strcmp(&header[0],"RIFF")!=0);
    assert(std::strcmp(&header[8],"WAVE")!=0);
    assert(std::strcmp(&header[12],"fmt ")!=0);

    // fmtチャンクのバイト数
    variable::u32 fmt_size;
    std::memcpy(&fmt_size,&header[16],sizeof(fmt_size));

    // フォーマットIDから拡張部分までのヘッダを読み込む
    variable::u8 *buffer = new variable::u8[fmt_size];
    std::memset(buffer,0,sizeof(variable::u8)*fmt_size);
    std::fread(buffer,sizeof(variable::u8),fmt_size,file);

    // チャンネル数の取得
    std::memcpy(&m_ChannelNum,&buffer[2],sizeof(m_ChannelNum));

    // サンプリング周波数の取得
    std::memcpy(&m_SamplingRate,&buffer[4],sizeof(m_SamplingRate));

    // 量子化ビット数の取得
    std::memcpy(&m_BitParSample,&buffer[14],sizeof(m_BitParSample));

    // factもしくはdataのIDとサイズを取得
    std::fread(buffer,sizeof(variable::u8),8,file);

    // 拡張分を捨てる
    if(!std::strcmp((const char *)buffer,"fact"))
    {
        fread(buffer,sizeof(variable::u8),4,file);
        fread(buffer,sizeof(variable::u8),8,file);
    }

    // dataチェック
    assert((buffer[0] == 'd') && (buffer[1] == 'a') && (buffer[2] == 't') && (buffer[3] == 'a'));

    // 波形データサイズの取得
    memcpy(&m_ChunkSize,&buffer[4],sizeof(m_ChunkSize));
    delete[] buffer;

    // データ部分を全て取り込む
    if(m_ChannelNum == 1 && m_BitParSample == 8)
    {
        m_Data.monarual8 = new variable::u8[m_ChunkSize];
        fread(m_Data.monarual8,sizeof(variable::u8),m_ChunkSize,file);
    }
    else if(m_ChannelNum == 1 && m_BitParSample == 16)
    {
        m_Data.monarual16 = new variable::s16[m_ChunkSize];
        fread(m_Data.monarual16,sizeof(variable::s16),m_ChunkSize,file);
    }
    else if(m_ChannelNum == 2 && m_BitParSample == 8)
    {
        m_Data.stereo8 = new SAMPLE_STEREO_8[m_ChunkSize];
        fread(m_Data.stereo8,sizeof(SAMPLE_STEREO_8),m_ChunkSize,file);
    }
    else if(m_ChannelNum == 2 && m_BitParSample == 16)
    {
        m_Data.stereo16 = new SAMPLE_STEREO_16[m_ChunkSize];
        fread(m_Data.stereo16,sizeof(SAMPLE_STEREO_16),m_ChunkSize,file);
    }
    else{assert(false && "unknown file");};

    // ファイルを閉じる
    std::fclose(file);
}

// @brief  : デストラクタ
//------------------------------------------------------------------------
Wave::~Wave()
{
    if(m_ChannelNum == 1 && m_BitParSample == 8)
    {
        delete[] m_Data.monarual8;
    }
    else if(m_ChannelNum == 1 && m_BitParSample == 16)
    {
        delete[] m_Data.monarual16;
    }
    else if(m_ChannelNum == 2 && m_BitParSample == 8)
    {
        delete[] m_Data.stereo8;
    }
    else if(m_ChannelNum == 2 && m_BitParSample == 16)
    {
        delete[] m_Data.stereo16;
    }
}

// プロパティ
//------------------------------------------------------------------------
int *Wave::Buffer(void) const{ return m_Data.buffer; };



};  // namespace resource
};  // namespace material
/******************************************************************************
|   End of File
*******************************************************************************/