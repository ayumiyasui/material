/*
# sphere.cpp

- 球クラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード -------------------------------------------------------------
#include "sphere.h"
#include <math.h>

namespace collision {

// @brief  : コンストラクタ
// @param  : 球の半径
//         : 級の中心地点の座標
//--------------------------------------------------------------------
Sphere::Sphere( float _length, const Vector3 &_pos ) :
    m_Length(_length),
    m_Position(_pos)
{
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
Sphere::~Sphere()
{
}

// @brief  : 中心地点の変更
// @param  : 中心地点
// @return : 前の中心地点
//--------------------------------------------------------------------
Vector3 Sphere::SetPos( const Vector3 &_set )
{
    Vector3 old = m_Position;
    m_Position = _set;
    return old;
}

// @brief  : 半径の変更
// @param  : 半径
// @return : 前の半径
//--------------------------------------------------------------------
float Sphere::SetLength( float _length )
{
    float old = m_Length;
    m_Length = _length;
    return old;
}

// @brief  : レイとの当たり判定
// @param  : レイのスタート
//         : レイの終了
//         : [out]衝突開始点
//         : [out]衝突終了点
// @return : 当たったのか？
// @note   : http://marupeke296.com/COL_3D_No24_RayToSphere.html
//--------------------------------------------------------------------
bool Sphere::IsHitRay( const Vector3 &_start, const Vector3 &_end, Vector3 *_start_hit, Vector3 *_end_hit ) const
{
    // レイの方向ベクトルの作成
    const Vector3 ray_vec = _end - _start;

    // レイの始めから円の中心点へのベクトル
    const Vector3 p(m_Position-_start);

    // レイの長さが0なら判定できない
    const float A = ray_vec.SquareLength();
    if( A == 0.0f ) return false;

    // レイと球が衝突しているのか？
    const float B = ray_vec.Dot(p);
    const float C = p.SquareLength() - m_Length * m_Length;
    float s = B * B - A * C;
    if(s < 0.0f) return false;

    // レイが反対方向になっていないか？
    s = sqrtf(s);
    const float inv_A = 1.0f / A;
    float a1 = (B-s) * inv_A;
    float a2 = (B+s) * inv_A;
    if( a1 < 0.0f || a2 < 0.0f ) return false;

    // レイと球は当たっている。
    // 衝突位置は計算するか？
    bool is_return_start    = _start_hit != nullptr;
    bool is_return_end      = _end_hit   != nullptr;
    bool is_not_return_hit_point = !(is_return_start || is_return_end);

    if(is_not_return_hit_point) return true;

    // 衝突スタート位置
    if(is_return_start)
    {
        _start_hit->x = _start.x + a1 * ray_vec.x;
        _start_hit->y = _start.y + a1 * ray_vec.y;
        _start_hit->z = _start.z + a1 * ray_vec.z;
    }

    // 衝突終了位置
    if(is_return_end)
    {
        _end_hit->x = _start.x + a2 * ray_vec.x;
        _end_hit->y = _start.y + a2 * ray_vec.y;
        _end_hit->z = _start.z + a2 * ray_vec.z;
    }

    return true;
}


}; // namespace collision
/******************************************************************************
|   End of File
*******************************************************************************/