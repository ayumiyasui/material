/*
# collision.h

- 当たり判定クラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __COLLISION_H__
#define __COLLISION_H__

#include "../math/vector3.h"

namespace collision {
class ICollision
{
public:
    // @brief  : レイとの当たり判定
    // @param  : レイのスタート
    //         : レイの終了
    //         : [out]衝突開始点
    //         : [out]衝突終了点
    // @return : 当たったのか？
    //--------------------------------------------------------------------
    virtual bool IsHitRay( const Vector3 &, const Vector3 &, Vector3 *_start_hit = nullptr, Vector3 *_end_hit = nullptr ) const = 0;

protected:
    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    ICollision(){};

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~ICollision(){};
};


}; // namespace collision
#endif //  __COLLISION_H__
/******************************************************************************
|   End of File
*******************************************************************************/