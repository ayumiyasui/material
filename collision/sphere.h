/*
# sphere.h

- 球クラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __COLLISION_SPHERE_H__
#define __COLLISION_SPHERE_H__

#include "collision.h"

namespace collision {
class Sphere : public ICollision
{
public:
    // @brief  : コンストラクタ
    // @param  : 球の半径
    //         : 級の中心地点の座標
    //--------------------------------------------------------------------
    Sphere( float, const Vector3 & );

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~Sphere();

    // @brief  : 中心地点の変更
    // @param  : 中心地点
    // @return : 前の中心地点
    //--------------------------------------------------------------------
    Vector3 SetPos( const Vector3 & );

    // @brief  : 半径の変更
    // @param  : 半径
    // @return : 前の半径
    //--------------------------------------------------------------------
    float SetLength( float );

    // @brief  : レイとの当たり判定
    // @param  : レイのスタート
    //         : レイの終了
    //         : [out]衝突開始点
    //         : [out]衝突終了点
    // @return : 当たったのか？
    //--------------------------------------------------------------------
    bool IsHitRay( const Vector3 &, const Vector3 &, Vector3 *_start_hit = nullptr, Vector3 *_end_hit = nullptr ) const override;

private:
    // メンバ変数
    //--------------------------------------------------------------------
    Vector3 m_Position;   // 中心座標
    float   m_Length;   // 半径
};


}; // namespace collision
#endif //  __COLLISION_SPHERE_H__
/******************************************************************************
|   End of File
*******************************************************************************/