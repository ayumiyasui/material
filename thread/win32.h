/*
# win32.h

- スレッドクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#if _WIN32
#ifndef __THREAD_H__
#define __THREAD_H__

//--- インクルード ------------------------------------------------------------
#include "../app/win32.h"
#include <process.h>

class Thread
{
public:
    // @brief  : 終了したのか？
    //--------------------------------------------------------------------
    bool IsEnd( void ) const
    {
        DWORD exit_code;
        GetExitCodeThread( handle, &exit_code );
        return exit_code != STILL_ACTIVE;
    };

protected:
    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    Thread()
    {
        handle = (HANDLE)_beginthreadex( NULL, 0, thread, this, 0, NULL );
    };

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~Thread()
    {
        if( INVALID_HANDLE_VALUE != handle ) CloseHandle( handle );
    };

private:
    // メンバ変数
    //--------------------------------------------------------------------
    HANDLE handle;

    // @brief  : スレッド
    //--------------------------------------------------------------------
    static unsigned int __stdcall thread( void *_this )
    {
        reinterpret_cast<Thread *>(_this)->Run();
        _endthreadex( 1 );
        return 0;
    };

    // @brief  : スレッド内関数
    //--------------------------------------------------------------------
    virtual void Run( void ) = 0;
};

#endif  // __THREAD_H__
#endif  // _WIN32
/******************************************************************************
|   End of File
*******************************************************************************/#pragma once
