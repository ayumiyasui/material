/*
# object.h

- オブジェクトクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __OBJECT_H__
#define __OBJECT_H__

#include "node.h"
#include "tempGetSet.h"
#include "tempNodeSingleton.h"

class Object : public Node
{
private:
    // 内部クラス
    //--------------------------------------------------------------------
    class UpdateEnable : public TempGetSet<bool>
    {
    public:
        UpdateEnable( bool &, Object * );
        ~UpdateEnable();
        UpdateEnable &operator = ( const bool &_set ){ Set(_set); return *this; };
    private:
        Object *m_Master;
        void Set( const bool & ) override;
    };

public:
    // @brief  : コンストラクタ
    // @param  : 親のScene
    //         : 更新する or しない
    //--------------------------------------------------------------------
    Object( Node *, bool _is_update = true );

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~Object();

    // プロパティ
    //--------------------------------------------------------------------
    UpdateEnable IsUpdate;  // アップデートするのか？

    // @brief  : すべての更新
    //--------------------------------------------------------------------
    void UpdateAll( void );

private:
    // フレンドクラス
    //--------------------------------------------------------------------
    friend UpdateEnable;
    friend TempNodeSingleton<Object>;

    // メンバ変数
    //--------------------------------------------------------------------
    bool m_IsUpdate; // 更新するのか

    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    Object();

    // @brief  : Enableになった時の初期化
    //--------------------------------------------------------------------
    virtual void OnEnable( void ){};

    // @brief  : Disableになった時の初期化
    //--------------------------------------------------------------------
    virtual void OnDisable( void ){};

    // @brief  : 更新
    //--------------------------------------------------------------------
    virtual void Update( void ){};
};



#endif //  __OBJECT_H__
/******************************************************************************
|   End of File
*******************************************************************************/