/*
# node.h

- ノードクラス
- ゲームフレームワークのベース

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __NODE_H__
#define __NODE_H__

#include <list>
#include <functional>
#include "tempGetter.h"
#include "tempGetSet.h"
#include "tempSingleton.h"

class Node
{
private:
    // 内部クラス
    //--------------------------------------------------------------------
    class DestroyFlag : public TempGetSet<bool>
    {
    public:
        DestroyFlag();
        ~DestroyFlag();
        DestroyFlag &operator = ( const bool & );
        void SetMaster(Node *);

    private:
        Node *m_Master;
        bool  m_Flag;
        void Set( const bool & ) override;
    };

public:
    // プロパティ
    //--------------------------------------------------------------------
    DestroyFlag IsDestroy;     // 壊すのか？

    // @brief  : ポインタの取得
    //--------------------------------------------------------------------
    Node *Next(void)   const;   // 次のポインタ
    Node *Prev(void)   const;   // 前のポインタ
    Node *Begin(void)  const;   // 最初の子
    Node *End(void)    const;   // 最後の子
    Node *Parent(void) const;   // 親

    // @brief  : 子の追加
    //--------------------------------------------------------------------
    void AddChild( Node * );

    // @brief  : 子の削除
    //--------------------------------------------------------------------
    void DeleteChild(void);

    // @brief  : 削除
    //--------------------------------------------------------------------
    void Destroy(void);

    // @brief  : ループ処理
    // @param  : ラムダ式(@return void, @param Node *)
    //--------------------------------------------------------------------
    void ForEach( std::function<void (Node *)> );

    // @brief  : ループ処理(後ろから)
    // @param  : ラムダ式(@return void, @param Node *)
    //--------------------------------------------------------------------
    void ForEachInversion( std::function<void (Node *)> );

protected:
    // @brief  : コンストラクタ
    // @param  : 親
    //--------------------------------------------------------------------
    Node( Node * );

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~Node();

private:
    // フレンドクラス
    //--------------------------------------------------------------------
    friend TempSingleton<Node>;

    // メンバ変数
    //--------------------------------------------------------------------
    Node *parent;       // 親
    Node *next;         // 次のオブジェクト
    Node *prev;         // 前のオブジェクト
    Node *first_child;  // 最初の子
    Node *last_child;   // 最後の子

    // @brief  : コンストラクタ
    // @note   : 一番の親になる
    //--------------------------------------------------------------------
    Node();

    // @brief  : 削除されるフラグが立ったとき
    //--------------------------------------------------------------------
    virtual void OnDestory(void){};

    // @brief  : リンクから削除
    //--------------------------------------------------------------------
    void UnLink( void );
};



#endif //  __NODE_H__
/******************************************************************************
|   End of File
*******************************************************************************/