/*
# node.h

- ノードクラス
- ゲームフレームワークのベース

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

// インクルード
//--------------------------------------------------------------------
#include "node.h"

#include <assert.h>
#include <stdio.h>


// 内部クラス
//--------------------------------------------------------------------
Node::DestroyFlag::DestroyFlag() :
    TempGetSet<bool>(m_Flag),
    m_Flag(false)
{
}
Node::DestroyFlag::~DestroyFlag()
{
}
Node::DestroyFlag &Node::DestroyFlag::operator = ( const bool &_set )
{
    Set(_set);
    return *this;
}
void Node::DestroyFlag::SetMaster(Node *_master)
{
    m_Master = _master;
}
void Node::DestroyFlag::Set( const bool &_set )
{
    if(_set) m_Master->OnDestory();
    TempGetSet<bool>::Set(_set);
}


// @brief  : コンストラクタ
//--------------------------------------------------------------------
Node::Node() :
    parent(nullptr),
    prev(nullptr),
    next(nullptr),
    first_child(nullptr),
    last_child(nullptr)
{
    IsDestroy.SetMaster(this);
}

// @brief  : コンストラクタ
//--------------------------------------------------------------------
Node::Node( Node *_parent ) :
    parent(_parent),
    prev(nullptr),
    next(nullptr),
    first_child(nullptr),
    last_child(nullptr)
{
    assert(_parent);
    parent->AddChild(this);
    IsDestroy.SetMaster(this);
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
Node::~Node()
{
    UnLink();
    DeleteChild();
};

// @brief  : ポインタの取得
//--------------------------------------------------------------------
Node *Node::Next( void ) const
{
    return next;
}
Node *Node::Prev( void ) const
{
    return prev;
}
Node *Node::Begin( void ) const
{
    return first_child;
}
Node *Node::End( void ) const
{
    return last_child;
}
Node *Node::Parent( void ) const
{
    return parent;
}

// @brief  : 子の追加
//--------------------------------------------------------------------
void Node::AddChild( Node *_add )
{
    _add->UnLink();

    // 一番最初の子ならそれを保存
    if(!first_child) first_child = _add;

    // 一番最後の子の次が追加された子
    if(last_child) last_child->next = _add;

    // 追加された子の前は最後の子
    _add->prev = End();

    // 追加された子の親は自分
    _add->parent = this;

    // 追加された子が最後になる
    last_child = _add; 
};

// @brief  : 子の削除
//--------------------------------------------------------------------
void Node::DeleteChild( void )
{
    ForEachInversion([](Node *_node){delete _node;});
}

// @brief  : 削除
//--------------------------------------------------------------------
void Node::Destroy()
{
    if( IsDestroy )
    {
        delete this;
        return;
    }
    ForEach([](Node *_node) { _node->Destroy(); } );
}

// @brief  : リンクから削除
//--------------------------------------------------------------------
void Node::UnLink( void )
{
    // 自分の前から自分を削除
    if( Prev() != nullptr ) prev->next = next;

    // 自分の次から自分を削除
    if( Next() != nullptr ) next->prev = prev;

    // 自分の親の先頭なら、自分の次を親の先頭にする
    if(Parent())
    {
        if( Parent()->Begin() == this ) parent->first_child = next;

        // 自分の親の最後なら、自分の前を親の最後にする
        if( Parent()->End() == this ) parent->last_child = prev;
    }

    prev = nullptr;
    next = nullptr;
    parent = nullptr;
}

// @brief  : ループ処理
// @param  : ラムダ式(@return void, @param Node *)
//--------------------------------------------------------------------
void Node::ForEach( std::function<void (Node *)> _function )
{
    auto i = Begin();
    while(i)
    {
        auto next = i->Next();
        _function(i);
        i = next;
    }
}

// @brief  : ループ処理(後ろから)
// @param  : ラムダ式(@return void, @param Node *)
//--------------------------------------------------------------------
void Node::ForEachInversion( std::function<void (Node *)>_function )
{
    auto i = End();
    while(i)
    {
        auto prev = i->Prev();
        _function(i);
        i = prev;
    }
}



/******************************************************************************
|   End of File
*******************************************************************************/