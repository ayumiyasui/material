/*
# tempPriority.h

- アクセサ(getter,setter)をC#のプロパティ風するためのクラス
- public継承をした他のテンプレートクラスで使用される

@note  :
- [C++のアクセサgetter, setterをかっこよく書きたい。](http://d.hatena.ne.jp/poyonon/20110828/1314487034)
- [C++でProperty（getter/setter）](http://qiita.com/HogeTatu/items/1bb3a394f88ba90cd37e)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __TEMPPRIORITY_H__
#define __TEMPPRIORITY_H__


template < typename T > class TempPriority
{
protected:
    // @brief  : コンストラクタ
    // @param  : 元の変数
    //--------------------------------------------------------------------
    explicit TempPriority( T &_value ) : m_Value(_value){};

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~TempPriority(){};

    // @brief  : Get関数
    // @return : 元の変数
    //--------------------------------------------------------------------
    T &Get() const{ return m_Value; };

    // @brief  : Set関数
    // @param  : 初期化したい変数
    //--------------------------------------------------------------------
    void Set( const T &_set ){ m_Value = _set; };

private:
    // メンバ変数
    //--------------------------------------------------------------------
    T &m_Value;   // 変更したい変数
};



#endif //  __TEMPPRIORITY_H__
/******************************************************************************
|   End of File
*******************************************************************************/