/*
# tempSingleton.h

- GoFのデザインパターン「シングルトン」のテンプレートクラス
- public継承で使用できる

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __TEMPNODESINGLETON_H__
#define __TEMPNODESINGLETON_H__

#include <assert.h>
#include "notCopy.h"
#include "node.h"

template < typename T > class TempNodeSingleton : private NotCopy, public Node
{
public:
    // @brief  : シングルトンの取得
    //--------------------------------------------------------------------
    static T& Instance( void )
    {
        if(!g_Instance) g_Instance = new T;
        return *g_Instance;
    };

protected:
    // グローバル変数
    //--------------------------------------------------------------------
    static T *g_Instance;

    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    TempNodeSingleton( Node *_parent = &TempSingleton<Node>::Instance() ) : Node(_parent) {};

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~TempNodeSingleton(){ g_Instance = nullptr; };
};

template < typename T >
T *TempNodeSingleton<T>::g_Instance = nullptr;



#endif //  __TEMPNODESINGLETON_H__
/******************************************************************************
|   End of File
*******************************************************************************/