/*
# tempGetSet.h

- getter,setterをC#のプロパティ風するためのクラス
- そのままテンプレートとして使うもよし、継承して処理を行うもよし

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __TEMPPGETSET_H__
#define __TEMPPGETSET_H__

#include "notCopy.h"
#include "tempPriority.h"

template < typename T > class TempGetSet : private TempPriority<T>
{
public:
    // @brief  : コンストラクタ
    // @param  : 元の変数
    //--------------------------------------------------------------------
    explicit TempGetSet( T &_value ) : TempPriority<T>(_value){};

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~TempGetSet(){};

    // @brief  : 型変換
    //--------------------------------------------------------------------
    operator const T &() const{ return TempPriority<T>::Get(); };

    // @brief  : 代入
    //--------------------------------------------------------------------
    TempGetSet<T> &operator = ( const T &_set ){ Set(_set); return *this; };

    // @brief  : アクセス指定子
    //--------------------------------------------------------------------
    T *operator ->() { return &TempPriority<T>::Get(); };

protected:
    // @brief  : Set関数
    //--------------------------------------------------------------------
    virtual void Set( const T &_set ){ TempPriority<T>::Set(_set); };

};



#endif //  __TEMPPGETSET_H__
/******************************************************************************
|   End of File
*******************************************************************************/