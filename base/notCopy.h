/*
# notCopy.h

- コピー禁止を行うクラス
- private継承をして使用。

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __NOTCOPY_H__
#define __NOTCOPY_H__



class NotCopy
{
private:
    // @brief  : コピーコンストラクタ
    //--------------------------------------------------------------------
    explicit NotCopy( const NotCopy & ){};

    // @brief  : 代入
    //--------------------------------------------------------------------
    void operator = ( const NotCopy & ){};

protected:
    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    NotCopy(){};

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~NotCopy(){};
};



#endif //  _NOTCOPY_H__
/******************************************************************************
|   End of File
*******************************************************************************/