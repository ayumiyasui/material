/*
# tempSmartPtr.h

- 参照された数を数え、使い終わったら消えるクラス
- public継承或いはテンプレートで使用できる

## 参考
- [cocos2d-xのクラス RefPtr](https://github.com/cocos2d/cocos2d-x/blob/v3/cocos/base/CCRefPtr.h)
- [スマートポインタテンプレートクラス - マルペケつくろー](http://marupeke296.com/CPP_SmartPointer.html)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __TEMPSMARTPTR_H__
#define __TEMPSMARTPTR_H__


#include "tempUseCount.h"

template < typename T > class TempSmartPtr
{
public:
    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    TempSmartPtr( TempUseCount<T> *_obj )
        : m_pObj( _obj )
    {
        if(m_pObj) m_pObj->Retain();
    };

    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    TempSmartPtr()
        : m_pObj(nullptr)
    {
    };

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~TempSmartPtr()
    {
        if(m_pObj) m_pObj->Release();
    };

    // 型変換
    //--------------------------------------------------------------------
    operator bool () const{ return (m_pObj != nullptr); };

    // @brief  : ポインタとして使用する
    //--------------------------------------------------------------------
    T *operator ->() { return  (T *)m_pObj; };
    T &operator  *() { return  *(T)m_pObj; };

    // @brief  : コピー
    //--------------------------------------------------------------------
    TempSmartPtr &operator = ( const TempSmartPtr<T> &_src )
    {
        m_pObj = (TempUseCount<T> *)_src.m_pObj;
        m_pObj->Retain();
        return *this;
    };

    TempSmartPtr( const TempSmartPtr<T> &_src )
    {
        m_pObj = (TempUseCount<T> *)_src.m_pObj;
        m_pObj->Retain();
    };

private:
    TempUseCount<T> *m_pObj;   // 使用するオブジェクト
};



#endif //  __TEMPSMARTPTR_H__
/******************************************************************************
|   End of File
*******************************************************************************/