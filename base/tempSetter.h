/*
# tempSetter.h

- setterをC#のプロパティ風するためのクラス
- そのままテンプレートとして使うもよし、継承して処理を行うもよし

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __TEMPPSETTER_H__
#define __TEMPPSETTER_H__


#include "notCopy.h"
#include "tempPriority.h"


template < typename T > class TempSetter : private TempPriority<T>, private NotCopy
{
public:
    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    explicit TempSetter( T &_value ) : TempPriority<T>(_value){};

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~TempSetter(){};

    // @brief  : 代入
    //--------------------------------------------------------------------
    TempSetter<T> &operator = ( const T &_set ){ TempPriority<T>::Set(_set); };

protected:
    // @brief  : Set関数
    //--------------------------------------------------------------------
    virtual void Set( const T &_set ){ TempPriority<T>::Set(_set); };
};



#endif //  __TEMPPSETTER_H__
/******************************************************************************
|   End of File
*******************************************************************************/