/*
# filePass.h

- ファイルパスクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __FILEPASS_H__
#define __FILEPASS_H__

#include <string>

#include "../base/tempGetter.h"


class FilePass
{
public:
    // @brief  : コンストラクタ
    // @param  : ファイルパス
    //--------------------------------------------------------------------
    FilePass( const std::string &_pass ) : m_Pass(_pass) {};

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~FilePass(){};

    // 型変換
    //--------------------------------------------------------------------
    operator const std::string &() const{ return m_Pass; };
    operator const char *() const{ if(m_Pass.empty()) return nullptr; return m_Pass.c_str(); };

private:
    // メンバ変数
    //--------------------------------------------------------------------
    std::string m_Pass; // ファイルパス
};



#endif  // __FILEPASS_H__
/******************************************************************************
|   End of File
*******************************************************************************/