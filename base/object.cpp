/*
# object.cpp

- オブジェクトクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード --------------------------------------------------------------
#include "object.h"

#include <assert.h>

#pragma warning(push)   // インクリメントでメンバ変数にthisを渡している
#pragma warning(disable:4355)
// @brief  : コンストラクタ
//--------------------------------------------------------------------
Object::Object() :
    Node(&TempSingleton<Node>::Instance()),
    m_IsUpdate(true),
    IsUpdate(m_IsUpdate,this)
{
}

// @brief  : コンストラクタ
// @param  : 親のObject
//         : 更新する or しない
//--------------------------------------------------------------------
Object::Object( Node *_parent, bool _is_update ) :
    Node(_parent),
    m_IsUpdate(_is_update),
    IsUpdate(m_IsUpdate,this)
{
}
#pragma warning(pop)

// @brief  : デストラクタ
//--------------------------------------------------------------------
Object::~Object()
{
}

// @brief  : すべての更新
//--------------------------------------------------------------------
void Object::UpdateAll( void )
{
    if(!m_IsUpdate) return;

    Update();

    if(!m_IsUpdate) return;
    if(IsDestroy)   return;
    ForEach(
        [](Node *_node){
            assert(dynamic_cast<Object *>(_node));
            ((Object *)_node)->UpdateAll();
        }
    );
}

// 内部クラス
//--------------------------------------------------------------------
Object::UpdateEnable::UpdateEnable( bool &_value, Object *_master ) :
    TempGetSet<bool>(_value),
    m_Master(_master)
{
}

Object::UpdateEnable::~UpdateEnable()
{
}

void Object::UpdateEnable::Set( const bool &_set )
{
    TempGetSet<bool>::Set(_set);
    (_set) ? m_Master->OnEnable() : m_Master->OnDisable();
}



/******************************************************************************
|   End of File
*******************************************************************************/