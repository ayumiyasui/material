/*
# tempUseCount.h

- 参照された数を数え、使い終わったら消えるクラス
- public継承で使用できる
- Destroy関数を後で呼ぶこと

## 参考
- [cocos2d-xのクラス Ref](https://github.com/cocos2d/cocos2d-x/blob/v3/cocos/base/CCRef.cpp)

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __TEMPUSECOUNT_H__
#define __TEMPUSECOUNT_H__

#include <assert.h>
#include "notCopy.h"
#include "tempGetter.h"


template < typename T > class TempUseCount : private NotCopy
{
public:
    // @brief  : 使用数を追加する
    //--------------------------------------------------------------------
    void Retain( void ){ ++m_CountLook; };

    // @brief  : 使用数を減らす
    //--------------------------------------------------------------------
    void Release( void ) { assert(m_CountLook - 1 >= 0);--m_CountLook; };

    // @brief  : 参照数の確認
    //--------------------------------------------------------------------
    TempGetter<unsigned int> CountLook;

protected:
    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    TempUseCount() : m_CountLook( 0 ), CountLook( m_CountLook ) {};

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~TempUseCount() {};

private:
    // @brief  : メンバ変数
    //--------------------------------------------------------------------
    unsigned int m_CountLook;    // 使用数

    // @brief  : 削除できるのか
    //--------------------------------------------------------------------
    bool IsReadyDestroy( void ) const { return m_CountLook <= 0; };
};



#endif //  __TEMPUSECOUNT_H__
/******************************************************************************
|   End of File
*******************************************************************************/