/*
# tempGetter.h

- getterをC#のプロパティ風するためのクラス
- そのままテンプレートとして使うもよし、継承して処理を行うもよし

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __TEMPPGETTER_H__
#define __TEMPPGETTER_H__


#include "notCopy.h"
#include "tempPriority.h"


template < typename T > class TempGetter : private TempPriority<T>
{
public:
    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    explicit TempGetter( T &_value ) : TempPriority<T>(_value){};

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~TempGetter(){};

    // @brief  : 型変換
    //--------------------------------------------------------------------
    operator const T &() const{ return TempPriority<T>::Get(); };

    // @brief  : アクセス指定子
    //--------------------------------------------------------------------
    const T &operator  *() const{ return  TempPriority<T>::Get(); };
    const T *operator ->() const{ return &TempPriority<T>::Get(); };
};



#endif //  __TEMPPGETTER_H__
/******************************************************************************
|   End of File
*******************************************************************************/