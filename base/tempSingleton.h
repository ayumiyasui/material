/*
# tempSingleton.h

- GoFのデザインパターン「シングルトン」のテンプレートクラス
- public継承で使用できる

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __TEMPSINGLETON_H__
#define __TEMPSINGLETON_H__


#include "notCopy.h"

template < typename T > class TempSingleton : private NotCopy
{
public:
    // @brief  : シングルトンの取得
    //--------------------------------------------------------------------
    static T& Instance( void )
    {
        static T g_Instance;
        return g_Instance;
    };

    // @brief  : ポインタとして使用する
    //--------------------------------------------------------------------
    T *operator ->() { return &Instance; };
    T &operator  *() { return Instance; };

protected:
    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    TempSingleton(){};

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~TempSingleton(){};
};



#endif //  __TEMPSINGLETON_H__
/******************************************************************************
|   End of File
*******************************************************************************/