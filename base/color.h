/*
# color.h

- カラークラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __COLOR_H__
#define __COLOR_H__

class Color
{
public:
    float r,g,b,a;

    Color( float _r = 0.0f, float _g = 0.0f, float _b = 0.0f, float _a = 1.0f ) :
      r(_r),g(_g),b(_b),a(_a){};

    Color( int _r , int _g, int _b) :
      r(_r/255.0f),g(_g/255.0f),b(_b/255.0f),a(1.0f){};
    ~Color(){};

#ifdef __D3DX9MATH_H__
    operator D3DXCOLOR () const { return D3DXCOLOR(r,g,b,a); };
#endif
};



#endif //  __COLOR_H__
/******************************************************************************
|   End of File
*******************************************************************************/