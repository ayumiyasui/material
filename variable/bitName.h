/*
# bitName.h

- 変数型
- メモリの大きさを気にする場合に使用
- 主にリソースの読み込みに多い

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef MATERIAL_VARIABLE_BIT_NAME_H__
#define MATERIAL_VARIABLE_BIT_NAME_H__

namespace material {
namespace variable {

//--- 変数型 -------------------------------------------------------------------
#if   _WIN64    // Windows 64bit
// 1byte
typedef unsigned char  u8;
typedef signed   char  s8;

// 2byte
typedef unsigned short u16;
typedef signed   short s16;

// 4byte
typedef unsigned int   u32;
typedef signed   int   s32;
typedef float          f32;

// 8 byte
typedef unsigned long  u64;
typedef signed   long  s64;
typedef double         f64;

#elif _WIN32    // Windows 32bit
// 1byte
typedef unsigned char  u8;
typedef signed   char  s8;

// 2byte
typedef unsigned short u16;
typedef signed   short s16;

// 4byte
typedef unsigned int   u32;
typedef signed   int   s32;
typedef float          f32;

// 8 byte
typedef unsigned long long u64;
typedef signed   long long s64;
typedef double             f64;
    
#elif __APPLE__ // XCode

#if __LP64__    // 64bit
// 1byte
typedef unsigned char  u8;
typedef signed   char  s8;
    
// 2byte
typedef unsigned short u16;
typedef signed   short s16;
    
// 4byte
typedef unsigned int   u32;
typedef signed   int   s32;
typedef float          f32;
    
// 8 byte
typedef unsigned long long u64;
typedef signed   long long s64;
typedef double             f64;
    
#elif           // 32bit
// 1byte
typedef unsigned char  u8;
typedef signed   char  s8;
    
// 2byte
typedef unsigned short u16;
typedef signed   short s16;
    
// 4byte
typedef unsigned int   u32;
typedef signed   int   s32;
typedef float          f32;
    
// 8 byte
typedef unsigned long long u64;
typedef signed   long long s64;
typedef double             f64;
    
#endif          // bitの違い
#endif          // OSの違い



//--- バッファ -----------------------------------------------------------------
// 2byte
union uni16
{
    u16  u;
    s16  s;
    char buf[2];
};
union uni16_u
{
    u16  u;
    char buf[2];
};
union uni16_s
{
    s16  s;
    char buf[2];
};

// 4byte
union uni32
{
    u32   u;
    s32   s;
    f32   f;
    char buf[4];
};
union uni32_u
{
    u32   u;
    char buf[4];
};
union uni32_s
{
    s32   s;
    char buf[4];
};
union uni32_f
{
    f32   f;
    char buf[4];
};

// 8byte
union uni64
{
    u64 u;
    s64 s;
    f64 f;
    char buf[8];
};
union uni64_u
{
    u64 u;
    char buf[8];
};
union uni64_s
{
    s64 s;
    char buf[8];
};
union uni64_f
{
    f64 f;
    char buf[8];
};


};  // namespace variable
};  // namespace material
#endif // MATERIAL_VARIABLE_BIT_NAME_H__
/******************************************************************************
|   End of File
*******************************************************************************/