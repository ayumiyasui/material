/*
# f8sNormal.h

- floatを256bitに変換する
- 元データよりも荒くなるが、リプレイのセーブデータを大幅に小さくできる

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef MATERIAL_VARIABLE_F8_NORMAL_H_
#define MATERIAL_VARIABLE_F8_NORMAL_H_

#include <assert.h>
#include "bitName.h"

namespace material {
namespace variable {

// -1.0f ~ 1.0fを-127 ~ 127に変換する
//-----------------------------------------------------------------------------
class f8sNormal
{
public:
    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    f8sNormal(){};
    f8sNormal(float _value){Set(_value);};
    f8sNormal(s8 _value){Set(_value);};

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~f8sNormal(){};

    // @brief  : 型変換
    //--------------------------------------------------------------------
    operator float () const{return m_Data/127.0f;};
    operator s8    () const{return m_Data;};

    // @brief  : 保存関数
    // @param  : 1.0f ~ 0.0f
    //--------------------------------------------------------------------
    void Set(float _value){assert(-1.0f<=_value&&_value<=1.0f); m_Data = (s8)(127.0f*_value);};
    void Set(s8 _value){m_Data = _value;};

private:
    // メンバ変数
    //--------------------------------------------------------------------
    s8 m_Data;  // データ
};



// 0.0f ~ 1.0fを0 ~ 255に変換する
//-----------------------------------------------------------------------------
class f8uNormal
{
public:
    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    f8uNormal(){};
    f8uNormal(float _value){Set(_value);};
    f8uNormal(u8 _value){Set(_value);};

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~f8uNormal(){};

    // @brief  : 型変換
    //--------------------------------------------------------------------
    operator float () const{return m_Data/255.0f;};
    operator u8    () const{return m_Data;};

    // @brief  : 保存関数
    // @param  : 1.0f ~ 0.0f
    //--------------------------------------------------------------------
    void Set(float _value){assert(0.0f<=_value&&_value<=1.0f); m_Data = (u8)(255.0f*_value);};
    void Set(u8 _value){m_Data = _value;};

private:
    // メンバ変数
    //--------------------------------------------------------------------
    u8 m_Data;  // データ
};


};  // namespace variable
};  // namespace material
#endif // MATERIAL_VARIABLE_F8_NORMAL_H_
/******************************************************************************
|   End of File
*******************************************************************************/