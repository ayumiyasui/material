/*
# vector2.h

- 2D用ベクトルクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __VECTOR2_H__
#define __VECTOR2_H__

//--- マクロ定義 --------------------------------------------------------------
#ifdef ZERO
#undef ZERO
#endif
#ifdef UP
#undef UP
#endif
#ifdef DOWN
#undef DOWN
#endif
#ifdef LEFT
#undef LEFT
#endif
#ifdef RIGHT
#undef RIGHT
#endif


class Matrix2D;
class Vector3;


class Vector2
{
public:
    // 定数
    //--------------------------------------------------------------------
    static const Vector2 ZERO;     // ゼロベクトル
    static const Vector2 LEFT;     // 左
    static const Vector2 RIGHT;    // 右
    static const Vector2 UP;       // 上
    static const Vector2 DOWN;     // 下
    static const Vector2 X;     // X軸
    static const Vector2 Y;     // Y軸

    // メンバ変数
    //--------------------------------------------------------------------
    float x;
    float y;

    // @brief : コンストラクタ
    //--------------------------------------------------------------------
    Vector2( float x = 0.0f, float y = 0.0f );

    // @brief : デストラクタ
    //--------------------------------------------------------------------
    ~Vector2();

    // @brief : 単項演算子
    //--------------------------------------------------------------------
    Vector2 operator + ( ) const;
    Vector2 operator - ( ) const;

    // @brief : キャスト
    //--------------------------------------------------------------------
    operator Vector3 () const;
#ifdef __D3DX9MATH_H__  // DiQuadX 9
    operator D3DXVECTOR2 () const;
#endif


    // @brief : 加算
    //--------------------------------------------------------------------
    Vector2  operator +  ( const Vector2 &v ) const;
    Vector2 &operator += ( const Vector2 &v );

    // @brief : 減算
    //--------------------------------------------------------------------
    Vector2  operator -  ( const Vector2 &v ) const;
    Vector2 &operator -= ( const Vector2 &v );

    // @brief : 乗算
    //--------------------------------------------------------------------
    Vector2  operator *  ( float f ) const;
    Vector2 &operator *= ( float f );

    // @brief : 除算
    //--------------------------------------------------------------------
    Vector2  operator /  ( float f ) const;
    Vector2 &operator /= ( float f );

    // @brief : 行列との計算
    //--------------------------------------------------------------------
    Vector2  operator *  ( const Matrix2D & ) const;
    Vector2 &operator *= ( const Matrix2D & );

    // @brief : 内積
    //--------------------------------------------------------------------
    float Dot( const Vector2 & ) const;

    // @brief : 外積
    //--------------------------------------------------------------------
    float Cross( const Vector2 & ) const;

    // @brief : 長さ
    //--------------------------------------------------------------------
    float Length( void ) const;
    float SquareLength( void ) const;

    // @brief : 角度
    //--------------------------------------------------------------------
    float Angle(void) const;
    static float RotationAngle( const Vector2 &_start, const Vector2 &_end );

    // @brief : 三角関数
    //--------------------------------------------------------------------
    float Cos(void) const;
    float Sin(void) const;
    float Tan(void) const;

    // @brief : 正規化
    //--------------------------------------------------------------------
    Vector2 Normalize( void ) const;
};



#endif //  __VECTOR2_H__
/******************************************************************************
|   End of File
*******************************************************************************/