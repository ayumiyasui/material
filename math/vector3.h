/*
# vector3.h

- 3D用ベクトルクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __VECTOR3_H__
#define __VECTOR3_H__

//--- マクロ定義 --------------------------------------------------------------
#ifdef ZERO
#undef ZERO
#endif
#ifdef UP
#undef UP
#endif
#ifdef DOWN
#undef DOWN
#endif
#ifdef LEFT
#undef LEFT
#endif
#ifdef RIGHT
#undef RIGHT
#endif
#ifdef NEAR
#undef NEAR
#endif
#ifdef FAR
#undef FAR
#endif


class Matrix3D;
class Vector2;


class Vector3
{
public:
    // 定数
    //--------------------------------------------------------------------
    static const Vector3 ZERO;     // ゼロベクトル
    static const Vector3 NEAR;     // 手前
    static const Vector3 FAR;      // 奥
    static const Vector3 LEFT;     // 左
    static const Vector3 RIGHT;    // 右
    static const Vector3 UP;       // 上
    static const Vector3 DOWN;     // 下
    static const Vector3 X;         // X軸
    static const Vector3 Y;         // Y軸
    static const Vector3 Z;         // Z軸

    // メンバ変数
    //--------------------------------------------------------------------
    float x, y, z;

    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    Vector3( float _x = 0.0f, float _y = 0.0f, float _z = 0.0f );

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~Vector3();

    // @brief : キャスト
    //--------------------------------------------------------------------
    operator Vector2 () const;
#ifdef __D3DX9MATH_H__  // DiQuadX 9
    operator D3DXVECTOR3 () const;
#endif

    // @brief : 単項演算子
    //--------------------------------------------------------------------
    Vector3 operator + ( ) const;
    Vector3 operator - ( ) const;

    // @brief : 加算
    //--------------------------------------------------------------------
    Vector3  operator +  ( const Vector3 &v ) const;
    Vector3 &operator += ( const Vector3 &v );

    // @brief : 減算
    //--------------------------------------------------------------------
    Vector3  operator -  ( const Vector3 &v ) const;
    Vector3 &operator -= ( const Vector3 &v );

    // @brief : 乗算
    //--------------------------------------------------------------------
    Vector3  operator *  ( float f ) const;
    Vector3 &operator *= ( float f );

    // @brief : 除算
    //--------------------------------------------------------------------
    Vector3  operator /  ( float f ) const;
    Vector3 &operator /= ( float f );

    // @brief : 行列との計算
    //--------------------------------------------------------------------
    Vector3  operator *  ( const Matrix3D & ) const;
    Vector3 &operator *= ( const Matrix3D & );

    // @brief : 内積
    //--------------------------------------------------------------------
    float Dot( const Vector3 & ) const;

    // @brief : 外積
    //--------------------------------------------------------------------
    Vector3 Cross( const Vector3 & ) const;

    // @brief : 長さの二乗
    //--------------------------------------------------------------------
    float SquareLength( void ) const;

    // @brief : 長さ
    //--------------------------------------------------------------------
    float Length( void ) const;

    // @brief : 角度
    //--------------------------------------------------------------------
    static float RotationAngle( const Vector3 &_start, const Vector3 &_end );

    // @brief : 回転軸
    //--------------------------------------------------------------------
    static Vector3 AxisOfRotation( const Vector3 &_start, const Vector3 &_end );

    // @brief : 正規化
    //--------------------------------------------------------------------
    Vector3 Normalize( void ) const;
};



#endif //  __VECTOR3_H__
/******************************************************************************
|   End of File
*******************************************************************************/