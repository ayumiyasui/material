/*
# quaternion.h

- 4元数クラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __QUATERNION_H__
#define __QUATERNION_H__

class Vector3;
class Matrix3D;

class Quaternion
{
public:
    // 定数
    //--------------------------------------------------------------------
    static const Quaternion IDENTITY;

    // @brief  : コンストラクタ
    // @param  : 回転軸
    //         : 回転角
    //--------------------------------------------------------------------
    Quaternion( const Vector3 &, float );

    // @brief  : コンストラクタ
    // @param  : Yaw Pitch Roll回転
    //--------------------------------------------------------------------
    Quaternion( float yaw, float pitch, float roll );

    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    Quaternion( float w, float x, float y, float z );

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~Quaternion();

    // @brief  : ノーマライズ
    //--------------------------------------------------------------------
    Quaternion Normalize(void) const;

    // @brief  : 行列に変換
    //--------------------------------------------------------------------
    operator Matrix3D () const;

    // @brief  : マイナス
    //--------------------------------------------------------------------
    Quaternion operator - () const;

    // @brief  : 掛け算
    //--------------------------------------------------------------------
    Quaternion  operator *  ( const Quaternion & ) const;
    Quaternion &operator *= ( const Quaternion & );
    Quaternion  operator *  ( float & ) const;
    Quaternion &operator *= ( float & );

    // @brief  : 球面線形補間
    //--------------------------------------------------------------------
    static Quaternion Sleap(const Quaternion &, const Quaternion &, float);

    // メンバ変数
    //--------------------------------------------------------------------
    float w, x, y, z;
};



#endif //  __QUATERNION_H__
/******************************************************************************
|   End of File
*******************************************************************************/