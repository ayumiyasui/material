/*
# Quad.h

- 四角クラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __QUAD_H__
#define __QUAD_H__

class Quad
{
public:
    // メンバ変数
    //--------------------------------------------------------------------
    float left;
    float right;
    float top;
    float bottom;

    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    Quad( const float _left = 0.0f, const float _right  = 0.0f,
          const float _top  = 0.0f, const float _bottom = 0.0f ) :
        left(_left),right(_right),top(_top),bottom(_bottom)
    {
    };

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~Quad(){};
};



#endif //  __RECTANGLE_H__
/******************************************************************************
|   End of File
*******************************************************************************/