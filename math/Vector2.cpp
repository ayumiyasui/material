/*
# vector2.cpp

- 2D用ベクトルクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード ------------------------------------------------------------
#include <math.h>
#ifdef _WIN32
#include <d3dx9math.h>
#endif

#include "vector3.h"
#include "matrix2d.h"

#include "vector2.h"


// 定数
//--------------------------------------------------------------------
const Vector2 Vector2::ZERO  = Vector2(  0.0f,  0.0f );
const Vector2 Vector2::LEFT  = Vector2( -1.0f,  0.0f );
const Vector2 Vector2::RIGHT = Vector2(  1.0f,  0.0f );
const Vector2 Vector2::UP    = Vector2(  0.0f, -1.0f );
const Vector2 Vector2::DOWN  = Vector2(  0.0f,  1.0f );
const Vector2 Vector2::X     = Vector2(  1.0f,  0.0f );
const Vector2 Vector2::Y     = Vector2(  0.0f,  1.0f );

// @brief : コンストラクタ
//--------------------------------------------------------------------
Vector2::Vector2( float x, float y ) :
    x(x),
    y(y)
{
}

// @brief : デストラクタ
//--------------------------------------------------------------------
Vector2::~Vector2()
{
}

// @brief : キャスト
//--------------------------------------------------------------------
Vector2 Vector2::operator + ( ) const
{
    return *this;
}
Vector2 Vector2::operator - ( ) const
{
    return Vector2( -this->x, -this->y );
}
Vector2::operator Vector3() const
{
    return Vector3(x,y,0.0f);
}
#ifdef _WIN32
Vector2::operator D3DXVECTOR2 () const
{
    return D3DXVECTOR2(x,y);
}
#endif

// @brief : 加算
//--------------------------------------------------------------------
Vector2 Vector2::operator + ( const Vector2 &v ) const
{
    return Vector2( this->x + v.x, this->y + v.y );
};
Vector2 &Vector2::operator += ( const Vector2 &v )
{
    this->x += v.x;
    this->y += v.y;
    return *this;
}

// @brief : 減算
//--------------------------------------------------------------------
Vector2 Vector2::operator - ( const Vector2 &v ) const
{
    return Vector2( this->x -v.x, this->y - v.y );
}
Vector2 &Vector2::operator -= ( const Vector2 &v )
{
    this->x -= v.x;
    this->y -= v.y;
    return *this;
}

// @brief : 乗算
//--------------------------------------------------------------------
Vector2 Vector2::operator * ( float f ) const
{
    return Vector2( this->x * f, this->y * f );
}
Vector2 &Vector2::operator *= ( float f )
{
    this->x *= f;
    this->y *= f;
    return *this;
}

// @brief : 除算
//--------------------------------------------------------------------
Vector2 Vector2::operator / ( float f ) const
{
    return Vector2( this->x / f, this->y / f );
}
Vector2 &Vector2::operator /= ( float f )
{
    float Inv = 1.0f / f;
    this->x *= Inv;
    this->y *= Inv;
    return *this;
}


// @brief : 行列との計算
//--------------------------------------------------------------------
Vector2  Vector2::operator *  ( const Matrix2D &_m ) const
{
    return Vector2(
        x * _m._11 + y * _m._12 + _m._13,
        x * _m._21 + y * _m._22 + _m._23
    );
}
Vector2 &Vector2::operator *= ( const Matrix2D &_m )
{
    *this = *this * _m;
    return *this;
}

// @brief : 内積
//--------------------------------------------------------------------
float Vector2::Dot( const Vector2 &_v ) const
{
    return x * _v.x + y * _v.y;
}

// @brief : 外積
//--------------------------------------------------------------------
float Vector2::Cross( const Vector2 &_v ) const
{
    return x * _v.y - y * _v.x;
}

// @brief : 長さ
//--------------------------------------------------------------------
float Vector2::Length( void ) const
{
    return sqrtf(SquareLength());
}
float Vector2::SquareLength(void) const
{
    return x * x + y * y;
}

// @brief : 角度
//--------------------------------------------------------------------
float Vector2::Angle(void) const
{
    return atan2f(y,x);
}

float Vector2::RotationAngle( const Vector2 &_start, const Vector2 &_end )
{
    return acosf(_start.Dot(_end));
}
// @brief : 三角関数
//--------------------------------------------------------------------
float Vector2::Cos(void) const{ return x / Length(); };
float Vector2::Sin(void) const{ return y / Length(); };
float Vector2::Tan(void) const{ return y / x; };

// @brief : 正規化
//--------------------------------------------------------------------
Vector2 Vector2::Normalize( void ) const
{
    return (*this) / Length();
}



/******************************************************************************
|   End of File
*******************************************************************************/