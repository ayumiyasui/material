/*
# matrix3d.h

- 3D用行列クラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __MATRIX3D_H__
#define __MATRIX3D_H__



class Matrix3D
{
public:
    // 定数
    //--------------------------------------------------------------------
    static const Matrix3D ZERO;
    static const Matrix3D IDENTITY;

    // メンバ変数
    //--------------------------------------------------------------------
    union
    {
        struct
        {
            float _11, _12, _13, _14;
            float _21, _22, _23, _24;
            float _31, _32, _33, _34;
            float _41, _42, _43, _44;
        };
        float m[4][4];
    };

    // @brief  : コンストラクタ
    // @param  : 行列の値
    //--------------------------------------------------------------------
    Matrix3D(
        float _11 = 1.0f, float _12 = 0.0f, float _13 = 0.0f, float _14 = 0.0f,
        float _21 = 0.0f, float _22 = 1.0f, float _23 = 0.0f, float _24 = 0.0f,
        float _31 = 0.0f, float _32 = 0.0f, float _33 = 1.0f, float _34 = 0.0f,
        float _41 = 0.0f, float _42 = 0.0f, float _43 = 0.0f, float _44 = 1.0f );

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    virtual ~Matrix3D();

    // @brief : キャスト
    //--------------------------------------------------------------------
#ifdef __D3DX9MATH_H__  // DiQuadX 9
    operator D3DXMATRIX () const;
    Matrix3D(const D3DXMATRIX &);
#endif

    // @brief  : 行列同士の乗算
    //--------------------------------------------------------------------
    Matrix3D  operator *  ( const Matrix3D & ) const;
    Matrix3D &operator *= ( const Matrix3D & );

    // @brief  : 逆行列
    //--------------------------------------------------------------------
    Matrix3D Inv(void) const;
};



#endif //  __MATRIX3D_H__
/******************************************************************************
|   End of File
*******************************************************************************/