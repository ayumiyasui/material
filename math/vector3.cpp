/*
# vector3.h

- 3D用ベクトルクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード ------------------------------------------------------------
#include <math.h>
#if _WIN32
#include <d3dx9math.h>
#endif

#include "vector2.h"
#include "matrix3d.h"

#include "vector3.h"

// 定数
//--------------------------------------------------------------------
const Vector3 Vector3::ZERO ( 0.0f, 0.0f, 0.0f);     // ゼロベクトル
const Vector3 Vector3::NEAR ( 0.0f, 0.0f,-1.0f);     // 手前
const Vector3 Vector3::FAR  ( 0.0f, 0.0f, 1.0f);     // 奥
const Vector3 Vector3::LEFT (-1.0f, 0.0f, 0.0f);     // 左
const Vector3 Vector3::RIGHT( 1.0f, 0.0f, 0.0f);     // 右
const Vector3 Vector3::UP   ( 0.0f, 1.0f, 0.0f);     // 上
const Vector3 Vector3::DOWN ( 0.0f,-1.0f, 0.0f);     // 下
const Vector3 Vector3::X    ( 1.0f, 0.0f, 0.0f);      // X
const Vector3 Vector3::Y    ( 0.0f, 1.0f, 0.0f);      // Y
const Vector3 Vector3::Z    ( 0.0f, 0.0f, 1.0f);      // Z

// @brief  : コンストラクタ
//--------------------------------------------------------------------
Vector3::Vector3( float _x, float _y, float _z ) :
    x(_x), y(_y), z(_z)
{
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
Vector3::~Vector3()
{
}

// @brief : キャスト
//--------------------------------------------------------------------
Vector3::operator Vector2 () const
{
    return Vector2(x,y);
}
#if _WIN32
Vector3::operator D3DXVECTOR3 () const
{
    return D3DXVECTOR3(x,y,-z);
}
#endif

// @brief : 単項演算子
//--------------------------------------------------------------------
Vector3 Vector3::operator + ( ) const
{
    return *this;
}
Vector3 Vector3::operator - ( ) const
{
    return Vector3(-x,-y,-z);
}

// @brief : 加算
//--------------------------------------------------------------------
Vector3  Vector3::operator +  ( const Vector3 &v ) const
{
    return Vector3( x + v.x, y + v.y, z + v.z );
}
Vector3 &Vector3::operator += ( const Vector3 &v )
{
    x += v.x;
    y += v.y;
    z += v.z;
    return *this;
}

// @brief : 減算
//--------------------------------------------------------------------
Vector3  Vector3::operator -  ( const Vector3 &v ) const
{
    return Vector3( x - v.x, y - v.y, z - v.z );
}
Vector3 &Vector3::operator -= ( const Vector3 &v )
{
    x -= v.x;
    y -= v.y;
    z -= v.z;
    return *this;
}

// @brief : 乗算
//--------------------------------------------------------------------
Vector3  Vector3::operator *  ( float _f ) const
{
    return Vector3( x * _f, y * _f, z * _f );
}
Vector3 &Vector3::operator *= ( float _f )
{
    x *= _f;
    y *= _f;
    z *= _f;
    return *this;
}

// @brief : 除算
//--------------------------------------------------------------------
Vector3  Vector3::operator /  ( float _f ) const
{
    const float inv = 1.0f / _f;
    return Vector3( x * inv, y * inv, z * inv );
}
Vector3 &Vector3::operator /= ( float _f )
{
    const float inv = 1.0f / _f;
    x *= inv;
    y *= inv;
    z *= inv;
    return *this;
}

// @brief : 行列との計算
//--------------------------------------------------------------------
Vector3  Vector3::operator *  ( const Matrix3D &_m ) const
{
    return Vector3(
        x * _m._11 + y * _m._12 + z * _m._13 + _m._14,
        x * _m._21 + y * _m._22 + z * _m._23 + _m._24,
        x * _m._31 + y * _m._32 + z * _m._33 + _m._34
    );
}

// @brief : 行列との計算
//--------------------------------------------------------------------
Vector3 &Vector3::operator *= ( const Matrix3D &_m )
{
    *this = *this * _m;
    return *this;
}

// @brief : 内積
//--------------------------------------------------------------------
float Vector3::Dot( const Vector3 &_v ) const
{
    return x * _v.x + y * _v.y + z * _v.z;
}

// @brief : 外積
//--------------------------------------------------------------------
Vector3 Vector3::Cross( const Vector3 &_v ) const
{
    return Vector3(
        y * _v.z - z * _v.y,
        z * _v.x - x * _v.z,
        x * _v.y - y * _v.x
    );
}

// @brief : 長さの二乗
//--------------------------------------------------------------------
float Vector3::SquareLength( void ) const
{
    return x * x + y * y + z * z;
}

// @brief : 長さ
//--------------------------------------------------------------------
float Vector3::Length( void ) const
{
    return sqrtf(SquareLength());
}

// @brief : 角度
//--------------------------------------------------------------------
float Vector3::RotationAngle( const Vector3 &_start, const Vector3 &_end )
{
    return acosf(_start.Dot(_end));
}

// @brief : 回転軸
//--------------------------------------------------------------------
Vector3 Vector3::AxisOfRotation( const Vector3 &_start, const Vector3 &_end )
{
    const Vector3 axis = _start.Cross(_end);
    return axis.Normalize();
}

// @brief : 正規化
//--------------------------------------------------------------------
Vector3 Vector3::Normalize( void ) const
{
    return (*this) / Length();
}


/******************************************************************************
|   End of File
*******************************************************************************/