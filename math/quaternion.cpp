/*
# quaternion.cpp

- 4元数クラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード ------------------------------------------------------------
#include <math.h>
#include "quaternion.h"

#include "math.h"
#include "vector3.h"
#include "matrix3d.h"

// 定数
//--------------------------------------------------------------------
const Quaternion Quaternion::IDENTITY = Quaternion(1.0f,0.0f,0.0f,0.0f);

// @brief  : コンストラクタ
//--------------------------------------------------------------------
Quaternion::Quaternion( float _w, float _x, float _y, float _z ) :
    w(_w),
    x(_x),
    y(_y),
    z(_z)
{
}

// @brief  : コンストラクタ
// @param  : 回転軸
//         : 回転角
//--------------------------------------------------------------------
Quaternion::Quaternion( const Vector3 &_axis, float _angle )
{
    if(_angle == 0.0f) *this = IDENTITY;
    const float rand = 0.5f * _angle;
    const float s = sinf(rand);
    const Vector3 nor = _axis.Normalize();

    w = cosf(rand);
    x = s * nor.x;
    y = s * nor.y;
    z = s * nor.z;
}

// @brief  : コンストラクタ
// @param  : Yaw Pitch Roll回転
//--------------------------------------------------------------------
Quaternion::Quaternion( float _yaw, float _pitch, float _roll )
{
    Quaternion x(Vector3(1.0f,0.0f,0.0f),_pitch);
    Quaternion y(Vector3(0.0f,1.0f,0.0f),_yaw);
    Quaternion z(Vector3(0.0f,0.0f,1.0f),_roll);

    *this  = y * x * z;
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
Quaternion::~Quaternion()
{
}

// @brief  : ノーマライズ
//--------------------------------------------------------------------
Quaternion Quaternion::Normalize(void) const
{
    const float inv_mag = 1.0f / sqrtf(w*w+x*x+y*y+z*z);
    return Quaternion(
        w * inv_mag,
        x * inv_mag,
        y * inv_mag,
        z * inv_mag
    );
}

// @brief  : 行列に変換
//--------------------------------------------------------------------
Quaternion::operator Matrix3D () const
{
    const float x2 = 2.0f * x * x;
    const float y2 = 2.0f * y * y;
    const float z2 = 2.0f * z * z;

    const float xy = 2.0f * x * y;
    const float yz = 2.0f * y * z;
    const float zx = 2.0f * z * x;

    const float wx = 2.0f * w * x;
    const float wy = 2.0f * w * y;
    const float wz = 2.0f * w * z;

    return Matrix3D(
        1.0f - y2 - z2,
        xy - wz,
        zx + wy,
        0.0f,

        xy + wz,
        1.0f - z2 - x2,
        yz - wx,
        0.0f,

        zx - wy,
        yz + wx,
        1.0f - x2 - y2,
        0.0f,

        0.0f, 0.0f, 0.0f, 1.0f
    );
}

// @brief  : マイナス
//--------------------------------------------------------------------
Quaternion Quaternion::operator - () const
{
    return Quaternion( w,-x,-y,-z);
}

// @brief  : 掛け算
//--------------------------------------------------------------------
Quaternion Quaternion::operator *  ( const Quaternion &_q ) const
{
    return Quaternion(
        w * _q.w - x * _q.x - y * _q.y - z * _q.z,
        y * _q.z - z * _q.y + w * _q.x + x * _q.w,
        z * _q.x - x * _q.z + w * _q.y + y * _q.w,
        x * _q.y - y * _q.x + w * _q.z + z * _q.w
    );
}

Quaternion &Quaternion::operator *= ( const Quaternion &_q )
{
    *this = *this * _q;
    return *this;
}
Quaternion  Quaternion::operator *  ( float &_value ) const
{
    return Quaternion(
        w * _value,
        y * _value,
        z * _value,
        x * _value
    );
}
Quaternion &Quaternion::operator *= ( float &_value )
{
    *this = *this * _value;
    return *this;
}

// @brief  : 球面線形補間
//--------------------------------------------------------------------
Quaternion Quaternion::Sleap(const Quaternion &_a, const Quaternion &_b, float _t)
{
    auto qr = _a.w * _b.w + _a.x * _b.x + _a.y * _b.y + _a.z * _b.z;
    auto ss = 1.0f - (qr * qr);
    if (ss == 0.0f)
    {
        return _a;
    }
    auto ph = acosf(qr);
    if(qr < 0.0 && ph > Math::PI * 0.5f)
    {
        qr = - _a.w * _b.w - _a.x * _b.x - _a.y * _b.y
        - _a.z * _b.z;
        ph = acosf(qr);
        auto inv_sin = 1.0f / sinf(ph);
        auto s1 = sinf(ph * (1.0f - _t)) * inv_sin;
        auto s2 = sinf(ph * _t         ) * inv_sin;
      
        return Quaternion(
            _a.w * s1 - _b.w * s2,
            _a.x * s1 - _b.x * s2,
            _a.y * s1 - _b.y * s2,
            _a.z * s1 - _b.z * s2
        );
    }

    auto inv_sin = 1.0f / sinf(ph);
    auto s1 = sinf(ph * (1.0f - _t)) * inv_sin;
    auto s2 = sinf(ph *  _t       )  * inv_sin;
    return Quaternion(
        _a.w * s1 + _b.w * s2,
        _a.x * s1 + _b.x * s2,
        _a.y * s1 + _b.y * s2,
        _a.z * s1 + _b.z * s2
    );
}


/******************************************************************************
|   End of File
*******************************************************************************/