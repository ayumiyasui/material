/*
# math.h

- 数学クラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/

//--- インクルード ------------------------------------------------------------
#include <assert.h>
#include <math.h>
#include "../renderer/directx9/directx9.h"

#include "math.h"
#include "vector2.h"
#include "vector3.h"
#include "quaternion.h"

// 定数
//--------------------------------------------------------------------
const float Math::PI = 3.141592654f;  // ラジアン

// @brief  : コンストラクタ
//--------------------------------------------------------------------
Math::Math()
{
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
Math::~Math()
{
}

// @brief  : ラジアンから度数法に変換
//--------------------------------------------------------------------
float Math::ToRadian(float _degree)
{
    return _degree * PI / 180.0f;
}

// @brief  : 度数法からラジアンに変換
//--------------------------------------------------------------------
float Math::ToDegree(float _radian)
{
    return _radian * 180.0f / PI;
}

// @brief  : 線形補間
// @param  : スタート
//         : 終了
//         : パーセント
//--------------------------------------------------------------------
template < typename T >
T Math::Leap( const T &_start, const T &_end, float _par )
{
    const T length = _end - _start;
    return length * _par + _start;
}
//--- 明示的インスタンス -------------------------------------------------
template <>
Quaternion Math::Leap( const Quaternion &_start, const Quaternion &_end, float _par )
{
    return Quaternion(
        (_end.w - _start.w) * _par + _start.w,
        (_end.x - _start.x) * _par + _start.x,
        (_end.y - _start.y) * _par + _start.y,
        (_end.z - _start.z) * _par + _start.z
    );
}
#define EXPLICIT_SPECIALIZATION(x) template x Math::Leap<x>(const x &,const x &,float);
EXPLICIT_SPECIALIZATION(float)
EXPLICIT_SPECIALIZATION(Vector2)
EXPLICIT_SPECIALIZATION(Vector3)
#ifdef _WIN32
EXPLICIT_SPECIALIZATION(D3DXVECTOR2)
EXPLICIT_SPECIALIZATION(D3DXVECTOR3)
#endif
#undef EXPLICIT_SPECIALIZATION

// @brief  : エルミート曲線
// @param  : スタート
//         : 終了
//         : ベクトル�@
//         : ベクトル�A
//         : パーセント
//--------------------------------------------------------------------
template < typename T >
T Math::HermiteCurve( const T & _start, const T &_end, const T &_v1, const T &_v2, float _t )
{
    const float inv_t = 1.0f - _t;
    const float t_inv = _t - 1.0f;

    return   t_inv * t_inv* (2.0f * _t + 1.0f) * _start
            + _t * _t * (3.0f-2.0f * _t) * _end
            + inv_t * inv_t * _t * _v1
            + _t * _t * t_inv * _v2;
}
//#define EXPLICIT_SPECIALIZATION(x) template x Math::HermiteCurve<x>(const x &,const x &,const x &,const x &,float);
//EXPLICIT_SPECIALIZATION(float)
//EXPLICIT_SPECIALIZATION(Vector2)
//EXPLICIT_SPECIALIZATION(Vector3)
//#undef EXPLICIT_SPECIALIZATION

// @brief  : ベジェ曲線
// @param  : スタート
//         : 終了
//         : 位置�@
//         : 位置�A
//         : パーセント
//--------------------------------------------------------------------
template < typename T >
T Math::BezierCurve( const T & _start, const T &_end, const T &_v1, const T &_v2, float _t )
{
    const float inv_t = 1.0f - _t;

    return   inv_t * inv_t * inv_t * _start
            + (T)3.0 * inv_t * inv_t * _t * _v1
            + (T)3.0 * inv_t * _t * _t * _v2
            + _t * _t * _t * _end;
}
//#define EXPLICIT_SPECIALIZATION(x) template x Math::BezierCurve<x>(const x &,const x &,const x &,const x &,float);
//EXPLICIT_SPECIALIZATION(float)
//EXPLICIT_SPECIALIZATION(Vector2)
//EXPLICIT_SPECIALIZATION(Vector3)
//#undef EXPLICIT_SPECIALIZATION

// @brief  : ポリゴンと線分のあたり判定
// @param  : スタート
//         : 終了
//         : ポリゴンの頂点�@
//         : ポリゴンの頂点�A
//         : ポリゴンの頂点�B
//--------------------------------------------------------------------
template <> bool Math::RayIsHitPolygon(
    const Vector3 &_start, const Vector3 &_end,
    const Vector3 &_p1,const Vector3 &_p2, const Vector3 &_p3 )
{
    // 法線計算
    Vector3 nor;
    {
        const Vector3 cross = _p1.Cross(_p2);
        nor = cross.Normalize();
    }

    // ポリゴンとの交点aを求める
    Vector3 a;
    {
        // 頂点_p1から始点および終点までのベクトルを求める
        Vector3 vec[2] = { _start - _p1, _end - _p1 };

        // 線分が平面を貫通しているのかを確かめる
        const float dot_start = vec[0].Dot(nor);
        {
            const float dot_end = vec[1].Dot(nor);
            if( dot_start * dot_end > 0 ) return false;
        }

        // 内積の値から平面上の点aを求める
        a.x = _start.x - ( dot_start * nor.x );
        a.y = _start.y - ( dot_start * nor.y );
        a.z = _start.z - ( dot_start * nor.z );
    }

    // 求めた交点がポリゴンの中にあるのかを調べる
    {
        // ポリゴンの各辺のベクトル
        const Vector3 p0p1 = _p2 - _p1;
        const Vector3 p1p2 = _p3 - _p2;
        const Vector3 p2p0 = _p1 - _p3;

        // 各頂点と交点とのベクトル
        const Vector3 p0a = a - _p1;
        const Vector3 p1a = a - _p2;
        const Vector3 p2a = a - _p3;

        // 各辺との外積を求める
        const Vector3 c[3] =
        {
            p1a.Cross(p0p1),
            p2a.Cross(p1p2),
            p0a.Cross(p2p0)
        };

        // 鋭角ならあたっている
        const float dot_1 = c[0].Dot(c[1]);
        const float dot_2 = c[0].Dot(c[2]);
        if( dot_1 > 0 && dot_2 > 0 ) return true;
    }

    return false;
}

#ifdef _WIN32
template <> bool Math::RayIsHitPolygon(
    const D3DXVECTOR3 &_start, const D3DXVECTOR3 &_end,
    const D3DXVECTOR3 &_p1,const D3DXVECTOR3 &_p2, const D3DXVECTOR3 &_p3 )
{
    // 法線計算
    D3DXVECTOR3 nor;
    {
        D3DXVec3Cross(&nor,&_p1,&_p2);
        D3DXVec3Normalize(&nor,&nor);
    }

    // ポリゴンとの交点aを求める
    D3DXVECTOR3 a;
    {
        // 頂点_p1から始点および終点までのベクトルを求める
        D3DXVECTOR3 vec[2] = { _start - _p1, _end - _p1 };

        // 線分が平面を貫通しているのかを確かめる
        const float dot_start = D3DXVec3Dot( &vec[0], &nor );
        {
            const float dot_end = D3DXVec3Dot( &vec[1], &nor );
            if( dot_start * dot_end > 0 ) return false;
        }

        // 内積の値から平面上の点aを求める
        a.x = _start.x - ( dot_start * nor.x );
        a.y = _start.y - ( dot_start * nor.y );
        a.z = _start.z - ( dot_start * nor.z );
    }

    // 求めた交点がポリゴンの中にあるのかを調べる
    {
        // ポリゴンの各辺のベクトル
        const D3DXVECTOR3 p0p1 = _p2 - _p1;
        const D3DXVECTOR3 p1p2 = _p3 - _p2;
        const D3DXVECTOR3 p2p0 = _p1 - _p3;

        // 各頂点と交点とのベクトル
        const D3DXVECTOR3 p0a = a - _p1;
        const D3DXVECTOR3 p1a = a - _p2;
        const D3DXVECTOR3 p2a = a - _p3;

        // 各辺との外積を求める
        D3DXVECTOR3 c[3];
        D3DXVec3Cross( &c[0], &p1a, &p0p1 );
        D3DXVec3Cross( &c[1], &p2a, &p1p2 );
        D3DXVec3Cross( &c[2], &p0a, &p2p0 );

        // 鋭角ならあたっている
        const float dot_1 = D3DXVec3Dot( &c[0], &c[1] );
        const float dot_2 = D3DXVec3Dot( &c[0], &c[2] );
        if( dot_1 > 0 && dot_2 > 0 ) return true;
    }

    return false;
}
#endif


/******************************************************************************
|   End of File
*******************************************************************************/