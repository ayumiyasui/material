/*
# win32.h

- Win32アプリケーション設定

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#if _WIN32
#ifndef __WIN32MACRO_H__
#define __WIN32MACRO_H__

//--- マクロ定義 --------------------------------------------------------------
//// メモリリークの場所を教えてくれるdefine
//#ifdef _DEBUG
//   #ifndef DBG_NEW
//      #define DBG_NEW new ( _NORMAL_BLOCK , __FILE__ , __LINE__ )
//      #define new DBG_NEW
//   #endif
//#endif  // _DEBUG
//
#ifndef CONFIG_APPNAME
#define CONFIG_APPNAME "Air Hack"
#endif

#ifndef SCREEN_WIDTH
#define SCREEN_WIDTH (1120)
#endif

#ifndef SCREEN_HEIGHT
#define SCREEN_HEIGHT (630)
#endif

#ifndef CONFIG_MAINPROC
#define CONFIG_MAINPROC   WndProc
#endif

#ifndef CONFIG_ICON
#define CONFIG_ICON   nullptr
#endif

#ifndef CONFIG_MENU
#define CONFIG_MENU   nullptr
#endif

#ifndef CONFIG_CURSOR
#define CONFIG_CURSOR LoadCursor(NULL,IDC_ARROW)
#endif

#ifndef CONFIG_ICON16x16
#define CONFIG_ICON16x16 nullptr
#endif

#ifndef CONFIG_WINDOW_STYLE
#define CONFIG_WINDOW_STYLE (WS_CAPTION | WS_VISIBLE | WS_SYSMENU)
#endif

#ifndef CONFIG_WINDOW_STYLE_EX
#define CONFIG_WINDOW_STYLE_EX WS_EX_APPWINDOW
#endif

#ifndef CONFIG_IS_FULL
#define CONFIG_IS_FULL FALSE
#endif

#endif  // __WIN32MACRO_H__
#endif  // _WIN32
/******************************************************************************
|   End of File
*******************************************************************************/