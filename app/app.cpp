/*
# app.cpp

- アプリケーションクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#if _WIN32

//--- インクルード ------------------------------------------------------------
#include "app.h"

#include "win32.h"
#include <MMSystem.h>
#include "win32Macro.h"

#include "../base/object.h"
#include "../renderer/2d/draw2d.h"
#include "../input/input.h"
#include "../input/inputKeyboard.h"


// @brief  : コンストラクタ
//--------------------------------------------------------------------
App::App() :
    m_Renderer(nullptr),
    m_Audio(nullptr)
    ,m_Win32(CONFIG_MAINPROC,GetModuleHandle(nullptr),
    CONFIG_ICON,CONFIG_CURSOR,
    CONFIG_MENU,CONFIG_APPNAME,
    CONFIG_ICON16x16, CONFIG_APPNAME,
    SCREEN_WIDTH,SCREEN_HEIGHT,
    CONFIG_WINDOW_STYLE,CONFIG_WINDOW_STYLE_EX,CONFIG_IS_FULL),
    m_nExecLastTime(timeGetTime()),
    m_nFPSCheakTime(timeGetTime()),
    m_nCntFrame(0),
    m_nFPSSetting(60),
    m_nFPS(0),
    Win32(m_Win32)
{
    timeBeginPeriod( 1 );    // 分解能を1/1000秒で処理する
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
App::~App()
{
    timeEndPeriod( 1 );
}

int App::Run( IRenderer *_renderer, IAudio *_audio, int _cmd_show )
{
    m_Renderer = _renderer;
    m_Audio    = _audio;

    // ウィンドウ表示
    m_Win32.EnableWindow( _cmd_show );

    Input::Instance();
    Keyboard::Instance();

    // メインループ
    while( m_Win32.IsNotEnd() )
    {
        // メッセージ処理
        m_Win32.UpdateMessage();

        bool is_Update = false;
        {
            DWORD NowTime = timeGetTime();              // 現在時刻

                                                        // FPSを確認する
            if( NowTime - m_nFPSCheakTime > 500 )    // 0.5秒毎に更新
            {
                m_nFPS = ( m_nCntFrame * 1000 ) / ( NowTime - m_nFPSCheakTime );    //FPSの平均値を出す
                m_nFPSCheakTime = NowTime;
                m_nCntFrame = 0;
            }

            if( NowTime - m_nExecLastTime >= 1000 / m_nFPSSetting )
            {
                m_nCntFrame++;             // フレームをカウントする
                m_nExecLastTime = NowTime; // 処理時間を入れる
                is_Update = true;
            }
        }

        // ゲーム処理
        if(is_Update)
        {
            Input::Instance().UpdateAll();
            TempNodeSingleton<Object>::Instance().UpdateAll();
            TempSingleton<Node>::Instance().Destroy();
            Renderer()->Print("FPS : %d\n", m_nFPS );
            Renderer()->Draw();
        }
        else
        {
            Sleep(1);
        }
    }

    // 終了処理
    TempNodeSingleton<Object>::Instance().DeleteChild();
    TempSingleton<Node>::Instance().DeleteChild();
    const int re = m_Win32.Destroy();
    return re;
}

#endif // _WIN32
/******************************************************************************
|   End of File
*******************************************************************************/