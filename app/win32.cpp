/*
|   Win32アプリケーションクラス  @Ayumi Yasui
|
*-----------------------------------------------------------------------------*/
#if _WIN32

//--- インクルード ------------------------------------------------------------
#include "win32.h"

/******************************************************************************
|   Win32アプリケーションクラス
*-----------------------------------------------------------------------------*/
// @brief  : コンストラクタ
// @param  : ウィンドウプロシージャ
//         : インスタンスハンドル
//         : アイコン
//         : カーソル
//         : メニュー
//         : ウィンドウクラス名
//         : 16 * 16 アイコン
//         : アプリケーション名
//         : クライアント領域の幅
//         : クライアント領域の高さ
//         : ウィンドウスタイル
//         : 拡張ウィンドウスタイル
//         : フルスクリーンにするか
//--------------------------------------------------------------------
Win32App::Win32App(
    WNDPROC _wndproc, HINSTANCE _instance,
    HICON  _icon,      HCURSOR _cursor,
    LPCSTR _menu,      LPCSTR _windowclass,
    HICON  _icon16x16, LPCSTR  _app_name,
    INT    _width,     INT     _height,
    DWORD  _wnd_style, DWORD   _wnd_style_ex, BOOL _is_full ) :
    CLASS_NAME(_windowclass),
    instance_handle(_instance)
{
    // メモリリークを検査しますよ
    _CrtDumpMemoryLeaks();
    _CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_CHECK_CRT_DF | _CRTDBG_LEAK_CHECK_DF );
    _CrtSetReportMode( _CRT_WARN, _CRTDBG_MODE_DEBUG );

    // ウィンドウクラス作成
    WNDCLASSEX wcex = {
        sizeof( WNDCLASSEX ),
        CS_CLASSDC,
        _wndproc,
        0,
        0,
        _instance,
        _icon,
        _cursor,
        ( HBRUSH )( COLOR_WINDOW + 1 ),
        _menu,
        _windowclass,
        _icon16x16
    };

    //ウィンドウクラスの登録
    const ATOM rc = RegisterClassEx( &wcex );
    if( rc == 0 ) delete this;

    // クライアント領域を計算
    RECT rect = { 0, 0, _width, _height };
    if( _is_full == FALSE )
    {
        AdjustWindowRectEx(
            &rect,                  // 計算済みの大きさ
            _wnd_style,             // ウィンドウスタイル
            (_menu) ? TRUE : FALSE, // メニューあり
            _wnd_style_ex           // 拡張ウィンドウスタイル
        );
        rect.right  = rect.right  - rect.left;
        rect.bottom = rect.bottom - rect.top;
        rect.left   = CW_USEDEFAULT;
        rect.top    = CW_USEDEFAULT;
    }
    window_handle = CreateWindowEx(
        _wnd_style_ex,  // 拡張ウィンドウスタイル
        CLASS_NAME,     // ウィンドウクラスの名前
        _app_name,      // ウィンドウのキャプション
        _wnd_style,     // ウィンドウのスタイル
        rect.left,      // ウィンドウの左上座標位置(X座標)
        rect.top,       // ウィンドウの左上座標位置(Y座標)
        rect.right,     // ウィンドウの幅
        rect.bottom,    // ウィンドウの高さ
        NULL,           // 親ウィンドウのハンドル
        NULL,           // 子ウィンドウのハンドル
        _instance,      // インスタンスハンドル
        NULL            // システムクラスの指定
    );
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
Win32App::~Win32App()
{
}

// @brief  : アプリケーションを終了させる
//--------------------------------------------------------------------
void Win32App::Uninit( void )
{
    DestroyWindow( window_handle );
}

// @brief  : ウィンドウ表示
//--------------------------------------------------------------------
void Win32App::EnableWindow( int _cmd_show )
{
    ShowWindow( window_handle, _cmd_show ); // ウィンドウの表示状態を設定
    UpdateWindow( window_handle );          // ウィンドウ1の状態を更新
}

// @brief  : メッセージ処理
// @return : 処理したのか
//--------------------------------------------------------------------
bool Win32App::UpdateMessage( void )
{
    // メッセージがあれば処理
    if( PeekMessage( &message, NULL, 0, 0, PM_REMOVE ) == 0 ) return false;
    TranslateMessage( &message ); // メッセージの翻訳
    DispatchMessage( &message );  // メッセージの送出
    return true;
}

// @brief  : ウィンドウの終了
// @return : 終了結果
//--------------------------------------------------------------------
int Win32App::Destroy( void )
{
    //ウィンドウクラスの登録解除
    UnregisterClass( CLASS_NAME, instance_handle );

    //戻り値
    return( (int)message.wParam );
}

// @brief : インスタンスハンドルの取得
//--------------------------------------------------------------------
HINSTANCE Win32App::GetHInstance( void ) const
{
    return instance_handle;
}

// @brief : ウィンドウハンドルの取得
//--------------------------------------------------------------------
HWND Win32App::GetHWnd( void ) const
{
    return window_handle;
}

// @brief : アプリケーションの終了メッセージ
//--------------------------------------------------------------------
bool Win32App::IsNotEnd( void ) const
{
    return message.message != WM_QUIT;
}

// @brief  : エラー表示
//--------------------------------------------------------------------
void Win32App::Error( HRESULT hr ) const
{
#ifdef _DEBUG
    LPVOID lpMsgBuf;
    FormatMessage(
        FORMAT_MESSAGE_ALLOCATE_BUFFER |    // 文字列をバッファとして
        FORMAT_MESSAGE_FROM_SYSTEM ,        // システムメッセージテーブルから 
        NULL,                               // メッセージ定義
        hr,                                // メッセージ識別子
        MAKELANGID( LANG_NEUTRAL, SUBLANG_DEFAULT ), // 既定の言語
        ( LPTSTR )&lpMsgBuf,                // 文字列の格納場所
        0,                                  // 最少文字列数
        NULL                                // 複数の値からなる配列
        );

    // 文字列表示
    OutputDebugString( ( LPCSTR )lpMsgBuf );

    // メッセージボックスを出す
    MessageBox( window_handle, ( LPCSTR )lpMsgBuf, "Error", MB_OK || MB_ICONERROR || MB_SYSTEMMODAL );

    // バッファ解放
    LocalFree( lpMsgBuf );
#endif
}

// デフォルトウィンドウプロシージャ
//--------------------------------------------------------------------
LRESULT CALLBACK WndProc( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam )
{
    switch( uMsg )
    {
        // ウィンドウ破棄時
    case WM_DESTROY:
        PostQuitMessage( 0 );
        break;

    // キーが押されたとき
    case WM_KEYDOWN:
        switch( wParam )
        {
            // ESC
            case VK_ESCAPE :
                DestroyWindow( hWnd );
                break;
        }
        break;

    default:
        break;
    }// switch( nMsg )

    return( DefWindowProc( hWnd, uMsg, wParam, lParam ) );
}


#endif  // _WIN32
/******************************************************************************
|   End of File
*******************************************************************************/