/*
# app.h

- アプリケーションクラス

@author : Ayumi Yasui
*-----------------------------------------------------------------------------*/
#pragma once
#ifndef __APP_H__
#define __APP_H__

//--- インクルード ------------------------------------------------------------
#include "../base/tempSingleton.h"
#include "../base/tempGetter.h"
#include "../base/object.h"
#include "../renderer/renderer.h"
#include "../audio/iAudio.h"

#if _WIN32
#include "win32.h"
#endif

#ifdef    __APPLE__

#ifdef __OBJC__
#import <Cocoa/Cocoa.h>
// クラス宣言
//------------------------------------------------------------------------------
@interface AppDelegate : NSObject <NSApplicationDelegate>
{
    bool      m_IsTerminate;
}
@property NSWindow *Window;
@property bool      IsTerminate;

@end
#else
class AppDelegate;
#endif
#endif // __APPLE__


class App : public TempSingleton<App>
{
public:
    // @brief  : 動かす
    //--------------------------------------------------------------------
#if _WIN32
    int Run( IRenderer *, IAudio *,int nCmdShow );
#endif
#ifdef __APPLE__
    int Run(IRenderer *_renderer, IAudio *_audio);
#endif

    // プロパティ
    //--------------------------------------------------------------------
    IRenderer * Renderer( void ) const{ return m_Renderer; };
    IAudio *    Audio( void ) const{ return m_Audio; };
#if _WIN32
    TempGetter<Win32App>    Win32;
#endif
#ifdef    __APPLE__
    AppDelegate *Delegate(void) const{ return m_AppDelegate; };
#endif // __APPLE__

private:
    // フレンドクラス
    //--------------------------------------------------------------------
    friend TempSingleton<App>;

    // メンバ変数
    //--------------------------------------------------------------------
    IRenderer        *m_Renderer;
    material::IAudio *m_Audio;
#if _WIN32
    Win32App  m_Win32;
    DWORD     m_nExecLastTime;    // 最終実行時間
    DWORD     m_nFPSCheakTime;    // 最終FPSチェック時間
    UINT      m_nCntFrame;        // チェックからのフレーム数
    UINT      m_nFPSSetting;      // 1秒何フレーム更新か
    UINT      m_nFPS;             // チェック時の平均FPS
#endif
#ifdef    __APPLE__
    AppDelegate *m_AppDelegate;
#endif // __APPLE__

    // @brief  : コンストラクタ
    //--------------------------------------------------------------------
    App();

    // @brief  : デストラクタ
    //--------------------------------------------------------------------
    ~App();

#ifdef    __APPLE__
    // @brief  : 現在時刻の取得
    //--------------------------------------------------------------------
    long GetNowTime(void) const;
    
#endif  //__APPLE__
};


#endif  // __APP_H__
/******************************************************************************
|   End of File
*******************************************************************************/