/*
 # app.mm
 
 - アプリケーションクラス
 
 @author : Ayumi Yasui
 *-----------------------------------------------------------------------------*/

//--- インクルード ------------------------------------------------------------
#include "app.h"

#import <Cocoa/Cocoa.h>
#include <sys/time.h>
#include "OSXMacro.h"
#include "input/inputMac.h"

// @brief  : コンストラクタ
//--------------------------------------------------------------------
App::App()
{
    // AppDelegateの作成
    m_AppDelegate = [[AppDelegate alloc] init];
    [[NSApplication sharedApplication] setDelegate:m_AppDelegate];

    // ウィンドウの作成
    NSUInteger windowStyle = (NSTitledWindowMask|NSTitledWindowMask|NSClosableWindowMask|NSMiniaturizableWindowMask|NSResizableWindowMask);
    
    NSRect windowRect = NSMakeRect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    NSWindow *window = [[NSWindow alloc]
                        initWithContentRect:windowRect
                        styleMask:windowStyle
                        backing:NSBackingStoreNonretained
                        defer:NO];
    
    // ウィンドウタイトルの入力
    NSString *title = @CONFIG_APPNAME;
    window.title = title;
    
    // Delegeteにウィンドウを追加
    m_AppDelegate.Window = window;

    [window makeKeyAndOrderFront:nil];
    [window makeMainWindow];                // メインウィンドウにする

    // メニューの追加
    NSMenu *mainMenu = [NSMenu alloc];
    //[mainMenu initWithTitle:@"Minimum"];
    
    NSMenuItem *fileMenu;
    fileMenu=[[NSMenuItem alloc] initWithTitle:@"File" action:NULL keyEquivalent:[NSString string]];
    [mainMenu addItem:fileMenu];
    
    NSMenu *fileSubMenu;
    fileSubMenu=[[NSMenu alloc] initWithTitle:@"File"];
    [fileMenu setSubmenu:fileSubMenu];
    
    NSMenuItem *fileMenu_Quit;
    fileMenu_Quit=[[NSMenuItem alloc] initWithTitle:@"Quit"  action:@selector(terminate:) keyEquivalent:@"q"];
    [fileMenu_Quit setTarget:NSApp];
    [fileSubMenu addItem:fileMenu_Quit];
    
    [NSApp setMainMenu:mainMenu];
    
    //NSWindowController *windowController = [[NSWindowController alloc] initWithWindow:window];
    //[windowController showWindow:nil];
    //[NSApp activateIgnoringOtherApps:YES];
}

// @brief  : デストラクタ
//--------------------------------------------------------------------
App::~App()
{
}

// @brief  : 動かす
//--------------------------------------------------------------------
int App::Run(IRenderer *_renderer, IAudio *_audio)
{
    m_Renderer = _renderer;
    m_Audio    = _audio;
    
    // Input
    InputMac::Instance();
    
    //  立ち上げ終了
    [NSApp finishLaunching];
    [m_AppDelegate.Window orderFrontRegardless];
    float frame = 1.0f/60.0f*1000.0f;
    
    // 現在時刻の取得
    long lastTime = GetNowTime();
    while (!m_AppDelegate.IsTerminate)
    {
        [m_AppDelegate.Window update];
        [[NSApp menu] update];
        //[m_AppDelegate.Window displayIfNeeded];
        
        // FPS調整
        // 現在時刻の取得
        long curTime = GetNowTime();
        
        // 余った時間をSleepに使う
        if (curTime - lastTime > frame)
        {
            lastTime = curTime;

            // 更新
            InputMac::Instance().Update();
            TempNodeSingleton<Object>::Instance().UpdateAll();
            TempSingleton<Node>::Instance().Destroy();
            
            // 描画
            Renderer()->Draw();
        }
    }
    
    TempSingleton<Node>::Instance().DeleteChild();
    return 0;
}

// @brief  : 現在時刻の取得
//--------------------------------------------------------------------
long App::GetNowTime(void) const
{
    long lLastTime = 0;
    struct timeval stCurrentTime;
    
    gettimeofday(&stCurrentTime,NULL);
    lLastTime = stCurrentTime.tv_sec*1000+stCurrentTime.tv_usec*0.001; //millseconds
    return lLastTime;
}



/*
|   @brief  : AppDelegate
|
*------------------------------------------------------------------------------*/
@implementation AppDelegate

// @brief  :アプリケーション立ち上げ終了時
//--------------------------------------------------------------------
- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    m_IsTerminate = false;
}

// @brief  :アプリケーションが終了する時
//--------------------------------------------------------------------
- (void)applicationWillTerminate:(NSNotification *)aNotification
{
    m_IsTerminate = true;
    TempNodeSingleton<Object>::Instance().DeleteChild();
}

// @brief  : ウィンドウが閉じられるとき
//--------------------------------------------------------------------
-(void)windowWillClose:(NSNotification *)aNotification
{
    [NSApp terminate:self]; // アプリケーションの終了
}

// @brief  : プロパティの実装
//--------------------------------------------------------------------
@synthesize IsTerminate = m_IsTerminate;


@end


/******************************************************************************
 |   End of File
 *******************************************************************************/